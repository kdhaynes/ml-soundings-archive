"""

ML Soundings

Hyperopt hyperparameter search functions.

Modified 2023/09
"""

from copy import deepcopy
from functions_metrics import mae, rmse
from functions_misc import pickle_dump
from functions_nn import setup_clear, setup_nn
from functools import partial
from hyperopt import fmin, tpe
from hyperopt import space_eval, STATUS_OK
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from time import time


DEFAULT_NN_MODEL_OPTS = {
    'network': 'NeuralNetwork',
    'exRef': 'HP',
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'seed': None,
    'standardizeX': True,
    'standardizeY': True}
DEFAULT_NN_RUN_OPTS = {
    'epochs': 600,
    'patience': 15}


def hp_trainNN(
        params, X, Y,
        Xval, Yval,
        returnModel=False,
        scoreMethod='r2_score'):

    paramsH = deepcopy(DEFAULT_NN_MODEL_OPTS)
    paramsH['activation_dense'] = params['activation_dense']
    paramsH['batchnorm_flag'] = params['batchnorm_flag']
    paramsH['dropout_dense_last'] = params['dropout_dense_last']

    ndense = params['n_dense']
    dropoutList = []
    for i in range(ndense):
        dropoutList.append(params['dropout_dense_list'])
    paramsH['dropout_dense_list'] = dropoutList

    ndenseList = [params['dense1']]
    if ndense > 1:
        ndenseList.append(params['dense2'])
    if ndense == 3:
        ndenseList.append(params['dense3'])
    paramsH['n_dense_list'] = ndenseList

    paramsH['loss_type'] = params['loss_type']
    paramsH['uq_n_members'] = params['uq_n_members']

    paramsR = deepcopy(DEFAULT_NN_RUN_OPTS)
    paramsR['batch_size'] = params['batch_size']
    paramsR['learning_rate'] = params['learning_rate']
    paramsR['optimizer'] = params['optimizer']

    expDict = {
        'modelOpts': paramsH,
        'runOpts': paramsR}

    setup_clear()
    n_inputs = X.shape[-1]
    if len(Y.shape) < 2:
        n_outputs = 1
    else:
        n_outputs = Y.shape[1]
    # print(f"INPUT: {n_inputs}")
    # print(f"OUTPUT: {n_outputs}")

    nnet = setup_nn(expDict,
                    n_inputs=n_inputs,
                    n_outputs=n_outputs)
    nnet.train(X, Y,
               validation=(Xval, Yval),
               verbose=False)

    Pval = nnet.use(Xval, returnType='')
    if scoreMethod == 'r2_score':
        scoreHere = r2_score(Yval, Pval)
    elif scoreMethod == 'mae':
        scoreHere = mae(Pval, Yval)
    elif scoreMethod == 'rmse':
        scoreHere = rmse(Pval, Yval)
    else:
        print(f"Unknown Score Method: {scoreMethod}")
        return

    if returnModel:
        return -scoreHere, nnet
    else:
        return -scoreHere


def hp_trainRF(
        params, X, Y,
        Xval, Yval,
        returnModel=False,
        scoreMethod='r2_score'):

    model = RandomForestRegressor(**params)
    model.fit(X, Y)

    Pval = model.predict(Xval)
    if scoreMethod == 'r2_score':
        scoreHere = r2_score(Yval, Pval)
    elif scoreMethod == 'mae':
        scoreHere = mae(Pval, Yval)
    elif scoreMethod == 'rmse':
        scoreHere = rmse(Pval, Yval)
    else:
        print(f"Unknown Score Method: {scoreMethod}")
        return

    if returnModel:
        return -scoreHere, model
    else:
        return -scoreHere


def hp_rff(params,
           X, Y,
           Xval, Yval,
           summary=True):
    """ Optimize function for the TPE search. """

    if summary:
        print("Params: {}".format(params))

    lossHere = hp_trainRF(
        params, X, Y, Xval, Yval)

    return {"loss": lossHere,
            "params": params,
            "status": STATUS_OK}


def hp_nnf(params,
           X, Y,
           Xval, Yval,
           summary=True):
    """ Optimize function for the TPE search. """

    if summary:
        print("Params: {}".format(params))

    lossHere = hp_trainNN(
        params, X, Y, Xval, Yval)

    return {"loss": lossHere,
            "params": params,
            "status": STATUS_OK}


def hp_run_trials(
        hyperopt_f,
        X, Y,
        Xval, Yval,
        countHP,
        spaceHP,
        trialsHP,
        bestLoss,
        bestParams,
        nEvals=10,
        nPrintInfo=1,
        saveFreq=10,
        saveFDir="./results_",
        summary=True,
        runVerbose=1):
    """ Run the Hyperopt trials. """

    # Setup Hyperopt
    timeStart = time()
    fpartial = partial(
        hyperopt_f,
        X=X,
        Y=Y,
        Xval=Xval,
        Yval=Yval,
        summary=summary)

    # Run trials
    for i in range(nEvals):
        countHP += 1
        print("Count: {}".format(countHP))

        # run Hyperopt forward one test
        best = fmin(
            fpartial, spaceHP, algo=tpe.suggest,
            max_evals=countHP, trials=trialsHP)

        # get best loss
        bestLossRun = trialsHP.best_trial["result"]["loss"]

        # get best params
        bestParamsRun = space_eval(spaceHP, best)

        # set current best loss and parameters
        if bestLossRun < bestLoss:
            bestLoss = bestLossRun
            bestParams = bestParamsRun

        if countHP % nPrintInfo == 0:
            resultsHere = trialsHP.results[-1]
            lossHere = resultsHere["loss"]
            print(
                "{}{:.3f}{}{:.3f}\n\n".format(
                    '  Best Loss: ', bestLoss,
                    ', Here: ', lossHere))

        # save if requested
        if saveFreq > 0 and countHP % saveFreq == 0:
            fileNow = saveFDir + str(countHP) + ".pkl"
            trialDumpDict = {
                'spaceHP': spaceHP,
                'trialsHP': trialsHP,
                'bestLoss': bestLoss,
                'bestParams': bestParams}
            pickle_dump(fileNow, trialDumpDict)

    # Print info
    if runVerbose:
        timeT = time() - timeStart
        print("")
        print("--------------------------------------")
        print("Finished Search in {:.0f}s".format(timeT))
        print("Best Parameters: {}".format(bestParams))
        print("Best CV Train Loss: {:.2f}".format(bestLoss))
        print("")

    return bestLoss, bestParams
