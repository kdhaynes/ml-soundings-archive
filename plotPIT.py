"""
ML Soundings Project

Program to plot the PIT diagram.

For temperature use:
[vo.exp015, vo.exp137, vo.exp515, vo.exp527]

For dewpoint use:
[vo.exp015, vo.exp137, vo.exp515, vo.exp527]
Modified: 2022/06
"""

# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp

from importlib import reload

import var_opts as vo


# %%
# User Options
expList = [vo.exp015, vo.exp137,
           vo.exp515, vo.exp527]
labelList = ['LIN CRPS', 'NN SHASH',
             'UNet CRPS', 'UNet SHASH']
colorList = ['mediumseagreen', 'darkorange',
             'darkslateblue', 'mediumslateblue']

plotPIT = True
savePIT = True

refLow = 0
refHigh = 246

targetNames = ['Temperature', 'Dewpoint']

restrictData = [4, 8]
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Read Data
reload(fm)
(RAPtr, RAPv, RAPte,
 RTMAtr, RTMAv, RTMAte,
 GOEStr, GOESv, GOESte,
 RAOBtr, RAOBv, RAOBte,
 FILEtr, FILEv, FILEte) = fm.load_data(
    useShannon,
    restrictData=restrictData,
    returnFileInfo=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
YTest = fm.organize_data_y(RAOBte)


# %%
# Read Model Output
dDict = {}
nTargets = len(targetNames)

for expDict in expList:
    expRef = fm.get_exp_refname(expDict)
    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    pDict = fm.pickle_read(filePreds)
    PTest_mean = pDict['PREDtest']
    PTest_std = pDict['PREDstd']
    try:
        PTest_mc = pDict['PREDmc']
    except KeyError:
        PTest_mc = None

    eDict = {}
    if PTest_mc is not None:
        print(f" {expRef} using ensemble PIT.")
        eDict = fm.get_pit_points_ens(
            YTest[:, refLow:refHigh, :],
            PTest_mc[:, refLow:refHigh, :, :])
    else:
        eDict = fm.get_pit_points(
            YTest[:, refLow:refHigh, :],
            PTest_mean[:, refLow:refHigh, :],
            PTest_std[:, refLow:refHigh, :],
            pDict=eDict)
    eDict['YTest'] = YTest
    eDict['PREDtest'] = PTest_mean
    eDict['PREDstd'] = PTest_std

    dDict[expRef] = eDict


# %%
# Plot PIT
if plotPIT:
    reload(fm)
    reload(fp)

    saveFileList = []
    if savePIT:
        save_file = 'PIT_'
        for exp in expList:
            expRef = fm.get_exp_refname(exp)
            save_file += expRef + '_'

        for tg in range(nTargets):
            saveFileList.append(save_file + targetNames[tg] + '.png')
    else:
        for tg in range(nTargets):
            saveFileList.append('')

    for tg in range(nTargets):
        fp.plot_pit_dict(
            dDict,
            bar_color=colorList,
            bar_label=labelList,
            font_size=18,
            legend_loc='lower center',
            save_file=saveFileList[tg],
            targetNum=tg,
            title=targetNames[tg])
