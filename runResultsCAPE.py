"""
ML Soundings Project

Program to plot results.

NOTE:
This program uses the mean results saved during the simulation,
it does not recalculate any values nor include any individual cases.

Modified: 2023/09
"""


# %%
# Import libraries
import functions_cape as fc

from functions_misc import combineDicts
from functions_misc import get_exp_filenamePreds
from functions_misc import get_exp_filenameResults
from functions_misc import pickle_read
from importlib import reload


# %%
# Set user input options
rListLIN = ['LIN701', 'LIN702',
            'LIN705', 'LIN706', 'LIN707']
rListLIN_BEST = ['LIN005', 'LIN001']

rListRF = ['RF801', 'RF802', 'RF803', 'RF804',
           'RF805', 'RF850', 'RF860']

rListNN = ['NN901', 'NN902', 'NN905', 'NN906', 'NN907',
           'NN915']
rListNN_ABL = ['NN980', 'NN981', 'NN982',
               'NN983', 'NN984']
rListNN_BEST = ['NN150', 'NN189']

rListUNet_BEST = ['UNet585', 'UNet587']

# -------------------------
rList = rListLIN + rListRF + rListNN

printResults = True
sortResults = True
sortName = 'ML CAPE RMSE'

showImprove = True
sortImprove = True
sortNameImprove = '% CAPE RMSE'

printImprove = True

useShannon = True
missing = None


# %%
# Read results files
rDict = {}
for expName in rList:
    resultFile = get_exp_filenameResults(
        useShannon,
        expRef=expName)
    rDictT = pickle_read(resultFile)
    try:
        rDictML = rDictT['resultsML']
        rDictRAP = rDictT['resultsRAP']
        rDict[expName] = combineDicts(
            rDictRAP, rDictML)
    except KeyError:

        predFile = get_exp_filenamePreds(
            useShannon, None,
            expRef=expName)
        pDict = pickle_read(predFile)
        rDict = rDictT


# %%
# Display Results
if printResults:
    reload(fc)
    dfShow = fc.df_results_show_cape(
        rDict,
        num_to_show=30,
        sort=sortResults, sortName=sortName)


# %%
# Display Improvements
if showImprove:
    reload(fc)
    dfShow = fc.df_results_show_improve_cape(
        rDict, num_to_show=30,
        sort=sortResults,
        sortName=sortNameImprove)
