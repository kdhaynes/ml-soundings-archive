"""
ML Soundings Project

Program to try predicting CAPE and CIN
directly using a Random Forest or NN.

Modified: 2023/09
"""


# %%
# Import Libraries
import functions_cape as fc
import functions_hp as fhp
import functions_misc as fm

from hyperopt import hp
from hyperopt import Trials


from sklearn.metrics import r2_score

from var_opts import exp901 as expDict


# %%
# Set User Options

shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Hyperopt Options
doSearch = True
modelSummary = True
nEvals = 200
saveFreq = 10
saveFDir = '/mnt/data1/kdhaynes/mlsoundings/hp/nn_'

spaceHP = {
    'activation_dense': hp.choice(
        'activation_dense', ['relu', 'tanh']),
    'batchnorm_flag': hp.choice(
        'batchnorm_flag', [True, False]),
    'batch_size': hp.choice(
        'batch_size', [128, 256]),
    'dense1': hp.choice(
        'dense1', range(10, 1000, 10)),
    'dense2': hp.choice(
        'dense2', range(10, 1000, 10)),
    'dense3': hp.choice(
        'dense3', range(10, 1000, 10)),
    'dropout_dense_last': hp.choice(
        'dropout_dense_last', [0.0, 0.05, 0.1, 0.15]),
    'dropout_dense_list': hp.choice(
        'dropout_dense_list', [0.0, 0.05]),
    'learning_rate': hp.choice(
        'learning_rate', [0.0001, 0.0001, 0.005, 0.001]),
    'loss_type': hp.choice(
        'loss_type',
        ['mse', 'mae', 'crps', 'uq_normal']),
    'n_dense': hp.choice(
        'n_dense', [1, 2, 3]),
    'optimizer': hp.choice(
        'optimizer', ['adam', 'nadam']),
    'uq_n_members': hp.choice(
        'uq_n_members', [40, 50, 60])}


# %%
# Load Data
expName = fm.get_exp_refname(expDict)
print('\nINFO: Running experiment {}'.format(expName))

mOpts = expDict['modelOpts']
restrictData = fm.get_exp_restrict_data(expDict)
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrictData,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
(RAPtrain, RAPval, RAPtest,
 RTMAtrain, RTMAval, RTMAtest,
 GOEStrain, GOESval, GOEStest,
 RAOBtrain, RAOBval, RAOBtest,
 RAOBtrain_cape_cin, RAOBval_cape_cin,
 RAOBtest_cape_cin,
 RAPtrain_cape_cin, RAPval_cape_cin,
 RAPtest_cape_cin) = data


# %%
# Organize data
Xtr, Ytr, Xval, Yval, _, _ = fc.organize_data_cape(
    expDict, data)
nTrainAll = Xtr.shape[0]
nFeaturesAll = Xtr.shape[1]
nTargetsAll = Ytr.shape[1]
rapR2 = r2_score(Yval, RAPval_cape_cin)


# %%
# Search Using HyperOpt TPE
if doSearch:
    cList = []
    accList = []
    paramsList = []

    trialsHP = Trials()
    bestLoss = float("inf")
    bestParams = {}

    # set count
    countHP = len(trialsHP)

    # set save directory
    bestLoss, bestParams = fhp.hp_run_trials(
        fhp.hp_nnf,
        Xtr, Ytr, Xval, Yval,
        countHP,
        spaceHP,
        trialsHP,
        bestLoss,
        bestParams,
        nEvals=nEvals,
        saveFreq=saveFreq,
        saveFDir=saveFDir,
        summary=modelSummary)

    print("Finished Running HyperOpt Search!\n")


# %%
# Print results

print("\nBest Results: ")
print(f"Best Loss: {bestLoss:.3f}")
print(f"Best Params: {bestParams}")
