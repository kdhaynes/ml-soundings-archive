"""
ML Soundings Project

Program to plot the spread-skill diagram.

For dewpoint:
spread_bins = [0.0, 1.5, 3.0, 4.5,
               6.0, 7.5, 9., 20]
spread_last = 10
spread_ticks = [0, 2, 4, 6, 8, 10]
spread_width = 0.8

For temperature:
spread_bins = [0.4, 0.6, 0.8, 1.0,
               1.4, 1.8, 10]
spread_last = 2
spread_ticks = [0.5, 1.0, 1.5, 2.0]
spread_width = 0.2

Modified: 2023/09
"""

# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import var_opts as vo

from importlib import reload


# %%
# User Options
expList = [vo.exp015, vo.exp137,
           vo.exp515, vo.exp527]
labelList = ['LIN CRPS', 'NN SHASH',
             'UNet CRPS', 'UNet SHASH']
colorList = ['mediumseagreen', 'darkorange',
             'darkslateblue', 'mediumslateblue']

plotSpreadSkill = True
saveSpreadSkill = True
saveSSName = 'mls_ss_t.png'

refLow = 0
refHigh = 220

spread_bins = [0.4, 0.6, 0.8, 1.0,
               1.4, 1.8, 10]
spread_last = 2
spread_ticks = [0.5, 1.0, 1.5, 2.0]
spread_width = 0.2

legend_loc = 'lower right'
plot_hist = 'inset'
shiftx = 0.04
shifty = -0.02

targetName = 'Temperature'
targetRef = 0
xLabel = 'Spread (Std Dev)'


useShannon = True
shuffleAll = True
shuffleSites = False


# %%
# Read Data and Predictions
reload(fm)
dDict = {}
nTargets = 1

for expDict in expList:
    exRef = fm.get_exp_refname(expDict)
    restrictData = fm.get_exp_restrict_data(expDict)
    data = fm.load_data(
        useShannon,
        restrictData=restrictData,
        returnFileInfo=True,
        shuffleAll=shuffleAll,
        shuffleSites=shuffleSites)
    RAOBte = data[11]
    YTest = fm.organize_data_y(RAOBte)

    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    pDict = fm.pickle_read(filePreds)
    PTest_mean = pDict['PREDtest']
    PTest_std = pDict['PREDstd']

    eDict = fm.get_spread_skill_points_uq(
        YTest[:, refLow:refHigh, targetRef],
        PTest_mean[:, refLow:refHigh, targetRef],
        PTest_std[:, refLow:refHigh, targetRef],
        bins=spread_bins,
        spread_last=spread_last)
    dDict[exRef] = eDict
print("Read True Data and Predictions.")


# %%
# Plot Spread-Skill
if plotSpreadSkill:
    reload(fp)

    if saveSpreadSkill:
        save_file = saveSSName
    else:
        save_file = ''

    fp.plot_spread_skill_dict_uq(
        dDict,
        labelx=xLabel,
        legend_show=True,
        legend_loc=legend_loc,
        line_color=colorList,
        line_label=labelList,
        plot_hist=plot_hist,
        save_file=save_file,
        spread_ticks=spread_ticks,
        title=targetName,
        units='$\degree$C')
