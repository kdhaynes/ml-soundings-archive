"""
ML Soundings Research

Modified: 2023/07
"""


import functions_metrics as fm
import metpy.calc as mcalc
import metpy.units as munits
import numpy as np
import os
import pandas as pd
import pickle

from datetime import datetime
from IPython.display import display
from netCDF4 import Dataset
from scipy import interpolate
from scipy.fft import fft, fftfreq
from scipy.stats import norm


#############
# DEFAULTS

ATTRIBUTES_NBINS = 40

CAPE_INDX = 0
CIN_INDX = 1

DEWPOINT_COLUMN_KEY = 'dewpoints_deg_c'
PRESSURE_COLUMN_KEY = 'pressures_mb'
TEMPERATURE_COLUMN_KEY = 'temperatures_deg_c'

GOES_CHANNELS = [8, 9, 10, 11, 13, 14, 15, 16]

ML_INDX_DEWPOINT = 1
ML_INDX_TEMPERATURE = 0

PIT_BIN_EDGES = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5,
                 0.6, 0.7, 0.8, 0.9, 1.0]

RAOB_INDX_DEWPOINT = 2
RAOB_INDX_HEIGHT = 3
RAOB_INDX_PRESSURE = 0
RAOB_INDX_TEMPERATURE = 1

RAP_INDX_DEWPOINT = 2
RAP_INDX_HEIGHT = 3
RAP_INDX_PRESSURE = 0
RAP_INDX_TEMPERATURE = 1

RTMA_INDX_DEWPOINT = 1
RTMA_INDX_PRESSURE = 0
RTMA_INDX_TEMPERATURE = 2

DEFAULT_NLEVELS = 256
SFC_ERROR_LEVEL = 25
REF_ERROR_LEVEL = 238

ALT_DATA_DIR = \
    '/Users/kdhaynes/Data/cira/mlsoundings/nc/'
ALT_RESULT_DIR = \
    '/Users/kdhaynes/Data/cira/mlsoundings/results/'
MAIN_DATA_DIR = \
    '/mnt/data1/kdhaynes/mlsoundings/npy/'
MAIN_RESULT_DIR = \
    '/mnt/data1/kdhaynes/mlsoundings/results/'
MAIN_DATA_FILE = 'mlsoundings_dataset.nc'

EVAL_KEY_LIST = [
    'mlT_test_rmse_sfc', 'mlTD_test_rmse_sfc',
    'mlT_test_rmse', 'mlTD_test_rmse',
    'mlCAPE_test_rmse', 'mlCIN_test_rmse']
NAMES_TO_SHOW_ORIGCC = [
    'rapCAPE_test_rmse',
    'mlCAPE_test_rmse',
    'rapT_test_rmse_sfc',
    'mlT_test_rmse_sfc',
    'rapTD_test_rmse_sfc',
    'mlTD_test_rmse_sfc',
    'rapCIN_test_rmse',
    'mlCIN_test_rmse',
    'rapT_test_rmse',
    'mlT_test_rmse',
    'rapTD_test_rmse',
    'mlTD_test_rmse']
NAMES_TO_SHOWCC = [
    'rap_CAPE', 'ml_CAPE',
    'rapT_sfc', 'mlT_sfc',
    'rapTD_sfc', 'mlTD_sfc',
    'rap_CIN', 'ml_CIN',
    'rapT_rmse', 'mlT_rmse',
    'rapTD_rmse', 'mlTD_rmse']
NAMES_TO_SHOWCC_IMPROVE = [
    '%_CAPE', '%_TSfc', '%_TDSfc',
    '%_CIN', '%_T', '%_TD']


##############################
# FUNCTIONS
def calculate_autocorrelation(x, length=100):
    "Auto-Correlation Function for lags up to length."""
    length = np.min([length, x.shape[0]])
    return np.array(
        [1] + [np.corrcoef(x[:-i], x[i:])[0, 1]
               for i in range(1, length)])


def calculate_cape_cin_arrays(
        pressureA, temperatureA, dewpointA, type='ml'):
    """
    INPUT: (nSamples, nLevels)
    OUTPUT: (nSamples, 2)
    """
    psHere = pressureA.copy() * munits.units.hPa
    tHere = temperatureA.copy() * munits.units.degC
    tdHere = dewpointA.copy() * munits.units.degC

    if type == 'ml':
        capeF = mcalc.mixed_layer_cape_cin
    elif type == 'sfc':
        capeF = mcalc.surface_based_cape_cin
    elif type == 'unstable':
        capeF = mcalc.most_unstable_cape_cin
    else:
        print("Unknown CAPE calculation type: {}".format(type))

    nSamples = pressureA.shape[0]
    cape = np.zeros((nSamples))
    cin = np.zeros((nSamples))
    for i in range(nSamples):
        try:
            capeU, cinU = capeF(
                psHere[i, :], tHere[i, :], tdHere[i, :])
            cape[i] = capeU.magnitude
            cin[i] = cinU.magnitude
        except mcalc.InvalidSoundingError:
            cape[i] = 0.
            cin[i] = 0.
        except ValueError:
            cape[i] = 0.
            cin[i] = 0.

    return cape, cin


def calculate_discard(ytrue, ymean, ystd,
                      bins=None, returnBins=False):
    if bins is None:
        nbins = 10
        bins = np.linspace(0., 0.9, nbins)
    else:
        nbins = len(bins)

    ytrue1d = ytrue.reshape(-1)
    ymean1d = ymean.reshape(-1)
    ystd1d = ystd.reshape(-1)
    nsamples = ystd1d.shape[0]

    yrefs = np.argsort(ystd1d)
    ytrueSorted = ytrue1d[yrefs]
    ymeanSorted = ymean1d[yrefs]

    rmseOut = np.empty((nbins))
    for i in range(nbins):
        iCutoff = nsamples - int(nsamples * bins[i])
        ytrueH = ytrueSorted[:iCutoff]
        ymeanH = ymeanSorted[:iCutoff]
        rmseOut[i] = fm.rmse(ytrueH, ymeanH)

    if returnBins:
        return rmseOut, bins
    else:
        return rmseOut


def calculate_FFT(arrayIn, returnX=False):
    nLevs = arrayIn.shape[0]
    T = 1.0 / nLevs
    xFFT = fftfreq(nLevs, T)[:nLevs // 2]
    arrayFFT = fft(arrayIn)
    arrayFFT = 2.0 / nLevs * np.abs(arrayFFT[0:nLevs // 2])
    if returnX:
        return arrayFFT, xFFT
    else:
        return arrayFFT


def calculate_ignorance(
    ytrue, ymean, ystd,
        nBins=10, probMin=0.0001):

    nPts = ytrue.shape[0]
    if len(ytrue.shape) > 1:
        ytrue = ytrue.reshape(-1)
    if len(ymean.shape) > 1:
        ymean = ymean.reshape(-1)
    if len(ystd.shape) > 1:
        ystd = ystd.reshape(-1)
    ymin = ymean - 3. * ystd
    ymax = ymean + 3. * ystd

    ign_score = 0.
    for i in range(nPts):
        minH = ymin[i]
        maxH = ymax[i]
        trueH = ytrue[i]
        if trueH >= minH and trueH <= maxH:
            bin_edges = np.array(
                [value for value in np.linspace(
                    minH, maxH, num=nBins + 1)])
            bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
            indx = find_nearest(bin_centers, trueH)

            dist = norm(ymean[i], ystd[i])
            probability = dist.pdf(bin_centers[indx])
            ign_score += np.log2(probability)

        else:
            ign_score += np.log2(probMin)

    ign_score = -1. * ign_score / nPts
    return ign_score


def calculate_ignorance_ens(
    ytrue, ypred,
        nBins=10, probMin=0.0001):

    nEns = ypred.shape[-1]
    nPts = ytrue.size

    ypred = ypred.reshape((nPts, nEns))
    ytrue = ytrue.reshape(-1)
    ymean = np.mean(ypred, axis=-1).reshape(-1)
    ystd = np.std(ypred, axis=-1).reshape(-1)
    ymin = ymean - 3. * ystd
    ymax = ymean + 3. * ystd

    ign_score = 0.
    for i in range(nPts):
        minH = ymin[i]
        maxH = ymax[i]
        trueH = ytrue[i]

        if trueH >= minH and trueH <= maxH:
            bin_edges = [
                value for value in np.linspace(
                    minH, maxH, num=nBins + 1)]
            density, bin_centers = get_histogram(
                ypred[i, :], bins=bin_edges, density=True)
            indx = find_nearest(bin_centers, trueH)
            probability = np.max([density[indx], probMin])
            ign_score += np.log2(probability)

        else:
            ign_score += np.log2(probMin)

    ign_score = -ign_score / nPts
    return ign_score


def calculate_profile_rmses(
        Y, T,
        refLevel=REF_ERROR_LEVEL,
        sfcLevel=SFC_ERROR_LEVEL):
    """
    note: mean_rmse is the same as tot_rmse
    ---
    params:
        Y: np.array(N, 256)
        T: np.array(N, 256)
    """

    tot_rmse = np.sqrt((np.mean((Y - T)**2, axis=0)))
    mean_rmse = fm.rmse(Y[:, :refLevel],
                        T[:, :refLevel])

    rmse_sfc = np.sqrt(
        (np.mean((Y[:, :sfcLevel] - T[:, :sfcLevel])**2, axis=0)))
    mean_rmse_sfc = fm.rmse(Y[:, :sfcLevel], T[:, :sfcLevel])

    return tot_rmse, mean_rmse, rmse_sfc, mean_rmse_sfc


def calculate_profile_rmses_samples(
    Y, T,
    refLevel=REF_ERROR_LEVEL,
        sfcLevel=SFC_ERROR_LEVEL):
    """
    Expects:
        Y: np.array(N, 256)
        T: np.array(N, 256)
    """

    nSamples = Y.shape[0]
    nLevels = Y.shape[1]
    tot_rmse = np.empty((nSamples, nLevels))
    mean_rmse = np.empty((nSamples))

    sfc_rmse = np.empty((nSamples, sfcLevel))
    sfc_mean_rmse = np.empty((nSamples))

    for i in range(nSamples):
        yHere = Y[i, :]
        tHere = T[i, :]
        tot_rmse[i, :] = np.sqrt((tHere - yHere)**2)
        mean_rmse[i] = fm.rmse(
            tHere[:refLevel],
            yHere[:refLevel])

        yHere = Y[i, :sfcLevel]
        tHere = T[i, :sfcLevel]
        sfc_rmse[i, :] = np.sqrt((tHere - yHere)**2)
        sfc_mean_rmse[i] = fm.rmse(tHere, yHere)

    return tot_rmse, mean_rmse, sfc_rmse, sfc_mean_rmse


def calculate_profile_rmses_samples3D(
        Y, T,
        refLevel=REF_ERROR_LEVEL,
        sfcLevel=SFC_ERROR_LEVEL):
    """
    Expects:
        Y: np.array(N, 256, 2)
        T: np.array(N, 256, 2)
    """

    nSamples = Y.shape[0]
    nLevels = Y.shape[1]
    nVars = Y.shape[2]

    tot_rmse = np.empty((nSamples, nLevels * nVars))
    mean_rmse = np.empty((nSamples))

    sfc_rmse = np.empty((nSamples, sfcLevel * nVars))
    sfc_mean_rmse = np.empty((nSamples))

    YCalc = Y.reshape((nSamples, nLevels * nVars))
    TCalc = T.reshape((nSamples, nLevels * nVars))

    YCalcSfc = Y[:, :sfcLevel, :].reshape(
        (nSamples, sfcLevel * nVars))
    TCalcSfc = T[:, :sfcLevel, :].reshape(
        (nSamples, sfcLevel * nVars))
    for i in range(nSamples):
        yHere = YCalc[i, :]
        tHere = TCalc[i, :]
        tot_rmse[i, :] = np.sqrt((tHere - yHere)**2)
        mean_rmse[i] = fm.rmse(
            tHere[:refLevel], yHere[:refLevel])

        yHere = YCalcSfc[i, :]
        tHere = TCalcSfc[i, :]
        sfc_rmse[i, :] = np.sqrt((tHere - yHere)**2)
        sfc_mean_rmse[i] = fm.rmse(tHere, yHere)

    return tot_rmse, mean_rmse, sfc_rmse, sfc_mean_rmse


def calculate_results(
    RAOB, Y, Pred,
        calcCAPE=False,
        indxT=ML_INDX_TEMPERATURE,
        indxTD=ML_INDX_DEWPOINT,
        name='ml',
        RAPP=None,
        RAOBCC=None,
        rDict=None,
        refLevel=REF_ERROR_LEVEL,
        type='test'):

    # get weights
    if len(Y.shape) == 3:
        Yhere = Y[..., 0]
        Yweights = Y
    else:
        Yhere = Y
        Yweights = organize_data_mse_weighted_ps(
            Y, RAOB[:, :, RAOB_INDX_PRESSURE])

    # get output shape
    nsamples = Y.shape[0]
    nlevs = int(Y.shape[1] / 2)
    outShape = (nsamples, nlevs, 2)

    # reshape Y to separate columns for temp/dewpoint
    Pred2D = np.reshape(Pred, outShape)
    Y2D = np.reshape(Yhere, outShape)

    # setup dictionary for results
    if rDict is None:
        rDict = {}

    # calculate combined errors
    rDict[f'{name}_{type}_mse'] = fm.mse(
        Y2D[:, :refLevel, :],
        Pred2D[:, :refLevel, :])
    rDict[f'{name}_{type}_mse_sfc'] = fm.mse(
        Y2D[:, :SFC_ERROR_LEVEL, :],
        Pred2D[:, :SFC_ERROR_LEVEL, :])
    rDict[f'{name}_{type}_mse_weighted'] = \
        fm.mse_weighted_ps2D(Yweights, Pred)

    rDict[f'{name}_{type}_rmse'] = fm.rmse(
        Y2D[:, :refLevel, :],
        Pred2D[:, :refLevel, :])
    rDict[f'{name}_{type}_rmse_sfc'] = fm.rmse(
        Y2D[:, :SFC_ERROR_LEVEL, :],
        Pred2D[:, :SFC_ERROR_LEVEL, :])
    rDict[f'{name}_{type}_rmse_weighted'] = \
        fm.rmse_weighted_ps2D(Yweights, Pred)

    # calculate temperature and dewpoint errors
    (rmseT, mean_rmseT, rmse_sfc, mean_rmse_sfc) = \
        calculate_profile_rmses(
        RAOB[:, :, RAOB_INDX_TEMPERATURE],
        Pred2D[:, :, indxT])
    rDict[f'{name}T_{type}_rmse'] = mean_rmseT
    rDict[f'{name}T_{type}_rmse_sfc'] = mean_rmse_sfc
    rDict[f'{name}T_{type}_profile_rmse'] = rmseT
    rDict[f'{name}T_{type}_profile_rmse_sfc'] = rmse_sfc

    (rmseTD, mean_rmseTD, rmse_sfc, mean_rmse_sfc) = \
        calculate_profile_rmses(
            RAOB[:, :, RAOB_INDX_DEWPOINT],
            Pred2D[:, :, indxTD])
    rDict[f'{name}TD_{type}_rmse'] = mean_rmseTD
    rDict[f'{name}TD_{type}_rmse_sfc'] = mean_rmse_sfc
    rDict[f'{name}TD_{type}_profile_rmse'] = rmseTD
    rDict[f'{name}TD_{type}_profile_rmse_sfc'] = rmse_sfc

    (rmseT, mean_rmseT, rmse_sfc, mean_rmse_sfc) = \
        calculate_profile_rmses_samples(
            RAOB[:, :, RAOB_INDX_TEMPERATURE],
            Pred2D[:, :, indxT])
    rDict[f'{name}T_{type}_case_rmse'] = mean_rmseT
    rDict[f'{name}T_{type}_case_rmse_sfc'] = mean_rmse_sfc
    rDict[f'{name}T_{type}_case_profile_abs_err'] = rmseT
    rDict[f'{name}T_{type}_case_profile_abs_err_sfc'] = rmse_sfc

    (rmseT, mean_rmseT, rmse_sfc, mean_rmse_sfc) = \
        calculate_profile_rmses_samples(
            RAOB[:, :, RAOB_INDX_DEWPOINT],
            Pred2D[:, :, indxTD])
    rDict[f'{name}TD_{type}_case_rmse'] = mean_rmseT
    rDict[f'{name}TD_{type}_case_rmse_sfc'] = mean_rmse_sfc
    rDict[f'{name}TD_{type}_case_profile_abs_err'] = rmseT
    rDict[f'{name}TD_{type}_case_profile_abs_err_sfc'] = rmse_sfc

    if calcCAPE:
        if RAOBCC is None:
            cape, cin = calculate_cape_cin_arrays(
                RAOB[..., RAOB_INDX_PRESSURE],
                RAOB[..., RAOB_INDX_TEMPERATURE],
                RAOB[..., RAOB_INDX_DEWPOINT])
        else:
            cape = RAOBCC[..., 0]
            cin = -1 * RAOBCC[..., 1]

        if RAPP is None:
            RAPP = RAOB[..., RAOB_INDX_PRESSURE]
        capeM, cinM = calculate_cape_cin_arrays(
            RAPP[:, :],
            Pred2D[..., indxT],
            Pred2D[..., indxTD])
        capeAE = np.abs(np.subtract(cape, capeM))
        cinAE = np.abs(np.subtract(cin, cinM))

        rDict[f'{name}CAPE_{type}'] = capeM
        rDict[f'{name}CAPE_{type}_rmse'] = fm.rmse(cape, capeM)
        rDict[f'{name}CAPE_{type}_case_abs_err'] = capeAE

        rDict[f'{name}CIN_{type}'] = cinM
        rDict[f'{name}CIN_{type}_rmse'] = \
            fm.rmse(cin, cinM)
        rDict[f'{name}CIN_{type}_case_abs_err'] = cinAE

    return rDict


def calculate_results_rap(
    RAOB, RAP,
        calcCAPE=False,
        name='rap',
        RAOBCC=None,
        RAPCC=None,
        refLevel=REF_ERROR_LEVEL,
        rDict=None,
        type='test'):

    raob_dims = [RAOB_INDX_TEMPERATURE,
                 RAOB_INDX_DEWPOINT]
    Y2 = RAOB[:, :, raob_dims].reshape(
        RAOB.shape[0], -1)
    rap_dims = [RAP_INDX_TEMPERATURE,
                RAP_INDX_DEWPOINT]
    RAP2 = RAP[:, :, rap_dims].reshape(RAP.shape[0], -1)
    if calcCAPE and RAPCC is not None \
            and RAOBCC is not None:
        rapDict = calculate_results(
            RAOB, Y2, RAP2, calcCAPE=False,
            name=name, rDict=rDict,
            refLevel=refLevel, type=type)
        rapDict = calculate_results_rapcc(
            RAOBCC, RAPCC, rDict=rapDict)
    else:
        rapDict = calculate_results(
            RAOB, Y2, RAP2,
            calcCAPE=calcCAPE,
            name=name,
            rDict=rDict,
            RAOBCC=RAOBCC,
            RAPP=RAP[..., RAOB_INDX_PRESSURE],
            refLevel=refLevel,
            type=type)
    return rapDict


def calculate_results_rapcc(Y, Pred,
                            model='rap',
                            rDict={},
                            type='test'):

    # calculate cape errors
    rDict[f'{model}CAPE_{type}_mae'] = fm.mae(
        Y[:, CAPE_INDX], Pred[:, CAPE_INDX])
    rDict[f'{model}CAPE_{type}_rmse'] = fm.rmse(
        Y[:, CAPE_INDX], Pred[:, CAPE_INDX])

    # calculate cin errors
    rDict[f'{model}CIN_{type}_mae'] = fm.mae(
        Y[:, CIN_INDX], Pred[:, CIN_INDX])
    rDict[f'{model}CIN_{type}_rmse'] = fm.rmse(
        Y[:, CIN_INDX], Pred[:, CIN_INDX])

    return rDict


def calculate_spline(array, snum=48):
    num = array.shape[0]
    x = np.linspace(1, num, num=num)
    t, c, k = interpolate.splrep(x, array, s=snum)
    return interpolate.BSpline(t, c, k)(x)


def calculate_spread_skill(
    ytrue, ymean, ystd,
        bins=None, nBins=20,
        returnBins=False):

    model_rmse = fm.rmse(ytrue, ymean)
    minBin = np.min([model_rmse.min(), ystd.min()])
    maxBin = np.ceil(np.max([model_rmse.max(), ystd.max()]))

    if not bins:
        bins = np.round(np.linspace(
            minBin, maxBin, nBins + 1), 1)
    else:
        nBins = len(bins) - 1

    error = np.empty((nBins))
    spread = np.empty((nBins))
    for i in range(nBins):
        refs = np.logical_and(ystd >= bins[i],
                              ystd < bins[i + 1])
        if np.count_nonzero(refs) > 0:
            ytrueBin = ytrue[refs]
            ymeanBin = ymean[refs]
            error[i] = fm.rmse(ytrueBin, ymeanBin)
            spread[i] = np.mean(ystd[refs])
        else:
            spread[i] = -999
            error[i] = -999

    if returnBins:
        return spread, error, bins
    else:
        return spread, error


def combineDicts(dict1, dict2):
    """Combine two dictionaries into a single output dictionary."""

    outDict = {}
    dict1Keys = list(dict1.keys())
    for key in dict1Keys:
        outDict[key] = dict1[key]

    dict2Keys = list(dict2.keys())
    for key in dict2Keys:
        outDict[key] = dict2[key]

    return outDict


def combine_layers(nCombine, data1,
                   data2=None,
                   data3=None):
    nLevs = data1.shape[1]
    nLevsNew = int(np.ceil(nLevs / nCombine))

    newShape1 = list(data1.shape)
    newShape1[1] = nLevsNew
    data1New = np.empty(newShape1)
    if data2 is not None:
        newShape2 = list(data2.shape)
        newShape2[1] = nLevsNew
        data2New = np.empty(newShape2)
    if data3 is not None:
        newShape3 = list(data3.shape)
        newShape3[1] = nLevsNew
        data3New = np.empty(newShape3)

    iVal = 0
    iNew = 0
    while iVal + nCombine <= nLevs:
        data1New[:, iNew, :] = np.mean(
            data1[:, iVal:iVal + nCombine, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iVal:iVal + nCombine, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iVal: iVal + nCombine, :], axis=1)
        iVal += nCombine
        iNew += 1

    if iVal != nLevs:
        data1New[:, iNew, :] = np.mean(data1[:, iVal:, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(data2[:, iVal:, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(data3[:, iVal:, :], axis=1)
        iNew += 1

    if iNew != nLevsNew:
        print("Problem Combining Layers!")

    if data3 is not None:
        return data1New, data2New, data3New
    elif data2 is not None:
        return data1New, data2New
    else:
        return data1New


def combine_layersA(nCombineSfc, nCombineA, data1,
                    data2=None, data3=None,
                    nLevsSfc=25,
                    returnLevs=False):
    nLevs = data1.shape[1]

    iComboStart = 0
    iComboStop = iComboStart + nCombineSfc
    iNew = 0
    while iComboStart <= nLevsSfc:
        iComboStart += nCombineSfc
        iComboStop += nCombineSfc
        iNew += 1
    while iComboStop <= nLevs:
        iComboStart += nCombineA
        iComboStop += nCombineA
        iNew += 1
    if iComboStop < nLevs:
        iNew += 1
    if returnLevs:
        return iNew
    else:
        nLevsNew = iNew

    newShape1 = list(data1.shape)
    newShape1[1] = nLevsNew
    data1New = np.empty(newShape1)
    if data2 is not None:
        newShape2 = list(data2.shape)
        newShape2[1] = nLevsNew
        data2New = np.empty(newShape2)
    if data3 is not None:
        newShape3 = list(data3.shape)
        newShape3[1] = nLevsNew
        data3New = np.empty(newShape3)

    iComboStart = 0
    iComboStop = iComboStart + nCombineSfc
    iNew = 0
    while iComboStart <= nLevsSfc:
        data1New[:, iNew, :] = np.mean(
            data1[:, iComboStart:iComboStop, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iComboStart:iComboStop, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iComboStart:iComboStop, :], axis=1)
        iComboStart += nCombineSfc
        iComboStop += nCombineSfc
        iNew += 1

    while iComboStop <= nLevs:
        data1New[:, iNew, :] = np.mean(
            data1[:, iComboStart:iComboStop, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iComboStart:iComboStop, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iComboStart:iComboStop, :], axis=1)
        iComboStart += nCombineA
        iComboStop += nCombineA
        iNew += 1

    if iComboStop < nLevs:
        data1New[:, iNew, :] = np.mean(
            data1[:, iComboStop:, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iComboStop:, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iComboStop:, :], axis=1)
        iNew += 1

    if iNew != nLevsNew:
        print("Problem Combining Layers!")

    if data3 is not None:
        return data1New, data2New, data3New
    elif data2 is not None:
        return data1New, data2New
    else:
        return data1New


def compute_average_altitude(altAll):
    """
    Compute average altitude(either height or pressure).
    altAll: np.array(nSites, nHeight)
    """

    altAvg = np.mean(altAll, axis=0)
    return altAvg


def check_data_dict(ftrain, fval, ftest,
                    verbose=True):

    fileList = [ftrain, fval, ftest]
    typeList = ['train', 'val', 'test']
    dataDict = {}
    for fList, ftype in zip(fileList, typeList):
        siteDict = {}
        siteList = []
        monthList = []
        keyList = []
        for file in fList:
            sName = file[:3]
            sTimeStr = file[4:14]
            sMon = file[9:11]
            sKey = f"{sName}{sMon}"
            sDate = datetime.strptime(
                sTimeStr, "%Y-%m-%d")
            if sName not in siteList:
                siteList.append(sName)
            if sMon not in monthList:
                monthList.append(sMon)

            if sKey not in keyList:
                keyList.append(sKey)
                siteDict[f"{sName}{sMon}Num"] = 1
                siteDict[f"{sName}{sMon}First"] = sDate
                siteDict[f"{sName}{sMon}Last"] = sDate
            else:
                siteDict[f"{sName}{sMon}Num"] += 1
                prevFirst = siteDict[f"{sName}{sMon}First"]
                if sDate < prevFirst:
                    siteDict[f"{sName}{sMon}First"] = sDate
                prevLast = siteDict[f"{sName}{sMon}Last"]
                if sDate > prevLast:
                    siteDict[f"{sName}{sMon}Last"] = sDate
        dataDict[ftype] = siteDict
        dataDict[f"{ftype}Sites"] = siteList
        dataDict[f"{ftype}Months"] = monthList

    if verbose:
        trainDict = dataDict['train']
        valDict = dataDict['val']
        testDict = dataDict['test']
        print("Site   nTr   nVal   nTe   Train < Test")
        for site in siteList:
            siteGood = True
            nTr = 0
            nTe = 0
            nVal = 0
            for mon in monthList:
                trainLast = trainDict[f"{site}{mon}Last"].\
                    strftime("%Y%m%d")
                testFirst = testDict[f"{site}{mon}First"].\
                    strftime("%Y%m%d")
                if testFirst < trainLast:
                    siteGood = False
                nTr += trainDict[f"{site}{mon}Num"]
                nVal += valDict[f"{site}{mon}Num"]
                nTe += testDict[f"{site}{mon}Num"]

            print("{}   {}   {}   {}   {}".format(
                site, nTr, nTe, nVal, siteGood))

    return dataDict


def create_contours(minVal, maxVal,
                    nContours,
                    match=False):
    if match:
        xVal = np.max([np.abs(minVal), np.abs(maxVal)])
        # if nContours % 2 == 1:
        #    nContours += 1
        interval = 2 * xVal / (nContours - 1)
    else:
        interval = (maxVal - minVal) / (nContours - 1)
    contours = np.empty((nContours))
    for i in range(nContours):
        contours[i] = minVal + i * interval
    return contours


def df_results_show(
    dictIn,
        names_to_show_orig=NAMES_TO_SHOW_ORIGCC,
        names_to_show=NAMES_TO_SHOWCC,
        num_to_show=-1,
        sort=False,
        sortName='mlTD_sfc'):
    df = pd.DataFrame.from_dict(dictIn, orient='index')
    dfShow = df[names_to_show_orig]
    for i in range(len(names_to_show)):
        dfShow = dfShow.rename(columns={
            names_to_show_orig[i]: names_to_show[i]})
    if sort:
        if num_to_show > 0:
            num_to_show = np.min([dfShow.shape[0], num_to_show])
        display(dfShow.sort_values(sortName)[:num_to_show])
    else:
        display(dfShow)

    return dfShow


def df_results_show_improve(
    dictIn,
        names_to_show_orig=NAMES_TO_SHOW_ORIGCC,
        names_to_show=NAMES_TO_SHOWCC_IMPROVE,
        num_to_show=-1,
        sort=False,
        sortName='mlTD_sfc'):
    df = pd.DataFrame.from_dict(dictIn, orient='index')
    nDiff = int(len(names_to_show))
    count = 0
    for i in range(nDiff):
        if i == 0:
            dfShow = \
                (df[names_to_show_orig[count]]
                 - df[names_to_show_orig[count + 1]]) \
                / df[names_to_show_orig[count]] * 100.
            dfShow = pd.DataFrame(dfShow, columns=[names_to_show[i]])
        else:
            dfShow[names_to_show[i]] = \
                (df[names_to_show_orig[count]]
                 - df[names_to_show_orig[count + 1]]) \
                / df[names_to_show_orig[count]] * 100.
        count += 2

    if sort:
        if num_to_show > 0:
            num_to_show = np.min([dfShow.shape[0], num_to_show])
        display(dfShow.sort_values(sortName, ascending=False)[:num_to_show])
    else:
        display(dfShow)

    return dfShow


def extract_filename_month(fileName):
    return int(fileName[9:11])


def extract_refs_by_month(monStart, monStop, FILEdata):
    """
    Get references to extract data only within
    the months specified.
    """

    nSamples = FILEdata.shape[0]
    nSave = 0
    refSave = np.zeros((nSamples), dtype='int')
    for i in range(nSamples):
        month = extract_filename_month(FILEdata[i])
        if month >= monStart \
                and month <= monStop:
            refSave[nSave] = i
            nSave += 1

    refSaveR = refSave[:nSave]
    return refSaveR


def find_mismatching_keys(dict1, dict2):
    dict1Keys = set(dict1.keys())
    dict2Keys = set(dict2.keys())
    return list(dict1Keys - dict2Keys)


def find_nearest(array, value, larger=False):
    if larger:
        # The implementation below is used so that when a value is halfway
        # between two points, the larger index is returned (like infind)
        idx = len(array) - np.abs((array - value))[::-1].argmin() - 1
    else:
        idx = (np.abs(array - value)).argmin()

    return idx


def get_attributes_diagram_points(
        y_true, y_pred,
        bad_val=-999.,
        min_val=None,
        max_val=None,
        nBins=ATTRIBUTES_NBINS,
        nTicks=6,
        y_train=None):

    nTargets = y_true.shape[-1]

    if min_val is None:
        minFlag = True
        min_val = np.empty((nTargets))
    else:
        minFlag = False

    if max_val is None:
        maxFlag = True
        max_val = np.empty((nTargets))
    else:
        maxFlag = False

    if minFlag or maxFlag:
        for tg in range(nTargets):
            min_val_temp, max_val_temp = get_min_max(
                y_pred[..., tg], y_true[..., tg])

            if minFlag:
                min_val[tg] = min_val_temp
            if maxFlag:
                max_val[tg] = max_val_temp

    mean_val = np.zeros((nTargets)) + bad_val
    msess = np.zeros((nTargets))
    obs_bins = np.zeros((nBins, nTargets)) + bad_val
    obs_counts = np.zeros((nBins, nTargets)) + bad_val
    obs_vals = np.zeros((nBins, nTargets)) + bad_val
    pred_bins = np.zeros((nBins, nTargets)) + bad_val
    pred_counts = np.zeros((nBins, nTargets)) + bad_val
    pred_vals = np.zeros((nBins, nTargets)) + bad_val
    tick_vals = np.zeros((nTicks, nTargets)) + bad_val
    rmse_vals = np.zeros((nTargets)) + bad_val

    for tg in range(nTargets):
        y_trueH = y_true[..., tg].reshape(-1)
        y_predH = y_pred[..., tg].reshape(-1)
        rmse_vals[tg] = fm.rmse(y_trueH, y_predH)

        if y_train is not None:
            y_trainH = y_train[..., tg].reshape(-1)
            mean_val[tg] = y_trainH.mean()
        else:
            mean_val[tg] = y_trueH.mean()

        if min_val is None or max_val is None:
            min_val[tg], max_val[tg] = get_min_max(y_pred, y_true)

        bin_centers = create_contours(
            min_val[tg], max_val[tg], nBins)
        bin_edges = get_edges(bin_centers)

        for i in range(nBins):
            gref = np.logical_and(
                y_predH >= bin_edges[i],
                y_predH < bin_edges[i + 1])
            if np.any(gref):
                pred_vals[i, tg] = np.mean(
                    y_predH[gref])
                obs_vals[i, tg] = np.mean(y_trueH[gref])

        yclimo = np.ones_like(y_trueH) * mean_val[tg]
        mseclimo = fm.mse(y_trueH, yclimo)
        msepred = fm.mse(y_trueH, y_predH)
        msess[tg] = (mseclimo - msepred) / mseclimo
        obs_counts[:, tg], obs_bins[:, tg] = get_histogram(
            y_trueH, bins=bin_edges)
        pred_counts[:, tg], pred_bins[:, tg] = get_histogram(
            y_predH, bins=bin_edges)
        tick_vals[:, tg] = list_to_int(
            np.linspace(min_val[tg], max_val[tg], nTicks))

    aDict = {}
    aDict['attr_min_val'] = min_val
    aDict['attr_max_val'] = max_val
    aDict['attr_mean_val'] = mean_val
    aDict['attr_msess'] = msess
    aDict['attr_obs_bins'] = obs_bins
    aDict['attr_obs_counts'] = obs_counts
    aDict['attr_obs_vals'] = obs_vals
    aDict['attr_pred_bins'] = pred_bins
    aDict['attr_pred_counts'] = pred_counts
    aDict['attr_pred_vals'] = pred_vals
    aDict['attr_rmse'] = rmse_vals
    aDict['attr_tick_vals'] = tick_vals
    return aDict


def get_best_model(rDDict, keyList=EVAL_KEY_LIST,
                   returnScoreList=False,
                   verbose=True):
    modelList = list(rDDict.keys())
    nModels = len(modelList)
    nKeys = len(keyList)

    resultArray = np.empty((nKeys, nModels))
    countM = 0
    rDict = {}
    for modelRef in modelList:
        rDict = rDDict[modelRef]

        countK = 0
        for key in keyList:
            resultArray[countK, countM] = rDict[key]
            countK += 1
        countM += 1

    resultANorm = np.empty((nKeys, nModels))
    for i in range(nKeys):
        keyMax = np.max(resultArray[i, :])
        keyMin = np.min(resultArray[i, :])
        resultANorm[i, :] = \
            (resultArray[i, :] - keyMin) / (keyMax - keyMin)
    resultANorm = 1. - resultANorm
    bestScores = np.sum(resultANorm, axis=0)
    if verbose:
        iSort = np.argsort(bestScores)
        print("\n   MODEL    SCORE")
        for i in iSort[::-1]:
            print("   {}   {:.4f}".format(modelList[i], bestScores[i]))

    if returnScoreList:
        return bestScores
    else:
        iSorts = np.argmax(bestScores)
        return modelList[iSorts]


def get_best_models(rDDict, keyList=EVAL_KEY_LIST,
                    verbose=True):
    modelList = list(rDDict.keys())
    nModels = len(modelList)
    nKeys = len(keyList)

    resultArray = np.empty((nKeys, nModels))
    countM = 0
    rDict = {}
    for modelRef in modelList:
        rDict = rDDict[modelRef]

        countK = 0
        for key in keyList:
            resultArray[countK, countM] = rDict[key]
            countK += 1
        countM += 1

    countK = 0
    bestList = []
    for key in keyList:
        arHere = resultArray[countK, :]
        iRef = np.argmin(arHere)
        bestList.append(modelList[iRef])
        if verbose:
            print('  {}: {}  RMSE: {:.4f}'.format(
                key, modelList[iRef], arHere[iRef]))
        countK += 1
    return bestList


def get_best_trial(rDictList, modelList,
                   returnIndex=False,
                   type='val',
                   verbose=False):
    rmseBest = 999.
    modelBest = None
    indexBest = -1

    count = 0
    keyList = rDictList[0].keys()
    for rDict, model in zip(rDictList, modelList):
        if f'ml_{type}_rmse' in keyList:
            rmseA = rDict[f'ml_{type}_rmse']
        else:
            rmseA = 0
        if f'mlT_{type}_rmse' in keyList:
            rmseT = rDict[f'mlT_{type}_rmse_sfc']
        else:
            rmseT = 0
        if f'mlTD_{type}_rmse' in keyList:
            rmseTD = rDict[f'mlTD_{type}_rmse_sfc']
        else:
            rmseTD = 0
        rmseHere = 0.4 * rmseTD + 0.4 * rmseT + 0.2 * rmseA

        if rmseHere < rmseBest:
            rmseBest = rmseHere
            modelBest = model
            indexBest = count

        count += 1
    if verbose:
        print("Best model Index: {}, RMSE: {:.3f}".format(
              indexBest, rmseBest))

    if returnIndex:
        return modelBest, indexBest
    else:
        return modelBest


def get_best_trial_cape(rDictList, modelList,
                        returnIndex=False,
                        type='val',
                        verbose=False):
    rmseBest = 999.
    modelBest = None
    indexBest = -1

    count = 0
    keyList = rDictList[0].keys()
    for rDict, model in zip(rDictList, modelList):
        if f'mlT_{type}_rmse' in keyList:
            rmseT = rDict[f'mlT_{type}_rmse']
        else:
            print("Missing T!")
            rmseT = 0
        if f'mlT_{type}_rmse_sfc' in keyList:
            rmseTS = rDict[f'mlT_{type}_rmse_sfc']
        else:
            print("Missing Sfc T!")
            rmseTS = 0
        if f'mlTD_{type}_rmse' in keyList:
            rmseTD = rDict[f'mlTD_{type}_rmse']
        else:
            print("Missing TD!")
            rmseTD = 0
        if f'mlTD_{type}_rmse_sfc' in keyList:
            rmseTDS = rDict[f'mlTD_{type}_rmse_sfc']
        else:
            print("Missing Sfc TD!")
            rmseTDS = 0
        if f'mlCAPE_{type}_rmse' in keyList:
            rmseCAPE = rDict[f'mlCAPE_{type}_rmse']
        else:
            print("Missing CAPE!")
            rmseCAPE = 0
        if f'mlCIN_{type}_rmse' in keyList:
            rmseCIN = rDict[f'mlCIN_{type}_rmse']
        else:
            print("Missing CIN!")
            rmseCIN = 0

        rmseHere = \
            0.1 * rmseT \
            + 0.2 * rmseTS \
            + 0.1 * rmseTD \
            + 0.2 * rmseTDS \
            + 0.3 * rmseCAPE \
            + 0.3 * rmseCIN

        if rmseHere < rmseBest:
            rmseBest = rmseHere
            modelBest = model
            indexBest = count

        count += 1
    if verbose:
        print("Best model Index: {}, RMSE: {:.3f}".format(
              indexBest, rmseBest))

    if returnIndex:
        return modelBest, indexBest
    else:
        return modelBest


def get_best_trial_cape_indx(
        rDictList,
        type='val',
        verbose=True):

    rmseBest = 999.
    indexBest = -1

    count = 0
    keyList = rDictList[0].keys()
    for rDict in rDictList:
        if f'mlT_{type}_rmse' in keyList:
            rmseT = rDict[f'mlT_{type}_rmse']
        else:
            print("Missing T!")
            rmseT = 0
        if f'mlT_{type}_rmse_sfc' in keyList:
            rmseTS = rDict[f'mlT_{type}_rmse_sfc']
        else:
            print("Missing Sfc T!")
            rmseTS = 0
        if f'mlTD_{type}_rmse' in keyList:
            rmseTD = rDict[f'mlTD_{type}_rmse']
        else:
            print("Missing TD!")
            rmseTD = 0
        if f'mlTD_{type}_rmse_sfc' in keyList:
            rmseTDS = rDict[f'mlTD_{type}_rmse_sfc']
        else:
            print("Missing Sfc TD!")
            rmseTDS = 0
        if f'mlCAPE_{type}_rmse' in keyList:
            rmseCAPE = rDict[f'mlCAPE_{type}_rmse']
        else:
            print("Missing CAPE!")
            rmseCAPE = 0
        if f'mlCIN_{type}_rmse' in keyList:
            rmseCIN = rDict[f'mlCIN_{type}_rmse']
        else:
            print("Missing CIN!")
            rmseCIN = 0

        rmseHere = \
            0.1 * rmseT \
            + 0.2 * rmseTS \
            + 0.1 * rmseTD \
            + 0.2 * rmseTDS \
            + 0.3 * rmseCAPE \
            + 0.3 * rmseCIN
        if verbose:
            print("Test Index {}: {:.3f}".format(
                count, rmseHere))

        if rmseHere < rmseBest:
            rmseBest = rmseHere
            indexBest = count
        count += 1
    if verbose:
        print("Best Model Index: {}, RMSE: {:.3f}".format(
              indexBest, rmseBest))

    return indexBest


def get_centers(xvals):
    centers = [xvals[j] + (xvals[j + 1] - xvals[j]) * 0.5
               for j in range(len(xvals) - 1)]
    return centers


def get_discard_points(
        y_true, y_pred, y_std,
        bins=None,
        discardDict=None):

    if bins is None:
        nbins = 10
        bins = np.linspace(0., 0.9, nbins)
    else:
        nbins = len(bins)

    nTargets = y_true.shape[-1]
    discard_mf_all = []
    discard_imprv_all = []
    discard_imprva_all = []
    discard_vals_all = []
    discard_bins_all = []

    for tg in range(nTargets):
        ytrue1d = y_true[..., tg].reshape(-1)
        ymean1d = y_pred[..., tg].reshape(-1)
        ystd1d = y_std[..., tg].reshape(-1)
        nsamples = ystd1d.shape[0]

        yrefs = np.argsort(ystd1d)
        ytrueSorted = ytrue1d[yrefs]
        ymeanSorted = ymean1d[yrefs]

        rmseOut = np.empty((nbins))
        for i in range(nbins):
            iCutoff = nsamples - int(nsamples * bins[i])
            ytrueH = ytrueSorted[:iCutoff]
            ymeanH = ymeanSorted[:iCutoff]
            rmseOut[i] = fm.rmse(ytrueH, ymeanH)

        dtiA = np.empty((nbins - 1))
        for i in range(nbins - 1):
            dtiA[i] = (rmseOut[i] - rmseOut[i + 1])

        dtmf = 0.
        for i in range(nbins - 1):
            if rmseOut[i] >= rmseOut[i + 1]:
                indicator = 1.
            else:
                indicator = 0.
            dtmf += indicator
        dtmf *= 1 / (nbins - 1)

        discard_mf_all.append(dtmf)
        discard_imprv_all.append(np.mean(dtiA))
        discard_imprva_all.append(dtiA)
        discard_vals_all.append(rmseOut)
        discard_bins_all.append(bins)

    if discardDict is None:
        discardDict = {}
    discardDict['discard_bins'] = discard_bins_all
    discardDict['discard_imprv'] = discard_imprv_all
    discardDict['discard_imprva'] = discard_imprva_all
    discardDict['discard_mf'] = discard_mf_all
    discardDict['discard_vals'] = discard_vals_all

    return discardDict


def get_edges(xvals):
    cmid = [xvals[j] - (xvals[j + 1] - xvals[j]) * 0.5
            for j in range(len(xvals) - 1)]
    c1 = [xvals[-1] - (xvals[-1] - xvals[-2]) * 0.5]
    c2 = [xvals[-1] + (xvals[-1] - xvals[-2]) * 0.5]
    return np.array(cmid + c1 + c2)


def get_exp_addTRef(expDict):
    keyStr = 'RAP_Temperature'
    featureNames = get_exp_featurenames_channels(expDict)
    if keyStr in featureNames:
        return featureNames.index(keyStr)
    else:
        return None


def get_exp_addTDRef(expDict):
    keyStr = 'RAP_Dewpoint'
    featureNames = get_exp_featurenames_channels(expDict)
    if keyStr in featureNames:
        return featureNames.index(keyStr)
    else:
        return None


def get_exp_dataInputNs(expDict, verbose=False):
    errString = "GET_EXP_DATAINPUTNs ERROR:"

    dataOpts = get_exp_dataOpts(expDict)
    if dataOpts is not None:
        try:
            nGOES = len(dataOpts['input_channels_goes'])
        except KeyError:
            if verbose:
                print("{} Missing input_channels_goes.".format(errString))
            nGOES = 0

        try:
            nGEList = dataOpts['input_extra_goes']
            nGOESExtra = 0
            for ch in nGEList:
                if ch == 0:
                    nGOESExtra += nGOES
                else:
                    nGOESExtra += 1
        except KeyError:
            if verbose:
                print("{} Missing input_extra_goes.".format(errString))
            nGOESExtra = 0
        nGOES += nGOESExtra

        try:
            nRAP = len(dataOpts['input_channels_rap'])
        except KeyError:
            if verbose:
                print("{} Missing input_channels_rap.".format(errString))
            nRAP = 0

        try:
            nRTMA = len(dataOpts['input_channels_rtma'])
        except KeyError:
            if verbose:
                print("{} Missing input_channels_rtma")
            nRTMA = 0

        try:
            nCC = len(dataOpts['input_rap_cape_cin'])
        except KeyError:
            if verbose:
                print("{} Missing input_rap_cape_cin")
            nCC = 0
    else:
        nGOES = nRAP = nRTMA = nCC = 0
    return nGOES, nRAP, nRTMA, nCC


def get_exp_dataOpts(expDict, verbose=False):
    errString = "GET_EXP_DATAOPTS ERROR:"
    try:
        return expDict['dataOpts']
    except KeyError:
        if verbose:
            print("{} Missing dataOpts.".format(errString))
        return None


def get_exp_dataOutputNs(expDict, verbose=False):
    errString = "GET_EXP_DATAOUTPUTNS ERROR:"
    dataOpts = get_exp_dataOpts(expDict)
    if dataOpts is not None:
        try:
            nOutputs = len(dataOpts['output_channels_raobs'])
        except KeyError:
            if verbose:
                print("{} Missing output_channels_raobs.".format(errString))
            nOutputs = 0
    else:
        nOutputs = 0
    return nOutputs


def get_exp_featurenames_channels(expDict):
    input_channels_goes = get_exp_inputGOES(expDict)
    input_channels_goese = get_exp_inputGOESExtra(expDict)
    input_channels_rap = get_exp_inputRAP(expDict)
    input_channels_rtma = get_exp_inputRTMA(expDict)
    input_rap_cape_cin = get_exp_inputCAPE(expDict)

    featureNames = []
    for infeat in input_channels_rap:
        featStr = 'RAP'
        if infeat == RAP_INDX_DEWPOINT:
            featStr += '_Dewpoint'
        elif infeat == RAP_INDX_HEIGHT:
            featStr += '_Height'
        elif infeat == RAP_INDX_PRESSURE:
            featStr += '_Pressure'
        elif infeat == RAP_INDX_TEMPERATURE:
            featStr += '_Temperature'
        else:
            print("Unknown RAP Feature")
        featureNames.append(featStr)

    for infeat in input_channels_rtma:
        featStr = 'RTMA'
        if infeat == RTMA_INDX_DEWPOINT:
            featStr += '_Dewpoint'
        elif infeat == RTMA_INDX_PRESSURE:
            featStr += '_Pressure'
        elif infeat == RTMA_INDX_TEMPERATURE:
            featStr += '_Temperature'
        else:
            print("Unknown RTMA Feature")
        featureNames.append(featStr)

    for infeat in input_channels_goes:
        strHere = "GOES_{:0>2d}".format(GOES_CHANNELS[infeat])
        featureNames.append(strHere)

    for infeat in input_channels_goese:
        if infeat == 0:
            for ii in input_channels_goes:
                strHere = "GOES_STD_{:0>2d}".format(GOES_CHANNELS[ii])
                featureNames.append(strHere)
        if infeat == 1:
            strHere = "GOES_10_8"
            featureNames.append(strHere)
        if infeat == 2:
            strHere = "GOES_13_15"
            featureNames.append(strHere)
        if infeat == 3:
            strHere = "GOES_14_13"
            featureNames.append(strHere)

    for infeat in input_rap_cape_cin:
        featStr = 'RAP'
        if infeat == CAPE_INDX:
            featStr += '_CAPE'
        elif infeat == CIN_INDX:
            featStr += '_CIN'
        else:
            print("Unknown RAP CAPE/CIN Feature")
        featureNames.append(featStr)

    return featureNames


def get_exp_filenamePreds(
    useShannon, expDict,
        cloudSize=None,
        cloudThresh=None,
        expRef=None):

    if useShannon:
        dirH = MAIN_RESULT_DIR
    else:
        dirH = ALT_RESULT_DIR

    if expRef is None:
        expRef = get_exp_refname(expDict)

    if cloudSize is not None:
        sizeStr = "_{:0>3d}".format(cloudSize)
    else:
        sizeStr = ''

    if cloudThresh is not None:
        threshStr = "_{}".format(cloudThresh)
    else:
        threshStr = ''
    return dirH + expRef + sizeStr + threshStr + '_preds.pkl'


def get_exp_filenameResults(
    useShannon,
        expDict={},
        expRef='',
        cloud=False,
        cloudSize=None,
        cloudThresh=None,
        strInfo=''):

    if useShannon:
        dirH = MAIN_RESULT_DIR
    else:
        dirH = ALT_RESULT_DIR

    if expDict:
        expRef = get_exp_refname(expDict)

    if cloud:
        cloudStr = '_cloud'
    else:
        cloudStr = ''

    if cloudSize is not None:
        sizeStr = "_{:0>3d}".format(cloudSize)
    else:
        sizeStr = ''

    if cloudThresh is not None:
        threshStr = "_{}".format(cloudThresh)
    else:
        threshStr = ''

    if strInfo:
        return dirH + expRef + cloudStr + sizeStr + threshStr \
            + strInfo + '.pkl'
    else:
        return dirH + expRef + cloudStr + sizeStr + threshStr \
            + '_results.pkl'


def get_exp_inputCAPE(expDict, verbose=False):
    erStr = "GET_EXP_INPUTCAPE ERROR:"
    keyStr = "input_rap_cape_cin"

    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return []


def get_exp_inputGOES(expDict, verbose=False):
    erStr = "GET_EXP_INPUTGOES ERROR:"
    keyStr = "input_channels_goes"

    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return []


def get_exp_inputGOESExtra(expDict, verbose=False):
    erStr = "GET_EXP_INPUTGOESEXTRA ERROR:"
    keyStr = "input_extra_goes"

    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return []


def get_exp_inputRAP(expDict, verbose=False):
    erStr = "GET_EXP_INPUTRAP ERROR:"
    keyStr = "input_channels_rap"

    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return []


def get_exp_inputRTMA(expDict, verbose=False):
    erStr = "GET_EXP_INPUTRTMA ERROR:"
    keyStr = "input_channels_rtma"

    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return []


def get_exp_loss(expDict, verbose=False):
    erStr = "GET_EXP_LOSS ERROR:"
    keyStr = "loss_type"
    try:
        return expDict[keyStr]
    except KeyError:
        try:
            modelOpts = get_exp_modelOpts(expDict)
            return modelOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return None


def get_exp_modelOpts(expDict, verbose=False):
    errStr = "GET_EXP_MODELOPTS ERROR:"
    try:
        modelOpts = expDict['modelOpts']
    except KeyError:
        if verbose:
            print("{} Missing modelOpts.".format(errStr))
        modelOpts = None
    return modelOpts


def get_exp_networkname(expDict, verbose=False):
    errString = 'GET_EXP_NETWORK_NAME ERROR:'
    keyStr = 'network'
    try:
        return expDict[keyStr]
    except KeyError:
        try:
            return expDict['modelOpts']['network']
        except KeyError:
            if verbose:
                print("{} Missing {}".format(errString, keyStr))
            return None


def get_exp_outputCAPE(expDict, verbose=False):
    erStr = "GET_EXP_OUTPUTCAPE ERROR:"
    keyStr = "output_cape_cin"

    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return []


def get_exp_outputRAOB(expDict, verbose=False):
    erStr = "GET_EXP_OUTPUTRAOB ERROR:"
    keyStr = "output_channels_raobs"

    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            if verbose:
                print("{} Missing {}".format(erStr, keyStr))
            return []


def get_exp_refname(expDict):
    errString = 'GET_EXP_REFNAME ERROR:'
    keyStr = 'exRef'
    try:
        return expDict[keyStr]
    except KeyError:
        try:
            return expDict['modelOpts'][keyStr]
        except KeyError:
            print("{} Missing {}".format(errString, keyStr))
            return None


def get_exp_restrict_data(expDict):
    errString = 'GET_EXP_RESTRICT_DATA ERROR:'
    keyStr = 'restrict_data'
    try:
        return expDict[keyStr]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyStr]
        except KeyError:
            print("{} {} {}".
                  format(errString,
                         'Missing data restriction specification',
                         'using all data.'))
            return None


def get_exp_runOpts(expDict, verbose=False):
    errStr = "GET_EXP_RUNOPTS ERROR:"
    try:
        return expDict['runOpts']
    except KeyError:
        if verbose:
            print("{} Missing runOpts.".format(errStr))
        return None


def get_exp_vcombine(expDict, verbose=False):
    errString = "GET_EXP_VCOMBINE ERROR:"
    keyString = 'vcombine'
    try:
        return expDict[keyString]
    except KeyError:
        try:
            dataOpts = get_exp_dataOpts(expDict)
            return dataOpts[keyString]
        except KeyError:
            if verbose:
                print("{}: Missing {}.".format(errString, keyString))
            return []


def get_feature_names(
    expDict, nFeatures,
        nLevs=DEFAULT_NLEVELS,
        verbose=False):

    errStr = "GET_FEATURE_NAMES ERROR:"
    try:
        dataOpts = expDict['dataOpts']
        input_channels_goes = dataOpts['input_channels_goes']
        input_channels_rap = dataOpts['input_channels_rap']
        input_channels_rtma = dataOpts['input_channels_rtma']
    except KeyError:
        print("{} Missing Input Data Specifications.".format(errStr))

    try:
        input_rap_cape_cin = dataOpts['input_rap_cape_cin']
    except KeyError:
        input_rap_cape_cin = []

    featureNames = []
    for infeat in input_channels_rap:
        featStr = 'RAP'
        if infeat == RAP_INDX_DEWPOINT:
            featStr += '_Dewpoint'
        elif infeat == RAP_INDX_HEIGHT:
            featStr += '_Height'
        elif infeat == RAP_INDX_PRESSURE:
            featStr += '_Pressure'
        elif infeat == RAP_INDX_TEMPERATURE:
            featStr += '_Temperature'
        else:
            print("Unknown RAP Feature")

        for i in range(nLevs):
            strHere = featStr + "{:0>3d}".format(i + 1)
            featureNames.append(strHere)

    for infeat in input_channels_rtma:
        featStr = 'RTMA'
        if infeat == RTMA_INDX_DEWPOINT:
            featStr += '_Dewpoint'
        elif infeat == RTMA_INDX_PRESSURE:
            featStr += '_Pressure'
        elif infeat == RTMA_INDX_TEMPERATURE:
            featStr += '_Temperature'
        else:
            print("Unknown RTMA Feature")
        featureNames.append(featStr)

    for infeat in input_channels_goes:
        strHere = "GOES_{:0>2d}".format(GOES_CHANNELS[infeat])
        featureNames.append(strHere)

    for infeat in input_rap_cape_cin:
        featStr = 'RAP'
        if infeat == CAPE_INDX:
            featStr += '_CAPE'
        elif infeat == CIN_INDX:
            featStr += '_CIN'
        else:
            print("Unknown RAP CAPE/CIN Feature")
        featureNames.append(featStr)

    if len(featureNames) != nFeatures:
        print("Mismatching nFeatures={} and names={}".format(
            nFeatures, len(featureNames)))
    else:
        if verbose:
            print("Feature Names: {}".format(featureNames))
    return featureNames


def get_format(value):
    if value < 0:
        return ':.1f'
    elif value < 10:
        return ':.2f'
    else:
        return ':.1f'


def get_histogram(var, bins=10, density=False, weights=None):
    counts, bin_edges = np.histogram(
        var, bins=bins, density=density, weights=weights)
    bin_centers = (bin_edges[:-1]
                   + bin_edges[1:]) / 2
    return counts, bin_centers


def get_improved_model(
    rDDict,
        returnScoreList=False,
        returnScoreUnsorted=False,
        type='test',
        verbose=True, verboseScore=False):

    expList = list(rDDict.keys())
    capeIList = []
    cinIList = []
    tIList = []
    tSfcIList = []
    tdIList = []
    tdSfcIList = []
    for exp in expList:
        rDict = rDDict[exp]
        keyList = rDict.keys()

        if f'mlCAPE_{type}_rmse' in keyList \
                and f'rapCAPE_{type}_rmse' in keyList:
            scoreCAPE = (rDict[f'rapCAPE_{type}_rmse']
                         - rDict[f'mlCAPE_{type}_rmse']) \
                / rDict[f'rapCAPE_{type}_rmse']
        else:
            scoreCAPE = 0.
        capeIList.append(scoreCAPE)

        if f'mlCIN_{type}_rmse' in keyList \
                and f'rapCIN_{type}_rmse' in keyList:
            scoreCIN = (rDict[f'rapCIN_{type}_rmse']
                        - rDict[f'mlCIN_{type}_rmse']) \
                / rDict[f'rapCIN_{type}_rmse']
        else:
            scoreCIN = 0.
        cinIList.append(scoreCIN)

        if f'mlT_{type}_rmse' in keyList \
                and f'rapT_{type}_rmse' in keyList:
            scoreT = (rDict[f'rapT_{type}_rmse']
                      - rDict[f'mlT_{type}_rmse']) \
                / rDict[f'rapT_{type}_rmse']
        else:
            scoreT = 0.
        tIList.append(scoreT)

        if f'mlTD_{type}_rmse' in keyList \
                and f'rapTD_{type}_rmse' in keyList:
            scoreTD = (rDict[f'rapTD_{type}_rmse']
                       - rDict[f'mlTD_{type}_rmse']) \
                / rDict[f'rapTD_{type}_rmse']
        else:
            scoreTD = 0.
        tdIList.append(scoreTD)

        if f'mlT_{type}_rmse_sfc' in keyList \
                and f'rapT_{type}_rmse_sfc' in keyList:
            scoreTSfc = (rDict[f'rapT_{type}_rmse_sfc']
                         - rDict[f'mlT_{type}_rmse_sfc']) \
                / rDict[f'rapT_{type}_rmse_sfc']
        else:
            scoreTSfc = 0.
        tSfcIList.append(scoreTSfc)

        if f'mlTD_{type}_rmse_sfc' in keyList \
                and f'rapTD_{type}_rmse_sfc' in keyList:
            scoreTDSfc = (rDict[f'rapTD_{type}_rmse_sfc']
                          - rDict[f'mlTD_{type}_rmse_sfc']) \
                / rDict[f'rapTD_{type}_rmse_sfc']
        else:
            scoreTDSfc = 0.
        tdSfcIList.append(scoreTDSfc)

    capeMax = np.max(capeIList)
    if capeMax < 0:
        capeScore = capeMax / np.array(capeIList)
    else:
        capeScore = np.array(capeIList) / capeMax
    capeScore = np.where(capeScore < 0., 0., capeScore)
    if verboseScore:
        print("CAPE Scores: {}".format(capeScore))

    cinMax = np.max(cinIList)
    if cinMax < 0:
        cinScore = cinMax / np.array(cinIList)
    else:
        cinScore = np.array(cinIList) / cinMax
    cinScore = np.where(cinScore < 0., 0., cinScore)
    if verboseScore:
        print("CIN Scores: {}".format(cinScore))

    tMax = np.max(tIList)
    if tMax < 0:
        tScore = tMax / np.array(tIList)
    else:
        tScore = np.array(tIList) / tMax
    tScore = np.where(tScore < 0., 0., tScore)
    if verboseScore:
        print("T Scores: {}".format(tScore))

    tdMax = np.max(tdIList)
    if tdMax < 0:
        tdScore = tdMax / np.array(tdIList)
    else:
        tdScore = np.array(tdIList) / tdMax
    tdScore = np.where(tdScore < 0., 0., tdScore)
    if verboseScore:
        print("TD Scores: {}".format(tdScore))

    tSfcMax = np.max(tSfcIList)
    if tSfcMax < 0:
        tSfcScore = tSfcMax / np.array(tSfcIList)
    else:
        tSfcScore = np.array(tSfcIList) / tSfcMax
    tSfcScore = np.where(tSfcScore < 0., 0., tSfcScore)
    if verboseScore:
        print("T SFC Scores: {}".format(tSfcScore))

    tdSfcMax = np.max(tdSfcIList)
    if tdSfcMax < 0:
        tdSfcScore = tdSfcMax / np.array(tdSfcIList)
    else:
        tdSfcScore = np.array(tdSfcIList) / tdSfcMax
    tdSfcScore = np.where(tdSfcScore < 0., 0., tdSfcScore)
    if verboseScore:
        print("TD SFC Scores: {}".format(tdSfcScore))

    overallScore = tScore + tdScore \
        + tSfcScore + tdSfcScore \
        + capeScore + cinScore
    iSort = np.argsort(overallScore)[::-1]
    if verbose:
        print("\n   MODEL    SCORE")
        for i in iSort[::-1]:
            print("   {}   {:.4f}".format(expList[i], overallScore[i]))
    iRef = np.argmax(overallScore)

    if returnScoreUnsorted:
        return overallScore
    elif returnScoreList:
        return np.array(expList)[iSort], \
            overallScore[iSort]
    else:
        return expList[iRef]


def get_pit_dvalue(pit_counts):
    dvalue = 0.
    nbins = pit_counts.shape[0]
    nbinsI = 1. / nbins

    pitTot = np.sum(pit_counts)
    pit_freq = np.divide(pit_counts, pitTot)
    for i in range(nbins):
        dvalue += (pit_freq[i] - nbinsI) \
            * (pit_freq[i] - nbinsI)
    dvalue = np.sqrt(dvalue / nbins)
    return dvalue


def get_pit_evalue(nbins, tsamples):
    evalue = (1 - 1 / nbins) / (tsamples * nbins)
    return np.sqrt(evalue)


def get_pit_points(y_true, y_pred, y_std,
                   pit_bins=PIT_BIN_EDGES,
                   pDict={}):

    nBins = len(pit_bins) - 1
    nSamples = y_true[..., 0].size
    nTargets = y_true.shape[-1]
    pit_evalue = get_pit_evalue(nBins, nSamples)

    pit_dvalues_all = []
    pit_values_all = []
    pit_centers_all = []
    pit_counts_all = []
    pit_weights_all = []
    for tg in range(nTargets):
        pit_values = norm.cdf(
            x=y_true[..., tg],
            loc=y_pred[..., tg],
            scale=y_std[..., tg]).reshape(-1)
        weights = np.ones_like(pit_values) \
            / pit_values.shape[0]
        pit_counts, bin_centers = get_histogram(
            pit_values,
            bins=pit_bins,
            weights=weights)
        pit_dvalues_all.append(get_pit_dvalue(pit_counts))
        pit_values_all.append(pit_values)
        pit_centers_all.append(bin_centers)
        pit_counts_all.append(pit_counts)
        pit_weights_all.append(weights)

    pDict['pit_centers'] = pit_centers_all
    pDict['pit_counts'] = pit_counts_all
    pDict['pit_dvalue'] = pit_dvalues_all
    pDict['pit_evalue'] = pit_evalue
    pDict['pit_evalue'] = pit_evalue
    pDict['pit_values'] = pit_values_all
    pDict['pit_weights'] = pit_weights_all
    return pDict


def get_pit_points_ens(ytrue, ypred,
                       pit_bins=PIT_BIN_EDGES):

    nTr = ytrue.size
    nPr = ypred.size
    if nTr == nPr:
        print("Using ensemble version of get_pit_points, ")
        print("   but the predictions are not ensembles.")
        return {}

    nBins = len(pit_bins) - 1
    nEns = ypred.shape[-1]
    nSamples = ytrue[..., 0].size
    nTargets = ytrue.shape[-1]
    pit_evalue = get_pit_evalue(nBins, nSamples)

    pit_dvalues_all = []
    pit_values_all = []
    pit_centers_all = []
    pit_counts_all = []
    pit_weights_all = []

    for tg in range(nTargets):
        ytrueT = ytrue[..., tg].reshape(-1)
        ypredT = ypred[:, :, tg, :].reshape((nSamples, nEns))
        ypredTS = np.sort(ypredT, axis=1)

        ytrueTE = np.repeat(
            ytrueT[..., np.newaxis], nEns, axis=-1)
        pred_diff = np.abs(np.subtract(
            ytrueTE, ypredTS))
        pit_values = np.divide(
            np.argmin(pred_diff, axis=-1),
            nEns)
        weights = np.ones_like(pit_values) / nSamples
        pit_counts, bin_centers = get_histogram(
            pit_values, bins=pit_bins, weights=weights)

        pit_dvalues_all.append(get_pit_dvalue(pit_counts))
        pit_values_all.append(pit_values)
        pit_centers_all.append(bin_centers)
        pit_counts_all.append(pit_counts)
        pit_weights_all.append(weights)

    pDict = {
        'pit_centers': pit_centers_all,
        'pit_counts': pit_counts_all,
        'pit_dvalue': pit_dvalues_all,
        'pit_evalue': pit_evalue,
        'pit_values': pit_values_all,
        'pit_weights': pit_weights_all}

    return pDict


def get_min_max(var1, var2):
    myMin = np.min([var1.min(), var2.min()])
    myMax = np.max([var1.max(), var2.max()])
    return myMin, myMax


def get_spread_skill_points(
    y_true, y_pred, y_std,
        nBins=12,
        sDict={},
        spread_bins=None,
        spread_last=None):

    nTargets = y_true.shape[-1]

    if not spread_bins:
        spread_bins = []
        for tg in range(nTargets):
            spread_bins.append(None)
        binFlag = True
    else:
        binFlag = False

    spread_counts = []
    spread_vals = []
    error_vals = []
    for tg in range(nTargets):
        spread_valsH, error_valsH, spread_binsH = \
            calculate_spread_skill(
                y_true[..., tg],
                y_pred[..., tg],
                y_std[..., tg],
                bins=spread_bins[tg], nBins=nBins,
                returnBins=True)
        if spread_last is not None:
            spread_valsH[-1] = spread_last[tg]
        spread_countsH, _ = get_histogram(
            y_std, bins=spread_binsH)

        if binFlag:
            spread_bins.append(spread_binsH)
        spread_counts.append(spread_countsH)
        spread_vals.append(spread_valsH)
        error_vals.append(error_valsH)

    sDict['ss_error_vals'] = error_vals
    sDict['ss_spread_bins'] = spread_bins
    sDict['ss_spread_counts'] = spread_counts
    sDict['ss_spread_vals'] = spread_vals
    return sDict


def get_spread_skill_points_uq(
        y_true, y_pred, y_std,
        bins=None,
        nBins=10,
        spread_last=None,
        verbose=True):

    if y_true.shape != y_pred.shape:
        print("Mismatching shapes:")
        print("   y_true: {}".format(y_true.shape))
        print("   y_pred: {}".format(y_pred.shape))
        return {}
    nPts = y_true.size

    if not bins:
        minBin = np.min([0., y_std.min()])
        maxBin = np.ceil(np.max(
            [fm.rmse(y_true, y_pred), y_std.max()]))
        bins = create_contours(minBin, maxBin, nBins + 1)
    else:
        nBins = len(bins) - 1

    ssRel = 0.
    error = np.zeros((nBins)) - 999.
    spread = np.zeros((nBins)) - 999.
    for i in range(nBins):
        refs = np.logical_and(
            y_std >= bins[i],
            y_std < bins[i + 1])
        nPtsBin = np.count_nonzero(refs)
        if nPtsBin > 0:
            ytrueBin = y_true[refs]
            ymeanBin = y_pred[refs]
            error[i] = fm.rmse(ytrueBin, ymeanBin)
            spread[i] = np.mean(y_std[refs])
            ssRel += (nPtsBin / nPts) \
                * np.abs(error[i] - spread[i])

    if spread_last is not None:
        spread[-1] = spread_last
    spread_counts, bin_centers = get_histogram(
        y_std, bins=bins)

    ssRatio = np.mean(y_std) / fm.rmse(y_true, y_pred)
    if verbose:
        print("Spread Skill Ratio: {:.4f}".format(ssRatio))
        print("       Reliability: {:.4f}".format(ssRel))

    sDict = {
        'ss_bin_edges': bins,
        'ss_bin_centers': bin_centers,
        'ss_error_vals': error,
        'ss_ratio': ssRatio,
        'ss_reliability': ssRel,
        'ss_spread_counts': spread_counts,
        'ss_spread_vals': spread}
    return sDict


def list_to_int(input_list):
    out_list = []
    for ele in input_list:
        out_list.append(int(ele))
    return out_list


def load_data(
        useShannon=False,
        fileName=MAIN_DATA_FILE,
        includeCAPE=False,
        restrictCAPELow=None,
        restrictCAPEHigh=None,
        restrictData=None,
        returnFileInfo=False,
        returnFileOnly=False,
        returnIndices=False,
        shuffleAll=True,
        shuffleSites=True,
        verbose=True):
    """
    Load the data for experiments.
    Splits data into train, validation, and test subsets.
    Assumes all NaNs are removed prior
    """

    if useShannon:
        dirName = MAIN_DATA_DIR
    else:
        dirName = ALT_DATA_DIR

    raob, rap, goes, rtma, files, \
        raob_cape_cin, rap_cape_cin = \
        read_data(dirName, fileName)

    if verbose:
        print("INFO: total data shape -- ")
        print("   GOES: {}, RAOB: {}".format(goes.shape, raob.shape))
        print("   RAP: {}, RTMA: {}".format(rap.shape, rtma.shape))
        if includeCAPE:
            print("   RAOB_CAPE_CIN: {}, RAP_CAPE_CIN: {}".format(
                raob_cape_cin.shape, rap_cape_cin.shape))

    train_i, val_i, test_i = partition_standard_indicies(
        files, shuffleAll=shuffleAll,
        shuffleSites=shuffleSites)
    if verbose:
        print("INFO: partitioned data shape -- ")
        print("   train: {}".format(len(train_i)))
        print("   val: {}".format(len(val_i)))
        print("   test: {}".format(len(test_i)))
    RAPtrain, RAPval, RAPtest = rap[train_i], \
        rap[val_i], rap[test_i]
    RTMAtrain, RTMAval, RTMAtest = rtma[train_i], \
        rtma[val_i], rtma[test_i]
    GOEStrain, GOESval, GOEStest = goes[train_i], \
        goes[val_i], goes[test_i]
    RAOBtrain, RAOBval, RAOBtest = raob[train_i], \
        raob[val_i], raob[test_i]
    FILEtrain, FILEval, FILEtest = files[train_i], \
        files[val_i], files[test_i]

    if includeCAPE:
        if restrictCAPEHigh is not None:
            raob_cape_cin[..., CAPE_INDX] = np.where(
                raob_cape_cin[..., CAPE_INDX] > restrictCAPEHigh,
                restrictCAPEHigh,
                raob_cape_cin[..., CAPE_INDX])
            rap_cape_cin[..., CAPE_INDX] = np.where(
                rap_cape_cin[..., CAPE_INDX] > restrictCAPEHigh,
                restrictCAPEHigh,
                rap_cape_cin[..., CAPE_INDX])

        raob_cape_cin[..., CIN_INDX] *= -1.
        raob_cape_cin = np.where(
            raob_cape_cin < 0., 0.,
            raob_cape_cin)

        rap_cape_cin[..., CIN_INDX] *= -1.
        rap_cape_cin = np.where(
            rap_cape_cin < 0., 0.,
            rap_cape_cin)

        RAOBtrain_cape_cin = raob_cape_cin[train_i]
        RAOBval_cape_cin = raob_cape_cin[val_i]
        RAOBtest_cape_cin = raob_cape_cin[test_i]
        RAPtrain_cape_cin = rap_cape_cin[train_i]
        RAPval_cape_cin = rap_cape_cin[val_i]
        RAPtest_cape_cin = rap_cape_cin[test_i]

    if restrictData is not None:
        refTrain = extract_refs_by_month(
            restrictData[0], restrictData[1], FILEtrain)
        FILEtrain = FILEtrain[refTrain]
        GOEStrain = GOEStrain[refTrain, ...]
        INDXtrain = np.array(train_i)[refTrain]
        RAOBtrain = RAOBtrain[refTrain, ...]
        RAPtrain = RAPtrain[refTrain, ...]
        RTMAtrain = RTMAtrain[refTrain, ...]
        if includeCAPE:
            RAOBtrain_cape_cin = RAOBtrain_cape_cin[refTrain, ...]
            RAPtrain_cape_cin = RAPtrain_cape_cin[refTrain, ...]

        refVal = extract_refs_by_month(
            restrictData[0], restrictData[1], FILEval)
        INDXval = np.array(val_i)[refVal]
        FILEval, GOESval, RAOBval, RAPval, RTMAval = FILEval[
            refVal], GOESval[refVal], RAOBval[refVal], \
            RAPval[refVal], RTMAval[refVal]
        if includeCAPE:
            RAOBval_cape_cin = RAOBval_cape_cin[refVal, ...]
            RAPval_cape_cin = RAPval_cape_cin[refVal, ...]

        refTest = extract_refs_by_month(
            restrictData[0], restrictData[1], FILEtest)
        INDXtest = np.array(test_i)[refTest]
        FILEtest, GOEStest, RAOBtest, RAPtest, RTMAtest = \
            FILEtest[refTest], GOEStest[refTest], RAOBtest[refTest], \
            RAPtest[refTest], RTMAtest[refTest]
        if includeCAPE:
            RAOBtest_cape_cin = RAOBtest_cape_cin[refTest, ...]
            RAPtest_cape_cin = RAPtest_cape_cin[refTest, ...]

        if verbose:
            print("INFO: restricted data shape -- ")
            print("   train: {}".format(RAPtrain.shape[0]))
            print("   val: {}".format(RAPval.shape[0]))
            print("   test: {}".format(RAPtest.shape[0]))

    if includeCAPE and restrictCAPELow is not None:
        CAPEtrain = RAOBtrain_cape_cin[..., CAPE_INDX]
        CAPERtrain = RAPtrain_cape_cin[..., CAPE_INDX]
        refTrain = np.logical_and(
            CAPEtrain >= restrictCAPELow, CAPERtrain >= restrictCAPELow)
        INDXtrain = INDXtrain[refTrain]
        FILEtrain, GOEStrain, RAOBtrain, RAPtrain, RTMAtrain = \
            FILEtrain[refTrain], \
            GOEStrain[refTrain], \
            RAOBtrain[refTrain], \
            RAPtrain[refTrain], RTMAtrain[refTrain]
        RAOBtrain_cape_cin = RAOBtrain_cape_cin[refTrain]
        RAPtrain_cape_cin = RAPtrain_cape_cin[refTrain]

        CAPEval = RAOBval_cape_cin[..., CAPE_INDX]
        CAPERval = RAPval_cape_cin[..., CAPE_INDX]
        refVal = np.logical_and(
            CAPEval >= restrictCAPELow, CAPERval >= restrictCAPELow)
        INDXval = INDXval[refVal]
        FILEval, GOESval, RAOBval, RAPval, RTMAval = \
            FILEval[refVal], \
            GOESval[refVal], \
            RAOBval[refVal], \
            RAPval[refVal], RTMAval[refVal]
        RAOBval_cape_cin = RAOBval_cape_cin[refVal]
        RAPval_cape_cin = RAPval_cape_cin[refVal]

        CAPEtest = RAOBtest_cape_cin[..., CAPE_INDX]
        CAPERtest = RAPtest_cape_cin[..., CAPE_INDX]
        refTest = np.logical_and(
            CAPEtest >= restrictCAPELow, CAPERtest >= restrictCAPELow)
        INDXtest = INDXtest[refTest]
        FILEtest, GOEStest, RAOBtest, RAPtest, RTMAtest = \
            FILEtest[refTest], \
            GOEStest[refTest], \
            RAOBtest[refTest], \
            RAPtest[refTest], RTMAtest[refTest]
        RAOBtest_cape_cin = RAOBtest_cape_cin[refTest]
        RAPtest_cape_cin = RAPtest_cape_cin[refTest]

        if verbose:
            print("INFO: removing low CAPE data shape -- ")
            print("   train: {}".format(RAPtrain.shape[0]))
            print("   val: {}".format(RAPval.shape[0]))
            print("   test: {}".format(RAPtest.shape[0]))

    if returnFileOnly:
        return FILEtrain, FILEval, FILEtest

    if returnIndices:
        return INDXtrain, INDXval, INDXtest

    if includeCAPE and returnFileInfo:
        return (RAPtrain, RAPval, RAPtest,
                RTMAtrain, RTMAval, RTMAtest,
                GOEStrain, GOESval, GOEStest,
                RAOBtrain, RAOBval, RAOBtest,
                FILEtrain, FILEval, FILEtest,
                RAOBtrain_cape_cin, RAOBval_cape_cin, RAOBtest_cape_cin,
                RAPtrain_cape_cin, RAPval_cape_cin, RAPtest_cape_cin)
    elif returnFileInfo:
        return (RAPtrain, RAPval, RAPtest,
                RTMAtrain, RTMAval, RTMAtest,
                GOEStrain, GOESval, GOEStest, RAOBtrain, RAOBval, RAOBtest,
                FILEtrain, FILEval, FILEtest)
    elif includeCAPE:
        return (RAPtrain, RAPval, RAPtest,
                RTMAtrain, RTMAval, RTMAtest,
                GOEStrain, GOESval, GOEStest,
                RAOBtrain, RAOBval, RAOBtest,
                RAOBtrain_cape_cin, RAOBval_cape_cin, RAOBtest_cape_cin,
                RAPtrain_cape_cin, RAPval_cape_cin, RAPtest_cape_cin)
    else:
        return (RAPtrain, RAPval, RAPtest,
                RTMAtrain, RTMAval, RTMAtest,
                GOEStrain, GOESval, GOEStest, RAOBtrain, RAOBval, RAOBtest)


def organize_data(expDict, data, verbose=True):

    input_cape = get_exp_inputCAPE(expDict)
    input_channels_goes = get_exp_inputGOES(expDict)
    input_channels_rap = get_exp_inputRAP(expDict)
    input_channels_rtma = get_exp_inputRTMA(expDict)
    input_extra_goes = get_exp_inputGOESExtra(expDict)
    output_dims_raobs = get_exp_outputRAOB(expDict)
    loss_type = get_exp_loss(expDict)
    vcombine = get_exp_vcombine(expDict)

    if verbose:
        print('INFO: data organization -')
        print("   CAPE/CIN Input: {}".format(input_cape))
        print("   GOES Input: {}".format(input_channels_goes))
        print("   GOES Input Extra: {}".format(input_extra_goes))
        print("   RAP Input: {}".format(input_channels_rap))
        print("   RTMA Input: {}".format(input_channels_rtma))
        print("   RAOB Output: {}".format(output_dims_raobs))
        print("   Vertical Combination: {}".format(vcombine))

    (RAPtrain, RAPval, RAPtest,
     RTMAtrain, RTMAval, RTMAtest,
     GOEStrain, GOESval, GOEStest,
     RAOBtrain, RAOBval, RAOBtest,
     _, _, _,
     RAPCCtrain, RAPCCval, RAPCCtest) = data

    if vcombine:
        if len(vcombine) == 1:
            RAPtr, RAPv, RAPte = combine_layers(
                vcombine[0], RAPtrain, RAPval, RAPtest)
        else:
            RAPtr, RAPv, RAPte = combine_layersA(
                vcombine[0], vcombine[1],
                RAPtrain, RAPval, RAPtest)
    else:
        RAPtr = RAPtrain
        RAPv = RAPval
        RAPte = RAPtest

    Xti, Xvi, Xei = [], [], []
    ia = (1, 2)
    if input_channels_rtma:
        Xti.append(RTMAtrain[..., input_channels_rtma].mean(axis=ia))
        Xvi.append(RTMAval[..., input_channels_rtma].mean(axis=ia))
        Xei.append(RTMAtest[..., input_channels_rtma].mean(axis=ia))
    if input_channels_goes:
        Xti.append(GOEStrain[..., input_channels_goes].mean(axis=ia))
        Xvi.append(GOESval[..., input_channels_goes].mean(axis=ia))
        Xei.append(GOEStest[..., input_channels_goes].mean(axis=ia))
    if input_extra_goes:
        for i in input_extra_goes:
            if i == 0:
                Xti.append(np.std(
                    GOEStrain[..., input_channels_goes], axis=ia))
                Xvi.append(np.std(
                    GOESval[..., input_channels_goes], axis=ia))
                Xei.append(np.std(
                    GOEStest[..., input_channels_goes], axis=ia))
            if i == 1:  # GOES 10 - 8
                t1 = np.expand_dims(
                    GOEStrain[..., 2].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStrain[..., 0].mean(axis=ia), axis=1)
                Xti.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOESval[..., 2].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOESval[..., 0].mean(axis=ia), axis=1)
                Xvi.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOEStest[..., 2].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStest[..., 0].mean(axis=ia), axis=1)
                Xei.append(np.subtract(t1, t2))

            if i == 2:  # GOES 13 - 15
                t1 = np.expand_dims(
                    GOEStrain[..., 4].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStrain[..., 6].mean(axis=ia), axis=1)
                Xti.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOESval[..., 4].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOESval[..., 6].mean(axis=ia), axis=1)
                Xvi.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOEStest[..., 4].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStest[..., 6].mean(axis=ia), axis=1)
                Xei.append(np.subtract(t1, t2))

            if i == 3:  # GOES 14 - 13
                t1 = np.expand_dims(
                    GOEStrain[..., 5].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStrain[..., 4].mean(axis=ia), axis=1)
                Xti.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOESval[..., 5].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOESval[..., 4].mean(axis=ia), axis=1)
                Xvi.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOEStest[..., 5].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStest[..., 4].mean(axis=ia), axis=1)
                Xei.append(np.subtract(t1, t2))

    if input_cape:
        Xti.append(RAPCCtrain[..., input_cape])
        Xvi.append(RAPCCval[..., input_cape])
        Xei.append(RAPCCtest[..., input_cape])

    # train
    Xtr = RAPtr[:, :, input_channels_rap]
    Xti = np.concatenate(Xti, axis=1) if len(Xti) else None
    Tt = RAOBtrain[:, :, output_dims_raobs].reshape(
        RAOBtrain.shape[0], -1)

    # validation
    Xvr = RAPv[:, :, input_channels_rap]
    Xvi = np.concatenate(Xvi, axis=1) if len(Xvi) else None
    Tv = RAOBval[:, :, output_dims_raobs].reshape(
        RAOBval.shape[0], -1)

    # test
    Xer = RAPte[:, :, input_channels_rap]
    Xei = np.concatenate(Xei, axis=1) if len(Xei) else None
    Te = RAOBtest[:, :, output_dims_raobs].reshape(
        RAOBtest.shape[0], -1)

    if loss_type == 'mse_weighted':
        Tt = organize_data_mse_weighted_ps(
            Tt, RAOBtrain[..., RAOB_INDX_PRESSURE])
        Tv = organize_data_mse_weighted_ps(
            Tv, RAOBval[..., RAOB_INDX_PRESSURE])
        Te = organize_data_mse_weighted_ps(
            Te, RAOBtest[..., RAOB_INDX_PRESSURE])

    if verbose:
        Xti_print = Xti.shape if Xti is not None else 'None'
        Xvi_print = Xvi.shape if Xvi is not None else 'None'
        Xei_print = Xei.shape if Xei is not None else 'None'

        print('INFO: data dimensions --')
        print('   Train Shape X: {}, {} and T: {}'.format(
            Xtr.shape, Xti_print, Tt.shape))
        print('   Validation Shape X: {}, {} and T: {}'.format(
            Xvr.shape, Xvi_print, Tv.shape))
        print('   Test Shape X: {}, {} and T: {}'.format(
            Xer.shape, Xei_print, Te.shape))

    return [Xtr, Xti], Tt, [Xvr, Xvi], Tv, [Xer, Xei], Te


def organize_data_mse_weighted_ps(YArray, PSArray,
                                  weights=(0.5, 0.5)):

    YShape = YArray.shape
    nSamples = YShape[0]
    nZ = YShape[1]

    YNew = np.zeros((nSamples, nZ, 2))
    YNew[..., 0] = YArray.copy()

    PSTot = np.sum(PSArray, axis=1)
    PSWeights = np.divide(PSArray, PSTot[:, None])
    nV = PSWeights.shape[1]

    YNew[:, :nV, 1] = np.multiply(PSWeights, weights[0])
    YNew[:, nV:, 1] = np.multiply(PSWeights, weights[1])

    return YNew


def organize_data_y(RAOB):
    YTest = np.concatenate(
        (np.expand_dims(RAOB[..., RAOB_INDX_TEMPERATURE], axis=2),
         np.expand_dims(RAOB[..., RAOB_INDX_DEWPOINT], axis=2)),
        axis=2)
    return YTest


def partition_standard_indicies(
        files,
        percentages=(0.75, 0.10, 0.15),
        shuffleAll=True,
        shuffleSites=False,
        seed=1234):
    """Partition data to have an equal proportion of locations per data set.
    ---
    params:
        percentages: list
            (train, validation, test)
    """
    validateFraction = 0
    if isinstance(percentages, (tuple, list)):
        trainFraction = percentages[0]
        testFraction = percentages[-1]
        if len(percentages) == 3:
            validateFraction = percentages[1]

    elif isinstance(percentages, float):
        trainFraction = percentages
        testFraction = 1 - trainFraction

    else:
        raise TypeError(
            f'percentages {percentages} must be of the following \
                (train, val, test) or 0.8 for train')

    if shuffleSites or shuffleAll:
        np.random.seed(seed)

    content = np.array([f.split('_') for f in files])
    locs = content[:, 0:1]
    mons = pd.to_datetime(
        content[:, 1], format='%Y-%m-%dT%H:%M:%S.%f').\
        month.values.reshape(-1, 1)
    classes = np.unique(locs)
    months = np.unique(mons)

    train_i = []
    val_i = []
    test_i = []

    for c in classes:
        # all indicies for class c
        c_i = np.where(locs == c)[0]
        for m in months:
            # all indicies for month m
            m_i = np.where(mons == m)[0]
            # intersection between month and location indicies
            cm_i = np.intersect1d(c_i, m_i)

            if shuffleSites:  # shuffle indicies
                np.random.shuffle(cm_i)

            # partitioned indicies for class c
            n = len(cm_i)
            nTrain = round(trainFraction * n)
            nValidate = round(validateFraction * n)
            nTest = round(testFraction * n)
            if nTrain + nValidate + nTest > n:
                nTest = n - nTrain - nValidate

            train_i += cm_i[:nTrain].tolist()
            if nValidate > 0:
                val_i += cm_i[nTrain:nTrain + nValidate].tolist()
            test_i += cm_i[nTrain + nValidate:nTrain
                           + nValidate + nTest].tolist()

    if shuffleAll:  # shuffle all indicies
        np.random.shuffle(train_i)
        np.random.shuffle(val_i)
        np.random.shuffle(test_i)

    if validateFraction > 0:
        return train_i, val_i, test_i
    else:
        return train_i, test_i


def pickle_dump(filename, data,
                protocol=pickle.DEFAULT_PROTOCOL,
                verbose=True):
    """
    Pickle file creation to dump variables.
    By default, use the default protocol.
    The other protocol option is pickle.HIGHEST_PROTOCOL
    """

    # check to make sure directory exists
    lRef = filename.rindex('/')
    filedir = filename[:lRef]
    if not os.path.isdir(filedir):
        os.makedirs(filedir)

    with open(filename, "wb") as handle:
        pickle.dump(data, handle, protocol=protocol)
    if verbose:
        print("Created File: {}".format(filename))
    return


def pickle_read(filename):
    """Pickle file read."""

    exists = os.path.isfile(filename)
    if exists:
        with open(filename, "rb") as handle:
            data = pickle.load(handle)
        return data
    else:
        print("PICKLE_READ FILE NOT FOUND: {}".format(filename))
        return None


def print_results(dict1, type='test'):
    keyList = list(dict1.keys())
    if f"ml_{type}_rmse_sfc" in keyList:
        print("   RAP RMSE Tot: {:.3f}, Sfc: {:.3f}".format(
            dict1[f'rap_{type}_rmse'],
            dict1[f'rap_{type}_rmse_sfc']))
        print("   ML RMSE Tot: {:.3f}, Sfc: {:.3f}".format(
            dict1[f'ml_{type}_rmse'],
            dict1[f'ml_{type}_rmse_sfc']))
    if f'mlT_{type}_rmse_sfc' in keyList:
        print("   RAP RMSE T: {:.3f}, Sfc: {:.3f}".format(
            dict1[f'rapT_{type}_rmse'],
            dict1[f'rapT_{type}_rmse_sfc']))
        print("   ML RMSE T: {:.3f}, Sfc: {:.3f}".format(
            dict1[f'mlT_{type}_rmse'],
            dict1[f'mlT_{type}_rmse_sfc']))
    if f'mlTD_{type}_rmse_sfc' in keyList:
        print("   RAP RMSE TD: {:.3f}, Sfc: {:.3f}".format(
            dict1[f'rapTD_{type}_rmse'],
            dict1[f'rapTD_{type}_rmse_sfc']))
        print("   ML RMSE TD: {:.3f}, Sfc: {:.3f}".format(
            dict1[f'mlTD_{type}_rmse'],
            dict1[f'mlTD_{type}_rmse_sfc']))
    if f'mlCAPE_{type}_rmse' in keyList:
        print("   RAP RMSE CAPE: {:.3f}, CIN: {:.3f}".format(
            dict1[f'rapCAPE_{type}_rmse'],
            dict1[f'rapCIN_{type}_rmse']))
        print("   ML RMSE CAPE: {:.3f}, CIN: {:.3f}".format(
            dict1[f'mlCAPE_{type}_rmse'],
            dict1[f'mlCIN_{type}_rmse']))
    return


def read_data(dirName=MAIN_DATA_DIR,
              fileName=MAIN_DATA_FILE):
    ds = Dataset(dirName + fileName)
    nsoundings = ds.dimensions['nsoundings'].size
    nprofile = ds.dimensions['nprofile'].size

    rds = ds['radiosonde']
    filenames = np.empty(nsoundings, dtype='object')
    filenames[:] = rds.variables['file_name'][:]

    raob = np.empty((nsoundings, nprofile, 4))
    raob[..., RAOB_INDX_DEWPOINT] = \
        rds.variables['raob_dewpoint'][...]
    raob[..., RAOB_INDX_HEIGHT] = \
        rds.variables['raob_geopotential_height'][...]
    raob[..., RAOB_INDX_PRESSURE] = \
        rds.variables['raob_pressure'][...]
    if rds['raob_pressure'].units == 'Pa':
        raob[..., RAOB_INDX_PRESSURE] /= 100.
    raob[..., RAOB_INDX_TEMPERATURE] = \
        rds.variables['raob_temperature'][...]

    raob_cape = np.empty((nsoundings, 2))
    raob_cape[..., CAPE_INDX] = \
        rds.variables['raob_cape'][...]
    raob_cape[..., CIN_INDX] = \
        rds.variables['raob_cin'][...]

    rds = ds['rap']
    rap = np.empty((nsoundings, nprofile, 4))
    rap[..., RAP_INDX_DEWPOINT] = \
        rds.variables['rap_dewpoint'][...]
    rap[..., RAP_INDX_HEIGHT] = \
        rds.variables['rap_geopotential_height'][...]
    rap[..., RAP_INDX_PRESSURE] = \
        rds.variables['rap_pressure'][...]
    if rds['rap_pressure'].units == 'Pa':
        rap[..., RAP_INDX_PRESSURE] /= 100.
    rap[..., RAP_INDX_TEMPERATURE] = \
        rds.variables['rap_temperature'][...]

    rap_cape = np.empty((nsoundings, 2))
    rap_cape[..., CAPE_INDX] = \
        rds.variables['rap_cape'][...]
    rap_cape[..., CIN_INDX] = \
        rds.variables['rap_cin'][...]

    rds = ds['goes']
    nbands = rds.dimensions['nbands'].size
    nx = rds.dimensions['nx'].size
    ny = rds.dimensions['ny'].size
    goes = np.empty((nsoundings, nx, ny, nbands))
    goes[..., 0] = \
        rds.variables['goes_band08'][...]
    goes[..., 1] = \
        rds.variables['goes_band09'][...]
    goes[..., 2] = \
        rds.variables['goes_band10'][...]
    goes[..., 3] = \
        rds.variables['goes_band11'][...]
    goes[..., 4] = \
        rds.variables['goes_band13'][...]
    goes[..., 5] = \
        rds.variables['goes_band14'][...]
    goes[..., 6] = \
        rds.variables['goes_band15'][...]
    goes[..., 7] = \
        rds.variables['goes_band16'][...]

    rds = ds['rtma']
    nx = rds.dimensions['nx'].size
    ny = rds.dimensions['ny'].size
    rtma = np.empty((nsoundings, nx, ny, 3))
    rtma[..., RTMA_INDX_DEWPOINT] = \
        rds.variables['rtma_dewpoint'][...]
    rtma[..., RTMA_INDX_PRESSURE] = \
        rds.variables['rtma_pressure'][...]
    if rds['rtma_pressure'].units == 'Pa':
        rtma[..., RTMA_INDX_PRESSURE] /= 100.
    rtma[..., RTMA_INDX_TEMPERATURE] = \
        rds.variables['rtma_temperature'][...]

    return raob, rap, goes, rtma, filenames, raob_cape, rap_cape


def read_indices(
        dirName=ALT_DATA_DIR,
        fileName=MAIN_DATA_FILE,
        seed=1234,
        shuffle=False):
    ds = Dataset(dirName + fileName)
    rds = ds['ml']
    ntest = rds.dimensions['ntest'].size
    test_i = np.empty((ntest))
    test_i[:] = rds.variables['test_indices'][:]

    ntrain = ds.dimensions['ntrain'].size
    train_i = np.empty((ntrain))
    train_i[:] = rds.variables['train_indices'][:]

    nval = ds.dimensions['nvalidate'].size
    val_i = np.empty((nval))
    val_i[:] = rds.variables['val_indices'][:]

    if shuffle:
        np.random.seed(seed)
        np.random.shuffle(train_i)
        np.random.shuffle(val_i)
        np.random.shuffle(test_i)

    return train_i, val_i, test_i
