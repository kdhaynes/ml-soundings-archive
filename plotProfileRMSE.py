"""
ML Soundings Project

Program to plot results.

Modified: 2022/02
"""


# %%
# Import Libraries
import numpy as np

import functions_misc as fm
import functions_plot as fp
from importlib import reload


# %%
# User Info

modelList = ['LIN015', 'NN137', 'UNet585']
modelNameList = ['LIN', 'NN', 'UNet']
modelColorList = [
    'mediumseagreen', 'darkorange', 'darkslateblue']

plotMeanProfileT = True
saveMeanProfileT = True

plotMeanProfileTD = True
saveMeanProfileTD = True

plotMeanProfileSfcT = False
saveMeanProfileSfcT = False

plotMeanProfileSfcTD = False
saveMeanProfileSfcTD = False

useShannon = True
dirFigs = './'
refHigh = 246

shuffleAll = True
shuffleSites = False
restrict_data = [4, 8]


# %%
# Get original data
FILEtrain, FILEval, FILEtest = fm.load_data(
    useShannon,
    restrictData=restrict_data,
    returnFileOnly=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
data = fm.load_data(
    useShannon,
    restrictData=restrict_data,
    returnFileInfo=False,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
GOEStest = data[8]
RAOBtest = data[11]
RAPtest = data[2]
RTMAtest = data[5]
nSamples = RAOBtest.shape[0]

hAvg = fm.compute_average_altitude(
    RAOBtest[:, :, fm.RAOB_INDX_HEIGHT])
hAvg = fm.compute_average_altitude(
    RAOBtest[:, :, fm.RAOB_INDX_PRESSURE])
print("Data Test Shape: {}".format(RAOBtest.shape))


# %%
# Read results
nModels = len(modelList)
rapProfileRMSE = np.empty((2, refHigh))
mlProfileRMSE = np.empty((nModels, 2, refHigh))
countm = 0
for modelRef in modelList:
    fileName = fm.get_exp_filenameResults(
        useShannon, expRef=modelRef)
    rDict = fm.pickle_read(fileName)
    if countm == 0:
        rapProfileRMSE[0, :] = \
            rDict['rapT_test_profile_rmse'][:refHigh]
        rapProfileRMSE[1, :] = \
            rDict['rapTD_test_profile_rmse'][:refHigh]
    mlProfileRMSE[countm, 0, :] = \
        rDict['mlT_test_profile_rmse'][:refHigh]
    mlProfileRMSE[countm, 1, :] = \
        rDict['mlTD_test_profile_rmse'][:refHigh]
    countm += 1
print("Profile RMSE Shape: {}".format(
    mlProfileRMSE.shape))


# %%
# Plot profile of mean errors
reload(fp)
if plotMeanProfileT:
    if saveMeanProfileT:
        fileName = dirFigs + 'profileT'
        for modelRef in modelList:
            fileName += "_{}".format(modelRef)
        fileName += 'rmse.png'
    else:
        fileName = ''
    fp.plot_vertical_rmse_altitudeArrayI(
        mlProfileRMSE[:, 0, :refHigh],
        altitude=hAvg[:refHigh],
        colorList=modelColorList,
        figSize=(4, 7),
        labelList=modelNameList,
        rap=rapProfileRMSE[0, :refHigh],
        saveFile=fileName,
        title='Temperature')


# %%
# Plot profile of mean errors
if plotMeanProfileTD:
    if saveMeanProfileTD:
        fileName = dirFigs + 'profileTD'
        for modelRef in modelList:
            fileName += "_{}".format(modelRef)
        fileName += 'rmse.png'
    else:
        fileName = ''
    fp.plot_vertical_rmse_altitudeArrayI(
        mlProfileRMSE[:, 1, :refHigh],
        altitude=hAvg[:refHigh],
        colorList=modelColorList,
        figSize=(4, 7),
        labelList=modelNameList,
        rap=rapProfileRMSE[1, :refHigh],
        saveFile=fileName,
        title='Dewpoint')


# %%
# Plot Mean Surface RMSE Profile
if plotMeanProfileSfcT:
    if saveMeanProfileSfcT:
        fileName = dirFigs + 'profileSfcT'
        for modelRef in modelList:
            fileName += "_{}".format(modelRef)
        fileName += '.png'
    else:
        fileName = ''
    fp.plot_vertical_rmse_altitudeArray(
        mlProfileRMSE[:, 0, :fm.SFC_ERROR_LEVEL],
        altitude=hAvg[:fm.SFC_ERROR_LEVEL],
        colorList=modelColorList,
        rap=rapProfileRMSE[0, :],
        saveFile=fileName,
        title='Temperature')


# %%
# Plot Mean Surface RMSE Profile
if plotMeanProfileSfcTD:
    if saveMeanProfileSfcTD:
        fileName = dirFigs + 'profileSfcTD'
        for modelRef in modelList:
            fileName += "_{}".format(modelRef)
        fileName += '.png'
    else:
        fileName = ''
    fp.plot_vertical_rmse_altitudeArray(
        mlProfileRMSE[:, 1, :fm.SFC_ERROR_LEVEL],
        altitude=hAvg[:fm.SFC_ERROR_LEVEL],
        colorList=modelColorList,
        rap=rapProfileRMSE[1, :],
        saveFile=fileName,
        title='Dewpoint')
