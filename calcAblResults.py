# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import numpy as np


# %%
# User Info

rList_ABL_LIN = [
    ['LIN062', 'LIN063', 'LIN064', 'LIN065'],
    ['LIN066', 'LIN067', 'LIN068', 'LIN069'],
    ['LIN070', 'LIN071', 'LIN072', 'LIN073'],
    ['LIN086', 'LIN087', 'LIN088', 'LIN089'],
    ['LIN082', 'LIN083', 'LIN084', 'LIN015'],
    ['LIN074', 'LIN075', 'LIN076', 'LIN077'],
    ['LIN078', 'LIN079', 'LIN080', 'LIN081']]
rList_ABL_NN = [
    ['NN162', 'NN163', 'NN164', 'NN165'],
    ['NN166', 'NN167', 'NN168', 'NN169'],
    ['NN186', 'NN187', 'NN188', 'NN189'],
    ['NN170', 'NN171', 'NN172', 'NN173'],
    ['NN182', 'NN183', 'NN184', 'NN185'],
    ['NN174', 'NN175', 'NN176', 'NN177'],
    ['NN178', 'NN179', 'NN180', 'NN181'],
    ['NN194', 'NN195', 'NN196', 'NN137']]
rList_ABL_UNet = [
    ['UNet562', 'UNet563', 'UNet564', 'UNet565'],
    ['UNet566', 'UNet567', 'UNet568', 'UNet569'],
    ['UNet582', 'UNet583', 'UNet584', 'UNet585'],
    ['UNet574', 'UNet575', 'UNet576', 'UNet577'],
    ['UNet586', 'UNet587', 'UNet588', 'UNet589'],
    ['UNet570', 'UNet571', 'UNet572', 'UNet573'],
    ['UNet590', 'UNet591', 'UNet592', 'UNet593']]
rList = rList_ABL_LIN + rList_ABL_NN \
    + rList_ABL_UNet

saveBarPlot = True
figDir = '/home/kdhaynes/ml_soundings/figs.temp/'
useShannon = True


# %%
# Open results, see which is best per model type
tCount = np.zeros((4), dtype='int')
tSfcCount = np.zeros((4), dtype='int')
tdCount = np.zeros((4), dtype='int')
tdSfcCount = np.zeros((4), dtype='int')
capeCount = np.zeros((4), dtype='int')
cinCount = np.zeros((4), dtype='int')
nTests = len(rList)
for ms in rList:
    bestT = np.inf
    indexT = -1
    bestTSfc = np.inf
    indexTSfc = -1
    bestTD = np.inf
    indexTD = -1
    bestTDSfc = np.inf
    indexTDSfc = -1
    bestCAPE = np.inf
    indexCAPE = -1
    bestCIN = np.inf
    indexCIN = -1
    for i in range(4):
        expName = ms[i]
        resultFile = fm.get_exp_filenameResults(
            useShannon,
            expRef=expName)
        rDictT = fm.pickle_read(resultFile)
        keyList = list(rDictT.keys())

        tAll = rDictT['mlT_test_rmse']
        if tAll < bestT:
            bestT = tAll
            indexT = i

        tSfc = rDictT['mlT_test_rmse_sfc']
        if tSfc < bestTSfc:
            bestTSfc = tSfc
            indexTSfc = i

        tdAll = rDictT['mlT_test_rmse']
        if tdAll < bestTD:
            bestTD = tdAll
            indexTD = i

        tdSfc = rDictT['mlTD_test_rmse_sfc']
        if tdSfc < bestTDSfc:
            bestTDSfc = tdSfc
            indexTDSfc = i

        cape = rDictT['mlCAPE_test_rmse']
        if cape < bestCAPE:
            bestCAPE = cape
            indexCAPE = i

        cin = rDictT['mlCIN_test_rmse']
        if cin < bestCIN:
            bestCIN = cin
            indexCIN = i

    tCount[indexT] += 1
    tSfcCount[indexTSfc] += 1
    tdCount[indexTD] += 1
    tdSfcCount[indexTDSfc] += 1
    capeCount[indexCAPE] += 1
    cinCount[indexCIN] += 1


# %%
# Print Results:
print(f"T Counts: {tCount}")
print(f"TSfc Counts: {tSfcCount}")
print(f"TD Counts: {tdCount}")
print(f"TDSfc Counts: {tdSfcCount}")
print(f"CAPE Counts: {capeCount}")
print(f"CIN Counts: {cinCount}")


# %%
# Create bar plot
ncols = 6
nbars = 4
results = np.zeros((nbars, ncols))
results[:, 0] = tSfcCount[:]
results[:, 1] = tCount[:]
results[:, 2] = tdSfcCount[:]
results[:, 3] = tdCount[:]
results[:, 4] = capeCount[:]
results[:, 5] = cinCount[:]

myCols = ['T Sfc', 'T', 'TD Sfc', 'TD',
          'CAPE', 'CIN']
myBars = ['RAP', 'RAP + R',
          'RAP + G', 'RAP + R + G']
if saveBarPlot:
    saveFile = figDir + 'ablCount.png'
else:
    saveFile = ''
fp.plot_bars(myCols, results, myBars,
             colorList=fp.QUANT_ANTIQUE_LIST4,
             figSize=(6, 3),
             legendPos='lower left',
             saveFile=saveFile,
             ylabel='Count',
             xticksize=13,
             width=0.15)
