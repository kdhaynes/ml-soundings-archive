"""
ML Soundings Project

Program to plot second derivatives
in vertical profiles.

expList = ['LIN015', 'NN107',
           'UNet585', 'UNet569',
           'UNet587', 'UNet593']

Modified: 2022/11
"""


# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import numpy as np
from importlib import reload


# %%
# User Specifications
indxRAOB = 2   # 0=Pressure, 1=Temperature,
# 2=Dewpoint, 3=Height

expList = ['LIN015', 'NN137',
           'UNet585', 'UNet569',
           'UNet515', 'UNet527']
expNameList = ['LIN CRPS', 'NN SHASH',
               'UNet MAEW', 'UNet MAES',
               'UNet CRPS', 'UNet SHASH']
expColorList = ['mediumseagreen',
                'darkorange',
                'darkslateblue', 'darkslateblue',
                'mediumslateblue', 'mediumslateblue']
plotBars = False
saveBars = False

plotBox = True
saveBox = False

labelList = ['RAOB', 'RAP']
nwpColorList = ['black', 'deeppink']
nwpVarList = ['Pressure', 'Temperature',
              'Dewpoint', 'Height']
restrictData = [4, 8]
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Load Data
(RAPtr, RAPv, RAPte,
 _, _, _, _, _, _,
 RAOBtr, RAOBv, RAOBte,
 FILEtr, FILEv, FILEte) = fm.load_data(
    useShannon,
    includeCAPE=False,
    restrictData=restrictData,
    returnFileInfo=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
RAOB = RAOBte[..., indxRAOB]
RAP = RAPte[..., indxRAOB]
FILE = FILEte
nSamples = RAOB.shape[0]
nLevs = RAOB.shape[1]
varName = nwpVarList[indxRAOB]

print("Read in {} data:".format(varName))
print("    RAOB shape: {}".format(RAOB.shape))
print("    RAP shape:  {}".format(RAP.shape))


# %%
# Load ML Results
nML = len(expList)
ML = np.empty((nML, nSamples, nLevs))

count = 0
for refName in expList:
    predFile = fm.get_exp_filenamePreds(
        useShannon, None, expRef=refName)
    pDict = fm.pickle_read(predFile)
    ML[count, :, :] = pDict['PREDtest'][..., indxRAOB - 1]
    labelList.append(expNameList[count])
    count += 1
print("Read in ML {} data.".format(varName))
print("   ML shape: {}".format(ML.shape))


# %%
# Calculate Second Derivatives (DDXDV)
dRAOB = np.empty((nLevs - 1))
ddRAOB = np.zeros((nSamples, nLevs - 2))
dRAP = np.empty((nLevs - 1))
ddRAP = np.zeros((nSamples, nLevs - 2))
dML = np.empty((nML, nLevs - 1))
ddML = np.zeros((nML, nSamples, nLevs - 2))

for s in range(nSamples):
    for i in range(nLevs - 1):
        dRAOB[i] = RAOB[s, i + 1] - RAOB[s, i]
        dRAP[i] = RAP[s, i + 1] - RAP[s, i]
        for m in range(nML):
            dML[m, i] = ML[m, s, i + 1] - ML[m, s, i]

    for i in range(nLevs - 2):
        ddRAOB[s, i] = dRAOB[i + 1] - dRAOB[i]
        ddRAP[s, i] = dRAP[i + 1] - dRAP[i]
        for m in range(nML):
            ddML[m, s, i] = dML[m, i + 1] - dML[m, i]

dsRAOB = np.sum(np.abs(ddRAOB), axis=-1) / (ddRAOB.shape[-1])
dsRAP = np.sum(np.abs(ddRAP), axis=-1) / ddRAP.shape[-1]
dsML = np.sum(np.abs(ddML), axis=-1) / ddML.shape[-1]

print("Second Derivatives (ddx/dv)")
print("   RAOB Mean= {:.4f}, Min= {:.4f}, Max={:.4f}".format(
    dsRAOB.mean(), dsRAOB.min(), dsRAOB.max()))
print("   RAP Mean=  {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
    dsRAP.mean(), dsRAP.min(), dsRAP.max()))
for m in range(nML):
    print("   {} Mean= {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
        expList[m], dsML[m, :].mean(), dsML[m, :].min(), dsML[m, :].max()))


# %%
# Plot DDXDV Barplot
if plotBars:
    raobList = [dsRAOB.mean(), dsRAOB.min(), dsRAOB.max()]
    rapList = [dsRAP.mean(), dsRAP.min(), dsRAP.max()]
    plotList = [raobList, rapList]
    for m in range(nML):
        mlList = [dsML[m, :].mean(), dsML[m, :].min(),
                  dsML[m, :].max()]
        plotList.append(mlList)

    if saveBars:
        saveFile = 'ddxdv'
        for m in range(nML):
            saveFile += '_{}'.format(expList[m])
        saveFile += '.png'
    else:
        saveFile = ''
    reload(fp)
    fp.plot_anoms(labelList, plotList,
                  legendLoc='upper left',
                  saveFile=saveFile,
                  title='Profile Second Derivatives')


# %%
# Plot DDXDV Boxplot
if plotBox:
    reload(fp)
    dataList = [dsRAOB, dsRAP]
    colorList = nwpColorList
    for m in range(nML):
        dataList.append(dsML[m, :])
        colorList.append(expColorList[m])

    myTitle = nwpVarList[indxRAOB] + (' (second derivative)')
    if 'Temp' in myTitle:
        myy = '$\dfrac{d^2T}{dp^2}$'
        yLim = [0.0, 0.28]
        myshort = 't'
    elif 'Dew' in myTitle:
        myy = '$\dfrac{d^2TD}{dp^2}$'
        yLim = [0.0, 1]
        myshort = 'td'
    else:
        myy = '$\dfrac{d^2y}{dx^2}$'
        yLim = None
        myshort = 'o'

    if saveBox:
        saveFile = 'ddxdv_' + myshort + '.png'
    else:
        saveFile = ''
    fp.plot_box(dataList,
                colorList=colorList,
                plotWhiskers=False,
                saveFile=saveFile,
                sizeLabels=14,
                title=myTitle,
                xNames=labelList, xRotate=45,
                units='$\degree C$ $mb^{-2}$',
                yLabel=myy,
                yLim=yLim)
