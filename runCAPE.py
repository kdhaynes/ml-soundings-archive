"""
ML Soundings Project

Program to try predicting CAPE and CIN
directly using a Random Forest or NN.

Modified: 2023/09
"""


# %%
# Import Libraries
import functions_cape as fc
import functions_misc as fm

from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from time import time

from var_opts import exp801 as expDict


# %%
# Set User Options
subsetData = False
nSubset = -1
nSubsetFeatures = 2

savePredictions = True

calcResults = True
saveResults = True

shuffleAll = True
shuffleSites = False
useShannon = True
verbose = True


# %%
# Load Data
expName = fm.get_exp_refname(expDict)
print('\nINFO: Running experiment {}'.format(expName))

restrictData = fm.get_exp_restrict_data(expDict)
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrictData,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
(RAPtrain, RAPval, RAPtest,
 RTMAtrain, RTMAval, RTMAtest,
 GOEStrain, GOESval, GOEStest,
 RAOBtrain, RAOBval, RAOBtest,
 RAOBtrain_cape_cin, RAOBval_cape_cin,
 RAOBtest_cape_cin,
 RAPtrain_cape_cin, RAPval_cape_cin,
 RAPtest_cape_cin) = data


# %%
# Organize data
XtrainAll, YtrainAll, XvalAll, YvalAll, \
    XtestAll, YtestAll = fc.organize_data_cape(
        expDict, data)
nTrainAll = XtrainAll.shape[0]
nTestAll = XtestAll.shape[0]
nFeaturesAll = XtrainAll.shape[1]
nTargetsAll = YtrainAll.shape[1]


# %%
# Subset data
if subsetData:
    XTrain, YTrain = fc.subset_data(
        nSubset, nSubsetFeatures,
        XtrainAll, YtrainAll)
    XVal, YVal = fc.subset_data(
        nSubset, nSubsetFeatures,
        XvalAll, YvalAll)
    XTest, YTest, RAPTest = fc.subset_data(
        nSubset, nSubsetFeatures,
        XtestAll, YtestAll,
        RAP=RAPtest_cape_cin)

else:
    XTrain = XtrainAll
    XVal = XvalAll
    XTest = XtestAll
    RAPTest = RAPtest_cape_cin

    YTrain = YtrainAll
    YVal = YvalAll
    YTest = YtestAll
nTrain = XTrain.shape[0]
nVal = XVal.shape[0]
nTest = XTest.shape[0]
nFeatures = XTrain.shape[1]
nTargets = YTrain.shape[1]
print("Setup with {} train, {} validation, and {} test samples.".
      format(nTrain, nVal, nTest))


# %%
# Setup model
network_name = fm.get_exp_networkname(expDict)
if network_name == 'RandomForest':
    timeStart = time()
    modelOpts = expDict['modelOpts']
    model = RandomForestRegressor(
        bootstrap=modelOpts['bootstrap'],
        ccp_alpha=modelOpts['ccp_alpha'],
        criterion=modelOpts['criterion'],
        max_depth=modelOpts['max_depth'],
        max_features=modelOpts['max_features'],
        max_leaf_nodes=modelOpts['max_leaf_nodes'],
        max_samples=modelOpts['max_samples'],
        min_impurity_decrease=modelOpts['min_impurity_decrease'],
        min_samples_leaf=modelOpts['min_samples_leaf'],
        min_samples_split=modelOpts['min_samples_split'],
        min_weight_fraction_leaf=modelOpts['min_weight_fraction_leaf'],
        n_estimators=modelOpts['n_estimators']
    )
    print("{} Model Parameters: {}".format(
        network_name, model.get_params()))

    # ...train
    if YTrain.shape[1] == 1:
        YTrain = YTrain.reshape(-1)
    model.fit(XTrain, YTrain)
    print("Trained {} model ({:.2f} s)".format(
        network_name, time() - timeStart))

    # ...create predictions
    PTest = model.predict(XTest)
    print("Created predictions (shape = {})".
          format(PTest.shape))
    if YTest.shape[1] == 1:
        YTest = YTest.reshape(-1)
    print("   R2 Score: {:.3f}".format(
        model.score(XTest, YTest)))

    r2RAP = r2_score(RAOBtest_cape_cin,
                     RAPtest_cape_cin)
    print("   Baseline R2 Score: {:.3f}".format(r2RAP))

else:
    print(f"Unknown Method: {network_name}")


# %%
# Save predictions
if savePredictions:
    pDict = {
        'PREDtest': PTest,
        'PREDstd': None,
        'PREDmc': None}
    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    fm.pickle_dump(filePreds, pDict,
                   verbose=False)
    print("INFO: predictions saved --")
    print("  {}".format(filePreds))


# %%
# Calculate results
if calcResults:
    output = fm.get_exp_outputCAPE(expDict)
    if len(output) == 1:
        if output[0] == 0:
            results = fc.calculate_results_cape(
                YTest, PTest)
        else:
            results = fc.calculate_results_cin(
                YTest, PTest)
    else:
        results = fc.calculate_results_cape_cin(
            YTest, PTest)

    resultsRAP = fc.calculate_results_cape_cin(
        RAOBtest_cape_cin, RAPTest, model='rap')
    print("CAPE/CIN MAE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_cin_test_mae'],
                 resultsRAP['rap_cape_cin_test_mae']))
    print("        RMSE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_cin_test_rmse'],
                 resultsRAP['rap_cape_cin_test_rmse']))
    print("CAPE MAE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_test_mae'],
                 resultsRAP['rap_cape_test_mae']))
    print("    RMSE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_test_rmse'],
                 resultsRAP['rap_cape_test_rmse']))
    print("CIN MAE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cin_test_mae'],
                 resultsRAP['rap_cin_test_mae']))
    print("   RMSE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cin_test_rmse'],
                 resultsRAP['rap_cin_test_rmse']))


# %%
# Save results
if saveResults:
    fileResults = fm.get_exp_filenameResults(
        useShannon, expDict)
    outDict = {
        'resultsML': results,
        'resultsRAP': resultsRAP}
    fm.pickle_dump(fileResults, outDict, verbose=False)
    print('INFO: results saved --')
    print('  {}'.format(fileResults))
