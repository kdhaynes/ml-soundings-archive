"""
ML Soundings Project

Program to show results from various
model comparison metrics.

For dewpoint:
spread_bins = [0.0, 1.5, 3.0, 4.5,
               6.0, 7.5, 9., 10.5,
               12, 13.5, 15., 20.]
spread_last = 17.5

For temperature:
spread_bins = [0.4, 0.6, 0.8, 1.0,
               1.4, 1.8, 10]
spread_last = 2

Modified: 2023/09
"""

# %%
# Import Libraries
import functions_misc as fm

from functions_loss import create_sample_norm_tfp
from functions_metrics import mae
from importlib import reload
from IPython.display import display

from numpy import moveaxis
from pandas import DataFrame
from properscoring import crps_ensemble

import var_opts as vo


# %%
# User Options
expList = [vo.exp015, vo.exp137,
           vo.exp515, vo.exp527]
labelList = ['LIN', 'NN', 'UNet CRPS', 'UNet SHASH']

refLow = 0
refHigh = 246

nEns = 60
targetNum = 0

spread_bins = [0.4, 0.6, 0.8, 1.0,
               1.4, 1.8, 10]
spread_last = 2

targetNames = ['Temperature',
               'Dewpoint']
targetLabels = ['t', 'td']

restrict_data = [4, 8]
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Read Data
(RAPtr, RAPv, RAPte,
 RTMAtr, RTMAv, RTMAte,
 GOEStr, GOESv, GOESte,
 RAOBtr, RAOBv, RAOBte,
 FILEtr, FILEv, FILEte) = fm.load_data(
     useShannon,
    restrictData=restrict_data,
    returnFileInfo=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
YTrain = fm.organize_data_y(RAOBtr)
YTest = fm.organize_data_y(RAOBte)
YTrue = YTest[:, refLow:refHigh, targetNum].reshape(-1)
print("Read in data.")
print("   YTest Shape: {}".format(YTest.shape))
print("   YTrue Shape: {}".format(YTrue.shape))


# %%
# Read Predictions and Calculate Metrics
evalDict = {}

reload(fm)
iExp = 0
for expDict in expList:
    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    pDict = fm.pickle_read(filePreds)
    pDictKeys = list(pDict.keys())
    PTest_mean = pDict['PREDtest']
    PMean = PTest_mean[:, refLow:refHigh, targetNum].reshape(-1)

    PTest_std = pDict['PREDstd']
    PStd = PTest_std[:, refLow:refHigh, targetNum].reshape(-1)

    if 'PREDmc' in pDictKeys:
        PTest_ensa = pDict['PREDmc']
        PEns = PTest_ensa[:, refLow:refHigh,
                          targetNum, :]
        PEns = PEns.reshape((-1, PEns.shape[-1]))

    else:
        print("Calculating Ensemble Members!")
        PTest_ens = create_sample_norm_tfp(
            PTest_mean[:, refLow:refHigh, targetNum].reshape(-1),
            PTest_std[:, refLow:refHigh, targetNum].reshape(-1),
            nSamples=nEns)
        PTest_ens = moveaxis(PTest_ens, 0, -1)
        PEns = PTest_ens.reshape((-1, nEns))

        PTest_ensa = create_sample_norm_tfp(
            PTest_mean, PTest_std, nSamples=nEns)
        PTest_ensa = moveaxis(PTest_ensa, 0, -1)

    # crps1 = crps_score(YTrue, PEns)
    # crps2 = crps_score(YTrue, PMean)
    crpsE1 = crps_ensemble(YTrue, PEns)
    # crpsE2 = crps_ensemble(YTrue, PMean)

    attrDict = fm.get_attributes_diagram_points(
        YTest, PTest_mean, y_train=YTrain)
    msess = attrDict['attr_msess'][targetNum]
    mrmse = attrDict['attr_rmse'][targetNum]
    mmae = mae(YTrue, PMean)

    ssDict = fm.get_spread_skill_points_uq(
        YTest[:, refLow:refHigh, targetNum],
        PTest_mean[:, refLow:refHigh, targetNum],
        PTest_std[:, refLow:refHigh, targetNum],
        bins=spread_bins,
        spread_last=spread_last,
        verbose=False)
    ssRatio = ssDict['ss_ratio']
    ssReliability = ssDict['ss_reliability']

    dDict = fm.get_discard_points(
        YTest[:, refLow:refHigh, :],
        PTest_mean[:, refLow:refHigh, :],
        PTest_std[:, refLow:refHigh, :])
    dmf = dDict['discard_mf'][targetNum]
    dimprv = dDict['discard_imprv'][targetNum]

    # pDict2 = kfe.get_pit_points(
    #    YTest[:, refLow:refHigh, :],
    #    PTest_mean[:, refLow:refHigh, :],
    #    PTest_std[:, refLow:refHigh, :])
    # pitd2 = pDict2['pit_dvalue'][targetNum]

    pDict = fm.get_pit_points_ens(
        YTest[:, refLow:refHigh, :],
        PTest_ensa[:, refLow:refHigh, :, :])
    pitd = pDict['pit_dvalue'][targetNum]

    if 'PDP' in labelList[iExp]:
        mignorance = fm.calculate_ignorance(
            YTest[:, refLow:refHigh, targetNum],
            PTest_mean[:, refLow:refHigh, targetNum],
            PTest_std[:, refLow:refHigh, targetNum])
    else:
        mignorance = fm.calculate_ignorance_ens(
            YTest[:, refLow:refHigh, targetNum],
            PTest_ensa[:, refLow:refHigh, targetNum, :])

    mscores = {}
    mscores['RMSE'] = mrmse
    mscores['MAE'] = mmae
    mscores['CRPS'] = crpsE1.mean()
    mscores['MSESS'] = msess
    mscores['SSRAT'] = ssRatio
    mscores['SSREL'] = ssReliability
    mscores['MF'] = dmf
    mscores['DI'] = dimprv
    mscores['PITD'] = pitd
    mscores['IGN'] = mignorance

    evalDict[labelList[iExp]] = mscores
    iExp += 1

evalDF = DataFrame(evalDict)
display(evalDF.reindex(list(mscores.keys())))
