"""
ML Soundings Project

Program to find reference number for a
specific case.

Modified: 2023/09
"""


# %%
# Import Libraries
import functions_misc as fm


# %%
# User Options
findCaseStr = 'maf_2019-06-20'

restrictCAPELow = None
restrictCAPEHigh = None
restrictData = [4, 8]
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Read Data
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictCAPELow=restrictCAPELow,
    restrictCAPEHigh=restrictCAPEHigh,
    restrictData=restrictData,
    returnFileInfo=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
FILEtest = data[-7]
print("Read True Data.")


# %%
# Find index number for case
indx = 0
for str in FILEtest:
    if findCaseStr in str:
        print("Found! Position: {}, Full Name: {}".format(
            indx, str))
        indxCase = indx
    indx += 1
