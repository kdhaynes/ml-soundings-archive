# ML Soundings Archive

This is the code to reproduce the results in the *Artificial Intelligence for the Earth Systems (AIES)* article:

Exploring the Use of Machine Learning to Improve Vertical Profiles of Temperature and Moisture

K. Haynes, J. Stock, J. Dostalek, C. Anderson, and I. Ebert-Uphoff



## Code Description

The code consists of Python scripts, which can be run interactively using an editor or run via the command line.  The directory is structured so that the Python filenames indicate the functionality of the routines:
- Calc/Check: Routines to calculate or check results
- Functions: Modules to hold various helper definitions and functions
- Plot: Routines to create the plots in the publication
- Run: Routines to run the ML models
- Var: Module holding the input options for the different test runs.

To run the results, the directories of the data and output will need to be changed in the functions_misc.py file.  The associated variables to change are the MAIN_DATA_DIR and MAIN_RESULT_DIR.

## Associated data
The data to run all of the routines is available on Dryad at https://doi.org/10.5061/dryad.h70rxwdqn.

## License
This code is released under the terms of a Creative Commons Zero (CCO) waiver (https://creativecommons.org/public-domain/cc0/).

## Funding
This project was funded by NOAA grant NA19OAR4320073.

## Citation
Haynes, K., Stock, J., Dostalek, J., Anderson, C., and Ebert-Uphoff, I. (2023).  Code Archive for Exploring the Use of Machine Learning to Improve Vertical Profiles of Temperature and Moisture.  Zenodo.  https://doi.org/10.5281/zenodo.10041772

## Contact
Katherine Haynes
katherine.haynes@colostate.edu
25 October, 2023



