"""
ML Soundings Project

Program to plot results.

NOTE:
This program uses the mean results saved
during the simulation,
it does not recalculate any values
nor include any individual cases.

LIN005 - CRPS
LIN001 - MSE (much lower....)

BEST NN:
NN150 - CRPS
NN189 - MAEW

BEST UNet:
585 - MAEW
587 - CRPS

Modified: 2023/09
"""


# %%
# Import libraries
import functions_misc as fm
from importlib import reload


# %%
# Set user input options
rList_ABL_LIN = [
    'LIN062', 'LIN063', 'LIN064', 'LIN065',
    'LIN066', 'LIN067', 'LIN068', 'LIN069',
    'LIN070', 'LIN071', 'LIN072', 'LIN073',
    'LIN086', 'LIN087', 'LIN088', 'LIN089',
    'LIN082', 'LIN083', 'LIN084', 'LIN085',
    'LIN074', 'LIN075', 'LIN076', 'LIN077',
    'LIN078', 'LIN079', 'LIN080', 'LIN081']
rList_LOSS_LIN = [
    'LIN001', 'LIN002', 'LIN003', 'LIN004', 'LIN005',
    'LIN006', 'LIN007', 'LIN008']
rList_MISC_LIN = ['LIN015', 'LIN055', 'LIN058',
                  'LIN093']
rList_LIN = rList_ABL_LIN + rList_LOSS_LIN \
    + rList_MISC_LIN

rList_ABL_NN = [
    'NN162', 'NN163', 'NN164', 'NN165',
    'NN166', 'NN167', 'NN168', 'NN169',
    'NN186', 'NN187', 'NN188', 'NN189',
    'NN170', 'NN171', 'NN172', 'NN173',
    'NN182', 'NN183', 'NN184', 'NN185',
    'NN174', 'NN175', 'NN176', 'NN177',
    'NN178', 'NN179', 'NN180', 'NN181',
    'NN194', 'NN195', 'NN196', 'NN137']
rList_LOSS_NN = [
    'NN101', 'NN102', 'NN103', 'NN104', 'NN105',
    'NN106', 'NN107', 'NN108']
rList_MISC_NN = [
    'NN117', 'NN137', 'NN140',
    'NN150', 'NN151', 'NN157',
    'NN204']
rList_NN = rList_ABL_NN + rList_LOSS_NN \
    + rList_MISC_NN

rList_ABL_UNet = [
    'UNet562', 'UNet563', 'UNet564', 'UNet565',
    'UNet566', 'UNet567', 'UNet568', 'UNet569',
    'UNet582', 'UNet583', 'UNet584', 'UNet585',
    'UNet574', 'UNet575', 'UNet576', 'UNet577',
    'UNet586', 'UNet587', 'UNet588', 'UNet589',
    'UNet570', 'UNet571', 'UNet572', 'UNet573',
    'UNet590', 'UNet591', 'UNet592', 'UNet593']
rList_LOSS_UNet = [
    'UNet501', 'UNet502', 'UNet503', 'UNet504', 'UNet505',
    'UNet506', 'UNet507', 'UNet508']
rList_MISC_UNet = [
    'UNet515', 'UNet517',
    'UNet525', 'UNet527',
    'UNet531', 'UNet602', 'UNet603',
    'UNet605', 'UNet613', 'UNet615',
    'UNet628', 'UNet634', 'UNet635', 'UNet636',
    'UNet637', 'UNet640', 'UNet641', 'UNet642',
    'UNet685']
rList_UNet = rList_ABL_UNet + rList_LOSS_UNet \
    + rList_MISC_UNet

rList_ALL = rList_LIN + rList_NN \
    + rList_UNet

# -------------------------
rList = rList_ALL

printResults = True
sortResults = True
sortName = 'mlTD_sfc'  # 'mlTD_sfc'

showImprove = True
sortImprove = True
sortNameImprove = '%_T'

printImprove = True

useShannon = True
missing = None


# %%
# Read results files
rDict = {}
for expName in rList:
    resultFile = fm.get_exp_filenameResults(
        useShannon,
        expRef=expName)
    rDictT = fm.pickle_read(resultFile)
    if rDictT is None:
        continue

    keyList = list(rDictT.keys())
    if 'rapCAPE_test_rmse' not in keyList:
        rDictT['rapCAPE_test_rmse'] = missing
    if 'rapCIN_test_rmse' not in keyList:
        rDictT['rapCIN_test_rmse'] = missing
    if 'mlCAPE_test_rmse' not in keyList:
        rDictT['mlCAPE_test_rmse'] = missing
    if 'mlCIN_test_rmse' not in keyList:
        rDictT['mlCIN_test_rmse'] = missing
    rDict[expName] = rDictT


# %%
# Display Results
if printResults:
    reload(fm)
    dfShow = fm.df_results_show(
        rDict, num_to_show=30,
        sort=sortResults, sortName=sortName)
    expBestList = fm.get_best_models(rDict)
    expBest = fm.get_best_model(rDict, verbose=True)
    print("Overall best model: {}".format(expBest))


# %%
# Display Improvements
if showImprove:
    reload(fm)
    dfShow = fm.df_results_show_improve(
        rDict, num_to_show=30,
        sort=sortResults,
        sortName=sortNameImprove)


# %%
# Print improved overall model
if printImprove:
    reload(fm)
    expImprove = fm.get_improved_model(
        rDict, verbose=False)
    print("Most improved model: {}".format(expImprove))
    expImpList, expImpScore = fm.get_improved_model(
        rDict, returnScoreList=True, verbose=False)
    print("Top 10 Model List: ")
    myi = 10
    if expImpList.shape[0] < myi:
        myi = expImpList.shape[0]
    for i in range(myi):
        print(f"  {expImpList[i]}   {expImpScore[i]:.3f}")
