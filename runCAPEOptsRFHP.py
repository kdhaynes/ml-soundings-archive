"""
ML Soundings Project

Program to try predicting CAPE and CIN
directly using a Random Forest or NN.

Modified: 2023/09
"""


# %%
# Import Libraries
import functions_cape as fc
import functions_hp as fhp
import functions_misc as fm

from hyperopt import hp
from hyperopt import Trials


from sklearn.metrics import r2_score

from var_opts import exp801 as expDict


# %%
# Set User Options

shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Hyperopt Options
doSearch = True
modelSummary = True
nEvals = 20
saveFreq = 10
saveFDir = './'

spaceHP = {
    'bootstrap': hp.choice(
        'forest_weight', [True, False]),
    'ccp_alpha': hp.choice(
        'forest_alpha', [0.0, 0.001, 0.01]),
    'criterion': hp.choice(
        'forest_criterion',
        ['squared_error', 'absolute_error',
         'friedman_mse', 'poisson']),
    'max_depth': hp.choice(
        'forest_depth', [None, 5, 10, 20, 50]),
    'max_features': hp.choice(
        'forest_features', [None, "auto", "sqrt", "log2"]),
    'n_estimators': hp.choice(
        'forest_estimators', range(10, 200))}


# %%
# Load Data
expName = fm.get_exp_refname(expDict)
print('\nINFO: Running experiment {}'.format(expName))

mOpts = expDict['modelOpts']
restrictData = fm.get_exp_restrict_data(expDict)
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrictData,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
(RAPtrain, RAPval, RAPtest,
 RTMAtrain, RTMAval, RTMAtest,
 GOEStrain, GOESval, GOEStest,
 RAOBtrain, RAOBval, RAOBtest,
 RAOBtrain_cape_cin, RAOBval_cape_cin,
 RAOBtest_cape_cin,
 RAPtrain_cape_cin, RAPval_cape_cin,
 RAPtest_cape_cin) = data


# %%
# Organize data
Xtr, Ytr, Xval, Yval, _, _ = fc.organize_data_cape(
    expDict, data)
nTrainAll = Xtr.shape[0]
nFeaturesAll = Xtr.shape[1]
nTargetsAll = Ytr.shape[1]
rapR2 = r2_score(Yval, RAPval_cape_cin)


# %%
# Search Using HyperOpt TPE
if doSearch:
    cList = []
    accList = []
    paramsList = []

    trialsHP = Trials()
    bestLoss = float("inf")
    bestParams = {}

    # set count
    countHP = len(trialsHP)

    # set save directory
    bestLoss, bestParams = fhp.hp_run_trials(
        fhp.hp_rff,
        Xtr, Ytr, Xval, Yval,
        countHP,
        spaceHP,
        trialsHP,
        bestLoss,
        bestParams,
        nEvals=nEvals,
        saveFreq=saveFreq,
        saveFDir=saveFDir,
        summary=modelSummary)

    print("Finished Running HyperOpt Search!\n")


# %%
# Print results

print("\nBest Results: ")
print(f"Best Loss: {bestLoss:.3f}")
print(f"Best Params: {bestParams}")
