"""
ML Soundings Project

Program to compare model performances.
Uses two scores, 1. - Normalized RMSE,
and an improvement (RAP - Pred)/RAP.

Modified: 2023/07
"""


# %%
# Import Libraries
import numpy as np

from functions_plot import image_mscores_inv
from functions_plot import plot_bars_horiz
from functions_misc import get_exp_filenameResults
from functions_misc import get_improved_model
from functions_misc import pickle_read


# %%
# User Options
dirFigs = '/home/kdhaynes/ml_soundings/figs.temp/'

modelList = ['RAP',
             'LIN001', 'LIN002', 'LIN003',
             'LIN004', 'LIN015', 'LIN006',
             'LIN007',
             'NN101', 'NN102', 'NN103',
             'NN104', 'NN105', 'NN106',
             'NN137',
             'UNet501', 'UNet502', 'UNet503',
             'UNet504', 'UNet505', 'UNet506',
             'UNet527']
modelNames = [
    'RAP',
    'LIN MSE', 'LIN MAES', 'LIN MAEW',
    'LIN MSEW', 'LIN CRPS', 'LIN NORM',
    'LIN SHASH',
    'NN MSE', 'NN MAES', 'NN MAEW',
    'NN MSEW', 'NN CRPS', 'NN NORM',
    'NN SHASH',
    'UNet MSE', 'UNet MAES', 'UNet MAEW',
    'UNet MSEW', 'UNet CRPS', 'UNet NORM',
    'UNet SHASH']
keyList = ['mlT_test_rmse_sfc', 'mlTD_test_rmse_sfc',
           'mlT_test_rmse', 'mlTD_test_rmse',
           'mlCAPE_test_rmse', 'mlCIN_test_rmse']
keyListRAP = ['rapT_test_rmse_sfc', 'rapTD_test_rmse_sfc',
              'rapT_test_rmse', 'rapTD_test_rmse',
              'rapCAPE_test_rmse', 'rapCIN_test_rmse']
keyNames = ['SFC T', 'SFC TD', 'T', 'TD', 'CAPE', 'CIN']

saveStr = 'loss'
sortStr = 'best'

barTots = True
saveTots = False

plotImage = True
saveImage = False

useShannon = True


# %%
# Load results
nModels = len(modelList)
nKeys = len(keyList)
resultArray = np.empty((nKeys, nModels))

countM = 0
rDict = {}
fileName = get_exp_filenameResults(
    useShannon, None, expRef=modelList[1])
rDictT = pickle_read(fileName)
for i in range(len(keyList)):
    rmh = keyListRAP[i]
    mlh = keyList[i]
    resultArray[i, countM] = rDictT[rmh]
    rDictT[mlh] = rDictT[rmh]
rDict[modelList[0]] = rDictT
countM += 1

for modelRef in modelList[1:]:
    fileName = get_exp_filenameResults(
        useShannon, None, expRef=modelRef)
    rDictT = pickle_read(fileName)
    rDict[modelRef] = rDictT

    countK = 0
    for key in keyList:
        resultArray[countK, countM] = rDictT[key]
        countK += 1

    countM += 1


# %%
# Normalize
resultANorm = resultArray.copy()
for i in range(nKeys):
    keyMax = np.max(resultArray[i, :])
    keyMin = np.min(resultArray[i, :])
    resultANorm[i, :] = (resultArray[i, :] - keyMin) / (keyMax - keyMin)
resultANorm = 1. - resultANorm


# %%
# Get Most Improved and Best Model Scores
if barTots:
    improvedScores = get_improved_model(
        rDict,
        returnScoreUnsorted=True,
        verbose=False,
        verboseScore=False)
    improvedScores = np.array(improvedScores)
    bestScores = np.sum(resultANorm, axis=0)

    if sortStr == 'best':
        iSorts = np.argsort(bestScores)[::-1]
    elif sortStr == 'imp':
        iSorts = np.argsort(improvedScores)[::-1]
    elif sortStr == 'avg':
        sumScores = np.add(bestScores, improvedScores)
        iSorts = np.argsort(sumScores)[::-1]
    else:
        print("Unrecognized sort string....")
        iSorts = np.argsort(bestScores)[::-1]
    improvedScoresSort = improvedScores[iSorts]
    bestScoresSort = bestScores[iSorts]
    modelNamesSort = np.array(modelNames)[iSorts]

    scoresA = np.concatenate(
        (np.expand_dims(bestScoresSort, axis=0),
         np.expand_dims(improvedScoresSort, axis=0)), axis=0)
    barNames = ['Total Norm RMSE Score', 'Total Improved Score']
    barNames = ['Total NRS', 'Total NIS']
    colorNames = ['navy', 'yellowgreen']

    if saveTots:
        if saveStr:
            saveFile = dirFigs + 'modelCompareTotScores_' + saveStr + '.png'
        else:
            saveFile = dirFigs + 'modelCompareTotScores.png'
    else:
        saveFile = ''
    # plot_bars(modelNamesSort, scoresA, barNames,
    #          colorList=colorNames,
    #          figSize=(14, 6),
    #          rotation=90, saveFile=saveFile,
    #          xticksize=20, ylabel='Score')
    plot_bars_horiz(modelNamesSort, scoresA, barNames,
                    colorList=colorNames,
                    saveFile=saveFile,
                    xlabel='Score')


# %%
# Create plot
if plotImage:
    if saveImage:
        if saveStr:
            saveFile = dirFigs + 'modelCompareScores_' \
                + saveStr + '.png'
        else:
            saveFile = dirFigs + 'modelCompareScores.png'
    else:
        saveFile = ''
    # image_mscores(resultANorm,
    #              figSize=(20, 10),
    #              fontSize=20,
    #              pad=0.35,
    #              rotateX=90,
    #              saveFile=saveFile,
    #              valsX=modelNames, valsY=keyNames)
    image_mscores_inv(resultANorm,
                      figSize=(4, 8),
                      rotateX=65,
                      saveFile=saveFile,
                      valsX=keyNames,
                      valsY=modelNames)
