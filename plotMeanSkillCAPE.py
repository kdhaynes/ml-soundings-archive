"""
ML Soundings Project

Program to plot results.

Modified: 2023/09
"""


# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import numpy as np

from functions_metrics import rmse
from importlib import reload


# %%
# User Info

modelList = ['LIN015', 'NN137', 'UNet585']
modelNameList = ['LIN', 'NN', 'UNet']
modelColorList = ['mediumseagreen', 'darkorange',
                  'mediumslateblue']

vlevStart = 0
vlevStop = 246

plotMeanSkillT = True
saveMeanSkillT = False

plotImprvT = True
saveImprvT = False

plotMeanSkillTD = True
saveMeanSkillTD = False

plotImprvTD = True
saveImprvTD = False

plotProfile = False
saveProfile = False
plotProfileBin = -4

printCases = False

restrict_data = [4, 8]
CAPE_BINS = [
    0, 100, 200, 300, 400, 500, 600, 700, 800,
    900, 1000, 1100, 1200, 1300, 1400, 1500,
    1600, 1700, 1800, 1900, 2000, 2200, 2600, 5000]
CAPE_BINS = [
    10, 50, 90, 140, 200, 260, 320, 380,
    440, 550, 650, 750, 850,
    950, 1050, 1150, 1250, 1350, 1450, 1550,
    1700, 2000, 2400, 2800, 5000]
CAPE_BINS_LAST = 3200

dirFigs = '/home/kdhaynes/ml_soundings/figs.temp/'
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Get original data
if vlevStop < 30:
    suffix = 'Sfc.png'
else:
    suffix = '.png'
FILEtrain, FILEval, FILEtest = fm.load_data(
    useShannon,
    restrictData=restrict_data,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    returnFileOnly=True)
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrict_data,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    returnFileInfo=False)
GOEStest = data[8]
RAOBtest = data[11]
RAOBCCtest = data[14]
RAOBCAPE = RAOBCCtest[:, 0]
RAOBP = RAOBtest[:, vlevStart:vlevStop,
                 fm.RAOB_INDX_PRESSURE]
RAOBT = RAOBtest[:, vlevStart:vlevStop,
                 fm.RAOB_INDX_TEMPERATURE]
RAOBTD = RAOBtest[:, vlevStart:vlevStop,
                  fm.RAOB_INDX_DEWPOINT]

RAPtest = data[2]
RAPP = RAPtest[:, vlevStart:vlevStop,
               fm.RAP_INDX_PRESSURE]
RAPT = RAPtest[:, vlevStart:vlevStop,
               fm.RAP_INDX_TEMPERATURE]
RAPTD = RAPtest[:, vlevStart:vlevStop,
                fm.RAP_INDX_DEWPOINT]
RTMAtest = data[5]
nSamples = RAOBT.shape[0]
nLevs = RAOBT.shape[1]


# %%
# Read predictions
reload(fm)
nModels = len(modelList)
predsT = np.zeros((nModels, nSamples, nLevs)) - 999.
predsTD = np.zeros((nModels, nSamples, nLevs)) - 999.
countm = 0
for modelRef in modelList:
    fileName = fm.get_exp_filenamePreds(
        useShannon, None, expRef=modelRef)
    pDict = fm.pickle_read(fileName)
    predsHere = pDict['PREDtest']
    predsT[countm, :, :] = predsHere[:, vlevStart:vlevStop, 0]
    predsTD[countm, :, :] = predsHere[:, vlevStart:vlevStop, 1]
    countm += 1


# %%
# Calculate skill per CAPE bin
nBins = len(CAPE_BINS) - 1
binCenters = fm.get_centers(CAPE_BINS)
binCenters[-1] = CAPE_BINS_LAST
binCounts = np.empty((nBins))
refSave = np.empty((nSamples, nBins), dtype=bool)

raobMeanT = np.empty((nBins))
raobMeanTD = np.empty((nBins))
rapMeanT = np.empty((nBins))
rapMeanTD = np.empty((nBins))
rapRMSET = np.empty((nBins))
rapRMSETD = np.empty((nBins))
mlMeanT = np.empty((nModels, nBins))
mlMeanTD = np.empty((nModels, nBins))
mlRMSET = np.empty((nModels, nBins))
mlRMSETD = np.empty((nModels, nBins))
nmlImproveT = np.zeros((nModels, nBins))
nmlDegradeT = np.zeros((nModels, nBins))
nmlImproveTD = np.zeros((nModels, nBins))
nmlDegradeTD = np.zeros((nModels, nBins))
for i in range(nBins):
    refs = np.logical_and(
        RAOBCAPE >= CAPE_BINS[i],
        RAOBCAPE < CAPE_BINS[i + 1])
    refSave[:, i] = refs
    RAOBTH = RAOBT[refs, :]
    RAOBTDH = RAOBTD[refs, :]
    RAPTH = RAPT[refs, :]
    RAPTDH = RAPTD[refs, :]
    MLTH = predsT[:, refs, :]
    MLTDH = predsTD[:, refs, :]

    nCases = np.count_nonzero(refs)
    binCounts[i] = nCases
    raobMeanT[i] = RAOBTH.mean()
    raobMeanTD[i] = RAOBTDH.mean()
    rapMeanT[i] = RAPTH.mean()
    rapMeanTD[i] = RAPTDH.mean()
    rapRMSET[i] = rmse(RAOBTH, RAPTH)
    rapRMSETD[i] = rmse(RAOBTDH, RAPTDH)

    for mm in range(nModels):
        mlMeanT[mm, i] = MLTH[mm, ...].mean()
        mlMeanTD[mm, i] = MLTDH[mm, ...].mean()
        mlRMSET[mm, i] = rmse(RAOBTH, MLTH[mm, ...])
        mlRMSETD[mm, i] = rmse(RAOBTDH, MLTDH[mm, ...])

        for ic in range(nCases):
            rapRMSE = rmse(RAOBTH[ic, :], RAPTH[ic, :])
            mlRMSE = rmse(RAOBTH[ic, :], MLTH[mm, ic, :])
            if rapRMSE > mlRMSE:
                nmlImproveT[mm, i] += 1
            else:
                nmlDegradeT[mm, i] += 1

            rapRMSE = rmse(RAOBTDH[ic, :], RAPTDH[ic, :])
            mlRMSE = rmse(RAOBTDH[ic, :], MLTDH[mm, ic, :])
            if rapRMSE > mlRMSE:
                nmlImproveTD[mm, i] += 1
            else:
                nmlDegradeTD[mm, i] += 1
        if (nmlImproveTD[mm, i] + nmlDegradeTD[mm, i]) != nCases:
            print("Problem Deciding if Case Improves!")
nmlImprvFracT = nmlImproveT / binCounts
nmlImprvFracTD = nmlImproveTD / binCounts
nmlImprvTotT = np.sum(nmlImproveT, axis=1) / np.sum(binCounts)
nmlImprvTotTD = np.sum(nmlImproveTD, axis=1) / np.sum(binCounts)


# %%
# Plot mean skill for temperature
if plotMeanSkillT:
    if saveMeanSkillT:
        saveFile = dirFigs + 'meanSkillCAPE_T' + suffix
    else:
        saveFile = ''

    myLabelX = 'RAOB CAPE [$J kg^{-1}$]'
    myTitle = 'Temperature'

    predSkills = [rapRMSET]
    predColorList = ['deeppink']
    predLabelList = ['RAP']
    for i in range(nModels):
        predSkills.append(mlRMSET[i, ...])
        predColorList.append(modelColorList[i])
        predLabelList.append(modelNameList[i])

    fp.plot_mean_skills_cape(
        binCenters, predSkills,
        binCounts=binCounts,
        colorList=predColorList,
        labelx=myLabelX,
        predLabelList=predLabelList,
        saveFile=saveFile,
        title='Temperature',
        units='$\degree C$')


# %%
# Plot improvement for temperature
if plotImprvT:
    reload(fp)
    nmlTList = []
    nmlLList = []
    for i in range(nModels):
        nmlTList.append(nmlImprvFracT[i, :])
        myLabel = modelNameList[i] \
            + ' ({:.2f})'.format(nmlImprvTotT[i])
        nmlLList.append(myLabel)

    if saveImprvT:
        saveFile = dirFigs \
            + 'meanSkillCAPE_TImprv' + suffix
    else:
        saveFile = ''
    myLabelX = 'RAOB CAPE [$J kg^{-1}$]'
    if vlevStop < 30:
        myTitle = 'T Sfc'
    else:
        myTitle = 'T'
    fp.plot_improvement_bars(
        binCenters, nmlTList,
        colorList=modelColorList,
        figSize=(6, 3),
        labelx=myLabelX,
        legendLoc='upper right',
        legendnCol=3,
        predLabelList=nmlLList,
        saveFile=saveFile,
        title=myTitle,
        ylim=(0.3, 1.))


# %%
# Plot mean skill for dewpoint
if plotMeanSkillTD:
    reload(fp)
    if saveMeanSkillTD:
        saveFile = dirFigs \
            + 'meanSkillCAPE_TD' + suffix
    else:
        saveFile = ''

    myLabelX = 'RAOB CAPE [$J kg^{-1}$]'
    myTitle = 'Dewpoint'

    predImprove = []
    predSkills = [rapRMSETD]
    predColorList = ['deeppink']
    predLabelList = ['RAP']
    for i in range(nModels):
        predImprove.append(nmlImprvFracTD[i, :])
        predSkills.append(mlRMSETD[i, ...])
        predColorList.append(modelColorList[i])
        predLabelList.append(modelNameList[i])

    fp.plot_mean_skills_cape(
        binCenters, predSkills,
        binCounts=binCounts,
        colorList=predColorList,
        labelx=myLabelX,
        legendShow=False,
        # predImprove=nmlImprvFracTD[-1, :],
        predLabelList=predLabelList,
        saveFile=saveFile,
        title=myTitle,
        units='$\degree C$')


# %%
# Plot improvement for dewpoint
if plotImprvTD:
    reload(fp)
    nmlTDList = []
    nmlLList = []
    for i in range(nModels):
        nmlTDList.append(nmlImprvFracTD[i, :])
        myLabel = modelNameList[i] \
            + ' ({:.2f})'.format(nmlImprvTotTD[i])
        nmlLList.append(myLabel)
    if saveImprvTD:
        saveFile = dirFigs \
            + 'meanSkillCAPE_TDImprv' + suffix
    else:
        saveFile = ''
    myLabelX = 'RAOB CAPE [$J kg^{-1}$]'
    if vlevStop < 30:
        myTitle = 'TD Sfc'
    else:
        myTitle = 'TD'
    fp.plot_improvement_bars(
        binCenters, nmlTDList,
        colorList=modelColorList,
        figSize=(6, 3),
        labelx=myLabelX,
        legendLoc='lower right',
        legendnCol=3,
        predLabelList=nmlLList,
        saveFile=saveFile,
        title=myTitle,
        ylim=(0.3, 1.))


# %%
# Plot Sounding For Specific Bin
if plotProfile:
    myRefs = refSave[:, plotProfileBin]
    print("Plotting Bin Center: {}".format(binCenters[plotProfileBin]))
    print("    nSamples: {}".format(np.count_nonzero(myRefs)))

    sRAOBP = RAOBP[myRefs, :]
    sRAOBT = RAOBT[myRefs, :]
    sRAOBTD = RAOBTD[myRefs, :]
    sDict = {}
    sDict['RAOB'] = {
        fm.PRESSURE_COLUMN_KEY: sRAOBP.mean(axis=0),
        fm.TEMPERATURE_COLUMN_KEY: sRAOBT.mean(axis=0),
        fm.DEWPOINT_COLUMN_KEY: sRAOBTD.mean(axis=0),
        'color': 'black'}

    sRAPP = RAPP[myRefs, :]
    sRAPT = RAPT[myRefs, :]
    sRAPTD = RAPTD[myRefs, :]
    sDict['RAP'] = {
        fm.PRESSURE_COLUMN_KEY: sRAPP.mean(axis=0),
        fm.TEMPERATURE_COLUMN_KEY: sRAPT.mean(axis=0),
        fm.DEWPOINT_COLUMN_KEY: sRAPTD.mean(axis=0),
        'color': 'deeppink'}
    print("  RAP RMSE T={:.3f} TD={:.3f}".format(
        rmse(sRAOBT, sRAPT), rmse(sRAOBTD, sRAPTD)))

    count = 0
    for mName in modelNameList:
        sMLT = predsT[count, myRefs, :]
        sMLTD = predsTD[count, myRefs, :]
        sDict[mName] = {
            fm.PRESSURE_COLUMN_KEY: sRAPP.mean(axis=0),
            fm.TEMPERATURE_COLUMN_KEY: sMLT.mean(axis=0),
            fm.DEWPOINT_COLUMN_KEY: sMLTD.mean(axis=0),
            'color': modelColorList[count]}

        print("  {} RMSE T={:.3f}, TD={:.3f}".format(
            mName, rmse(sRAOBT, sMLT), rmse(sRAOBTD, sMLTD)))
        count += 1

    fp.plot_sounding_results_dict(sDict)


# %%
if printCases:
    myRefs = refSave[:, plotProfileBin]
    nCases = np.count_nonzero(myRefs)
    print("Bin Center: {}".format(binCenters[plotProfileBin]))
    print("   {} Cases:".format(np.count_nonzero(myRefs)))
    filesHere = FILEtest[myRefs]
    print("    {}".format(filesHere))

    rmseWorstT = 0
    rmseWorstTRef = 0
    rmseWorstTD = 0
    rmseWorstTDRef = 0

    myRAOBT = RAOBT[myRefs, :]
    myRAOBTD = RAOBTD[myRefs, :]
    myMLT = predsT[-1, myRefs, :]
    myMLTD = predsTD[-1, myRefs, :]
    for i in range(nCases):
        rmseTH = rmse(myRAOBT[i, :], myMLT[i, :])
        if rmseTH > rmseWorstT:
            rmseWorstT = rmseTH
            rmseWorstTRef = i

        rmseTDH = rmse(myRAOBTD[i, :], myMLTD[i, :])
        if rmseTDH > rmseWorstTD:
            rmseWorstTD = rmseTDH
            rmseWorstTDRef = i

    print("Worse T Case: {}, RMSE: {}".format(
        filesHere[rmseWorstTRef].upper(),
        rmseWorstT))

    print("Worse TD Case: {}, RMSE: {}".format(
        filesHere[rmseWorstTDRef].upper(),
        rmseWorstTD))
