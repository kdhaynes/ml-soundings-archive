"""
ML Soundings Project

Program to show results from RAP.

Modified: 2023/09
"""

# %%
# Import Libraries
import functions_misc as fm
import properscoring

from functions_metrics import mae, rmse


# %%
# User Options

refLow = 0
refHigh = 246

targetNames = ['Temperature',
               'Dewpoint']
targetLabels = ['t', 'td']

restrict_data = [4, 8]
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Read Data
(RAPtr, RAPv, RAPte,
 RTMAtr, RTMAv, RTMAte,
 GOEStr, GOESv, GOESte,
 RAOBtr, RAOBv, RAOBte,
 FILEtr, FILEv, FILEte) = fm.load_data(
     useShannon,
    restrictData=restrict_data,
    returnFileInfo=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
YTrain = fm.organize_data_y(RAOBtr)
YTest = fm.organize_data_y(RAOBte)
YTrueT = YTest[:, refLow:refHigh, 0].reshape(-1)
YTrueTD = YTest[:, refLow:refHigh, 1].reshape(-1)
print("Read in data.")
print("   YTest Shape: {}".format(YTest.shape))
print("   YTrue Shape: {}".format(YTrueT.shape))


# %%
# Read Predictions and Calculate Metrics
PTest_mean = RAPte[:, :,
                   [fm.RAP_INDX_TEMPERATURE,
                    fm.RAP_INDX_DEWPOINT]]
PTestT = RAPte[:, refLow:refHigh, fm.RAP_INDX_TEMPERATURE].reshape(-1)
PTestTD = RAPte[:, refLow:refHigh, fm.RAP_INDX_DEWPOINT].reshape(-1)


# %%
# RMSE
rmseRAPT = rmse(YTrueT, PTestT)
print(f"RAP T RMSE: {rmseRAPT:.4f}")
rmseRAPTD = rmse(YTrueTD, PTestTD)
print(f"RAP TD RMSE: {rmseRAPTD:.4f}")


# %%
# MAE
maeRAPT = mae(YTrueT, PTestT)
print(f"RAP T MAE: {maeRAPT:.4f}")
maeRAPTD = mae(YTrueTD, PTestTD)
print(f"RAP TD MAE: {maeRAPTD:.4f}")


# %%
# MSESS
attrDict = fm.get_attributes_diagram_points(
    YTest, PTest_mean, y_train=YTrain)
msessT = attrDict['attr_msess'][0]
msessTD = attrDict['attr_msess'][1]
print(f"RAP T MSESS: {msessT:.4f}")
print(f"RAP TD MSESS: {msessTD:.4f}")


# %%
# Calculate CRPS Gaussian
crpsRAPT = properscoring.crps_ensemble(
    YTrueT.reshape((YTrueT.shape[0], 1)),
    PTestT.reshape((PTestT.shape[0], 1))).mean()
crpsRAPTD = properscoring.crps_ensemble(
    YTrueTD.reshape((YTrueTD.shape[0], 1)),
    PTestTD.reshape((PTestT.shape[0], 1))).mean()
print(f"RAP T CRPS: {crpsRAPT:.4f}")
print(f"RAP TD CRPS: {crpsRAPTD:.4f}")
