"""
ML Soundings Project

Program to plot the sounding profiles.

For the UNet profile/surface:
Best T: 684/2392
Best TD: 147/1392
Improved T: 1989/1174
Improved TD: 592/1162

For the LIN015 profile/surface:
Best T: 684/1190
Best TD: 147/272
Improved T: 1610/329
Improved TD: 1316/1140

Best/Improved in paper:
2392, 1392, 1174, 1162

Possibles:
2286, 1246, 1468, 2461
1297, 2495, 2530, 1762

ML Does Bad:
558 - DDC_2020-05-13 (Dodge City, KA)
No reports
https://www.spc.noaa.gov/climo/reports/200513_rpts.html

ML Does OK:
1762 - SGF_2019-06-21
Wind and hail reports throughout the region, but no tornados
https://www.spc.noaa.gov/climo/reports/190621_rpts.gif

ML Does Good:
2216 - FWD_2020-04-28
Hail, but no tornado
https://www.spc.noaa.gov/climo/reports/200427_rpts.html

1404 - SHV_2020-04-12
Tornados reported
https://www.spc.noaa.gov/climo/reports/200412_rpts.html

Modified: 2022/09
"""


# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import numpy as np
from functions_metrics import rmse
from importlib import reload


# %%
# User Options

showML = True
expList = ['LIN015', 'NN137', 'UNet515']
nameList = ['LIN', 'NN', 'UNet']
colorList = ['mediumseagreen', 'darkorange',
             'mediumslateblue']

showRAP = True
colorRAP = 'deeppink'
colorRAOB = 'black'

plotProfileNum = 1174
showCAPE = False
temp_min = -30
temp_max = 45

plotSounding = True
saveSounding = True

showResultsCAPE = True

showLegend = True
refLow = 0
refHigh = 246

restrictCAPELow = None
restrictCAPEHigh = None
restrictData = [4, 8]
shuffleAll = True
shuffleSites = False
targetNames = ['Temperature', 'Dewpoint']
useShannon = True


# %%
# Read Data
(RAPtr, RAPv, RAPte,
 RTMAtr, RTMAv, RTMAte,
 GOEStr, GOESv, GOESte,
 RAOBtr, RAOBv, RAOBte,
 FILEtr, FILEv, FILEte,
 RAOBCCtr, RAOBCCv, RAOBCCte,
 RAPCCtr, RAPCCv, RAPCCte) = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictCAPELow=restrictCAPELow,
    restrictCAPEHigh=restrictCAPEHigh,
    restrictData=restrictData,
    returnFileInfo=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
print("Read True Data.")


# %%
# Open Predictions
nML = len(expList)
nSamples = RAOBte.shape[0]
nLevs = RAOBte.shape[1]
MLte = np.empty((nML, nSamples, nLevs, 2))

count = 0
rDict = {}
for refName in expList:
    rFile = fm.get_exp_filenameResults(
        useShannon, None, expRef=refName)
    rDictTemp = fm.pickle_read(rFile)
    rDict[refName] = rDictTemp

    predFile = fm.get_exp_filenamePreds(
        useShannon, None, expRef=refName)
    pDict = fm.pickle_read(predFile)
    MLte[count, ...] = pDict['PREDtest']
    count += 1
print("Read in ML data.")
print("   ML shape: {}".format(MLte.shape))


# %%
# Plot sounding
if plotSounding:
    reload(fp)
    indx = plotProfileNum
    sDict = {}
    sDict['RAOB'] = {
        fm.PRESSURE_COLUMN_KEY:
        RAOBte[indx, :, fm.RAOB_INDX_PRESSURE],
        fm.TEMPERATURE_COLUMN_KEY:
        RAOBte[indx, :, fm.RAOB_INDX_TEMPERATURE],
        fm.DEWPOINT_COLUMN_KEY:
        RAOBte[indx, :, fm.RAOB_INDX_DEWPOINT],
        'color': colorRAOB}

    RAOBHere = RAOBte[indx, :refHigh, 1:3]
    RAPHere = RAPte[indx, :refHigh, 1:3]
    rapRMSE = rmse(RAOBHere, RAPHere)
    rapTRMSE = rmse(RAOBHere[:, 0], RAPHere[:, 0])
    rapTDRMSE = rmse(RAOBHere[:, 1], RAPHere[:, 1])
    print('RAP T RMSE: {:.4f}, SFC: {:.4f}'.format(
        rapTRMSE,
        rmse(RAOBHere[:25, 0], RAPHere[:25, 0])))
    print('RAP TD RMSE: {:.4f}, SFC: {:.4f}'.format(
        rapTDRMSE,
        rmse(RAOBHere[:25, 1], RAPHere[:25, 1])))

    if showRAP:
        sDict[f'RAP ({rapRMSE:.2f})'] = {
            fm.PRESSURE_COLUMN_KEY:
            RAPte[indx, :, fm.RAP_INDX_PRESSURE],
            fm.TEMPERATURE_COLUMN_KEY:
            RAPte[indx, :, fm.RAP_INDX_TEMPERATURE],
            fm.DEWPOINT_COLUMN_KEY:
            RAPte[indx, :, fm.RAP_INDX_DEWPOINT],
            'color': colorRAP}

    if showML:
        for i in range(nML):
            mlRMSE = rmse(RAOBHere, MLte[i, indx, :refHigh, :])
            myKey = nameList[i] \
                + f" ({mlRMSE:.2f})"
            sDict[myKey] = {
                fm.PRESSURE_COLUMN_KEY:
                RAPte[indx, :, fm.RAP_INDX_PRESSURE],
                fm.TEMPERATURE_COLUMN_KEY: MLte[i, indx, :, 0],
                fm.DEWPOINT_COLUMN_KEY: MLte[i, indx, :, 1],
                'color': colorList[i]}
            print("{} T RMSE: {:.4f}, SFC: {:.4f}".format(
                nameList[i],
                rmse(RAOBHere[:, 0], MLte[i, indx, :refHigh, 0]),
                rmse(RAOBHere[:25, 0], MLte[i, indx, :25, 0])))
            print("{} TD RMSE: {:.4f}, SFC: {:.4f}".format(
                nameList[i],
                rmse(RAOBHere[:, 1], MLte[i, indx, :refHigh, 1]),
                rmse(RAOBHere[:25, 1], MLte[i, indx, :25, 1])))

    if saveSounding:
        saveFile = 'sounding_{}.png'.format(plotProfileNum)
    else:
        saveFile = None
    title = FILEte[indx].upper()
    title = title[:title.index(':')]
    fp.plot_sounding_results_dict(
        sDict,
        cape_cin=showCAPE,
        temp_min=temp_min,
        temp_max=temp_max,
        file_name=saveFile,
        show_legend=showLegend,
        title_string=title)


# %%
# Show CAPE results
if showResultsCAPE:
    obsCAPE = RAOBCCte[indx, 0]
    rapCAPE = RAPCCte[indx, 0]
    rapErrCAPE = np.abs(RAOBCCte[indx, 0] - RAPCCte[indx, 0])

    obsCIN = RAOBCCte[indx, 1]
    rapCIN = RAPCCte[indx, 1]
    rapErrCIN = np.abs(RAOBCCte[indx, 1] - RAPCCte[indx, 1])

    for key in list(rDict.keys()):
        myDict = rDict[key]
        mlCAPE = myDict['mlCAPE_test'][plotProfileNum]
        mlErrCAPE = \
            myDict['mlCAPE_test_case_abs_err'][plotProfileNum]
        mlCIN = myDict['mlCIN_test'][plotProfileNum]
        mlErrCIN = myDict['mlCIN_test_case_abs_err'][plotProfileNum]

        print("\nCAPE OBS= {:.3f}, RAP= {:.3f}, {}= {:.3f}".
              format(obsCAPE, rapCAPE, key, mlCAPE))
        print("     RAP RMSE: {:.3f}, {} RMSE: {:.3f}".format(
            rapErrCAPE, key, mlErrCAPE))

        print(" CIN OBS= {:.3f}, RAP= {:.3f}, {}= {:.3f}".
              format(
                  obsCIN, rapCIN, key, -1 * mlCIN))
        print("     RAP ERR: {:.3f}, {} ERR: {:.3f}".format(
            rapErrCIN, key, mlErrCIN))
