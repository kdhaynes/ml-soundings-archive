"""
ML Soundings Project

Program to perform
Fourier transform on
sounding profiles.

Modified: 2022/09
"""


# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import numpy as np
from importlib import reload


# %%
# User Specifications

indxRAOB = 1
# 0=Pressure, 1=Temperature, # 2=Dewpoint, 3=Height
sampleRAOB = 723  # Sample number to try

expList = ['LIN015', 'NN137', 'UNet585']
expNameList = ['LIN CRPS', 'NN SHASH', 'UNet MAEW']
expColorList = ['mediumseagreen',
                'darkorange',
                'mediumslateblue']
plotFourierProfile = False
saveFourierProfile = False

plotFourier = False
saveFourier = False

levsAutoCorr = 40
plotAutoCorr = False
saveAutoCorr = False
plotAutoCorrProfile = False
saveAutoCorrProfile = False

plotDXDV = False
saveDXDV = False

plotDXDVProfile = True
saveDXDVProfile = False
myAbs = True

plotBestLines = False
saveBestLines = False

plotSplines = False
saveSplines = False

plotSplineAnoms = False
saveSplineAnoms = False

plotSplineAnomsDD = False
saveSplineAnomsDD = False

plotProfileDiffs = False
saveProfileDiffs = False

plotFFTDiffs = False
saveFFTDiffs = False

plotDDXDV = False
saveDDXDV = False

restrictData = [4, 8]
useShannon = True
shuffleAll = True
shuffleSites = False

labelList = ['RAOB', 'RAP']
varNameList = ['Pressure', 'Temperature',
               'Dewpoint', 'Height']


# %%
# Load Data
(RAPtr, RAPv, RAPte,
 _, _, _, _, _, _,
 RAOBtr, RAOBv, RAOBte,
 FILEtr, FILEv, FILEte) = fm.load_data(
    useShannon,
    includeCAPE=False,
    restrictData=restrictData,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    returnFileInfo=True)
RAOB = RAOBte[..., indxRAOB]
RAP = RAPte[..., indxRAOB]
FILE = FILEte
nSamples = RAOB.shape[0]
nLevs = RAOB.shape[1]
varName = varNameList[indxRAOB]

print("Read in {} data:".format(varName))
print("    RAOB shape: {}".format(RAOB.shape))
print("    RAP shape:  {}".format(RAP.shape))


# %%
# Load ML Results
nML = len(expList)
ML = np.empty((nML, nSamples, nLevs))

count = 0
for refName in expList:
    predFile = fm.get_exp_filenamePreds(
        useShannon, None, expRef=refName)
    pDict = fm.pickle_read(predFile)
    ML[count, :, :] = pDict['PREDtest'][..., indxRAOB - 1]
    labelList.append(
        expNameList[count])
    count += 1
print("Read in ML {} data.".format(varName))
print("   ML shape: {}".format(ML.shape))


# %%
# Pull Out Profiles
RAOBProfile = RAOB[sampleRAOB, :]
RAPProfile = RAP[sampleRAOB, :]
MLProfile = ML[:, sampleRAOB, :]
nameProfile = FILE[sampleRAOB]
nameProfile = nameProfile[0:nameProfile.index(":")]
print("Pulled Out Profile: {}".format(nameProfile))


# %%
# Perform Profile Fourier Analysis
if plotFourierProfile:
    reload(fm)
    fftRAOB, xfft = fm.calculate_FFT(
        RAOBProfile, returnX=True)
    fftRAP = fm.calculate_FFT(RAPProfile)
    fftList = [fftRAOB, fftRAP]

    fftML = np.empty((nML, fftRAOB.shape[0]))
    for i in range(nML):
        fftML[i, :] = fm.calculate_FFT(
            MLProfile[i, :])
        fftList.append(fftML[i, :])

    if saveFourierProfile:
        saveFile = 'FFT_' + nameProfile + '.png'
    else:
        saveFile = ''
    fp.plot_fft(xfft, fftList,
                labelList=labelList,
                saveFile=saveFile,
                title=nameProfile)


# %%
# Perform Fourier Analysis on all samples
if plotFourier:
    _, xfft = fm.calculate_FFT(RAOB[0, :], returnX=True)
    nLevsFFT = xfft.shape[0]
    fftRAOB = np.empty((nSamples, nLevsFFT))
    fftRAP = np.empty((nSamples, nLevsFFT))
    fftML = np.empty((nML, nSamples, nLevsFFT))

    for i in range(nSamples):
        fftRAOB[i, :] = fm.calculate_FFT(
            RAOB[i, :])
        fftRAP[i, :] = fm.calculate_FFT(
            RAP[i, :])

        for m in range(nML):
            fftML[m, i, :] = fm.calculate_FFT(
                ML[m, i, :])

    fftList = [fftRAOB.mean(axis=0), fftRAP.mean(axis=0)]
    for m in range(nML):
        fftList.append(fftML[m, :, :].mean(axis=0))

    if saveFourier:
        saveFile = 'FFT'
        for expName in expList:
            saveFile += '_' + expName
        saveFile += '.png'
    else:
        saveFile = ''
    fp.plot_fft(xfft, fftList,
                labelList=labelList,
                saveFile=saveFile,
                title='Mean FFT',
                xlims=[80, 130],
                ylims=[0.2, 0.4])


# %%
# Check Auto-Correlation
if plotAutoCorr or plotAutoCorrProfile:
    reload(fm)
    reload(fp)
    acRAOB = np.empty((nSamples, levsAutoCorr))
    acRAP = np.empty((nSamples, levsAutoCorr))
    acML = np.empty((nML, nSamples, levsAutoCorr))

    for i in range(nSamples):
        acRAOB[i, :] = fm.calculate_autocorrelation(
            RAOB[i, :], length=levsAutoCorr)
        acRAP[i, :] = fm.calculate_autocorrelation(
            RAP[i, :], length=levsAutoCorr)
        for m in range(nML):
            acML[m, i, :] = fm.calculate_autocorrelation(
                ML[m, i, :], length=levsAutoCorr)

    if plotAutoCorr:
        acList = [acRAOB.mean(axis=0), acRAP.mean(axis=0)]
        for m in range(nML):
            acList.append(acML[m, :, :].mean(axis=0))

        if saveAutoCorr:
            saveFile = 'AC'
            for expName in expList:
                saveFile += '_' + expName
            saveFile += '.png'
        else:
            saveFile = ''
        fp.plot_autocorrelation(acList,
                                labelList=labelList,
                                saveFile=saveFile,
                                title='Mean Auto-Correlations')

    if plotAutoCorrProfile:
        acList = [acRAOB[sampleRAOB, :],
                  acRAP[sampleRAOB, :]]
        for m in range(nML):
            acList.append(acML[m, sampleRAOB, :])

        if saveAutoCorrProfile:
            saveFile = 'AC_' + nameProfile + '.png'
        else:
            saveFile = ''
        fp.plot_autocorrelation(acList,
                                labelList=labelList,
                                saveFile=saveFile,
                                title=nameProfile)


# %%
# Print Vertical Derivatives (dx/dv)
if plotDXDV:
    dRAOB = np.zeros((nSamples))
    dRAP = np.zeros((nSamples))
    dML = np.zeros((nML, nSamples))

    for s in range(nSamples):
        for i in range(nLevs - 1):
            # dRAOB[s] += np.abs(RAOB[s, i + 1] - RAOB[s, i])
            # dRAP[s] += np.abs(RAP[s, i + 1] - RAP[s, i])
            # for m in range(nML):
            #    dML[m, s] += np.abs(ML[m, s, i + 1] - ML[m, s, i])

            dRAOB[s] += RAOB[s, i + 1] - RAOB[s, i]
            dRAP[s] += RAP[s, i + 1] - RAP[s, i]
            for m in range(nML):
                dML[m, s] += ML[m, s, i + 1] - ML[m, s, i]
        dRAOB[s] /= (nLevs - 1)
        dRAP[s] /= (nLevs - 1)
        dML[:, s] /= (nLevs - 1)

    print("Profile Derivatives (dx/dv)")
    print("   RAOB Mean= {:.4f}, Min= {:.4f}, Max={:.4f}".format(
        dRAOB.mean(), dRAOB.min(), dRAOB.max()))
    print("   RAP Mean=  {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
        dRAP.mean(), dRAP.min(), dRAP.max()))
    for m in range(nML):
        print("   {} Mean= {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
            expList[m], dML[m, :].mean(), dML[m, :].min(), dML[m, :].max()))

    meanValues = [dRAOB.mean(), dRAP.mean()]
    maxValues = [dRAOB.max(), dRAP.max()]
    minValues = [dRAOB.min(), dRAP.min()]
    for m in range(nML):
        meanValues.append(dML[m, :].mean())
        maxValues.append(dML[m, :].max())
        minValues.append(dML[m, :].min())

    reload(fp)
    fp.plot_dxdv(
        labelList,
        [meanValues, minValues, maxValues])


# %%
# Print Vertical Derivative Profiles (dx/dv)
if plotDXDVProfile:
    reload(fp)
    myz = 150
    dRAOB = np.zeros((myz - 1))
    dRAP = np.zeros((myz - 1))
    dML = np.zeros((nML, myz - 1))

    for s in range(nSamples):
        for i in range(myz - 1):
            dp = RAOBte[s, i + 1, 0] - RAOBte[s, i, 0]
            dpRAP = RAPte[s, i + 1, 0] - RAPte[s, i, 0]
            if myAbs:
                dRAOB[i] += \
                    np.abs((RAOB[s, i + 1] - RAOB[s, i])
                           / dp)
                dRAP[i] += \
                    np.abs((RAP[s, i + 1] - RAP[s, i])
                           / dpRAP)
                for m in range(nML):
                    dML[m, i] += \
                        np.abs((ML[m, s, i + 1]
                               - ML[m, s, i])
                               / dpRAP)
            else:
                dRAOB[i] += (RAOB[s, i + 1] - RAOB[s, i]) \
                    / dp
                dRAP[i] += (RAP[s, i + 1] - RAP[s, i]) \
                    / dpRAP
                for m in range(nML):
                    dML[m, i] += \
                        (ML[m, s, i + 1] - ML[m, s, i]) \
                        / dpRAP

    dRAOB /= nSamples
    dRAP /= nSamples
    for m in range(nML):
        dML[m, :] /= nSamples

    print("Profile Derivatives (dx/dv)")
    print(" RAOB Mean= {:.4f}, Min= {:.4f}, Max={:.4f}".
          format(dRAOB.mean(),
                 dRAOB.min(), dRAOB.max()))
    print(" RAP Mean=  {:.4f}, Min= {:.4f}, Max= {:.4f}".
          format(dRAP.mean(),
                 dRAP.min(), dRAP.max()))
    for m in range(nML):
        print(" {} Mean= {:.4f}, Min= {:.4f}, Max= {:.4f}".
              format(expList[m],
                     dML[m, :].mean(),
                     dML[m, :].min(),
                     dML[m, :].max()))

    if indxRAOB == 1:
        mxLabel = '$\dfrac{dT}{dp}$ ($\degree$C)'
    else:
        mxLabel = '$\dfrac{dTD}{dp}$ ($\degree$C)'
    avgAltitude = fm.compute_average_altitude(
        RAOBte[:, :myz, 0])
    avgAltitudeCenters = np.array(
        fm.get_centers(avgAltitude))
    plotList = [dRAOB, dRAP]
    labelList = ['RAOB', 'RAP']
    colorList = ['black', 'deeppink']
    for m in range(nML):
        plotList.append(dML[m, :])
        labelList.append(expNameList[m])
        colorList.append(expColorList[m])

    if saveDXDVProfile:
        if indxRAOB == 1:
            saveFile = 'ddxdp_t_profile.png'
        else:
            saveFile = 'ddxdp_td_profile.png'
    else:
        saveFile = ''
    fp.plot_profiles(plotList,
                     colorList=colorList,
                     labelList=labelList,
                     labelX=mxLabel,
                     labelY='Pressure (mb)',
                     saveFile=saveFile,
                     title=varNameList[indxRAOB],
                     usePressure=True,
                     yLine=avgAltitudeCenters)


# %%
# Plot best line
if plotBestLines:
    reload(fp)
    yLine = np.array(range(0, nLevs))

    a, b, c = np.polyfit(yLine, RAOBProfile, 2)
    RAOBLine = a * yLine * yLine + b * yLine + c

    a, b, c = np.polyfit(yLine, RAPProfile, 2)
    RAPLine = a * yLine * yLine + b * yLine + c

    MLLine = np.empty((nML, RAPLine.shape[0]))
    for m in range(nML):
        a, b, c, = np.polyfit(yLine, MLProfile[m, :], 2)
        MLLine[m, :] = a * yLine * yLine + b * yLine + c

    arList = [RAOBProfile, RAPProfile]
    fitList = [RAOBLine, RAPLine]
    for m in range(nML):
        arList.append(MLProfile[m, :])
        fitList.append(MLLine[m, :])

    if saveBestLines:
        saveFile = 'LineFits_{}.png'.format(nameProfile)
    else:
        saveFile = ''
    fp.plot_lineFits(yLine, arList, fitList,
                     labelList=labelList,
                     saveFile=saveFile,
                     title='2-Order Poly Fit')


# %%
# Interpolate
if plotSplines:
    reload(fm)
    RAOBSpline = fm.calculate_spline(RAOBProfile)
    RAPSpline = fm.calculate_spline(RAPProfile)
    MLSpline = np.empty((nML, RAPSpline.shape[0]))
    for m in range(nML):
        MLSpline[m, :] = fm.calculate_spline(
            MLProfile[m, :])

    if plotSplines:
        arList = [RAOBProfile, RAPProfile]
        fitList = [RAOBSpline, RAPSpline]
        for m in range(nML):
            arList.append(MLProfile[m, :])
            fitList.append(MLSpline[m, :])

        if saveSplines:
            saveFile = 'SplineFits_{}.png'.format(nameProfile)
        else:
            saveFile = ''
        yLine = np.linspace(1, RAPSpline.shape[0], num=RAPSpline.shape[0])
        fp.plot_lineFits(yLine, arList, fitList,
                         labelList=labelList,
                         saveFile=saveFile,
                         title='Spline Fit')


# %%
# Plot Spline Anomalies
if plotSplineAnoms or plotSplineAnomsDD:
    reload(fm)
    RAOBSplines = np.empty((nSamples, nLevs))
    RAPSplines = np.empty((nSamples, nLevs))
    MLSplines = np.empty((nML, nSamples, nLevs))

    for s in range(nSamples):
        RAOBSplines[s, :] = fm.calculate_spline(RAOB[s, :])
        RAPSplines[s, :] = fm.calculate_spline(RAP[s, :])

        for m in range(nML):
            MLSplines[m, s, :] = fm.calculate_spline(
                ML[m, s, :])

    RAOBAnoms = np.abs(RAOB - RAOBSplines)
    RAPAnoms = np.abs(RAP - RAPSplines)
    MLAnoms = np.abs(ML - MLSplines)

    RAOBAnomsT = np.sum(RAOBAnoms, axis=1) / nLevs
    RAPAnomsT = np.sum(RAPAnoms, axis=1) / nLevs
    MLAnomsT = np.sum(MLAnoms, axis=2) / nLevs

    print("Profile Anomalies")
    print("   RAOB Mean= {:.4f}, Min= {:.4f}, Max={:.4f}".
          format(
              RAOBAnomsT.mean(), RAOBAnomsT.min(), RAOBAnomsT.max()))
    print("   RAP Mean=  {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
        RAPAnomsT.mean(), RAPAnomsT.min(), RAPAnomsT.max()))
    for m in range(nML):
        print("   {} Mean= {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
            expList[m], MLAnomsT[m, :].mean(), MLAnomsT[m, :].min(),
            MLAnomsT[m, :].max()))

    if plotSplineAnoms:
        raobList = [RAOBAnomsT.mean(), RAOBAnomsT.min(), RAOBAnomsT.max()]
        rapList = [RAPAnomsT.mean(), RAPAnomsT.min(), RAPAnomsT.max()]
        plotList = [raobList, rapList]
        for m in range(nML):
            mlList = [MLAnomsT[m, :].mean(), MLAnomsT[m, :].min(),
                      MLAnomsT[m, :].max()]
            plotList.append(mlList)

        if saveSplineAnoms:
            saveFile = 'SplineAnoms'
            for m in range(nML):
                saveFile += '_{}'.format(expList[m])
            saveFile += '.png'
        else:
            saveFile = ''
        fp.plot_anoms(labelList, plotList,
                      saveFile=saveFile,
                      title='Profile Anomalies')

    if plotSplineAnomsDD:
        daRAOB = np.zeros(nSamples)
        daRAP = np.zeros(nSamples)
        daML = np.zeros((nML, nSamples))
        for s in range(nSamples):
            diffRAOB = 0
            diffRAP = 0
            for i in range(nLevs - 1):
                diffRAOB += np.abs(RAOBAnoms[s, i + 1] - RAOBAnoms[s, i])
                diffRAP += np.abs(RAPAnoms[s, i + 1] - RAPAnoms[s, i])
            daRAOB[s] = diffRAOB / (nLevs - 1)
            daRAP[s] = diffRAP / (nLevs - 1)

            for m in range(nML):
                diffML = 0
                for i in range(nLevs - 1):
                    diffML += np.abs(MLAnoms[m, s, i + 1] - MLAnoms[m, s, i])
                daML[m, s] = diffML / (nLevs - 1)

        raobList = [daRAOB.mean(), daRAOB.min(), daRAOB.max()]
        rapList = [daRAP.mean(), daRAP.min(), daRAP.max()]
        plotList = [raobList, rapList]
        for m in range(nML):
            mlList = [daML[m, :].mean(), daML[m, :].min(),
                      daML[m, :].max()]
            plotList.append(mlList)

        if saveSplineAnomsDD:
            saveFile = 'SplineAnomsDD'
            for m in range(nML):
                saveFile += '_{}'.format(expList[m])
            saveFile += '.png'
        else:
            saveFile = ''
        fp.plot_anoms(labelList, plotList,
                      legendLoc='upper center',
                      saveFile=saveFile,
                      title='Profile Anomaly Derivatives')


# %%
# Plot Profile Diffs
if plotProfileDiffs:
    reload(fp)
    diffRap = RAPProfile - RAOBProfile
    arList = [diffRap]
    laList = ['RAP']

    diffML = np.empty((nML, nLevs))
    for m in range(nML):
        diffML[m, :] = MLProfile[m, :] - RAOBProfile
        arList.append(diffML[m, :])
        laList.append(expList[m])

    if saveProfileDiffs:
        saveFile = 'diffs_RAP'
        for m in range(nML):
            saveFile += '_{}'.format(expList[m])
        saveFile += '.png'
    else:
        saveFile = ''
    fp.plot_profiles(arList, labelList=laList,
                     saveFile=saveFile, title=nameProfile)


# %%
# Plot Fourier analysis of profile diffs
if plotFFTDiffs:
    reload(fm)
    RAPdiff = RAP - RAOB
    MLdiff = np.empty((nML, nSamples, nLevs))
    for m in range(nML):
        MLdiff[m, :, :] = ML[m, :, :] - RAOB

    _, xfft = fm.calculate_FFT(
        RAPdiff[0, :], returnX=True)
    nLevsFFT = xfft.shape[0]
    fftRAPdiff = np.empty((nSamples, nLevsFFT))
    fftMLdiff = np.empty((nML, nSamples, nLevsFFT))

    for i in range(nSamples):
        fftRAPdiff[i, :] = fm.calculate_FFT(
            RAPdiff[i, :])
        for m in range(nML):
            fftMLdiff[m, i, :] = fm.calculate_FFT(
                MLdiff[m, i, :])

    fftList = [fftRAPdiff.mean(axis=0)]
    laList = ['RAP']
    for m in range(nML):
        fftList.append(fftMLdiff[m, :, :].mean(axis=0))
        laList.append(expList[m])

    if saveFFTDiffs:
        saveFile = 'FFTDiffs_RAP'
        for expName in expList:
            saveFile += '_' + expName
        saveFile += '.png'
    else:
        saveFile = ''
    fp.plot_fft(xfft, fftList,
                labelList=laList,
                saveFile=saveFile,
                title='Differences Mean FFT',
                xlims=[0, 127])


# %%
# Plot DDXDV
if plotDDXDV:
    dRAOB = np.empty((nLevs - 1))
    ddRAOB = np.zeros((nSamples, nLevs - 2))
    dRAP = np.empty((nLevs - 1))
    ddRAP = np.zeros((nSamples, nLevs - 2))
    dML = np.empty((nML, nLevs - 1))
    ddML = np.zeros((nML, nSamples, nLevs - 2))

    for s in range(nSamples):
        for i in range(nLevs - 1):
            dRAOB[i] = RAOB[s, i + 1] - RAOB[s, i]
            dRAP[i] = RAP[s, i + 1] - RAP[s, i]
            for m in range(nML):
                dML[m, i] = ML[m, s, i + 1] - ML[m, s, i]

        for i in range(nLevs - 2):
            ddRAOB[s, i] = dRAOB[i + 1] - dRAOB[i]
            ddRAP[s, i] = dRAP[i + 1] - dRAP[i]
            for m in range(nML):
                ddML[m, s, i] = dML[m, i + 1] - dML[m, i]

    dsRAOB = np.sum(np.abs(ddRAOB), axis=-1) / (ddRAOB.shape[-1])
    dsRAP = np.sum(np.abs(ddRAP), axis=-1) / ddRAP.shape[-1]
    dsML = np.sum(np.abs(ddML), axis=-1) / ddML.shape[-1]

    print("Second Derivatives (ddx/dv)")
    print("   RAOB Mean= {:.4f}, Min= {:.4f}, Max={:.4f}".format(
        dsRAOB.mean(), dsRAOB.min(), dsRAOB.max()))
    print("   RAP Mean=  {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
        dsRAP.mean(), dsRAP.min(), dsRAP.max()))
    for m in range(nML):
        print("   {} Mean= {:.4f}, Min= {:.4f}, Max= {:.4f}".format(
            expList[m], dsML[m, :].mean(), dsML[m, :].min(), dsML[m, :].max()))

    raobList = [dsRAOB.mean(), dsRAOB.min(), dsRAOB.max()]
    rapList = [dsRAP.mean(), dsRAP.min(), dsRAP.max()]
    plotList = [raobList, rapList]
    for m in range(nML):
        mlList = [dsML[m, :].mean(), dsML[m, :].min(),
                  dsML[m, :].max()]
        plotList.append(mlList)

    title = 'Profile Second Derivatives'
    if saveDDXDV:
        saveFile = 'DDXDV'
        if indxRAOB == 1:
            saveFile += 'T'
            title = 'T ' + title
        elif indxRAOB == 2:
            saveFile += 'TD'
            title = 'TD ' + title
        for m in range(nML):
            saveFile += '_{}'.format(expList[m])
        saveFile += '.png'
    else:
        saveFile = ''

    fp.plot_anoms(labelList, plotList,
                  labelY='$\degree C$',
                  legendLoc='upper left',
                  saveFile=saveFile,
                  title=title)
