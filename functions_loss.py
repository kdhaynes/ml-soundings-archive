"""
ML Soundings Research

Neural Network Classes.
Originally from Jason Stock.

Modified: 2022/03
"""

# %%
# Import Libraries
import numpy as np
import scipy
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.python.framework import ops
from tensorflow.python.ops import math_ops
from tensorflow.python.keras import backend as K


# Set Default Values
DEFAULT_NSAMPLES = 100000
DEFAULT_NSAMPLES_NORMAL = 100
DEFAULT_NSAMPLES_SINH = 500
DEFAULT_STD_MIN = 0.01
DEFAULT_STD_MAX = 1.e8
DEFAULT_TAU_MIN = 0.04
DEFAULT_TAU_MAX = 2.0


# %%
# INFO FUNCTIONS
def create_sample_norm_tfp(mu, sigma,
                           nSamples=2000,
                           Ymeans=None,
                           Ystds=None):
    cond_dist = tfp.distributions.Normal(mu, sigma)
    sample = cond_dist.sample(nSamples).numpy()
    if (Ymeans is not None) and (Ystds is not None):
        sample = sample * Ystds + Ymeans
    return sample


def create_sample_sinh_tfp(
        mu, sigma, gamma, tau,
        nSamples=2000,
        Ymeans=None, Ystds=None):
    cond_dist = tfp.distributions.SinhArcsinh(
        mu, sigma, gamma, tau)
    sample = cond_dist.sample(nSamples).numpy()
    if (Ymeans is not None) and (Ystds is not None):
        sample = sample * Ystds + Ymeans
    return sample


def determine_constraintmap_vals(a, b, pred_array,
                                 verbose=False):
    """Function to constrain values between
    a and b for use in loss functions.

    From Barnes, Barnes, and Gordillo (2021).
    Eq. 10
    """

    one = 1.
    pexp = np.add(one, np.exp(pred_array))
    pfirst = np.add(a, np.divide(one, pexp))
    pstd = np.multiply(np.subtract(b, a), pfirst)

    if verbose:
        print("   New array min: {:.4f}, max: {:.4f}".format(
            pstd.min(), pstd.max()))
    return pstd


def determine_sinh_loss(mu=0.3, sigma=1.0, gamma=0.0, tau=1.0,
                        xStart=0.29, xStop=0.31, xStep=0.001):
    x_array = tf.range(xStart, xStop, xStep)
    cond_dist = tfp.distributions.SinhArcsinh(mu, sigma, gamma, tau)
    loss = -cond_dist.log_prob(x_array).numpy()
    return tf.reduce_mean(loss)


# %%
# LOSS FUNCTIONS
def loss_crps_sample_score(y_true, y_pred):
    """Calculates the Continuous Ranked Probability Score (CRPS)
    for finite ensemble members.
    This implementation is based on the identity:
    .. math::
        CRPS(F, x) = E_F|X - x| - 1/2 * E_F|X - X'|
    where X and X' denote independent random variables drawn from
    the forecast distribution F, and E_F denotes the expectation
    value under F.

    Following the aproach of
    https://github.com/TheClimateCorporation/properscoring
    for the actual implementation.
    Adapted from http://www.cs.columbia.edu/~blei/

    Reference
    ---------
    Tilmann Gneiting and Adrian E. Raftery (2005).
        Strictly proper scoring rules, prediction, and estimation.
        University of Washington Department of Statistics Technical
        Report no. 463R.
        https://www.stat.washington.edu/research/reports/2004/tr463R.pdf
    """

    # Variable names below reference equation terms in docstring above
    term_one = tf.reduce_mean(tf.abs(
        tf.subtract(y_pred, y_true)), axis=-1)
    term_two = tf.reduce_mean(
        tf.abs(
            tf.subtract(tf.expand_dims(y_pred, -1),
                        tf.expand_dims(y_pred, -2))
        ),
        axis=(-2, -1)
    )
    half = tf.constant(-0.5, dtype=term_two.dtype)
    score = tf.add(term_one, tf.multiply(half, term_two))
    score = tf.reduce_mean(score)
    return score


def loss_crps_sample_score2D(y_true, y_pred):
    """Calculates the Continuous Ranked Probability Score (CRPS)
    for finite ensemble members.
    This implementation is based on the identity:
    .. math::
        CRPS(F, x) = E_F|X - x| - 1/2 * E_F|X - X'|
    where X and X' denote independent random variables drawn from
    the forecast distribution F, and E_F denotes the expectation
    value under F.

    Following the aproach of
    https://github.com/TheClimateCorporation/properscoring
    for the actual implementation.
    Adapted from http://www.cs.columbia.edu/~blei/

    Reference
    ---------
    Tilmann Gneiting and Adrian E. Raftery (2005).
        Strictly proper scoring rules, prediction, and estimation.
        University of Washington Department of Statistics Technical
        Report no. 463R.
        https://www.stat.washington.edu/research/reports/2004/tr463R.pdf

    NOTE: TO GET THIS TO WORK FOR PREDICTING MORE THAN A SINGLE TARGET,
      THE Y_TRUE MUST BE EXPANDED BY A DIMENSION (OF 1 AS A FILLER)
      IN ORDER TO BE COMPATABILE WITH THE Y_PRED FOR THE OPERATIONS.
    """

    # Variable names below reference equation terms in docstring above
    term_one = tf.reduce_mean(tf.abs(
        tf.subtract(y_pred,
                    tf.expand_dims(y_true, -1))), axis=-1)
    term_two = tf.reduce_mean(
        tf.abs(
            tf.subtract(tf.expand_dims(y_pred, -1),
                        tf.expand_dims(y_pred, -2))
        ),
        axis=(-2, -1)
    )
    half = tf.constant(-0.5, dtype=term_two.dtype)
    score = tf.add(term_one, tf.multiply(half, term_two))
    score = tf.reduce_mean(score)
    return score


def loss_mae2D(y_true2D, y_pred):
    return K.mean(math_ops.abs(
        tf.subtract(y_true2D[..., 0], y_pred)))


def loss_mae_normal(y_true, y_pred2D):
    return K.mean(math_ops.abs(
        tf.subtract(y_true, y_pred2D[..., 0])))


def loss_mae_sinh(y_true, y_pred):

    mu = tf.cast(y_pred[..., 0], tf.float64)
    std = tf.math.exp(tf.cast(y_pred[..., 1], tf.float64))
    skew = tf.cast(y_pred[..., 2], tf.float64)
    tau = tf.math.exp(tf.cast(y_pred[..., 3], tf.float64))
    y_mean = sinh_mean(mu, std, skew, tau)

    return K.mean(math_ops.abs(
        tf.subtract(y_true, y_mean)))


def loss_mae_surface(y_true, y_pred,
                     all_val=0.2, sfc_val=0.8,
                     sfc_level=25):
    sfc2 = int(sfc_level * 2)
    y_true_sfc = y_true[..., :sfc2]
    y_pred_sfc = y_pred[..., :sfc2]
    sfcScore = K.mean(math_ops.abs(tf.subtract(
        y_true_sfc, y_pred_sfc)))
    allScore = K.mean(math_ops.abs(tf.subtract(
        y_true, y_pred)))

    return sfcScore * sfc_val + allScore * all_val


def loss_mae_surface_stacked(y_true, y_pred, sfc_level=25):
    nhalf = int(y_true.shape[1] * 0.5)
    y_true_sfcT = y_true[..., :sfc_level]
    y_true_sfcTD = y_true[..., nhalf:nhalf + sfc_level]
    y_true_sfc = tf.concat([y_true_sfcT, y_true_sfcTD], axis=1)

    y_pred_sfcT = y_pred[..., :sfc_level]
    y_pred_sfcTD = y_pred[..., nhalf:nhalf + sfc_level]
    y_pred_sfc = tf.concat([y_pred_sfcT, y_pred_sfcTD], axis=1)

    return K.mean(math_ops.abs(tf.subtract(
        y_true_sfc, y_pred_sfc)))


def loss_mae_weighted(y_true, y_pred, weight):
    return K.mean(weight * math_ops.abs(y_pred - y_true))


def loss_mse2D(y_true2D, y_pred, name=None):
    if name is not None:
        loss_mse2D.__name__ = name
    return K.mean(tf.square(tf.subtract(y_true2D[..., 0], y_pred)))


def loss_mse_normal(y_true, y_pred2D, name=None):
    if name is not None:
        loss_mse2D.__name__ = name
    return K.mean(tf.square(tf.subtract(y_true, y_pred2D[..., 0])))


def loss_mse_sinh(y_true, y_pred):
    mu = tf.cast(y_pred[..., 0], tf.float64)
    std = tf.math.exp(tf.cast(y_pred[..., 1], tf.float64))
    skew = tf.cast(y_pred[..., 2], tf.float64)
    tau = tf.math.exp(tf.cast(y_pred[..., 3], tf.float64))
    y_mean = sinh_mean(mu, std, skew, tau)

    return K.mean(tf.square(tf.subtract(y_true, y_mean)))


def loss_mse_seperated(y_true, y_pred):
    y_pred = ops.convert_to_tensor_v2(y_pred)
    y_true = math_ops.cast(y_true, y_pred.dtype)
    seperation = int(y_true.shape[1] * 0.2)
    diff1 = math_ops.square(math_ops.squared_difference(
        y_pred[:, :seperation], y_true[:, :seperation]))
    diff2 = math_ops.squared_difference(
        y_pred[:, seperation:], y_true[:, seperation:])
    diff = tf.concat([diff1, diff2], axis=1)
    return K.mean(diff, axis=-1)


def loss_mse_weighted_ps2D(y_true2D, y_pred):
    y_true = y_true2D[..., 0]
    y_weights = y_true2D[..., 1]
    return K.mean(K.sum(
        tf.multiply(y_weights,
                    tf.square(tf.subtract(y_pred, y_true))), axis=1))


def loss_mse_weighted_ps_normal(y_true2D, y_pred2D):
    y_pred = y_pred2D[..., 0]
    y_true = y_true2D[..., 0]
    y_weights = y_true2D[..., 1]
    return K.mean(K.sum(
        tf.multiply(y_weights,
                    tf.square(tf.subtract(y_pred, y_true))), axis=1))


def loss_rmse2D(y_true2D, y_pred):
    return K.sqrt(K.mean(tf.square(tf.subtract(y_true2D[..., 0], y_pred))))


def loss_rmse_normal(y_true, y_pred2D):
    return K.sqrt(K.mean(tf.square(tf.subtract(y_true, y_pred2D[..., 0]))))


def loss_rmse_sinh(y_true, y_pred):
    mu = tf.cast(y_pred[..., 0], tf.float64)
    std = tf.math.exp(tf.cast(y_pred[..., 1], tf.float64))
    skew = tf.cast(y_pred[..., 2], tf.float64)
    tau = tf.math.exp(tf.cast(y_pred[..., 3], tf.float64))
    y_mean = sinh_mean(mu, std, skew, tau)

    return K.sqrt(K.mean(tf.square(
        tf.subtract(y_true, y_mean))))


def loss_uq_normal(y_true, y_pred):
    """
    This is the log-probability loss to calculate uncertainty
    with a normal distribution.
    This form predicts the uncertainty mean and standard deviation
    for a normal distribution.
    From Barnes, Barnes, & Gordillo (2021).
    """

    y_pred64 = tf.cast(y_pred, tf.float64)
    y_true64 = tf.cast(y_true, tf.float64)

    # network prediction of the value
    mu = y_pred64[..., 0]

    # network prediction of uncertainty
    std = tf.math.exp(y_pred64[..., 1])

    # normal distribution defined by N(mu,sigma)
    norm_dist = tfp.distributions.Normal(mu, std)

    # compute the log as -log(p)
    loss = -norm_dist.log_prob(y_true64)

    return tf.reduce_mean(loss)


def loss_uq_normalr(stdMin=0.01, stdMax=1.e8):
    """
    This is the log-probability loss to calculate uncertainty
    with a normal distribution.
    This form predicts the uncertainty mean and standard deviation
    for a normal distribution.
    From Barnes, Barnes, & Gordillo (2021).
    """

    def loss(y_true, y_pred):
        y_pred64 = tf.cast(y_pred, tf.float64)
        y_true64 = tf.cast(y_true, tf.float64)

        # network prediction of the value
        mu = y_pred64[..., 0]

        # network prediction of uncertainty
        max = tf.cast(stdMax, tf.float64)
        min = tf.cast(stdMin, tf.float64)
        one = tf.cast(1., tf.float64)
        pexp = tf.add(one,
                      tf.exp(y_pred64[..., 1]))
        pfirst = tf.add(min, tf.divide(one, pexp))
        std = tf.multiply(tf.subtract(max, min), pfirst)

        # normal distribution defined by N(mu,sigma)
        norm_dist = tfp.distributions.Normal(mu, std)

        # compute the log as -log(p)
        loss = -norm_dist.log_prob(y_true64)
        return tf.reduce_mean(loss)
    return loss


def loss_uq_sinh(y_true, y_pred):
    """
    This is the log-probability loss to calculate uncertainty
    with a sinh-arcsinh normal distribution.
    This form predicts the uncertainty mean,
    standard deviation (scale),
    skewness and tailweight (tau).
    From Barnes, Barnes, & Gordillo (2021).
    """

    mu = tf.cast(y_pred[..., 0], tf.float64)
    std = tf.math.exp(tf.cast(y_pred[..., 1], tf.float64))
    skew = tf.cast(y_pred[..., 2], tf.float64)
    tau = tf.math.exp(tf.cast(y_pred[..., 3], tf.float64))

    cond_dist = tfp.distributions.SinhArcsinh(
        loc=mu, scale=std, skewness=skew, tailweight=tau)
    loss = -cond_dist.log_prob(tf.cast(y_true, tf.float64))

    return tf.reduce_mean(loss)


def loss_uq_sinhr(stdMin=0.01, stdMax=1.e8):
    """
    This is the log-probability loss to calculate uncertainty
    with a sinh-arcsinh normal distribution.
    This form predicts the uncertainty mean,
    standard deviation (scale),
    skewness and tailweight (tau).
    From Barnes, Barnes, & Gordillo (2021).
    """

    def loss(y_true, y_pred):
        y_pred64 = tf.cast(y_pred, tf.float64)
        y_true64 = tf.cast(y_true, tf.float64)

        mu = y_pred64[..., 0]
        skew = y_pred64[..., 2]
        tau = tf.math.exp(y_pred64[..., 3])

        max = tf.cast(stdMax, tf.float64)
        min = tf.cast(stdMin, tf.float64)
        one = tf.cast(1., tf.float64)
        pexp = tf.add(one,
                      tf.exp(y_pred64[..., 1]))
        pfirst = tf.add(min, tf.divide(one, pexp))
        std = tf.multiply(tf.subtract(max, min), pfirst)

        cond_dist = tfp.distributions.SinhArcsinh(
            loc=mu, scale=std, skewness=skew, tailweight=tau)
        loss = -cond_dist.log_prob(y_true64)
        return tf.reduce_mean(loss)

    return loss


def loss_uq_sinhrt(stdMin=0.01, stdMax=1.e8, tauMin=0.01, tauMax=1.0):
    """
    This is the log-probability loss to calculate uncertainty
    with a sinh-arcsinh normal distribution.
    This form predicts the uncertainty mean,
    standard deviation (scale),
    skewness and tailweight (tau).
    From Barnes, Barnes, & Gordillo (2021).
    """

    def loss(y_true, y_pred):
        y_pred64 = tf.cast(y_pred, tf.float64)
        y_true64 = tf.cast(y_true, tf.float64)

        mu = y_pred64[..., 0]
        skew = y_pred64[..., 2]

        max = tf.cast(stdMax, tf.float64)
        min = tf.cast(stdMin, tf.float64)
        one = tf.cast(1., tf.float64)
        pexp = tf.add(one,
                      tf.exp(y_pred64[..., 1]))
        pfirst = tf.add(min, tf.divide(one, pexp))
        std = tf.multiply(tf.subtract(max, min), pfirst)

        max = tf.cast(tauMax, tf.float64)
        min = tf.cast(tauMin, tf.float64)
        pexp = tf.add(one,
                      tf.exp(y_pred64[..., 3]))
        pfirst = tf.add(min, tf.divide(one, pexp))
        tau = tf.multiply(tf.subtract(max, min), pfirst)

        cond_dist = tfp.distributions.SinhArcsinh(
            loc=mu, scale=std, skewness=skew, tailweight=tau)
        loss = -cond_dist.log_prob(y_true64)
        return tf.reduce_mean(loss)

    return loss


# %%
# SINH-ARCSINH ROUTINES
def sinh_jones_pewsey_P(q):
    """
    Arguments
    ---------
    q : float or double , array like
    Returns
    -------
    P_q : array like of same shape as q.

    Notes
    -----
    * The strange constant 0.25612... is "sqrt(sqrt(e) / (8* pi))"
    computed with a high-precision calculator.
    """

    return 0.25612601391340369863537463 * (
        scipy.special.kv((q + 1) / 2, 0.25)
        + scipy.special.kv((q - 1) / 2, 0.25))


def sinh_mean(mu, sigma, gamma, tau):
    """ The distribution mean.

    Arguments
    ---------
    mu : float or double (batch size x 1) Tensor
        The location parameter.
    sigma : float or double (batch size x 1) Tensor
        The scale parameter. Must be strictly positive.
        Must be the same shape and dtype as mu.
    gamma : float or double (batch size x 1) Tensor
        The skewness parameter.
        Must be the same shape and dtype as mu.
    tau : float or double (batch size x 1) Tensor
        The tail - weight parameter.
        Must be strictly positive.
        Must be the same shape and dtype as mu.

    Returns
    -------
    x: float or double (batch size x 1) Tensor.
    The computed distribution mean values .
    """
    evX = tf.math.sinh(gamma / tau) * sinh_jones_pewsey_P(1.0 / tau)
    return mu + sigma * evX


def sinh_median(mu, sigma, gamma, tau):
    """ The distribution median.

    Arguments
    ---------
    mu : float or double (batch size x 1) Tensor
        The location parameter.
    sigma : float or double (batch size x 1) Tensor
        The scale parameter.
        Must be strictly positive.
        Must be the same shape and dtype as mu.
    gamma : float or double (batch size x 1) Tensor
        The skewness parameter.
        Must be the same shape and dtype as mu.
    tau : float or double ( batch size x 1) Tensor
        The tail-weight parameter.
        Must be strictly positive.
        Must be the same shape and dtype as mu.

    Returns
    -------
    x : float or double (batch size x 1) Tensor.
        The computed distribution mean values .

    """
    return mu + sigma * tf.math.sinh(gamma / tau)


def sinh_stddev(mu, sigma, gamma, tau):
    """ The distribution standard deviation.

    Arguments
    ---------
    mu : float or double (batch size x 1) Tensor
        The location parameter.
    sigma : float or double ( batch size x 1) Tensor
        The scale parameter.
        Must be strictly positive.
        Must be the same shape and dtype as mu.
    gamma : float or double (batch size x 1) Tensor
        The skewness parameter.
        Must be the same shape and dtype as mu.
    tau : float or double (batch size x 1) Tensor
        The tail-weight parameter.
        Must be strictly positive.
        Must be the same shape and dtype as mu.

    Returns
    -------
    x : float or double (batch size x 1) Tensor.
        The computed distribution mean values.
    """
    return tf.math.sqrt(sinh_variance(mu, sigma, gamma, tau))


def sinh_variance(mu, sigma, gamma, tau):
    """ The distribution variance.

    Arguments
    ---------
    mu : float or double (batch size x 1) Tensor
        The location parameter.
    sigma : float or double (batch size x 1) Tensor
        The scale parameter.
        Must be strictly positive.
        Must be the same shape and dtype as mu.
    gamma : float or double (batch size x 1) Tensor
        The skewness parameter.
        Must be the same shape and dtype as mu.
    tau : float or double ( batch size x 1) Tensor
        The tail-weight parameter.
        Must be strictly positive.
        Must be the same shape and dtype as mu.

    Returns
    -------
    x : float or double (batch size x 1) Tensor.
    The computed distribution mean values .

    Notes
    -----
    * This code uses two basic formulas:
        var(X) = E(X^2) - (E(X))^2
        var(a*X + b) = a^2 * var(X)
    * The E(X) and E(X^2) are computed using the
        moment equations given on page 764 of [1].
    """
    evX = tf.math.sinh(gamma / tau) * sinh_jones_pewsey_P(1.0 / tau)
    evX2 = (tf.math.cosh(2 * gamma / tau)
            * sinh_jones_pewsey_P(2.0 / tau) - 1.0) / 2
    return tf.math.square(sigma) * (evX2 - tf.math.square(evX))


# %%
# UNSCALE ROUTINES
def unscale_normal_tf(mu, sigma, YMeans, YStds,
                      nSamples=DEFAULT_NSAMPLES_NORMAL,
                      uqLow=2.5, uqHigh=97.5):

    cond_dist = tfp.distributions.Normal(mu, sigma)
    sample_scaled = cond_dist.sample(nSamples).numpy()
    sample = YStds * sample_scaled + YMeans

    pmean = np.mean(sample, axis=0)
    pstd = np.std(sample, axis=0)
    pmin = np.percentile(sample, uqLow, axis=0)
    pmax = np.percentile(sample, uqHigh, axis=0)

    return pmean, pstd, pmin, pmax


def unscale_sinh_tf(mu, sigma, gamma, tau, YMeans, YStds,
                    nSamples=DEFAULT_NSAMPLES_SINH,
                    uqLow=2.5, uqHigh=97.5):

    cond_dist = tfp.distributions.SinhArcsinh(mu, sigma, gamma, tau)
    sample_scaled = cond_dist.sample(nSamples).numpy()
    sample = YStds * sample_scaled + YMeans

    pmean = np.mean(sample, axis=0)
    pmedian = np.median(sample, axis=0)
    pstd = np.std(sample, axis=0)
    pmin = np.percentile(sample, uqLow, axis=0)
    pmax = np.percentile(sample, uqHigh, axis=0)

    return pmean, pmedian, pstd, pmin, pmax
