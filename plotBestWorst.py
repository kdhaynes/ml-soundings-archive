"""
ML Soundings Project

Program to plot results.

Modified: 2022/02
"""


# %%
# Import Libraries
import numpy as np

from functions_metrics import rmse
from functions_metrics import rmse_weighted_ps_profile

import functions_misc as fm
import functions_plot as fp

from var_opts import exp585 as expDict


# %%
# User Info

modelColor = 'mediumslateblue'
modelName = 'UNet'
rapColor = 'deeppink'

useSfcRMSE = True

plotMeanProfile = False
saveMeanProfile = False

plotMeanProfileSfc = False
saveMeanProfileSfc = False

plotSpecProfile = False
plotSpecProfileNum = 2476

plotSpecSounding = False
plotSpecSoundingNum = 2476

plotSpecCombo = False
plotSpecComboNum = 1925
saveSpecCombo = False

plotBestProfile = False
plotWorstProfile = False

plotWorstSounding = True
plotBestSounding = True
plotBiggestImprovement = True
plotBiggestDegredation = True

mapGOESPatch = False
mapGOESPatchNum = 926

plotGOESPatch = False
plotGOESPatchNum = 1000

plotRTMAPatch = False
plotRTMAPatchNum = 926

plotCombo = False
plotComboNum = 1  # Number of figures per type
plotComboTop = [True, False]  # True=best, False=worst
plotComboType = ['T', 'TD', 'I']  # T, TD, I
saveCombo = False
saveComboDPI = 100

restrict_data = [4, 8]
restrictCAPELow = None
restrictCAPEHigh = None
shuffleAll = True
shuffleSites = False
useShannon = True
dirFigs = '/home/kdhaynes/ml_soundings/figs/figs.220906/'


# %%
# Get original data
restrict_data = fm.get_exp_restrict_data(expDict)
FILEtrain, FILEval, FILEtest = fm.load_data(
    useShannon,
    restrictCAPELow=restrictCAPELow,
    restrictCAPEHigh=restrictCAPEHigh,
    restrictData=restrict_data,
    returnFileOnly=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictCAPELow=restrictCAPELow,
    restrictCAPEHigh=restrictCAPEHigh,
    restrictData=restrict_data,
    returnFileInfo=False,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
GOEStest = data[8]
RAOBtest = data[11]
RAPtest = data[2]
RTMAtest = data[5]
print("Data Test Shape: {}".format(RAOBtest.shape))


# %%
# Read predictions
fileName = fm.get_exp_filenamePreds(useShannon, expDict)
pDict = fm.pickle_read(fileName)
PREDtest = pDict['PREDtest']
print("Prediction Test Shape: {}".format(PREDtest.shape))


# %%
# Get errors per sample
ref = fm.REF_ERROR_LEVEL
sfc = fm.SFC_ERROR_LEVEL
it = fm.RAOB_INDX_TEMPERATURE
id = fm.RAOB_INDX_DEWPOINT
itr = fm.RAP_INDX_TEMPERATURE
idr = fm.RAP_INDX_DEWPOINT
rap_rmseT_value = rmse(
    RAOBtest[:, :ref, it],
    RAPtest[:, :ref, itr])
rap_rmseTsfc_value = rmse(
    RAOBtest[:, :sfc, it],
    RAPtest[:, :sfc, itr])
rap_rmseTD_value = rmse(
    RAOBtest[:, :ref, id],
    RAPtest[:, :ref, idr])
rap_rmseTDsfc_value = rmse(
    RAOBtest[:, :sfc, id],
    RAPtest[:, :sfc, idr])
rap_meanRMSEList = [rap_rmseT_value, rap_rmseTD_value]
rap_meanRMSEList_sfc = [rap_rmseTsfc_value, rap_rmseTDsfc_value]
(rap_rmseTAll, rap_mean_rmseTAll,
    rap_rmse_sfcTAll, rap_mean_rmse_sfcTAll) = \
    fm.calculate_profile_rmses_samples(
    RAOBtest[:, :, it],
    RAPtest[:, :, itr])
(rap_rmseTDAll, rap_mean_rmseTDAll,
    rap_rmse_sfcTDAll, rap_mean_rmse_sfcTDAll) = \
    fm.calculate_profile_rmses_samples(
    RAOBtest[:, :, id],
    RAPtest[:, :, idr])
(rap_rmseBAll, rap_mean_rmseBAll,
    rap_rmse_sfcBAll, rap_mean_rmse_sfcBAll) = \
    fm.calculate_profile_rmses_samples3D(
    RAOBtest[:, :, [it, id]],
    RAPtest[:, :, [itr, idr]])

(rmseTAll, mean_rmseTAll, rmse_sfcTAll, mean_rmse_sfcTAll) = \
    fm.calculate_profile_rmses_samples(
    RAOBtest[:, :, it],
    PREDtest[:, :, fm.ML_INDX_TEMPERATURE])
(rmseTDAll, mean_rmseTDAll, rmse_sfcTDAll, mean_rmse_sfcTDAll) = \
    fm.calculate_profile_rmses_samples(
    RAOBtest[:, :, id],
    PREDtest[:, :, fm.ML_INDX_DEWPOINT])
(rmseBAll, mean_rmseBAll, rmse_sfcBAll, mean_rmse_sfcBAll) = \
    fm.calculate_profile_rmses_samples3D(
    RAOBtest[:, :, [it, id]],
    PREDtest)

TsortRefs = np.argsort(mean_rmseTAll)
TDsortRefs = np.argsort(mean_rmseTDAll)
BsortRefs = np.argsort(mean_rmseBAll)
sfc_TsortRefs = np.argsort(mean_rmse_sfcTAll)
sfc_TDsortRefs = np.argsort(mean_rmse_sfcTDAll)
sfc_BsortRefs = np.argsort(mean_rmse_sfcBAll)

meanRMSEMeanT = np.mean(mean_rmseTAll)
minRMSEMeanT = np.min(mean_rmseTAll)
maxRMSEMeanT = np.max(mean_rmseTAll)
meanRMSEMeanTD = np.mean(mean_rmseTDAll)
minRMSEMeanTD = np.min(mean_rmseTDAll)
maxRMSEMeanTD = np.max(mean_rmseTDAll)
meanRMSEMeanB = np.mean(mean_rmseBAll)
minRMSEMeanB = np.min(mean_rmseBAll)
maxRMSEMeanB = np.max(mean_rmseBAll)
print("Calculated predicted RMSE per sample/sounding.")
print("   T Mean: {:.3f}, Min: {:.3f} and Max: {:.3f}".
      format(meanRMSEMeanT, minRMSEMeanT, maxRMSEMeanT))
print("   TD Mean: {:.3f}, Min: {:.3f} and Max: {:.3f}".
      format(meanRMSEMeanTD, minRMSEMeanTD, maxRMSEMeanTD))
strFirst = "   Combined Mean: "
print("{}{:.3f}, Min: {:.3f} and Max: {:.3f}".
      format(
          strFirst,
          meanRMSEMeanB, minRMSEMeanB, maxRMSEMeanB))
print("Profile Reference Numbers (T,TD,B)")
print("   Best: {}".format(
    [TsortRefs[0], TDsortRefs[0], BsortRefs[0]]))
print("   Worst: {}".format(
    [TsortRefs[-1], TDsortRefs[-1], BsortRefs[-1]]))
print("   Sfc Best: {}".format(
    [sfc_TsortRefs[0], sfc_TDsortRefs[0],
     sfc_BsortRefs[0]]))
print("   Sfc Worst: {}".format(
    [sfc_TsortRefs[-1], sfc_TDsortRefs[-1],
     sfc_BsortRefs[-1]]))


# %%
# Calculate improvements per profile
improveTAll = np.subtract(
    rap_mean_rmseTAll, mean_rmseTAll)
improveTDAll = np.subtract(
    rap_mean_rmseTDAll, mean_rmseTDAll)
improveBAll = np.subtract(
    rap_mean_rmseBAll, mean_rmseBAll)
TimproveRefs = np.argsort(improveTAll)
TDimproveRefs = np.argsort(improveTDAll)
BimproveRefs = np.argsort(improveBAll)

sfc_improveTAll = np.subtract(
    rap_mean_rmse_sfcTAll, mean_rmse_sfcTAll)
sfc_improveTDAll = np.subtract(
    rap_mean_rmse_sfcTDAll, mean_rmse_sfcTDAll)
sfc_improveBAll = np.subtract(
    rap_mean_rmse_sfcBAll, mean_rmse_sfcBAll)
sfc_TimproveRefs = np.argsort(sfc_improveTAll)
sfc_TDimproveRefs = np.argsort(sfc_improveTDAll)
sfc_BimproveRefs = np.argsort(sfc_improveBAll)

minImproveT = np.min(improveTAll)
maxImproveT = np.max(improveTAll)
minImproveTD = np.min(improveTDAll)
maxImproveTD = np.max(improveTDAll)
minImproveB = np.min(improveBAll)
maxImproveB = np.max(improveBAll)
print("Calculated improvements per sample/sounding.")
strF = "   Temperature Profile Mean Improvement Min: "
print("{}{:.3f} and Max: {:.3f}".
      format(strF, minImproveT, maxImproveT))
strF = "   Dewpoint Provile Mean Improvement Min: "
print("{}{:.3f} and Max: {:.3f}".
      format(strF, minImproveTD, maxImproveTD))
strF = "   Combined Profile Mean Improvement Min: "
print("{}{:.3f} and Max: {:.3f}".
      format(strF, minImproveB, maxImproveB))
print("Profile Reference Numbers (T,TD,B)")
print("   Most Improved: {}".format(
    [TimproveRefs[-1], TDimproveRefs[-1],
     BimproveRefs[-1]]))
print("   Most Degraded: {}".format(
    [TimproveRefs[0], TDimproveRefs[0],
     BimproveRefs[0]]))
print("   Sfc Most Improved: {}".format(
    [sfc_TimproveRefs[-1], sfc_TDimproveRefs[-1], sfc_BimproveRefs[-1]]))
print("   Sfc Most Degraded: {}".format(
    [sfc_TimproveRefs[0], sfc_TDimproveRefs[0], sfc_BimproveRefs[0]]))


# %%
# Get weighted errors per profile
weighted_rmseTAll = rmse_weighted_ps_profile(
    RAOBtest[..., it],
    PREDtest[..., fm.ML_INDX_TEMPERATURE],
    RAOBtest[..., fm.RAOB_INDX_PRESSURE])
weighted_rmseTDAll = rmse_weighted_ps_profile(
    RAOBtest[..., id],
    PREDtest[..., fm.ML_INDX_DEWPOINT],
    RAOBtest[..., fm.RAOB_INDX_PRESSURE])
rap_weighted_rmseTAll = rmse_weighted_ps_profile(
    RAOBtest[..., it],
    RAPtest[..., itr],
    RAOBtest[..., fm.RAOB_INDX_PRESSURE])
rap_weighted_rmseTDAll = rmse_weighted_ps_profile(
    RAOBtest[..., id],
    RAPtest[..., idr],
    RAOBtest[..., fm.RAOB_INDX_PRESSURE])


# %%
# Calculate Best/Worst Using Surface if Requested ================
if useSfcRMSE:
    TsortRefs = sfc_TsortRefs
    TDsortRefs = sfc_TDsortRefs
    BsortRefs = sfc_BsortRefs
    TimproveRefs = sfc_TimproveRefs
    TDimproveRefs = sfc_TDimproveRefs
    BimproveRefs = sfc_BimproveRefs
    print("USING SFC RANKINGS FOR BEST/WORST\n")
else:
    print("USING TOTAL PROFILE FOR BEST/WORST\n")


# %%
# Plot profile of mean errors
if plotMeanProfile or plotMeanProfileSfc:
    (rap_rmseT, rap_mean_rmseT, rap_rmse_sfcT, rap_mean_rmse_sfcT) = \
        fm.calculate_profile_rmses(
            RAOBtest[:, :, it],
            RAPtest[:, :, itr])
    (rap_rmseTD, rap_mean_rmseTD, rap_rmse_sfcTD, rap_mean_rmse_sfcTD) = \
        fm.calculate_profile_rmses(
            RAOBtest[:, :, id],
            RAPtest[:, :, idr])
    (rmseT, mean_rmseT, rmse_sfcT, mean_rmse_sfcT) = \
        fm.calculate_profile_rmses(
        RAOBtest[:, :, it],
        PREDtest[:, :, fm.ML_INDX_TEMPERATURE])
    (rmseTD, mean_rmseTD,
     rmse_sfcTD, mean_rmse_sfcTD) = \
        fm.calculate_profile_rmses(
            RAOBtest[:, :, id],
            PREDtest[:, :, fm.ML_INDX_DEWPOINT])

    expName = fm.get_exp_refname(expDict).upper()
    expTitle = expName + " "
    hAvg = fm.compute_average_altitude(
        RAOBtest[:, :, fm.RAOB_INDX_HEIGHT])

    nameType = '_mean_vertical_'
    if plotMeanProfile:
        rap_vrmseList = [rap_rmseT, rap_rmseTD]
        rap_meanRMSEList = [rap_mean_rmseT, rap_mean_rmseTD]
        pred_vrmseList = [rmseT, rmseTD]
        pred_meanRMSEList = [mean_rmseT, mean_rmseTD]
        if saveMeanProfile:
            fileName = dirFigs \
                + expName + nameType + 'rmse.png'
        else:
            fileName = ''

        fp.plot_vertical_rmse_altitude(
            rap_vrmseList, pred_vrmseList,
            altitude=hAvg,
            base_color=rapColor,
            base_meanRMSEList=rap_meanRMSEList,
            ml_color=modelColor,
            ml_meanRMSEList=pred_meanRMSEList,
            label2=modelName, plotVLine=False,
            saveFile=fileName)

    if plotMeanProfileSfc:
        rap_sfc_vrmseList = [rap_rmse_sfcT, rap_rmse_sfcTD]
        rap_meanRMSEList_sfc = [rap_mean_rmse_sfcT, rap_mean_rmse_sfcTD]
        pred_sfc_vrmseList = [rmse_sfcT, rmse_sfcTD]
        pred_meanRMSEList_sfc = [mean_rmse_sfcT, mean_rmse_sfcTD]
        if saveMeanProfileSfc:
            fileName = dirFigs \
                + expName + nameType + 'rmse_sfc.png'
        else:
            fileName = ''
        fp.plot_vertical_rmse_altitude(
            rap_sfc_vrmseList, pred_sfc_vrmseList,
            altitude=hAvg[:sfc],
            base_meanRMSEList=rap_meanRMSEList_sfc,
            ml_meanRMSEList=pred_meanRMSEList_sfc,
            label2=expTitle, plotVLine=False,
            saveFile=fileName)


# %%
# Plot profile of specific sounding/sample
if plotSpecProfile:
    # pred_vrmseList = [rmseTAll[plotSpecProfileNum, :],
    #                  rmseTDAll[plotSpecProfileNum, :]]
    # kfp.plot_vertical_rmse(rap_vrmseList, pred_vrmseList)
    print("Plotting Profile {}: {}".format(
        plotSpecProfileNum, FILEtest[plotSpecProfileNum][:17]))
    print("Surface RMSE T= {:.3f}, TD= {:.3f}, Both= {:.3f}".
          format(mean_rmse_sfcTAll[plotSpecProfileNum],
                 mean_rmse_sfcTDAll[plotSpecProfileNum],
                 mean_rmse_sfcBAll[plotSpecProfileNum]))

    trmse = rmseTAll[plotSpecProfileNum, :]
    trmse_mean = np.mean(trmse)
    tdrmse = rmseTDAll[plotSpecProfileNum, :]
    tdrmse_mean = np.mean(tdrmse)
    hAvg = fm.compute_average_altitude(
        RAOBtest[:, :, fm.RAOB_INDX_HEIGHT])
    fp.plot_vertical_rmse_baseline(
        trmse, trmse_mean,
        tdrmse, tdrmse_mean,
        altitude=hAvg)


# %%
# Plot sounding of specific sample
if plotSpecSounding:
    indx = plotSpecSoundingNum
    RAOBD = {
        fp.PRESSURE_COLUMN_KEY:
        RAOBtest[indx, :, fm.RAOB_INDX_PRESSURE],
        fp.TEMPERATURE_COLUMN_KEY:
        RAOBtest[indx, :, it],
        fp.DEWPOINT_COLUMN_KEY:
        RAOBtest[indx, :, id]}

    RAPD = {
        fp.PRESSURE_COLUMN_KEY:
        RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
        fp.TEMPERATURE_COLUMN_KEY:
        RAPtest[indx, :, itr],
        fp.DEWPOINT_COLUMN_KEY:
        RAPtest[indx, :, idr]}

    PREDD = {
        fp.PRESSURE_COLUMN_KEY:
        RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
        fp.TEMPERATURE_COLUMN_KEY:
        PREDtest[indx, :, fm.ML_INDX_TEMPERATURE],
        fp.DEWPOINT_COLUMN_KEY:
        PREDtest[indx, :, fm.ML_INDX_DEWPOINT]}

    title = FILEtest[indx][:17].upper()
    title += '  (RMSE T={:.2f}, TD={:.2f})'.\
        format(mean_rmseTAll[indx], mean_rmseTDAll[indx])
    print("  RMSE RAP T= {:.2f}, TD= {:.2f}".
          format(rap_mean_rmseTAll[indx], rap_mean_rmseTDAll[indx]))
    print("  RMSE PRED T= {:.2f}, TD= {:.2f}".
          format(mean_rmseTAll[indx], mean_rmseTDAll[indx]))
    print("  RMSE SFC PRED T= {:.2f}, TD= {:.2f}".
          format(mean_rmse_sfcTAll[indx], mean_rmse_sfcTDAll[indx]))
    print("  PRED RMSE IMPROVEMENT T= {:.2f}, TD= {:.2f}".
          format(improveTAll[indx], improveTDAll[indx]))
    fp.plot_sounding_results(
        RAOBD, RAPD, PREDD, title_string=title)


# %%
# Create specific combination plot
if plotSpecCombo:
    indx = plotSpecComboNum
    RAOBD = {
        fp.PRESSURE_COLUMN_KEY:
        RAOBtest[indx, :, fm.RAOB_INDX_PRESSURE].
            reshape(-1),
        fp.TEMPERATURE_COLUMN_KEY:
        RAOBtest[indx, :, it].reshape(-1),
        fp.DEWPOINT_COLUMN_KEY:
        RAOBtest[indx, :, id].reshape(-1)}

    RAPD = {
        fp.PRESSURE_COLUMN_KEY:
        RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
        fp.TEMPERATURE_COLUMN_KEY:
        RAPtest[indx, :, itr],
        fp.DEWPOINT_COLUMN_KEY:
        RAPtest[indx, :, idr]}

    PREDD = {
        fp.PRESSURE_COLUMN_KEY:
        RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
        fp.TEMPERATURE_COLUMN_KEY:
        PREDtest[indx, :, fm.ML_INDX_TEMPERATURE],
        fp.DEWPOINT_COLUMN_KEY:
        PREDtest[indx, :, fm.ML_INDX_DEWPOINT]}

    expName = fm.get_exp_refname(expDict).upper()
    siteName = FILEtest[indx][:17].upper()
    title = "\n {} {}".format(expName, siteName)
    text1 = 'Mean '
    text2 = 'RMSE:'
    myFormat = fm.get_format(
        rap_mean_rmseTAll[indx])
    text3 = ('RAP T= {' + myFormat + '}').format(rap_mean_rmseTAll[indx])
    myFormat = fm.get_format(
        mean_rmseTAll[indx])
    text4 = (' ML T= {' + myFormat + '}').format(mean_rmseTAll[indx])
    myFormat = fm.get_format(
        rap_mean_rmseTDAll[indx])
    text5 = ('TD= {' + myFormat + '}').format(rap_mean_rmseTDAll[indx])
    myFormat = fm.get_format(
        mean_rmseTDAll[indx])
    text6 = ('TD= {' + myFormat + '}').format(mean_rmseTDAll[indx])

    textw1 = 'Wghtd'
    textw2 = 'RMSE:'
    myFormat = fm.get_format(
        rap_weighted_rmseTAll[indx])
    textw3 = ('T= {' + myFormat + '}').format(rap_weighted_rmseTAll[indx])
    myFormat = fm.get_format(
        weighted_rmseTAll[indx])
    textw4 = ('T= {' + myFormat + '}').format(weighted_rmseTAll[indx])
    myFormat = fm.get_format(
        rap_weighted_rmseTDAll[indx])
    textw5 = ('TD= {' + myFormat + '}').format(rap_weighted_rmseTDAll[indx])
    myFormat = fm.get_format(
        weighted_rmseTDAll[indx])
    textw6 = ('TD= {' + myFormat + '}').format(weighted_rmseTDAll[indx])

    texts1 = 'Sfc  '
    texts2 = 'RMSE:'
    myFormat = fm.get_format(
        rap_mean_rmse_sfcTAll[indx])
    texts3 = ('T= {' + myFormat + '}').\
        format(rap_mean_rmse_sfcTAll[indx])
    myFormat = fm.get_format(
        mean_rmse_sfcTAll[indx])
    texts4 = ('T= {' + myFormat + '}').\
        format(mean_rmse_sfcTAll[indx])
    myFormat = fm.get_format(
        rap_mean_rmse_sfcTDAll[indx])
    texts5 = ('TD= {' + myFormat + '}').\
        format(rap_mean_rmse_sfcTDAll[indx])
    myFormat = fm.get_format(
        mean_rmse_sfcTDAll[indx])
    texts6 = ('TD= {' + myFormat + '}').\
        format(mean_rmse_sfcTDAll[indx])

    if saveSpecCombo:
        fileName = dirFigs \
            + expName + '_' \
            + siteName + '_combo.png'
    else:
        fileName = ''

    fp.plot_combo3(
        RAOBD, RAPD, PREDD,
        GOEStest[indx, ...],
        RTMAtest[indx, ...],
        saveFile=fileName,
        saveDPI=100,
        title=title,
        text1=text1, text2=text2,
        text3=text3, text4=text4,
        text5=text5, text6=text6,
        textw1=textw1, textw2=textw2,
        textw3=textw3, textw4=textw4,
        textw5=textw5, textw6=textw6,
        texts1=texts1, texts2=texts2,
        texts3=texts3, texts4=texts4,
        texts5=texts5, texts6=texts6,)


# %%
# Plot profile of worst sounding/sample
if plotWorstProfile:
    nPlots = 3
    iList = [TsortRefs[-1], TDsortRefs[-1], BsortRefs[-1]]
    tList = ['Temperature', 'Dewpoint', 'Combined']
    for i in range(nPlots):
        indx = iList[i]
        rap_wList = [rap_rmseTAll[indx, :],
                     rap_rmseTDAll[indx, :]]
        pred_wList = [rmseTAll[indx, :],
                      rmseTDAll[indx, :]]
        fp.plot_vertical_rmse(rap_wList,
                              pred_wList)
        print("Worst Predicted {} Profile: {}".format(
            tList[i], indx))
        print("  Sounding= {}".format(
            FILEtest[indx][:17]))
        print("  T RMSE= {:.3f}, TD RMSE={:.3f}".format(
            mean_rmseTAll[indx], mean_rmseTDAll[indx]))


# %%
# Plot profile of best sounding/sample
if plotBestProfile:
    nPlots = 3
    iList = [TsortRefs[0], TDsortRefs[0], BsortRefs[0]]
    tList = ['Temperature', 'Dewpoint', 'Combined']
    for i in range(nPlots):
        indx = iList[i]
        rap_wList = [rap_rmseTAll[indx, :],
                     rap_rmseTDAll[indx, :]]
        pred_wList = [rmseTAll[indx, :],
                      rmseTDAll[indx, :]]
        fp.plot_vertical_rmse(rap_wList,
                              pred_wList)
        print("Best Predicted {} Profile.".format(
            tList[i]))
        print("  T RMSE= {:.3f}, TD RMSE={:.3f}".format(
            mean_rmseTAll[indx], mean_rmseTDAll[indx]))


# %%
# Plot soundings for worst sounding/sample
if plotWorstSounding:
    nPlots = 3
    iList = [TsortRefs[-1], TDsortRefs[-1], BsortRefs[-1]]
    tList = ['Temperature', 'Dewpoint', 'Combined']
    for i in range(nPlots):
        indx = iList[i]
        RAOBD = {
            fp.PRESSURE_COLUMN_KEY:
            RAOBtest[indx, :, fm.RAOB_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAOBtest[indx, :, it],
            fp.DEWPOINT_COLUMN_KEY:
            RAOBtest[indx, :, id]}

        RAPD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAPtest[indx, :, itr],
            fp.DEWPOINT_COLUMN_KEY:
            RAPtest[indx, :, idr]}

        PREDD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_TEMPERATURE],
            fp.DEWPOINT_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_DEWPOINT]}

        title = FILEtest[indx][:17].upper()
        title += '  (RMSE T={:.2f}, TD={:.2f})'.\
            format(mean_rmseTAll[indx], mean_rmseTDAll[indx])
        print("Worst Predicted {} Profile: {}".format(
            tList[i], indx))
        print("  RMSE RAP T= {:.2f}, TD= {:.2f}".
              format(rap_mean_rmseTAll[indx], rap_mean_rmseTDAll[indx]))
        print("  RMSE PRED T= {:.2f}, TD= {:.2f}".
              format(mean_rmseTAll[indx], mean_rmseTDAll[indx]))
        print("  PRED RMSE IMPROVEMENT T= {:.2f}, TD= {:.2f}".
              format(improveTAll[indx], improveTDAll[indx]))
        fp.plot_sounding_results(
            RAOBD, RAPD, PREDD, title_string=title)


# %%
# Plot soundings for best sounding/sample
if plotBestSounding:
    nPlots = 3
    iList = [TsortRefs[0], TDsortRefs[0], BsortRefs[0]]
    tList = ['Temperature', 'Dewpoint', 'Combined']
    for i in range(nPlots):
        indx = iList[i]
        RAOBD = {
            fp.PRESSURE_COLUMN_KEY:
            RAOBtest[indx, :, fm.RAOB_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAOBtest[indx, :, it],
            fp.DEWPOINT_COLUMN_KEY:
            RAOBtest[indx, :, id]}

        RAPD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAPtest[indx, :, itr],
            fp.DEWPOINT_COLUMN_KEY:
            RAPtest[indx, :, idr]}

        PREDD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_TEMPERATURE],
            fp.DEWPOINT_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_DEWPOINT]}

        title = FILEtest[indx][:17].upper()
        title += '  (RMSE RAP T/D={:.2f}/{:.2f},'.\
            format(rap_mean_rmseTAll[indx], rap_mean_rmseTDAll[indx])
        title += ' PRED T/D={:.2f}/{:.2f})'.\
            format(mean_rmseTAll[indx], mean_rmseTDAll[indx])
        print("Best Predicted {} Profile: {}".
              format(tList[i], indx))
        print("  RMSE RAP T= {:.2f}, TD= {:.2f}".
              format(rap_mean_rmseTAll[indx], rap_mean_rmseTDAll[indx]))
        print("  RMSE PRED T= {:.2f}, TD= {:.2f}".
              format(mean_rmseTAll[indx], mean_rmseTDAll[indx]))
        print("  PRED RMSE IMPROVEMENT T= {:.2f}, TD= {:.2f}".
              format(improveTAll[indx], improveTDAll[indx]))
        fp.plot_sounding_results(
            RAOBD, RAPD, PREDD, title_string=title)


# %%
# Plot sounding of case where the improvement was the most
if plotBiggestImprovement:
    nPlots = 3
    iList = [TimproveRefs[-1], TDimproveRefs[-1], BimproveRefs[-1]]
    tList = ['Temperature', 'Dewpoint', 'Combined']
    for i in range(nPlots):
        indx = iList[i]
        RAOBD = {
            fp.PRESSURE_COLUMN_KEY:
            RAOBtest[indx, :, fm.RAOB_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAOBtest[indx, :, it],
            fp.DEWPOINT_COLUMN_KEY:
            RAOBtest[indx, :, id]}

        RAPD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAPtest[indx, :, itr],
            fp.DEWPOINT_COLUMN_KEY:
            RAPtest[indx, :, idr]}

        PREDD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_TEMPERATURE],
            fp.DEWPOINT_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_DEWPOINT]}

        title = FILEtest[indx][:17].upper()
        title += '  (RMSE RAP T/D={:.2f}/{:.2f},'.\
            format(rap_mean_rmseTAll[indx], rap_mean_rmseTDAll[indx])
        title += ' PRED T/D={:.2f}/{:.2f})'.\
            format(mean_rmseTAll[indx], mean_rmseTDAll[indx])

        print("Most Improved Predicted {} Profile: {}".
              format(tList[i], indx))
        print("  RMSE RAP T= {:.2f}, TD= {:.2f}".
              format(rap_mean_rmseTAll[indx], rap_mean_rmseTDAll[indx]))
        print("  RMSE PRED T= {:.2f}, TD= {:.2f}".
              format(mean_rmseTAll[indx], mean_rmseTDAll[indx]))
        print("  PRED RMSE IMPROVEMENT T= {:.2f}, TD= {:.2f}".
              format(improveTAll[indx], improveTDAll[indx]))
        fp.plot_sounding_results(
            RAOBD, RAPD, PREDD, title_string=title)


# %%
# Plot sounding of case where the degradation was the most
if plotBiggestDegredation:
    nPlots = 3
    iList = [TimproveRefs[0],
             TDimproveRefs[0],
             BimproveRefs[0]]
    tList = ['Temperature', 'Dewpoint', 'Combined']
    for i in range(nPlots):
        indx = iList[i]
        RAOBD = {
            fp.PRESSURE_COLUMN_KEY:
            RAOBtest[indx, :, fm.RAOB_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAOBtest[indx, :, it],
            fp.DEWPOINT_COLUMN_KEY:
            RAOBtest[indx, :, id]}

        RAPD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            RAPtest[indx, :, itr],
            fp.DEWPOINT_COLUMN_KEY:
            RAPtest[indx, :, idr]}

        PREDD = {
            fp.PRESSURE_COLUMN_KEY:
            RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
            fp.TEMPERATURE_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_TEMPERATURE],
            fp.DEWPOINT_COLUMN_KEY:
            PREDtest[indx, :, fm.ML_INDX_DEWPOINT]}

        title = FILEtest[indx][:17].upper()
        title += '  (T RAP/PRED={:.2f}/{:.2f},'.\
            format(rap_mean_rmseTAll[indx], mean_rmseTAll[indx])
        title += ' TD RAP/PRED={:.2f}/{:.2f})'.\
            format(rap_mean_rmseTDAll[indx], mean_rmseTDAll[indx])
        print("Most Degraded Predicted {} Profile.".format(tList[i]))
        print("  RMSE RAP T= {:.2f}, TD= {:.2f}, Combo= {:.2f}".
              format(rap_mean_rmseTAll[indx],
                     rap_mean_rmseTDAll[indx],
                     rap_mean_rmseBAll[indx]))
        print("  RMSE PRED T= {:.2f}, TD= {:.2f}, Combo= {:.2f}".
              format(mean_rmseTAll[indx], mean_rmseTDAll[indx],
                     mean_rmseBAll[indx]))
        print("  PRED RMSE IMPROVEMENT T= {:.2f}, TD= {:.2f}, Combo= {:.2f}".
              format(improveTAll[indx], improveTDAll[indx],
                     improveBAll[indx]))
        fp.plot_sounding_results(RAOBD, RAPD, PREDD, title_string=title)


# %%
# Map GOES patches
if mapGOESPatch:
    fp.plot_goes_patch_map(
        GOEStest[mapGOESPatchNum, :, :, :])


# %%
# Plot GOES patches
if plotGOESPatch:
    fp.plot_goes_patch_box(
        GOEStest[plotGOESPatchNum, :, :, :])


# %%
# Plot RTMA patches
if plotRTMAPatch:
    fp.plot_rtma_patch_box(
        RTMAtest[plotRTMAPatchNum, ...])


# %%
# Create combination plot
if plotCombo:
    nSamples = TDsortRefs.shape[0]
    for pType in plotComboType:
        for pTop in plotComboTop:
            if pType == 'TD' and pTop:
                indxList = TDsortRefs[:plotComboNum]
                print("Plotting Best TD References: {}".format(indxList))
                cStart = 1
            elif pType == 'TD' and not pTop:
                indxList = TDsortRefs[-plotComboNum:]
                cStart = nSamples - plotComboNum + 1
                print("Plotting Worst TD References: {}".format(indxList))
            elif pType == 'T' and pTop:
                indxList = TsortRefs[:plotComboNum]
                cStart = 1
                print("Plotting Best T References: {}".format(indxList))
            elif pType == 'T' and not pTop:
                indxList = TsortRefs[-plotComboNum:]
                cStart = nSamples - plotComboNum + 1
                print("Plotting Worse T References: {}".format(indxList))
            elif pType == 'I' and pTop:
                indxList = BimproveRefs[::-1][:plotComboNum]
                cStart = 1
                print("Plotting Most Improved References: {}".format(indxList))
            elif pType == 'I' and not pTop:
                indxList = BimproveRefs[::-1][-plotComboNum:]
                cStart = nSamples - plotComboNum + 1
                print("Plotting Most Degraded References: {}".format(indxList))
            else:
                print("UNRECOGNIZED PLOT OPTIONS: {} and {}".format(
                    pType, pTop))

            for indx in indxList:
                RAOBD = {
                    fp.PRESSURE_COLUMN_KEY:
                    RAOBtest[indx, :, fm.RAOB_INDX_PRESSURE].reshape(-1),
                    fp.TEMPERATURE_COLUMN_KEY:
                    RAOBtest[indx, :, it].reshape(-1),
                    fp.DEWPOINT_COLUMN_KEY:
                    RAOBtest[indx, :, id].reshape(-1)}

                RAPD = {
                    fp.PRESSURE_COLUMN_KEY:
                    RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
                    fp.TEMPERATURE_COLUMN_KEY:
                    RAPtest[indx, :, itr],
                    fp.DEWPOINT_COLUMN_KEY:
                    RAPtest[indx, :, idr]}

                PREDD = {
                    fp.PRESSURE_COLUMN_KEY:
                    RAPtest[indx, :, fm.RAP_INDX_PRESSURE],
                    fp.TEMPERATURE_COLUMN_KEY:
                    PREDtest[indx, :, fm.ML_INDX_TEMPERATURE],
                    fp.DEWPOINT_COLUMN_KEY:
                    PREDtest[indx, :, fm.ML_INDX_DEWPOINT]}

                expName = fm.get_exp_refname(expDict).upper()
                siteName = FILEtest[indx][:17].upper()
                title = "\n {} {}".format(expName, siteName)
                text1 = 'Mean '
                text2 = 'RMSE:'
                myFormat = fm.get_format(
                    rap_mean_rmseTAll[indx])
                text3 = ('RAP T= {' + myFormat + '}').\
                    format(rap_mean_rmseTAll[indx])
                myFormat = fm.get_format(
                    mean_rmseTAll[indx])
                text4 = (' ML T= {' + myFormat + '}').\
                    format(mean_rmseTAll[indx])
                myFormat = fm.get_format(
                    rap_mean_rmseTDAll[indx])
                text5 = ('TD= {' + myFormat + '}').\
                    format(rap_mean_rmseTDAll[indx])
                myFormat = fm.get_format(
                    mean_rmseTDAll[indx])
                text6 = ('TD= {' + myFormat + '}').\
                    format(mean_rmseTDAll[indx])

                textw1 = 'Wghtd'
                textw2 = 'RMSE:'
                myFormat = fm.get_format(
                    rap_weighted_rmseTAll[indx])
                textw3 = ('T= {' + myFormat + '}').\
                    format(rap_weighted_rmseTAll[indx])
                myFormat = fm.get_format(
                    weighted_rmseTAll[indx])
                textw4 = ('T= {' + myFormat + '}').\
                    format(weighted_rmseTAll[indx])
                myFormat = fm.get_format(
                    rap_weighted_rmseTDAll[indx])
                textw5 = ('TD= {' + myFormat + '}').\
                    format(rap_weighted_rmseTDAll[indx])
                myFormat = fm.get_format(
                    weighted_rmseTDAll[indx])
                textw6 = ('TD= {' + myFormat + '}').\
                    format(weighted_rmseTDAll[indx])

                texts1 = 'Sfc  '
                texts2 = 'RMSE:'
                myFormat = fm.get_format(
                    rap_mean_rmse_sfcTAll[indx])
                texts3 = ('T= {' + myFormat + '}').\
                    format(rap_mean_rmse_sfcTAll[indx])
                myFormat = fm.get_format(
                    mean_rmse_sfcTAll[indx])
                texts4 = ('T= {' + myFormat + '}').\
                    format(mean_rmse_sfcTAll[indx])
                myFormat = fm.get_format(
                    rap_mean_rmse_sfcTDAll[indx])
                texts5 = ('TD= {' + myFormat + '}').\
                    format(rap_mean_rmse_sfcTDAll[indx])
                myFormat = fm.get_format(
                    mean_rmse_sfcTDAll[indx])
                texts6 = ('TD= {' + myFormat + '}').\
                    format(mean_rmse_sfcTDAll[indx])

                if saveCombo:
                    fileName = dirFigs + pType \
                        + '{:0>4d}'.format(cStart) + '_' \
                        + fm.get_exp_refname(expDict) + '_' \
                        + siteName
                    fileName += '.png'
                else:
                    fileName = ''
                fp.plot_combo3(
                    RAOBD, RAPD, PREDD,
                    GOEStest[indx, ...],
                    RTMAtest[indx, ...],
                    saveDPI=saveComboDPI,
                    saveFile=fileName,
                    title=title,
                    text1=text1, text2=text2,
                    text3=text3, text4=text4,
                    text5=text5, text6=text6,
                    textw1=textw1, textw2=textw2,
                    textw3=textw3, textw4=textw4,
                    textw5=textw5, textw6=textw6,
                    texts1=texts1, texts2=texts2,
                    texts3=texts3, texts4=texts4,
                    texts5=texts5, texts6=texts6,)
                cStart += 1


# %%
