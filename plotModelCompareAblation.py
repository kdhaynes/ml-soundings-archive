"""
ML Soundings Project

Program to compare model performances.
Uses two scores, 1. - Normalized RMSE,
and an improvement (RAP - Pred)/RAP.

Top LIN: LIN015 (CRPS)
Top NN: NN107 (SHASH)  NN103 (MAEW)
Top UNet: UNet503 (MAEW)

Modified: 2023/07
"""


# %%
# Import Libraries
import numpy as np

from functions_plot import image_mscores_inv
from functions_plot import plot_bars_horiz
from functions_misc import get_exp_filenameResults
from functions_misc import get_improved_model
from functions_misc import pickle_read


# %%
# User Options
dirFigs = '/home/kdhaynes/ml_soundings/figs.temp/'
dirIn = '/mnt/data1/kdhaynes/mlsoundings/results/'

modelList = ['RAP',
             'LIN082', 'LIN083', 'LIN084', 'LIN015',
             'NN204', 'NN195', 'NN196', 'NN137',
             'UNet582', 'UNet583', 'UNet584', 'UNet503']
modelNames = ['RAP',
              'LIN', 'LIN R',
              'LIN G', 'LIN R+G',
              'NN', 'NN R',
              'NN G', 'NN R+G',
              'UNet', 'UNet R',
              'UNet G', 'UNet R+G']
keyList = ['mlT_test_rmse_sfc', 'mlTD_test_rmse_sfc',
           'mlT_test_rmse', 'mlTD_test_rmse',
           'mlCAPE_test_rmse', 'mlCIN_test_rmse']
keyListRAP = ['rapT_test_rmse_sfc', 'rapTD_test_rmse_sfc',
              'rapT_test_rmse', 'rapTD_test_rmse',
              'rapCAPE_test_rmse', 'rapCIN_test_rmse']
keyNames = ['SFC T', 'SFC TD', 'T', 'TD', 'CAPE', 'CIN']

saveStr = 'abl'
barTots = True
saveTots = False
sortPref = 'best'

plotImage = True
saveImage = False

useShannon = True


# %%
# Load results
nModels = len(modelList)
nKeys = len(keyList)
resultArray = np.empty((nKeys, nModels))

countM = 0
rDict = {}
fileName = get_exp_filenameResults(
    useShannon, None, expRef=modelList[1])
rDictT = pickle_read(fileName)
for i in range(len(keyList)):
    rmh = keyListRAP[i]
    mlh = keyList[i]
    resultArray[i, countM] = rDictT[rmh]
    rDictT[mlh] = rDictT[rmh]
rDict[modelList[0]] = rDictT
countM += 1

for modelRef in modelList[1:]:
    fileName = get_exp_filenameResults(
        useShannon, expRef=modelRef)
    rDictT = pickle_read(fileName)
    rDict[modelRef] = rDictT

    countK = 0
    for key in keyList:
        resultArray[countK, countM] = rDictT[key]
        countK += 1

    countM += 1


# %%
# Normalize
resultANorm = resultArray.copy()
for i in range(nKeys):
    keyMax = np.max(resultArray[i, :])
    keyMin = np.min(resultArray[i, :])
    resultANorm[i, :] = (resultArray[i, :] - keyMin) / (keyMax - keyMin)
resultANorm = 1. - resultANorm


# %%
# Get Most Improved and Best Model Scores
if barTots:
    improvedScores = get_improved_model(
        rDict,
        returnScoreUnsorted=True,
        verbose=False,
        verboseScore=False)
    improvedScores = np.array(improvedScores)
    bestScores = np.sum(resultANorm, axis=0)

    if sortPref == 'best':
        iSorts = np.argsort(bestScores)[::-1]
    elif sortPref == 'imp':
        iSorts = np.argsort(improvedScores)[::-1]
    else:
        avgScores = bestScores + improvedScores
        iSorts = np.argsort(avgScores)[::-1]
    improvedScoresSort = improvedScores[iSorts]
    bestScoresSort = bestScores[iSorts]
    modelNamesSort = np.array(modelNames)[iSorts]

    scoresA = np.concatenate(
        (np.expand_dims(bestScoresSort, axis=0),
         np.expand_dims(improvedScoresSort, axis=0)), axis=0)
    barNames = ['Total Norm RMSE Score', 'Total Improved Score']
    barNames = ['Total NRS', 'Total NIS']
    colorNames = ['navy', 'yellowgreen']

    if saveTots:
        if saveStr:
            saveFile = dirFigs + 'modelCompareTotScores_' + saveStr + '.png'
        else:
            saveFile = dirFigs + 'modelCompareTotScores.png'
    else:
        saveFile = ''
    plot_bars_horiz(modelNamesSort, scoresA, barNames,
                    colorList=colorNames,
                    saveFile=saveFile,
                    xlabel='Score')


# %%
# Create plot
if plotImage:
    if saveImage:
        if saveStr:
            saveFile = dirFigs + 'modelCompareScores_' + saveStr + '.png'
        else:
            saveFile = dirFigs + 'modelCompareScores.png'
    else:
        saveFile = ''
    image_mscores_inv(resultANorm,
                      figSize=(4, 8),
                      rotateX=65,
                      saveFile=saveFile,
                      valsX=keyNames,
                      valsY=modelNames)
