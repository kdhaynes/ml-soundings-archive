"""
ML Soudings Experiment Driver

Modified: 2023/07
"""


# %%
# Import Libraries
import functions_misc as fm
import functions_nn as fnn
import time

import var_opts as vo


# %%
# Set User Info
shuffleAll = True
shuffleSites = False

nTrials = 5
runList = [vo.exp615]

calcCAPE = True
savePredictions = True
saveResults = True

refGPU = 0
setGPU = True
restrictGPU = True
useShannon = True


# %%
# Setup GPU
if useShannon:
    fnn.setup_gpu(restrictGPU=restrictGPU,
                  setGPU=setGPU,
                  numGPU=refGPU)


# %%
# Perform Experiments
for expDict in runList:
    expName = fm.get_exp_refname(expDict)
    print('\nINFO: Running experiment {}'.format(expName))
    addTRef = fm.get_exp_addTRef(expDict)
    addTDRef = fm.get_exp_addTDRef(expDict)
    output_dims = fm.get_exp_outputRAOB(expDict)
    restrictData = fm.get_exp_restrict_data(expDict)

    data = fm.load_data(
        useShannon,
        includeCAPE=True,
        restrictData=restrictData,
        returnFileInfo=False,
        shuffleAll=shuffleAll,
        shuffleSites=shuffleSites)
    (_, _, FILEte) = fm.load_data(
        useShannon,
        includeCAPE=True,
        restrictData=restrictData,
        returnFileOnly=True,
        shuffleAll=shuffleAll,
        shuffleSites=shuffleSites,
        verbose=False)
    RAPCCte = data[17]
    RAPte = data[2]
    RAOBCCte = data[14]
    RAOBte = data[11]
    RAOBv = data[10]

    XTrain, YTrain, XVal, YVal, \
        XTest, YTest = fm.organize_data(expDict, data)
    if XTrain[0] is not None:
        nIn = XTrain[0].shape[1]
    else:
        nIn = 0

    # Loop model training
    vDictList = []
    modelList = []
    print("INFO:  training --")
    for im in range(nTrials):
        start_t = time.time()
        nnet = fnn.setup_nn(
            expDict, n_levs_in=nIn,
            addTRef=addTRef, addTDRef=addTDRef)

        nnet.train(XTrain, YTrain, validation=(XVal, YVal))

        PVal_mean = nnet.use(XVal, returnType='')
        PREDv = PVal_mean.reshape(RAOBv[:, :, output_dims].shape)
        vDict = fm.calculate_results(
            RAOBv, YVal, PVal_mean,
            calcCAPE=True, type='val')
        vDictList.append(vDict)
        modelList.append(nnet)
        print("  Trained iteration {}: {:.3f}s".format(
            im, time.time() - start_t))

    # Get the best model of the multiples trained
    nnetB = fm.get_best_trial_cape(
        vDictList, modelList,
        verbose=True)
    del vDictList
    del modelList

    # Make predictions on test data
    PTest_mean, PTest_std = nnetB.use(XTest)
    PREDte = PTest_mean.reshape(RAOBte[:, :, output_dims].shape)
    if PTest_std is not None:
        PREDstd = PTest_std.reshape(
            RAOBte[:, :, output_dims].shape)
    else:
        PREDstd = None

    n1 = RAOBte.shape[0]
    n2 = RAOBte.shape[1]
    ne = expDict['modelOpts']['uq_n_members']
    PTest_ens = nnetB.use(XTest, returnType='mc')
    if PTest_ens.shape[-1] == ne:
        PREDmc = PTest_ens.reshape((n1, n2, 2, ne))
    else:
        PREDmc = None

    if savePredictions:
        pDict = {'FILEtest': FILEte,
                 'PREDtest': PREDte,
                 'PREDstd': PREDstd,
                 'PREDmc': PREDmc}
        filePreds = \
            fm.get_exp_filenamePreds(
                useShannon, expDict)
        fm.pickle_dump(filePreds, pDict, verbose=False)
        print("INFO: predictions saved --")
        print("  {}".format(filePreds))

    # Get results
    if saveResults:
        hAvg = \
            fm.compute_average_altitude(
                RAOBte[:, :, fm.RAOB_INDX_HEIGHT])
        pAvg = \
            fm.compute_average_altitude(
                RAOBte[:, :, fm.RAOB_INDX_PRESSURE])
        rDict = {
            'dataOpts': fm.get_exp_dataOpts(expDict),
            'modelOpts': fm.get_exp_modelOpts(expDict),
            'runOpts': fm.get_exp_runOpts(expDict),
            'history': nnetB.history,
            'hAvg': hAvg,
            'pAvg': pAvg}
        rDict = fm.calculate_results_rap(
            RAOBte, RAPte,
            calcCAPE=calcCAPE,
            RAOBCC=RAOBCCte,
            RAPCC=RAPCCte,
            rDict=rDict)
        rDict = fm.calculate_results(
            RAOBte, YTest, PTest_mean,
            calcCAPE=calcCAPE,
            RAOBCC=RAOBCCte,
            RAPP=RAPte[..., fm.RAOB_INDX_PRESSURE],
            rDict=rDict)

        fileResults = fm.get_exp_filenameResults(
            useShannon, expDict)
        fm.pickle_dump(fileResults, rDict, verbose=False)
        print('INFO: results saved --')
        print('  {}'.format(fileResults))
        fm.print_results(rDict)

    print('INFO: Finished Experiment {} ({:.2f}s)'.format(
        expName, time.time() - start_t))
    fnn.setup_clear()
    del nnetB


print("\nFinished All Experiments.")
