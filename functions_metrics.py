"""
ML Soundings Research

Katherine Haynes
Modified: 2021/11
"""

import numpy as np


def crps_score(A, B):

    nA = A.size
    asorted = np.sort(A.reshape(-1))
    aproportional = 1. * np.arange(nA) / (nA - 1)

    nB = B.size
    bsorted = np.sort(B.reshape(-1))

    if nB > nA:
        iii = np.linspace(0, nB - 1, num=nA).astype(int)
        bsorted2 = bsorted[iii]
    else:
        bsorted2 = bsorted

    dp = aproportional[2] - aproportional[1]
    score = np.sum(np.abs(np.subtract(asorted, bsorted2))) * dp
    return score


def merr(A, B):
    return np.mean(np.subtract(A, B))


def mae(A, B):
    return np.mean(np.abs(np.subtract(A, B)))


def mae2D(A2D, B):
    return np.mean(np.abs(np.subtract(A2D[..., 0], B)))


def mse(A, B):
    return np.mean(np.square(np.subtract(A, B)))


def mse2D(A2D, B):
    return np.mean(np.square(
        np.subtract(A2D[..., 0], B)))


def mse_weighted_ps(A, B, PS):
    PSTot = np.sum(PS, axis=1)
    PSWeights = np.divide(PS, PSTot[:, None])
    return np.mean(np.sum(
        np.multiply(PSWeights,
                    np.square(np.subtract(A, B))), axis=1))


def mse_weighted_ps2D(A2D, B):
    AV = A2D[..., 0]
    AW = A2D[..., 1]
    return np.mean(np.sum(
        np.multiply(AW,
                    np.square(np.subtract(AV, B))), axis=1))


def percent_correct(actual, predicted):
    return 100 * np.mean(actual == predicted)


def rmse(A, B):
    return np.sqrt(np.mean((A - B)**2))


def rmse2D(A2D, B):
    return np.sqrt(np.mean(np.square(
        np.subtract(A2D[..., 0], B))))


def rmseT(Aall, Ball, nLevBoth=512, refT=0):
    if Aall.shape[1] == nLevBoth:
        nlevs = int(nLevBoth * 0.5)
        outShape = (Aall.shape[0], nlevs, 2)
        A2D = np.reshape(Aall, outShape)
        B2D = np.reshape(Ball, outShape)
        AHere = A2D[..., refT]
        BHere = B2D[..., refT]
    elif Aall.shape[1] == 2:
        AHere = Aall[..., refT]
        BHere = Ball[..., refT]
    else:
        AHere = Aall
        BHere = Ball
    return rmse(AHere, BHere)


def rmseTD(Aall, Ball, nLevBoth=512, refTD=1):
    if Aall.shape[1] == nLevBoth:
        nlevs = int(nLevBoth * 0.5)
        outShape = (Aall.shape[0], nlevs, 2)
        A2D = np.reshape(Aall, outShape)
        B2D = np.reshape(Ball, outShape)
        AHere = A2D[..., refTD]
        BHere = B2D[..., refTD]
    elif Aall.shape[1] == 2:
        AHere = Aall[..., refTD]
        BHere = Ball[..., refTD]
    else:
        AHere = Aall
        BHere = Ball
    return rmse(AHere, BHere)


def rmse_negative(A, B):
    return -np.sqrt(np.mean((A - B)**2))


def rmse_sfc(Aall, Ball, nLevBoth=512, sfcLev=25):
    if Aall.shape[1] == nLevBoth:
        nlevs = int(nLevBoth * 0.5)
        outShape = (Aall.shape[0], nlevs, 2)
        A2D = np.reshape(Aall, outShape)
        B2D = np.reshape(Ball, outShape)
        AHere = A2D[:, :sfcLev, :]
        BHere = B2D[:, :sfcLev, :]
    else:
        AHere = Aall[..., :sfcLev]
        BHere = Ball[..., :sfcLev]
    return rmse(AHere, BHere)


def rmse_sfcT(Aall, Ball, nLevBoth=512, refT=0, sfcLev=25):
    if Aall.shape[1] == nLevBoth:
        nlevs = int(nLevBoth * 0.5)
        outShape = (Aall.shape[0], nlevs, 2)
        A2D = np.reshape(Aall, outShape)
        B2D = np.reshape(Ball, outShape)
        AHere = A2D[..., refT]
        BHere = B2D[..., refT]
    else:
        AHere = Aall
        BHere = Ball
    return rmse_sfc(AHere, BHere, sfcLev=sfcLev)


def rmse_sfcTD(Aall, Ball, nLevBoth=512, refTD=1, sfcLev=25):
    if Aall.shape[1] == nLevBoth:
        nlevs = int(nLevBoth * 0.5)
        outShape = (Aall.shape[0], nlevs, 2)
        A2D = np.reshape(Aall, outShape)
        B2D = np.reshape(Ball, outShape)
        AHere = A2D[..., refTD]
        BHere = B2D[..., refTD]
    else:
        AHere = Aall
        BHere = Ball
    return rmse_sfc(AHere, BHere, sfcLev=sfcLev)


def rmse_sfc_combo(Aall, Ball, sfcLev=25, sfcWeight=0.6):
    rmse1 = rmse(Aall, Ball)
    rmse2 = rmse(Aall[..., :sfcLev], Ball[..., :sfcLev])

    rmse1Weight = 1.0 - sfcWeight
    return rmse1 * rmse1Weight + rmse2 * sfcWeight


def rmse_weighted_ps(A, B, PS):
    if len(A.shape) >= 2:
        PSTot = np.sum(PS, axis=1)
        PSWeights = np.divide(PS, PSTot[:, None])
        return np.sqrt(np.mean(np.sum(
            np.multiply(PSWeights, np.square(np.subtract(A, B))), axis=1)))
    else:
        PSTot = np.sum(PS)
        PSWeights = np.divide(PS, PSTot)
        return np.sqrt(np.sum(np.multiply(
            PSWeights,
            np.square(np.subtract(A, B)))))


def rmse_weighted_ps2D(A2D, B):
    A = A2D[..., 0]
    PS = A2D[..., 1]
    PSTot = np.sum(PS, axis=1)
    PSWeights = np.divide(PS, PSTot[:, None])
    return np.sqrt(np.mean(np.sum(
        np.multiply(PSWeights, np.square(np.subtract(A, B))), axis=1)))


def rmse_weighted_ps_profile(A, B, PS):
    """
    params:
        A, B, PS: np.array (N, 256)
    """

    if len(A.shape) < 2:
        print("Error calculating weighted profile RMSE.")
        return -999

    PSMean = np.mean(PS, axis=0)
    PSTot = np.sum(PSMean)
    PSWeights = np.divide(PSMean, PSTot)

    sqErr = np.square(np.subtract(A, B))
    rmse_weighted = np.sqrt(np.sum(np.multiply(
        sqErr, PSWeights), axis=1))
    return rmse_weighted


def rmse_weighted_ps_profile2D(A2D, B):
    """
    params:
        A: np.array (N, 256, 2)
           indice 0: values
                  1: weights
        B: np.array (N, 256)
    """

    A = A2D[..., 0]
    PSWeights = A2D[..., 1]

    sqErr = np.square(np.subtract(A, B))
    # rmse_unweighted = np.sqrt(np.mean(sqErr, axis=1))
    rmse_weighted = np.sqrt(
        np.sum(np.multiply(sqErr, PSWeights), axis=1))
    return rmse_weighted
