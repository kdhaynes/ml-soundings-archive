"""
ML Soundings Project

Plot error bars on a sounding.

UNet Soundings (Tot/Sfc):
Worst T: 155/1971  920
Worst TD: 2366/1966    1934
Degraded T: 1514/237  1162
Degraded TD: 2535/1674

LIN (Tot/Sfc):
Worst T: 155/1971
Worst TD: 2366/1966
Degraded T: 60/808
Degraded TD: 2297/2001

Modified: 2022/11
"""

# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import numpy as np

from functions_metrics import rmse
from importlib import reload
from var_opts import exp527 as expDict


# %%
# User Options
expName = 'UNet'

index = 1762
temp_min = -15
temp_max = 35

refLow = 0
refHigh = 246

restrictCAPELow = None
restrictCAPEHigh = None
restrictData = [4, 8]

colorMLL = 'mediumslateblue'
colorMLUQ = 'mediumslateblue'
colorRAP = 'deeppink'
colorRAOB = 'black'

plotSoundingUQ = False
saveSoundingUQ = False
saveName = 'degradedTDSfc2'
showLegendUQ = True
showLegendUQLoc = 'lower left'

plotMeanSoundingUQ = True
saveMeanSoundingUQ = False
temp_minM = -15
temp_maxM = 40

plotConvAct = False
saveConvAct = False
convCAPE = 800
convCIN = 40

useShannon = True
shuffleAll = True
shuffleSites = False


# %%
# Read Data
(RAPtr, RAPv, RAPte,
 RTMAtr, RTMAv, RTMAte,
 GOEStr, GOESv, GOESte,
 RAOBtr, RAOBv, RAOBte,
 FILEtr, FILEv, FILEte,
 RAOBCCtr, RAOBCCv, RAOBCCte,
 RAPCCtr, RAPCCv, RAPCCte) = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictCAPELow=restrictCAPELow,
    restrictCAPEHigh=restrictCAPEHigh,
    restrictData=restrictData,
    returnFileInfo=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
YTest = fm.organize_data_y(RAOBte)
pAvg = fm.compute_average_altitude(RAOBte[:, :, 0])
print("Read True Data.")
print("    YTest Shape= {}".format(YTest.shape))


# %%
# Open Predictions
filePreds = fm.get_exp_filenamePreds(
    useShannon, expDict)
refName = fm.get_exp_refname(expDict)
nTargets = 2
pDict = fm.pickle_read(filePreds)
PTest_mean = pDict['PREDtest']
PTest_std = pDict['PREDstd']
print("Read {} Results File.".format(refName))
print("    PTest_mean Shape: {}".format(PTest_mean.shape))
print("    PTest_std Shape: {}".format(PTest_std.shape))


# %%
# Plot uncertainty on specific sounding
if plotSoundingUQ:
    reload(fp)
    RAOBHere = RAOBte[index, :refHigh, 1:3]
    RAPHere = RAPte[index, :refHigh, 1:3]
    PTestHere = PTest_mean[index, :refHigh, :]
    rapRMSE = rmse(RAOBHere, RAPHere)
    mlRMSE = rmse(RAOBHere, PTestHere)

    sDict = {}
    sDict['RAOB'] = {
        fm.PRESSURE_COLUMN_KEY: RAOBte[
            index, :refHigh, fm.RAOB_INDX_PRESSURE],
        fm.TEMPERATURE_COLUMN_KEY: RAOBte[
            index, :refHigh, fm.RAOB_INDX_TEMPERATURE],
        fm.DEWPOINT_COLUMN_KEY: RAOBte[
            index, :refHigh, fm.RAOB_INDX_DEWPOINT],
        'color': colorRAOB}

    sDict[f'RAP ({rapRMSE:.2f})'] = {
        fm.PRESSURE_COLUMN_KEY:
        RAPte[index, :refHigh, fm.RAP_INDX_PRESSURE],
        fm.TEMPERATURE_COLUMN_KEY:
        RAPte[index, :refHigh, fm.RAP_INDX_TEMPERATURE],
        fm.DEWPOINT_COLUMN_KEY:
        RAPte[index, :refHigh, fm.RAP_INDX_DEWPOINT],
        'color': colorRAP}

    sDict[f'{expName} ({mlRMSE:.2f})'] = {
        fm.PRESSURE_COLUMN_KEY:
        RAPte[index, :refHigh, fm.RAP_INDX_PRESSURE],
        fm.TEMPERATURE_COLUMN_KEY:
        PTest_mean[index, :refHigh, fm.ML_INDX_TEMPERATURE],
        fm.DEWPOINT_COLUMN_KEY:
        PTest_mean[index, :refHigh, fm.ML_INDX_DEWPOINT],
        'color': colorMLL,
        'colorUQ': colorMLUQ,
        't_err_left': PTest_mean[index, :refHigh, 0]
            - 1.96 * PTest_std[index, :refHigh, 0],
        't_err_right': PTest_mean[index, :refHigh, 0]
            + 1.96 * PTest_std[index, :refHigh, 0],
        'td_err_left': PTest_mean[index, :refHigh, 1]
            - 1.96 * PTest_std[index, :refHigh, 1],
        'td_err_right': PTest_mean[index, :refHigh, 1]
            + 1.96 * PTest_std[index, :refHigh, 1]
    }

    print('RAP RMSE T= {:.4f}, TD= {:.4f}'.format(
        rmse(RAOBHere[..., 0], RAPHere[..., 0]),
        rmse(RAOBHere[..., 1], RAPHere[..., 1])))
    print("ML RMSE T= {:.4f}, TD= {:.4f}".format(
        rmse(RAOBHere[..., 0], PTestHere[..., 0]),
        rmse(RAOBHere[..., 1], PTestHere[..., 1])))

    meanSfcTUQ = np.mean(PTest_std[:, :25, 0])
    meanSfcTDUQ = np.mean(PTest_std[:, :25, 1])
    hereSfcTUQ = np.mean(PTest_std[index, :25, 0])
    hereSfcTDUQ = np.mean(PTest_std[index, :25, 1])
    print("ML SFC T UQ Here: {:.3f}, Avg: {:.3f}".
          format(hereSfcTUQ, meanSfcTUQ))
    print("ML Sfc TD UQ Here: {:.3f}, Avg: {:.3f}".
          format(hereSfcTDUQ, meanSfcTDUQ))

    if saveSoundingUQ:
        saveFile = 'soundingUQ_{}.png'.format(saveName)
    else:
        saveFile = None
    title = FILEte[index].upper()
    title = title[:title.index(':')]
    fp.plot_sounding_results_uq(
        sDict,
        cape_cin=False,
        show_legend=showLegendUQ,
        show_legend_loc=showLegendUQLoc,
        temp_min=temp_min,
        temp_max=temp_max,
        file_name=saveFile,
        title_string=title,
        use_uq_gray=False)


# %%
# Plot Mean Sounding and UQ
if plotMeanSoundingUQ:
    reload(fp)
    RAOBpm = RAOBte[:, :, fm.RAOB_INDX_PRESSURE].mean(axis=0)
    RAOBtm = RAOBte[:, :, fm.RAOB_INDX_TEMPERATURE].mean(axis=0)
    RAOBdm = RAOBte[..., fm.RAOB_INDX_DEWPOINT].mean(axis=0)
    RAPpm = RAPte[..., fm.RAP_INDX_PRESSURE].mean(axis=0)
    RAPtm = RAPte[..., fm.RAP_INDX_TEMPERATURE].mean(axis=0)
    RAPdm = RAPte[..., fm.RAP_INDX_DEWPOINT].mean(axis=0)
    MLpm = RAPpm
    MLtm = PTest_mean[..., fm.ML_INDX_TEMPERATURE].mean(axis=0)
    MLdm = PTest_mean[..., fm.ML_INDX_DEWPOINT].mean(axis=0)
    MLtstdm = PTest_std[..., fm.ML_INDX_TEMPERATURE].mean(axis=0)
    MLdstdm = PTest_std[..., fm.ML_INDX_DEWPOINT].mean(axis=0)
    t_err_left = MLtm - 1.96 * MLtstdm
    t_err_right = MLtm + 1.96 * MLtstdm
    td_err_left = MLdm - 1.96 * MLdstdm
    td_err_right = MLdm + 1.96 * MLdstdm

    sDict = {}
    sDict['RAOB'] = {
        fm.PRESSURE_COLUMN_KEY: RAOBpm,
        fm.TEMPERATURE_COLUMN_KEY: RAOBtm,
        fm.DEWPOINT_COLUMN_KEY: RAOBdm,
        'color': colorRAOB}

    sDict['RAP'] = {
        fm.PRESSURE_COLUMN_KEY: RAPpm,
        fm.TEMPERATURE_COLUMN_KEY: RAPtm,
        fm.DEWPOINT_COLUMN_KEY: RAPdm,
        'color': colorRAP}

    sDict[expName] = {
        fm.PRESSURE_COLUMN_KEY: MLpm,
        fm.TEMPERATURE_COLUMN_KEY: MLtm,
        fm.DEWPOINT_COLUMN_KEY: MLdm,
        'color': colorMLL,
        'colorUQ': colorMLUQ,
        't_err_left': t_err_left,
        't_err_right': t_err_right,
        'td_err_left': td_err_left,
        'td_err_right': td_err_right
    }

    RAOBHere = RAOBte[index, :, 1:3]
    RAPHere = RAPte[index, :, 1:3]
    print('RAP RMSE: {:.4f}'.format(
        rmse(RAOBHere, RAPHere)))

    if saveMeanSoundingUQ:
        saveFile = 'soundingUQ_mean.png'
    else:
        saveFile = None
    title = 'Mean Test Sounding'
    fp.plot_sounding_results_uq(
        sDict,
        cape_cin=False,
        temp_min=temp_minM,
        temp_max=temp_maxM,
        file_name=saveFile,
        show_legend=True,
        title_string=title,
        use_uq_gray=False)


# %%
# Plot Mean Convectively Active
if plotConvAct:
    nCA = 0
    nSamples = RAOBte.shape[0]
    nLevs = RAOBte.shape[1]
    for i in range(nSamples):
        if RAOBCCte[i, 0] > convCAPE \
                and RAOBCCte[i, 1] < convCIN:
            nCA += 1
    print("Num Convectively-Active Samples: {}".format(nCA))

    caSize = (nCA, nLevs)
    caRAPp = np.empty(caSize)
    caRAPt = np.empty(caSize)
    caRAPd = np.empty(caSize)
    caRAOBp = np.empty(caSize)
    caRAOBt = np.empty(caSize)
    caRAOBd = np.empty(caSize)
    caMLt = np.empty(caSize)
    caMLd = np.empty(caSize)
    caMLtstd = np.empty(caSize)
    caMLdstd = np.empty(caSize)
    count = 0
    for i in range(nSamples):
        if RAOBCCte[i, 0] > convCAPE \
                and RAOBCCte[i, 1] < convCIN:
            caRAPp[count, :] = RAPte[i, :, fm.RAP_INDX_PRESSURE]
            caRAPt[count, :] = RAPte[i, :, fm.RAP_INDX_TEMPERATURE]
            caRAPd[count, :] = RAPte[i, :, fm.RAP_INDX_DEWPOINT]
            caRAOBp[count, :] = RAOBte[i, :, fm.RAOB_INDX_PRESSURE]
            caRAOBt[count, :] = RAOBte[i, :, fm.RAOB_INDX_TEMPERATURE]
            caRAOBd[count, :] = RAOBte[i, :, fm.RAOB_INDX_DEWPOINT]
            caMLt[count, :] = PTest_mean[i, :, 0]
            caMLd[count, :] = PTest_mean[i, :, 1]
            caMLtstd[count, :] = PTest_std[i, :, 0]
            caMLdstd[count, :] = PTest_std[i, :, 1]
            count += 1

    RAOBpm = caRAOBp.mean(axis=0)
    RAOBtm = caRAOBt.mean(axis=0)
    RAOBdm = caRAOBd.mean(axis=0)
    RAPpm = caRAPp.mean(axis=0)
    RAPtm = caRAPt.mean(axis=0)
    RAPdm = caRAPd.mean(axis=0)
    MLpm = RAPpm
    MLtm = caMLt.mean(axis=0)
    MLdm = caMLd.mean(axis=0)
    MLtstdm = caMLtstd.mean(axis=0)
    MLdstdm = caMLdstd.mean(axis=0)
    t_err_left = MLtm - 1.96 * MLtstdm
    t_err_right = MLtm + 1.96 * MLtstdm
    td_err_left = MLdm - 1.96 * MLdstdm
    td_err_right = MLdm + 1.96 * MLdstdm

    sDict = {}
    sDict['RAOB'] = {
        fm.PRESSURE_COLUMN_KEY: RAOBpm,
        fm.TEMPERATURE_COLUMN_KEY: RAOBtm,
        fm.DEWPOINT_COLUMN_KEY: RAOBdm,
        'color': colorRAOB}

    sDict['RAP'] = {
        fm.PRESSURE_COLUMN_KEY: RAPpm,
        fm.TEMPERATURE_COLUMN_KEY: RAPtm,
        fm.DEWPOINT_COLUMN_KEY: RAPdm,
        'color': colorRAP}

    sDict[expName] = {
        fm.PRESSURE_COLUMN_KEY: MLpm,
        fm.TEMPERATURE_COLUMN_KEY: MLtm,
        fm.DEWPOINT_COLUMN_KEY: MLdm,
        'color': colorMLL,
        'colorUQ': colorMLUQ,
        't_err_left': t_err_left,
        't_err_right': t_err_right,
        'td_err_left': td_err_left,
        'td_err_right': td_err_right
    }

    RAOBHere = RAOBte[index, :, 1:3]
    RAPHere = RAPte[index, :, 1:3]
    print('RAP RMSE: {:.4f}'.format(
        rmse(RAOBHere, RAPHere)))

    if saveConvAct:
        saveFile = 'soundingUQ_meanCA.png'
    else:
        saveFile = None
    title = 'Mean Test Convectively Active'
    fp.plot_sounding_results_uq(
        sDict,
        cape_cin=False,
        temp_min=temp_minM,
        temp_max=temp_maxM,
        file_name=saveFile,
        show_legend=False,
        title_string=title,
        use_uq_gray=False)
