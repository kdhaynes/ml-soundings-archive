"""
ML Soundings Research

Functions to predict CAPE/CIN directly.

Modified: 2023/07
"""

from functions_metrics import mae, rmse
from functions_misc import combine_layers, combine_layersA
from functions_misc import CAPE_INDX as CAPEI
from functions_misc import CIN_INDX as CINI
from IPython.display import display

import numpy as np
import pandas as pd


NAMES_TO_SHOW_ORIG_CAPE = [
    'ml_cape_cin_test_mae', 'rap_cape_cin_test_mae',
    'ml_cape_cin_test_rmse', 'rap_cape_cin_test_rmse',
    'ml_cape_test_mae', 'rap_cape_test_mae',
    'ml_cape_test_rmse', 'rap_cape_test_rmse',
    'ml_cin_test_mae', 'rap_cin_test_mae',
    'ml_cin_test_rmse', 'rap_cin_test_rmse']
NAMES_TO_SHOW_CAPE = [
    'ML MAE', 'RAP MAE',
    'ML RMSE', 'RAP RMSE',
    'ML CAPE MAE', 'RAP CAPE MAE',
    'ML CAPE RMSE', 'RAP CAPE RMSE',
    'ML CIN MAE', 'RAP CIN MAE',
    'ML CIN RMSE', 'RAP CIN RMSE']
NAMES_TO_SHOW_IMPROVE_CAPE_ORIG = [
    'cape_test_mae', 'cape_test_rmse',
    'cin_test_mae', 'cin_test_rmse'
]
NAMES_TO_SHOW_IMPROVE_CAPE = [
    '% CAPE MAE', '% CAPE RMSE',
    '% CIN MAE', '% CIN RMSE']


def calculate_results_cape(
        Y, Pred,
        badval=np.nan,
        model='ml', type='test'):

    # setup dictionary for results
    rDict = {}

    # calculate combined errors
    rDict[f'{model}_cape_cin_{type}_mae'] = \
        badval
    rDict[f'{model}_cape_cin_{type}_rmse'] = \
        badval

    # calculate cape errors
    rDict[f'{model}_cape_{type}_mae'] = \
        mae(Y, Pred)
    rDict[f'{model}_cape_{type}_rmse'] = \
        rmse(Y, Pred)

    rDict[f'{model}_cin_{type}_mae'] = badval
    rDict[f'{model}_cin_{type}_rmse'] = badval

    # calculate mean and std of differences
    diff = np.subtract(Pred, Y)
    rDict[f'{model}_cape_{type}_diff_mean'] = \
        np.mean(diff)
    rDict[f'{model}_cape_{type}_diff_std'] = \
        np.std(diff)

    rDict[f'{model}_cin_{type}_diff_mean'] = badval
    rDict[f'{model}_cin_{type}_diff_std'] = badval

    return rDict


def calculate_results_cape_cin(
        Y, Pred,
        model='ml', type='test'):

    # setup dictionary for results
    rDict = {}

    # calculate combined errors
    rDict[f'{model}_cape_cin_{type}_mae'] = \
        mae(Y, Pred)
    rDict[f'{model}_cape_cin_{type}_rmse'] = \
        rmse(Y, Pred)

    # calculate cape errors
    rDict[f'{model}_cape_{type}_mae'] = \
        mae(Y[:, CAPEI], Pred[:, CAPEI])
    rDict[f'{model}_cape_{type}_rmse'] = \
        rmse(Y[:, CAPEI], Pred[:, CAPEI])

    rDict[f'{model}_cin_{type}_mae'] = \
        mae(Y[:, CINI], Pred[:, CINI])
    rDict[f'{model}_cin_{type}_rmse'] = \
        rmse(Y[:, CINI], Pred[:, CINI])

    # calculate mean and std of differences
    diff = np.subtract(Pred, Y)
    rDict[f'{model}_cape_{type}_diff_mean'] = \
        np.mean(diff[:, CAPEI])
    rDict[f'{model}_cape_{type}_diff_std'] = \
        np.std(diff[:, CAPEI])

    rDict[f'{model}_cin_{type}_diff_mean'] = \
        np.mean(
        diff[:, CINI])
    rDict[f'{model}_cin_{type}_diff_std'] = \
        np.std(diff[:, CINI])

    return rDict


def calculate_results_cin(
    Y, Pred,
    badval=np.nan,
        model='ml', type='test'):

    # setup dictionary for results
    rDict = {}

    # calculate combined errors
    rDict[f'{model}_cape_cin_{type}_mae'] = \
        badval
    rDict[f'{model}_cape_cin_{type}_rmse'] = \
        badval

    # calculate cape errors
    rDict[f'{model}_cape_{type}_mae'] = badval
    rDict[f'{model}_cape_{type}_rmse'] = badval

    rDict[f'{model}_cin_{type}_mae'] = \
        mae(Y, Pred)
    rDict[f'{model}_cin_{type}_rmse'] = \
        rmse(Y, Pred)

    # calculate mean and std of differences
    diff = np.subtract(Pred, Y)
    rDict[f'{model}_cin_{type}_diff_mean'] = \
        np.mean(diff)
    rDict[f'{model}_cin_{type}_diff_std'] = \
        np.std(diff)

    rDict[f'{model}_cape_{type}_diff_mean'] = badval
    rDict[f'{model}_cape_{type}_diff_std'] = badval

    return rDict


def df_results_show_cape(
    dictIn,
        names_to_show_orig=NAMES_TO_SHOW_ORIG_CAPE,
        names_to_show=NAMES_TO_SHOW_CAPE,
        num_to_show=-1,
        sort=False,
        sortName='cape_cin_test_rmse'):
    df = pd.DataFrame.from_dict(
        dictIn, orient='index')
    dfShow = df[names_to_show_orig]
    for i in range(len(names_to_show_orig)):
        dfShow = dfShow.rename(columns={
            names_to_show_orig[i]: names_to_show[i]})
    if sort:
        if num_to_show > 0:
            num_to_show = np.min([dfShow.shape[0], num_to_show])
        display(dfShow.sort_values(sortName)[:num_to_show])
    else:
        display(dfShow)

    return dfShow


def df_results_show_improve_cape(
    dictIn,
        names_to_show_orig=NAMES_TO_SHOW_IMPROVE_CAPE_ORIG,
        names_to_show=NAMES_TO_SHOW_IMPROVE_CAPE,
        num_to_show=-1,
        sort=False,
        sortName='mlTD_sfc'):

    df = pd.DataFrame.from_dict(dictIn, orient='index')
    nDiff = int(len(names_to_show))
    count = 0
    for i in range(nDiff):
        nameh = names_to_show_orig[count]
        if i == 0:
            nameh
            dfShow = \
                (df[f'rap_{nameh}']
                 - df[f'ml_{nameh}']) \
                / df[f'rap_{nameh}'] * 100.
            dfShow = pd.DataFrame(
                dfShow, columns=[names_to_show[i]])
        else:
            dfShow[names_to_show[i]] = \
                (df[f'rap_{nameh}']
                 - df[f'ml_{nameh}']) \
                / df[f'rap_{nameh}'] * 100.
        count += 1

    if sort:
        if num_to_show > 0:
            num_to_show = np.min(
                [dfShow.shape[0], num_to_show])
        display(dfShow.sort_values(
            sortName, ascending=False)[:num_to_show])
    else:
        display(dfShow)

    return dfShow


def organize_data_cape(expDict, data,
                       im_dim=(1, 2), verbose=True):

    try:
        dataOpts = expDict['dataOpts']
        input_channels_goes = dataOpts['input_channels_goes']
        input_channels_rap = dataOpts['input_channels_rap']
        input_channels_rtma = dataOpts['input_channels_rtma']
        input_rap_cape_cin = dataOpts['input_rap_cape_cin']
    except KeyError:
        print("Missing Input Channel Information.")
        return None, None, None, None, None, None

    try:
        vcombine = dataOpts['vcombine']
    except KeyError:
        vcombine = []

    try:
        output_cape_cin = dataOpts['output_cape_cin']
    except KeyError:
        print("Missing Output Specification, Predicting CAPE and CIN.")
        output_cape_cin = [0, 1]

    if verbose:
        print('INFO: data organization --')
        print("   RAP Input: {}".format(input_channels_rap))
        print("   Vertical Combination: {}".format(vcombine))
        print("   RTMA Input: {}".format(input_channels_rtma))
        print("   GOES Input: {}".format(input_channels_goes))
        print("   RAP_CAPE_CIN Input: {}".format(input_rap_cape_cin))
        print("   Output: {}".format(output_cape_cin))
    (RAPtrain, RAPval, RAPtest,
     RTMAtrain, RTMAval, RTMAtest,
     GOEStrain, GOESval, GOEStest,
     _, _, _,
     RAOBtrain_cape_cin, RAOBval_cape_cin, RAOBtest_cape_cin,
     RAPtrain_cape_cin, RAPval_cape_cin, RAPtest_cape_cin) = data

    if vcombine:
        if len(vcombine) == 1:
            RAPtr, RAPv, RAPte = combine_layers(
                vcombine[0], RAPtrain, RAPval, RAPtest)
        else:
            RAPtr, RAPv, RAPte = combine_layersA(
                vcombine[0], vcombine[1],
                RAPtrain, RAPval, RAPtest)
    else:
        RAPtr = RAPtrain
        RAPv = RAPval
        RAPte = RAPtest

    #
    # train
    Xtrain = RAPtr[:, :, input_channels_rap].reshape(
        RAPtr.shape[0], -1)
    if input_channels_rtma:
        Xtrain = np.hstack(
            (Xtrain,
                RTMAtrain[:, :, :, input_channels_rtma].mean(axis=im_dim)))
    if input_channels_goes:
        Xtrain = np.hstack(
            (Xtrain, GOEStrain[:, :, :,
                               input_channels_goes].mean(axis=im_dim)))
    if input_rap_cape_cin:
        Xtrain = np.hstack(
            (Xtrain, RAPtrain_cape_cin[..., input_rap_cape_cin]))
    Ytrain = RAOBtrain_cape_cin[..., output_cape_cin]

    # validation
    Xval = RAPv[:, :, input_channels_rap].reshape(RAPv.shape[0], -1)
    if input_channels_rtma:
        Xval = np.hstack(
            (Xval, RTMAval[:, :, :, input_channels_rtma].mean(axis=im_dim)))
    if input_channels_goes:
        Xval = np.hstack(
            (Xval, GOESval[:, :, :, input_channels_goes].mean(axis=im_dim)))
    if input_rap_cape_cin:
        Xval = np.hstack(
            (Xval, RAPval_cape_cin[..., input_rap_cape_cin]))
    Yval = RAOBval_cape_cin[..., output_cape_cin]

    # test
    Xtest = RAPte[:, :, input_channels_rap].reshape(
        RAPte.shape[0], -1)
    if input_channels_rtma:
        Xtest = np.hstack(
            (Xtest, RTMAtest[:, :, :, input_channels_rtma].mean(axis=im_dim)))
    if input_channels_goes:
        Xtest = np.hstack(
            (Xtest, GOEStest[:, :, :, input_channels_goes].mean(axis=im_dim)))
    if input_rap_cape_cin:
        Xtest = np.hstack(
            (Xtest, RAPtest_cape_cin[..., input_rap_cape_cin]))
    Ytest = RAOBtest_cape_cin[..., output_cape_cin]

    if verbose:
        print('INFO: data dimensions --')
        print('   Train Shape X: {} and Y: {}'.format(
            Xtrain.shape, Ytrain.shape))
        print('   Val Shape X: {} and Y: {}'.format(
            Xval.shape, Yval.shape))
        print('   Test Shape X: {} and Y: {}'.format(
            Xtest.shape, Ytest.shape))

    return Xtrain, Ytrain, Xval, Yval, Xtest, Ytest


def random_refs(nTotal, nSelect):
    """
    Pull out nSelect number of random references
    from an array of size nTotal."""
    errString = "RANDOM_REFS ERROR:"
    errWarning = "CANNOT SELECT ARRAY BIGGER THAN ORIGINAL!"

    if nSelect > nTotal:
        print("{} {}".format(errString, errWarning))
        nSelect = nTotal

    totArray = np.arange(nTotal)
    np.random.shuffle(totArray)
    return totArray[:nSelect]


def subset_data(nSamples, nFeatures, XAll, YAll,
                returnRefs=False,
                verbose=True):

    nSamplesAll = XAll.shape[0]
    nFeaturesAll = XAll.shape[1]
    if nSamples > 0:
        refsTrain = random_refs(nSamplesAll, nSamples)
    else:
        refsTrain = np.arange(nSamplesAll)

    if nFeatures > 0:
        refsFeatures = np.arange(nFeatures)
    else:
        refsFeatures = np.arange(nFeaturesAll)

    XSubset = XAll[refsTrain, ...]
    XSubset = XSubset[..., refsFeatures]
    YSubset = YAll[refsTrain, ...]

    if verbose:
        if nSamples > 0:
            print("Subset Data from {} to {} Samples".
                  format(nSamplesAll, nSamples))
        if nFeatures > 0:
            print("Subset Data from {} to {} Features".
                  format(nFeaturesAll, nFeatures))

    if returnRefs:
        return XSubset, YSubset, refsTrain
    else:
        return XSubset, YSubset
