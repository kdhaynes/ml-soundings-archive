"""
ML Soundings Project

Program to try predicting CAPE and CIN
directly using an NN.

Modified: 2022/02
"""


# %%
# Import Libraries

import functions_cape as fc
import functions_misc as fm
import functions_nn as fnn

from functions_plot import plot_loss
from sklearn.metrics import r2_score
from var_opts import exp984 as expDict


# %%
# Set User Options
subsetData = False
nSubset = -1
nSubsetFeatures = 2

plotLoss = True
savePredictions = True

calcResults = True
saveResults = True

shuffleAll = True
shuffleSites = False

restrictGPU = True
setGPU = True
useShannon = True
verbose = True


# %%
# Set Defaults Based On User Preferences
if useShannon:
    fnn.setup_gpu(restrictGPU, setGPU)


# %%
# Load Data
expName = fm.get_exp_refname(expDict)
print('\nINFO: Running experiment {}'.format(expName))

restrictData = fm.get_exp_restrict_data(expDict)
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrictData,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
(RAPtrain, RAPval, RAPtest,
 RTMAtrain, RTMAval, RTMAtest,
 GOEStrain, GOESval, GOEStest,
 RAOBtrain, RAOBval, RAOBtest,
 RAOBtrain_cape_cin, RAOBval_cape_cin,
 RAOBtest_cape_cin,
 RAPtrain_cape_cin, RAPval_cape_cin,
 RAPtest_cape_cin) = data


# %%
# Organize data
XtrainAll, YtrainAll, XvalAll, YvalAll, \
    XtestAll, YtestAll = fc.organize_data_cape(
        expDict, data)
nTrainAll = XtrainAll.shape[0]
nTestAll = XtestAll.shape[0]
nFeaturesAll = XtrainAll.shape[1]
nTargetsAll = YtrainAll.shape[1]


# %%
# Subset data
if subsetData:
    XTrain, YTrain = fc.subset_data(
        nSubset, nSubsetFeatures,
        XtrainAll, YtrainAll)
    XVal, YVal = fc.subset_data(
        nSubset, nSubsetFeatures,
        XvalAll, YvalAll)
    XTest, YTest, RAPTest = fc.subset_data(
        nSubset, nSubsetFeatures,
        XtestAll, YtestAll,
        RAP=RAPtest_cape_cin)
else:
    XTrain = XtrainAll
    XVal = XvalAll
    XTest = XtestAll
    RAPTest = RAPtest_cape_cin

    YTrain = YtrainAll
    YVal = YvalAll
    YTest = YtestAll
nTrain = XTrain.shape[0]
nVal = XVal.shape[0]
nTest = XTest.shape[0]
nFeatures = XTrain.shape[1]
nTargets = YTrain.shape[1]
print("Setup with {} train, {} validation, and {} test samples.".
      format(nTrain, nVal, nTest))


# %%
# Setup model
network_name = fm.get_exp_networkname(expDict)
nnet = fnn.setup_nn(
    expDict,
    n_inputs=nFeatures,
    n_outputs=nTargets)
nnet.model.summary()


# %%
# ...train
nnet.train(XTrain, YTrain,
           validation=(XVal, YVal),
           verbose=verbose)
print("Trained {} model ({:.2f}s)".format(
    network_name, nnet.training_time))


# %%
# Plot loss
if plotLoss:
    histDict = nnet.history
    plot_loss(histDict)


# %%
# ...create predictions
PTest, PStd = nnet.use(XTest)
print("Created predictions (shape= {})".format(PTest.shape))
print("   R2 Score: {:.3f}".format(
    r2_score(YTest, PTest)))

r2RAP = r2_score(RAOBtest_cape_cin, RAPtest_cape_cin)
print("   Baseline R2 Score: {:.3f}".format(r2RAP))

n1 = RAOBtest_cape_cin.shape[0]
ne = expDict['modelOpts']['uq_n_members']
PTest_ens = nnet.use(XTest, returnType='mc')
if PTest_ens.shape[-1] == ne:
    PREDmc = PTest_ens.reshape((n1, nTargets, ne))
else:
    PREDmc = None

if savePredictions:
    pDict = {
        'PREDtest': PTest,
        'PREDstd': PStd,
        'PREDmc': PREDmc}
    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    fm.pickle_dump(filePreds, pDict,
                   verbose=False)
    print("INFO: predictions saved --")
    print("  {}".format(filePreds))


# %%
# Calculate results
if calcResults:
    results = fc.calculate_results_cape_cin(
        YTest, PTest)
    resultsRAP = fc.calculate_results_cape_cin(
        YTest, RAPTest, model='rap')
    print("CAPE/CIN MAE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_cin_test_mae'],
                 resultsRAP['rap_cape_cin_test_mae']))
    print("        RMSE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_cin_test_rmse'],
                 resultsRAP['rap_cape_cin_test_rmse']))
    print("CAPE MAE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_test_mae'],
                 resultsRAP['rap_cape_test_mae']))
    print("    RMSE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cape_test_rmse'],
                 resultsRAP['rap_cape_test_rmse']))
    print("CIN MAE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cin_test_mae'],
                 resultsRAP['rap_cin_test_mae']))
    print("   RMSE ML: {:.2f}, RAP: {:.2f}".
          format(results['ml_cin_test_rmse'],
                 resultsRAP['rap_cin_test_rmse']))


# %%
# Save results
if saveResults:
    fileResults = fm.get_exp_filenameResults(
        useShannon, expDict)
    outDict = {
        'resultsML': results,
        'resultsRAP': resultsRAP}
    fm.pickle_dump(fileResults, outDict, verbose=False)
    print('INFO: results saved --')
    print('  {}'.format(fileResults))
