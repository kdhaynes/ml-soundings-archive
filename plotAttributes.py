"""
ML Soundings Project

Program to plot the attributes/reliability diagram.

Modified: 2023/09
"""

# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
from functions_metrics import rmse
from importlib import reload

import var_opts as vo


# %%
# User Options
includeRAP = True
expList = [vo.exp015, vo.exp107,
           vo.exp515, vo.exp527]
labelList = ['RAP',
             'LIN CRPS', 'NN SHASH',
             'UNet CRPS', 'UNet SHASH']
colorList = ['deeppink',
             'mediumseagreen', 'darkorange',
             'darkslateblue', 'mediumslateblue']

plotAttributesDiagram = True
saveAttributesDiagram = False

refLow = 0
refHigh = fm.REF_ERROR_LEVEL
refHighRAP = 246

legend_show = True
marker = 'x'
marker_size = 6
nBins = 40
plot_pred_hist = False
targetNames = ['Temperature', 'Dewpoint']
targetLabels = ['t', 'td']

restrictData = [4, 8]
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Read RAP data
min_val = None
max_val = None
dDict = {}
if includeRAP:
    (RAPtr, RAPv, RAPte,
     RTMAtr, RTMAv, RTMAte,
     GOEStr, GOESv, GOESte,
     RAOBtr, RAOBv, RAOBte) = fm.load_data(
         useShannon,
         restrictData=restrictData,
         returnFileInfo=False,
         shuffleAll=shuffleAll,
         shuffleSites=shuffleSites)

    YTrain = fm.organize_data_y(RAOBtr)
    YTest = fm.organize_data_y(RAOBte)

    eDict = {}
    eDict['YTrain'] = YTrain
    eDict['YTest'] = YTest
    eDict['PREDtest'] = RAPte[:, :, [1, 2]]
    eDict['PREDstd'] = None

    eDict = fm.get_attributes_diagram_points(
        YTest[:, refLow:refHigh, :],
        RAPte[:, refLow:refHigh, [1, 2]],
        min_val=min_val,
        max_val=max_val,
        nBins=nBins,
        y_train=YTrain)
    print(f"Original Rap Errors: {eDict['attr_rmse']}")

    rapTrmse = rmse(RAOBte[:, :refHighRAP, 1],
                    RAPte[:, :refHighRAP, 1])
    rapTDrmse = rmse(RAOBte[:, :refHighRAP, 2],
                     RAPte[:, :refHighRAP, 2])
    rapErrs = [rapTrmse, rapTDrmse]
    print(f"RAP Errors: {rapErrs}")
    eDict['attr_rmse'] = rapErrs

    min_val = eDict['attr_min_val']
    max_val = eDict['attr_max_val']
    dDict['rap'] = eDict


# %%
# Read Data and Predictions
reload(fm)
nTargets = len(targetNames)

for expDict in expList:
    eDict = {}
    expRef = fm.get_exp_refname(expDict)
    restrictData = fm.get_exp_restrict_data(expDict)
    (RAPtr, RAPv, RAPte,
     RTMAtr, RTMAv, RTMAte,
     GOEStr, GOESv, GOESte,
     RAOBtr, RAOBv, RAOBte,
     FILEtr, FILEv, FILEte) = fm.load_data(
         useShannon,
         restrictData=restrictData,
         returnFileInfo=True,
         shuffleAll=shuffleAll,
         shuffleSites=shuffleSites)

    YTrain = fm.organize_data_y(RAOBtr)
    YTest = fm.organize_data_y(RAOBte)
    eDict['YTrain'] = YTrain
    eDict['YTest'] = YTest

    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    pDict = fm.pickle_read(filePreds)
    PTest_mean = pDict['PREDtest']
    PTest_std = pDict['PREDstd']
    eDict['PREDtest'] = PTest_mean
    eDict['PREDstd'] = PTest_std

    eDict = fm.get_attributes_diagram_points(
        YTest[:, refLow:refHigh, :],
        PTest_mean[:, refLow:refHigh, :],
        min_val=min_val,
        max_val=max_val,
        nBins=nBins,
        y_train=YTrain)

    fileResults = fm.get_exp_filenameResults(
        useShannon, expDict)
    rDict = fm.pickle_read(fileResults)
    eDict['attr_rmse'] = [
        rDict['mlT_test_rmse'],
        rDict['mlTD_test_rmse']]

    min_val = eDict['attr_min_val']
    max_val = eDict['attr_max_val']
    dDict[expRef] = eDict

print("Read True Data and Predictions.")


# %%
# Plot Attributes Diagram
if plotAttributesDiagram:
    reload(fp)

    for tg in range(nTargets):
        if saveAttributesDiagram:
            save_file = 'mls_attr_' + targetLabels[tg] + '.png'
        else:
            save_file = ''
        fp.plot_attributes_diagram_dict(
            dDict,
            legend_show=legend_show,
            line_color=colorList,
            line_label=labelList,
            line_width=1,
            marker=marker,
            marker_size=marker_size,
            plot_pred_hist=plot_pred_hist,
            save_file=save_file,
            show_msess=True,
            targetNum=tg,
            title=targetNames[tg],
            units='$\degree$C')
