"""
ML Soundings Project

Program to plot model performance for six different scores.

Best Models:
LIN: LIN015 (CRPS)
NN: NN107/181 (SHASH)  NN167/150 (CRPS)
UNet:
   UNet503 (MAEW),
   UNet569 (MAES),
   UNet587 (CRPS),
   UNet593 (SHASH)


Modified: 2023/09
"""


# %%
# Import Libraries
import numpy as np

from functions_plot import plot_bar_eval
from functions_misc import pickle_read


# %%
# User Options
dirFigs = '/home/kdhaynes/ml_soundings/figs.temp/'
dirIn = '/mnt/data1/kdhaynes/mlsoundings/results/'
modelList = ['UNet527', 'UNet515',
             'UNet602', 'UNet503',
             'NN137', 'LIN015']
modelNames = ['UNet SHASH', 'UNet CRPS',
              'UNet MAES', 'UNet MAEW',
              'NN SHASH', 'LIN CRPS']
keyList = ['mlT_test_rmse_sfc', 'mlTD_test_rmse_sfc',
           'mlT_test_rmse', 'mlTD_test_rmse',
           'mlCAPE_test_rmse', 'mlCIN_test_rmse']
rapKeyList = ['rapT_test_rmse_sfc', 'rapTD_test_rmse_sfc',
              'rapT_test_rmse', 'rapTD_test_rmse',
              'rapCAPE_test_rmse', 'rapCIN_test_rmse']
keyNames = ['SFC T', 'SFC TD', 'T', 'TD', 'CAPE', 'CIN']
titleNames = ['SFC TEMPERATURE', 'SFC DEWPOINT',
              'TEMPERATURE', 'DEWPOINT', 'CAPE', 'CIN']
colorNames = ['mediumslateblue', 'mediumslateblue',
              'darkslateblue', 'darkslateblue',
              'darkorange', 'mediumseagreen']
unitsList = ['${\degree}C$', '${\degree}C$',
             '${\degree}C$',
             '${\degree}C$', '$\ J\ kg^{-1}$',
             '$\ J\ kg^{-1}$']

saveBarPlots = True
sortBest = False


# %%
# Load results
nModels = len(modelList)
nKeys = len(keyList)
modelNamesArray = np.array(modelNames)
rapArray = np.empty((nKeys))
resultArray = np.empty((nKeys, nModels))

countM = 0
rDict = {}
for modelRef in modelList:
    fileName = dirIn + modelRef + '_results.pkl'
    rDictT = pickle_read(fileName)
    rDict[modelRef] = rDictT

    countK = 0
    for key in keyList:
        if countM == 0:
            rapArray[countK] = rDictT[rapKeyList[countK]]
        resultArray[countK, countM] = rDictT[key]
        countK += 1

    countM += 1


# %%
# Plot results
for i in range(nKeys):
    scores = resultArray[i, :]
    if sortBest:
        iSorts = np.argsort(scores)
        scoresTP = scores[iSorts]
        modelNamesTP = modelNamesArray[iSorts]
    else:
        scoresTP = scores
        modelNamesTP = modelNamesArray

    if saveBarPlots:
        saveFile = dirFigs + 'plotModelTopEval_' \
            + keyList[i] + '.png'
    else:
        saveFile = ''
    myLabel = 'RMSE [{}]'.format(unitsList[i])
    if 'SFC DEWPOINT' in titleNames[i]:
        myLim = [2.3, 2.84]
    elif 'SFC TEMPERATURE' in titleNames[i]:
        myLim = [0.88, 0.98]
    elif 'TEMPERATURE' in titleNames[i]:
        myLim = [0.92, 1.0]
    elif 'CIN' in titleNames[i]:
        myLim = [92, 110]
    else:
        myLim = None
    plot_bar_eval(modelNamesTP, scoresTP,
                  barColor=colorNames,
                  fontSize=18,
                  vLine=rapArray[i],
                  labelX=myLabel,
                  limX=myLim,
                  saveFile=saveFile,
                  title=keyNames[i], titleSize=22)
