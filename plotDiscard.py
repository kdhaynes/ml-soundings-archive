"""
ML Soundings Project

Program to plot the discard test results.

Modified: 2022/06
"""

# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
import var_opts as vo

from importlib import reload


# %%
# User Options
expList = [vo.exp015, vo.exp137,
           vo.exp515, vo.exp527]

labelList = ['LIN CRPS', 'NN SHASH',
             'UNet CRPS', 'UNet SHASH']
colorList = ['mediumseagreen', 'darkorange',
             'darkslateblue', 'mediumslateblue']

plotDiscard = True
saveDiscard = True

refLow = 0
refHigh = 246

targetNames = ['Temperature', 'Dewpoint']
shuffleAll = True
shuffleSites = False
useShannon = True


# %%
# Read Data
dDict = {}
nTargets = len(targetNames)

for expDict in expList:
    eDict = {}
    restrictData = fm.get_exp_restrict_data(expDict)
    data = fm.load_data(
        useShannon,
        restrictData=restrictData,
        returnFileInfo=True,
        shuffleAll=shuffleAll,
        shuffleSites=shuffleSites)
    RAOBte = data[11]
    YTest = fm.organize_data_y(RAOBte)

    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    pDict = fm.pickle_read(filePreds)
    PTest_mean = pDict['PREDtest']
    PTest_std = pDict['PREDstd']

    eDict['YTest'] = YTest
    eDict['PREDtest'] = PTest_mean
    eDict['PREDstd'] = PTest_std

    eDict = fm.get_discard_points(
        YTest[:, refLow:refHigh, :],
        PTest_mean[:, refLow:refHigh, :],
        PTest_std[:, refLow:refHigh, :],
        discardDict=eDict)

    expRef = fm.get_exp_refname(expDict)
    dDict[expRef] = eDict


# %%
# Plot Discard Test Results
reload(fp)
if plotDiscard:
    saveFileList = []
    if saveDiscard:
        save_file = 'Discard_'
        for exp in expList:
            expRef = fm.get_exp_refname(exp)
            save_file += expRef + '_'

        for tg in range(nTargets):
            saveFileList.append(save_file + targetNames[tg] + '.png')
    else:
        for tg in range(nTargets):
            saveFileList.append('')

    if len(expList) > 1:
        for tg in range(nTargets):
            if tg == 0:
                ylim = [0.4, 1.0]
            else:
                ylim = [1, 5.5]
            fp.plot_discard_dict(
                dDict,
                bar_color=colorList,
                bar_label=labelList,
                bar_label_results=True,
                font_size=18,
                legend_loc='upper right',
                save_file=saveFileList[tg],
                targetNum=tg,
                title=targetNames[tg] + " Discard Test",
                ylabel="RMSE  [$\degree C$]",
                ylim=ylim)

    else:
        for tg in range(nTargets):
            discard_vals, discard_bins = \
                fm.calculate_discard(
                    YTest[:, refLow:refHigh, tg],
                    PTest_mean[:, refLow:refHigh, tg],
                    PTest_std[:, refLow:refHigh, tg], returnBins=True)

            discard_bstr = fm.list_to_str(
                discard_bins, format="%.1f")
            fp.plot_bar(
                discard_bstr,
                discard_vals,
                figSize=(6, 4),
                fontSize=21,
                saveFile=saveFileList[tg],
                title=targetNames[tg] + " Discard Test",
                labelX='Fraction Removed',
                labelY='RMSE [$\degree C$]')
