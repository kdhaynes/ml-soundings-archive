"""
ML Soudings Experiment Driver

Modified: 2023/07
"""


# %%
# Import Libraries
import functions_misc as fa
import functions_nn as fnn
from copy import deepcopy
from functions_util import pickle_dump


# %%
# Set User Info
dropout_conv_rate = [0.0, 0.05, 0.1]
dropout_dense_last = [0.0, 0.05, 0.1]
kernel_reg = ['L1', 'L2']
kernel_reg_lval = [0.0, 0.001, 0.01, 0.1]

dataOpts = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts = {
    'exRef': 'UNet',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': True,
    'dropout_conv_rate': 0.,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [],
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0005,
    'optimizer': 'adam',
    'patience': 15}

shuffleAll = True
shuffleSites = False

calcCAPE = True
saveResults = True
saveDir = '/mnt/data1/kdhaynes/mlsoundings/results/'
saveFile = 'crps_dropout_test.pkl'

refGPU = 0
setGPU = True
restrictGPU = True
useShannon = True


# %%
# Setup GPU
if useShannon:
    fnn.setup_gpu(restrictGPU=restrictGPU,
                  setGPU=setGPU, numGPU=refGPU)


# %%
# Read Data
data = fa.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=dataOpts['restrict_data'],
    returnFileInfo=False,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
indxTr, indxVal, indxTe = fa.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=dataOpts['restrict_data'],
    returnFileInfo=False,
    returnIndices=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    verbose=False)
fileTr, fileVal, fileTe = fa.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=dataOpts['restrict_data'],
    returnFileInfo=False,
    returnFileOnly=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    verbose=False)

RAPCCv = data[16]
RAPv = data[1]
RAOBCCv = data[13]
RAOBv = data[10]


# %%
# Organize training/testing data
expDict = {'dataOpts': dataOpts,
           'modelOpts': modelOpts,
           'runOpts': runOpts}
addTRef = fa.get_exp_addTRef(expDict)
addTDRef = fa.get_exp_addTDRef(expDict)
output_dims = fa.get_exp_outputRAOB(expDict)
XTrain, YTrain, XVal, YVal, XTest, YTest = \
    fa.organize_data(
        expDict, data)
if XTrain[0] is not None:
    nIn = XTrain[0].shape[1]
else:
    nIn = 0


# %%
# Get RAP evaluation
rapDict = fa.calculate_results_rap(
    RAOBv, RAPv,
    type='val')
rapDict = fa.calculate_results_rapcc(
    RAOBCCv, RAPCCv,
    rDict=rapDict, type='val')


# %%
# Train model - loop model training
count = 0
vDictList = []
modelOptsList = []
print("INFO:  training --")
for dc in dropout_conv_rate:
    for dl in dropout_dense_last:
        for rl in kernel_reg:
            for rv in kernel_reg_lval:
                mOpts = deepcopy(modelOpts)
                mOpts['dropout_conv_rate'] = dc
                mOpts['dropout_dense_last'] = dl
                mOpts['kernel_reg'] = rl
                mOpts['kernel_reg_lval'] = rv
                expDict['modelOpts'] = mOpts
                vDict = deepcopy(rapDict)

                print(f"=== Training Model {count} ===")
                print(f"  Drop_conv= {dc}, Drop_dense= {dl}")
                print(f"  Reg= {rl}, val= {rv}")
                nnet = fnn.setup_nn(
                    expDict,
                    n_levs_in=nIn,
                    addTRef=addTRef,
                    addTDRef=addTDRef)

                nnet.train(XTrain, YTrain,
                           validation=(XVal, YVal))

                PVal_mean = nnet.use(XVal, returnType='')
                PREDv = PVal_mean.reshape(
                    RAOBv[:, :, output_dims].shape)
                vDict = fa.calculate_results(
                    RAOBv, YVal,
                    PVal_mean,
                    calcCAPE=True,
                    rDict=vDict,
                    type='val')
                vDictList.append(vDict)
                modelOptsList.append(mOpts)
                fa.print_results(vDict, type='val')

                count += 1
                fnn.setup_clear()
                del nnet

                # ... best model of the multiples trained
                nnetIndx = fa.get_best_trial_cape_indx(
                    vDictList, verbose=True)
                myDict = modelOptsList[nnetIndx]
                bestDCR = myDict['dropout_conv_rate']
                bestDDL = myDict['dropout_dense_last']
                bestK = myDict['kernel_reg']
                bestKV = myDict['kernel_reg_lval']
                print("  BEST Drop Conv: {}, Drop Last: {}".format(
                    bestDCR, bestDDL))
                print("  BEST Kernel Reg: {}, Val: {}\n".format(
                    bestK, bestKV))


# %%
# Save Results
if saveResults:
    outDict = {
        'bestIndex': nnetIndx,
        'dropout_conv_rate': bestDCR,
        'dropout_dense_last': bestDDL,
        'kernel_reg': bestK,
        'kernel_reg_lval': bestKV,
        'vDictList': vDictList,
        'modelOptsList': modelOptsList}
    pickle_dump(saveDir + saveFile,
                outDict)


# %%
# Print best results
myDict = vDictList[nnetIndx]
fa.print_results(myDict, type='val')
