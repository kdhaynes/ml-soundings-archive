"""
ML Soudings Experiment Driver

Modified: 2023/07
"""


# %%
# Import Libraries
import functions_misc as fm
import functions_nn as fnn
import time

from functions_plot import plot_loss
from importlib import reload
from var_opts import exp093 as expDict


# %%
# Set User Info
shuffleAll = True
shuffleSites = False

nTrials = 1
showSummary = False
plotLoss = True

calcCAPE = True

savePredictions = True
saveResults = True

refGPU = 0
setGPU = True
restrictGPU = True
useShannon = True


# %%
# Setup GPU
if useShannon:
    fnn.setup_gpu(restrictGPU=restrictGPU,
                  setGPU=setGPU, numGPU=refGPU)


# %%
# Read Data
expName = fm.get_exp_refname(expDict)
print('\nINFO: Running experiment {}'.format(expName))
addTRef = fm.get_exp_addTRef(expDict)
addTDRef = fm.get_exp_addTDRef(expDict)
loss_type = fm.get_exp_loss(expDict)
output_dims = fm.get_exp_outputRAOB(expDict)
restrictData = fm.get_exp_restrict_data(expDict)

data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrictData,
    returnFileInfo=False,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
indxTr, indxVal, indxTe = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrictData,
    returnFileInfo=False,
    returnIndices=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    verbose=False)
RAPCCte = data[17]
RAPte = data[2]
RAOBCCte = data[14]
RAOBte = data[11]
RAOBv = data[10]


# %%
# Get and check file names
reload(fm)
fileTr, fileVal, fileTe = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=restrictData,
    returnFileInfo=False,
    returnFileOnly=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    verbose=False)
dDict = fm.check_data_dict(
    fileTr, fileVal, fileTe,
    verbose=False)


# %%
# Organize training/testing data
XTrain, YTrain, XVal, YVal, XTest, YTest = \
    fm.organize_data(
        expDict, data)
if XTrain[0] is not None:
    nIn = XTrain[0].shape[1]
else:
    nIn = 0


# %%
# Train model - loop model training
vDictList = []
modelList = []
print("INFO:  training --")
for im in range(nTrials):
    start_t = time.time()
    nnet = fnn.setup_nn(expDict, n_levs_in=nIn,
                        addTRef=addTRef, addTDRef=addTDRef)
    if im == 0 and showSummary:
        nnet.model.summary()

    nnet.train(XTrain, YTrain,
               validation=(XVal, YVal))

    PVal_mean = nnet.use(XVal, returnType='')
    PREDv = PVal_mean.reshape(
        RAOBv[:, :, output_dims].shape)
    vDict = fm.calculate_results(
        RAOBv, YVal, PVal_mean,
        calcCAPE=True, type='val')
    vDictList.append(vDict)
    modelList.append(nnet)
    print("  Trained iteration {}: {:.3f}s".format(
        im, time.time() - start_t))


# %%
# Get the best model of the multiples trained
if len(vDictList) > 1:
    nnetB = fm.get_best_trial_cape(
        vDictList, modelList,
        verbose=True)
else:
    nnetB = modelList[0]


# %%
# Plot loss
if plotLoss:
    histDict = nnetB.history
    plot_loss(histDict)


# %%
# Make predictions on test data
PTest_mean, PTest_std = nnetB.use(XTest)
PREDte = PTest_mean.reshape(
    RAOBte[:, :, output_dims].shape)
if PTest_std is not None:
    PREDstd = PTest_std.reshape(
        RAOBte[:, :, output_dims].shape)
else:
    PREDstd = None

n1 = RAOBte.shape[0]
n2 = RAOBte.shape[1]
ne = expDict['modelOpts']['uq_n_members']
PTest_ens = nnetB.use(XTest, returnType='mc')
if PTest_ens.shape[-1] == ne:
    PREDmc = PTest_ens.reshape((n1, n2, 2, ne))
else:
    PREDmc = None

if savePredictions:
    pDict = {'FILEtest': fileTe,
             'PREDtest': PREDte,
             'PREDstd': PREDstd,
             'PREDmc': PREDmc}
    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    fm.pickle_dump(filePreds, pDict,
                   verbose=False)
    print("INFO: predictions saved --")
    print("  {}".format(filePreds))


# %%
# Get results
reload(fm)
hAvg = fm.compute_average_altitude(
    RAOBte[:, :, fm.RAOB_INDX_HEIGHT])
pAvg = fm.compute_average_altitude(
    RAOBte[:, :, fm.RAOB_INDX_PRESSURE])
rDict = {
    'dataOpts': fm.get_exp_dataOpts(expDict),
    'modelOpts': fm.get_exp_modelOpts(expDict),
    'runOpts': fm.get_exp_runOpts(expDict),
    'history': nnetB.history,
    'hAvg': hAvg,
    'pAvg': pAvg}
rDict = fm.calculate_results_rap(
    RAOBte, RAPte,
    calcCAPE=calcCAPE,
    RAOBCC=RAOBCCte,
    RAPCC=RAPCCte,
    rDict=rDict)
rDict = fm.calculate_results(
    RAOBte, YTest, PTest_mean,
    calcCAPE=calcCAPE,
    RAOBCC=RAOBCCte,
    RAPP=RAPte[..., fm.RAP_INDX_PRESSURE],
    rDict=rDict)

if saveResults:
    fileResults = fm.get_exp_filenameResults(
        useShannon, expDict)
    fm.pickle_dump(fileResults, rDict, verbose=False)
    print('INFO: results saved --')
    print('  {}'.format(fileResults))

keyList = list(rDict.keys())
if 'ml_test_rmse_sfc' in keyList:
    print("   RAP RMSE Tot: {:.3f}, Sfc: {:.3f}".format(
        rDict['rap_test_rmse'], rDict['rap_test_rmse_sfc']))
    print("   ML RMSE Tot: {:.3f}, Sfc: {:.3f}".format(
        rDict['ml_test_rmse'], rDict['ml_test_rmse_sfc']))
if 'mlT_test_rmse_sfc' in keyList:
    print("   RAP RMSE T: {:.3f}, Sfc: {:.3f}".format(
        rDict['rapT_test_rmse'], rDict['rapT_test_rmse_sfc']))
    print("   ML RMSE T: {:.3f}, Sfc: {:.3f}".format(
        rDict['mlT_test_rmse'], rDict['mlT_test_rmse_sfc']))
if 'mlTD_test_rmse_sfc' in keyList:
    print("   RAP RMSE TD: {:.3f}, Sfc: {:.3f}".format(
        rDict['rapTD_test_rmse'], rDict['rapTD_test_rmse_sfc']))
    print("   ML RMSE TD: {:.3f}, Sfc: {:.3f}".format(
        rDict['mlTD_test_rmse'], rDict['mlTD_test_rmse_sfc']))
if 'mlCAPE_test_rmse' in keyList:
    print("   RAP RMSE CAPE: {:.3f}, CIN: {:.3f}".format(
        rDict['rapCAPE_test_rmse'],
        rDict['rapCIN_test_rmse']))
    print("   ML RMSE CAPE: {:.3f}, CIN: {:.3f}".format(
        rDict['mlCAPE_test_rmse'],
        rDict['mlCIN_test_rmse']))
