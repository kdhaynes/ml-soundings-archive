"""
Functions to plot images.

Katherine Haynes
Created: 2022/05
"""

import colormaps
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import metpy.calc as mcalc
import metpy.plots as mplots
import metpy.units as munits
import numpy as np
import shapely
import warnings

from descartes import PolygonPatch
from matplotlib.patches import Polygon
from pylab import cm


# Attributes diagram information
ATTRIBUTES_NBINS = 40
AXIS_FONT_SIZE = 16.
CLIMO_LINE_COLOUR = np.full(3, 152. / 255)
CLIMO_LINE_WIDTH = 2.

HISTOGRAM_ALIGN = 'center'
HISTOGRAM_FACE_COLOUR = np.array(
    [228, 26, 28], dtype=float) / 255
HISTOGRAM_EDGE_COLOUR = np.full(3, 0.)
HISTOGRAM_EDGE_WIDTH = 2.
HISTOGRAM_FONT_SIZE = 14
POSITIVE_SKILL_AREA_OPACITY = 0.2
REFERENCE_LINE_COLOUR = np.full(3, 152. / 255)
REFERENCE_LINE_WIDTH = 2.
TITLE_FONT_SIZE = 18.
RELIABILITY_LINE_COLOUR = np.array(
    [228, 26, 28], dtype=float) / 255
ZERO_SKILL_LINE_COLOUR = np.array(
    [31, 120, 180], dtype=float) / 255
ZERO_SKILL_LINE_WIDTH = 2.

# Radiosonde key and size defaults
ALPHA_KEY = 'alpha'
CONTOUR_LINE_WIDTH_KEY = 'contour_line_width'
DEFAULT_FONT_SIZE_KEY = 'default_font_size'
DEFAULT_LABEL_SIZE_KEY = 'default_label_size'
DEWPOINT_COLOUR_KEY = 'dewpoint_colour'
DOTS_PER_INCH_KEY = 'dots_per_inch'
DRY_ADIABAT_COLOUR_KEY = 'dry_adiabat_colour'
FIGURE_WIDTH_KEY = 'figure_width_inches'
FIGURE_HEIGHT_KEY = 'figure_height_inches'
GRID_LINE_COLOUR_KEY = 'grid_line_colour'
GRID_LINE_WIDTH_KEY = 'grid_line_width'
HANDLE_LENGTH_KEY = 'legend_handle_length'
ISOHUME_COLOUR_KEY = 'isohume_colour'
LINESTYLE_T_KEY = 'linestyle_temperature'
LINESTYLE_TD_KEY = 'linestyle_dewpoint'
MAIN_LINE_COLOUR_KEY = 'main_line_colour'
MAIN_LINE_WIDTH_KEY = 'main_line_width'
MOIST_ADIABAT_COLOUR_KEY = 'moist_adiabat_colour'
NWP_LINE_COLOUR_KEY = 'nwp_line_colour'
PREDICTED_LINE_COLOUR_KEY = 'predicted_line_colour'
TEMPERATURE_COLOUR_KEY = 'temperature_colour'
TEMPERATURE_MIN_KEY = 'temperature_min'
TEMPERATURE_MAX_KEY = 'temperature_max'
TITLE_FONT_SIZE_KEY = 'title_font_size'
UQ_ALPHA_KEY = 'uq_alpha'

DEWPOINT_COLUMN_KEY = 'dewpoints_deg_c'
DEWPOINT_UQL_KEY = 'dewpoints_uql_deg_c'
DEWPOINT_UQH_KEY = 'dewpoints_uqh_deg_c'
PRESSURE_COLUMN_KEY = 'pressures_mb'
TEMPERATURE_COLUMN_KEY = 'temperatures_deg_c'
TEMPERATURE_UQL_KEY = 'temperatures_uql_deg_c'
TEMPERATURE_UQH_KEY = 'temperatures_uqh_deg_c'

COLOR_BLACK_NP = np.array([0, 0, 0], dtype=float)
COLOR_BLUEDARK_NP = np.array([2, 7, 93], dtype=float) / 255
COLOR_BLUELIGHT_NP = np.array([27, 158, 119], dtype=float) / 255
COLOR_BLUESKY_NP = np.array([44, 114, 230], dtype=float) / 255
COLOR_BROWN_NP = np.array([160, 128, 96], dtype=float) / 255
COLOR_GRAY_NP = np.array([152, 152, 152], dtype=float) / 255
COLOR_GREEN_NP = np.array([117, 112, 179], dtype=float) / 255
COLOR_MAROON_NP = np.array([162, 20, 47], dtype=float) / 255
COLOR_ORANGE_NP = np.array([255, 160, 16], dtype=float) / 255
COLOR_PINK_NP = np.array([255, 96, 208], dtype=float) / 255
COLOR_PURPLE_NP = np.array([160, 32, 255], dtype=float) / 255
COLOR_RED_NP = np.array([217, 95, 2], dtype=float) / 255

DEFAULT_LINE_WIDTH = 1.5
DEFAULT_FEAT_CMAP = 'viridis'
DEFAULT_PRESSURE_MIN = 100
DEFAULT_PRESSURE_MAX = 1000
DEFAULT_TEMPERATURE_MAX = 45
DEFAULT_TEMPERATURE_MIN = -55

DEFAULT_OPTION_DICT = {
    ALPHA_KEY: 0.4,
    CONTOUR_LINE_WIDTH_KEY: 1,
    DEFAULT_FONT_SIZE_KEY: 16,
    DEFAULT_LABEL_SIZE_KEY: 12,
    DEWPOINT_COLOUR_KEY: 'tab:green',
    DOTS_PER_INCH_KEY: 300,
    FIGURE_HEIGHT_KEY: 8,
    FIGURE_WIDTH_KEY: 8,
    DRY_ADIABAT_COLOUR_KEY: COLOR_RED_NP,
    GRID_LINE_COLOUR_KEY: COLOR_GRAY_NP,
    GRID_LINE_WIDTH_KEY: 0.5,
    HANDLE_LENGTH_KEY: 2.,
    ISOHUME_COLOUR_KEY: COLOR_BLUELIGHT_NP,
    LINESTYLE_T_KEY: 'solid',
    LINESTYLE_TD_KEY: 'dashdot',
    MAIN_LINE_COLOUR_KEY: COLOR_BLACK_NP,
    MAIN_LINE_WIDTH_KEY: 2.,
    MOIST_ADIABAT_COLOUR_KEY: COLOR_GREEN_NP,
    NWP_LINE_COLOUR_KEY: COLOR_ORANGE_NP,
    PREDICTED_LINE_COLOUR_KEY: COLOR_PURPLE_NP,
    TEMPERATURE_COLOUR_KEY: COLOR_BLACK_NP,
    TEMPERATURE_MIN_KEY: DEFAULT_TEMPERATURE_MIN,
    TEMPERATURE_MAX_KEY: DEFAULT_TEMPERATURE_MAX,
    TITLE_FONT_SIZE_KEY: 16,
    UQ_ALPHA_KEY: 0.5}

DEFAULT_GOES_CHANNELS = [8, 9, 10, 11, 13, 14, 15, 16]
DEFAULT_GOES_UNITS = 'Radiance  $(W$ $m^{-2}$ $sr^{-1}$ $micron^{-1}$)'
DEFAULT_RTMA_LABELS = ['T', 'TD', 'P']

QUANT_COLOR_LIST = [
    'mediumseagreen', 'darkorange',
    'mediumslateblue', 'deeppink',
    'limegreen']
QUANT_COLOR_ANOMS = [
    'black', 'deeppink',
    'seagreen', 'tab:cyan',
    'tab:orange', 'tab:brown',
    'tab:purple', 'thistle',
    'tab:olive', 'tab:gray']


def rescale(y):
    return (y - np.min(y)) \
        / (np.max(y) - np.min(y))


QUANT_ANTIQUE_LIST = colormaps.antique(
    rescale(range(11)))
QUANT_ANTIQUE_LIST4 = colormaps.antique(
    rescale(range(2, 11)))


def _get_positive_skill_area(
        mean_value_in_training,
        min_value_in_plot,
        max_value_in_plot):
    """Returns positive-skill area (where BSS > 0) for attributes diagram.

    :param mean_value_in_training: Mean of target variable in training data.
    :param min_value_in_plot: Minimum value in plot (for both x- and y-axes).
    :param max_value_in_plot: Max value in plot (for both x- and y-axes).
    :return: x_coords_left: length-5 numpy array of x-coordinates for left part
        of positive-skill area.
    :return: y_coords_left: Same but for y-coordinates.
    :return: x_coords_right: length-5 numpy array of x-coordinates for right
        part of positive-skill area.
    :return: y_coords_right: Same but for y-coordinates.
    """

    x_coords_left = np.array([
        min_value_in_plot, mean_value_in_training, mean_value_in_training,
        min_value_in_plot, min_value_in_plot
    ])
    y_coords_left = np.array([
        min_value_in_plot, min_value_in_plot, mean_value_in_training,
        (min_value_in_plot + mean_value_in_training) / 2, min_value_in_plot
    ])

    x_coords_right = np.array([
        mean_value_in_training, max_value_in_plot, max_value_in_plot,
        mean_value_in_training, mean_value_in_training
    ])
    y_coords_right = np.array([
        mean_value_in_training,
        (max_value_in_plot + mean_value_in_training) / 2,
        max_value_in_plot, max_value_in_plot, mean_value_in_training
    ])

    return x_coords_left, y_coords_left, x_coords_right, y_coords_right


def _get_zero_skill_line(
        mean_value_in_training,
        min_value_in_plot,
        max_value_in_plot):
    """Returns zero-skill line (where BSS = 0) for attributes diagram.

    :param mean_value_in_training: Mean of target variable in training data.
    :param min_value_in_plot: Minimum value in plot (for both x- and y-axes).
    :param max_value_in_plot: Max value in plot (for both x- and y-axes).
    :return: x_coords: length-2 numpy array of x-coordinates.
    :return: y_coords: Same but for y-coordinates.
    """

    x_coords = np.array([min_value_in_plot, max_value_in_plot],
                        dtype=float)
    y_coords = 0.5 * (mean_value_in_training + x_coords)

    return x_coords, y_coords


def _init_save_img(option_dict):
    option_dict[DEFAULT_FONT_SIZE_KEY] = 16
    option_dict[TITLE_FONT_SIZE_KEY] = 18
    option_dict[FIGURE_WIDTH_KEY] = 8
    option_dict[FIGURE_HEIGHT_KEY] = 8
    option_dict[MAIN_LINE_WIDTH_KEY] = 3
    option_dict[GRID_LINE_WIDTH_KEY] = 2
    option_dict[CONTOUR_LINE_WIDTH_KEY] = 2


def _init_skewT(option_dict, fig=None, gs=None):

    if fig is None:
        plt.rc('font', size=option_dict[DEFAULT_FONT_SIZE_KEY])
        plt.rc('axes', titlesize=option_dict[TITLE_FONT_SIZE_KEY])
        plt.rc('axes', labelsize=option_dict[DEFAULT_FONT_SIZE_KEY])
        plt.rc('xtick', labelsize=option_dict[DEFAULT_FONT_SIZE_KEY])
        plt.rc('ytick', labelsize=option_dict[DEFAULT_FONT_SIZE_KEY])
        plt.rc('legend', fontsize=option_dict[DEFAULT_FONT_SIZE_KEY])
        plt.rc('figure', titlesize=option_dict[TITLE_FONT_SIZE_KEY])

        fig = plt.figure(
            figsize=(option_dict[FIGURE_WIDTH_KEY],
                     option_dict[FIGURE_HEIGHT_KEY]))

    skewt_object = mplots.SkewT(
        fig, aspect=95, rotation=45, subplot=gs)

    return fig, skewt_object


def _list_pull_valid(list1, list2=None,
                     badVal=-999):

    refs = []
    for i in range(len(list1)):
        if list1[i] > badVal:
            refs.append(i)

    if list2 is not None:
        return list1[refs], list2[refs]
    else:
        return list1[refs]


def _plot_attr_diagram_background(
        axes_object, mean_value_in_training, min_value_in_plot,
        max_value_in_plot):
    """Plots background (reference lines and polygons) of attributes diagram.

    :param axes_object: Will plot on these axes (instance of
        `matplotlib.axes._subplots.AxesSubplot`).
    :param mean_value_in_training: Mean of target variable in training data.
    :param min_value_in_plot: Minimum value in plot (for both x- and y-axes).
    :param max_value_in_plot: Max value in plot (for both x- and y-axes).
    """

    x_coords_left, y_coords_left, x_coords_right, y_coords_right = (
        _get_positive_skill_area(
            mean_value_in_training=mean_value_in_training,
            min_value_in_plot=min_value_in_plot,
            max_value_in_plot=max_value_in_plot
        )
    )

    skill_area_colour = mcolors.to_rgba(
        ZERO_SKILL_LINE_COLOUR,
        POSITIVE_SKILL_AREA_OPACITY
    )

    this_list = _vertex_arrays_to_list(
        vertex_x_coords=x_coords_left, vertex_y_coords=y_coords_left
    )
    left_polygon_object = shapely.geometry.Polygon(shell=this_list)
    left_patch_object = PolygonPatch(
        left_polygon_object, lw=0, ec=skill_area_colour, fc=skill_area_colour
    )
    axes_object.add_patch(left_patch_object)

    this_list = _vertex_arrays_to_list(
        vertex_x_coords=x_coords_right,
        vertex_y_coords=y_coords_right
    )
    right_polygon_object = shapely.geometry.Polygon(shell=this_list)
    right_patch_object = PolygonPatch(
        right_polygon_object, lw=0, ec=skill_area_colour, fc=skill_area_colour
    )
    axes_object.add_patch(right_patch_object)

    no_skill_x_coords, no_skill_y_coords = _get_zero_skill_line(
        mean_value_in_training=mean_value_in_training,
        min_value_in_plot=min_value_in_plot,
        max_value_in_plot=max_value_in_plot
    )

    axes_object.plot(
        no_skill_x_coords, no_skill_y_coords, color=ZERO_SKILL_LINE_COLOUR,
        linestyle='solid', linewidth=ZERO_SKILL_LINE_WIDTH
    )

    climo_x_coords = np.full(2, mean_value_in_training)
    climo_y_coords = np.array([min_value_in_plot, max_value_in_plot])
    axes_object.plot(
        climo_x_coords, climo_y_coords, color=CLIMO_LINE_COLOUR,
        linestyle='dashed', linewidth=CLIMO_LINE_WIDTH
    )

    axes_object.plot(
        climo_y_coords, climo_x_coords, color=CLIMO_LINE_COLOUR,
        linestyle='dashed', linewidth=CLIMO_LINE_WIDTH
    )


def _plot_inset_counts(
        figure_object, bin_centers, bin_counts,
        line_colour=QUANT_COLOR_ANOMS,
        line_width=DEFAULT_LINE_WIDTH,
        has_predictions=False,
        marker='x',
        shift_x=0.0,
        shift_y=0.0,
        tick_vals=None,
        title=None):
    """Plots histogram as inset in attributes diagram."""

    if has_predictions:
        inset_axes_object = figure_object.add_axes(
            [0.675 + shift_x, 0.225 + shift_y, 0.2, 0.2])
    else:
        inset_axes_object = figure_object.add_axes(
            [0.22 + shift_x, 0.625 + shift_y, 0.2, 0.2])

    for i in range(len(bin_centers)):
        bin_centersH, bin_valsH = _list_pull_valid(
            bin_centers[i], bin_counts[i])
        bin_valsH = np.array(bin_valsH)
        bin_freqsH = bin_valsH / bin_valsH.sum()

        inset_axes_object.plot(
            bin_centersH, bin_freqsH,
            color=line_colour[i],
            marker=marker,
            linewidth=line_width)

    inset_axes_object.set_ylim(bottom=0.)

    if tick_vals is None:
        x_tick_values = bin_centersH
        x_tick_labels = [str(int(b)) for b in bin_centers]
        inset_axes_object.set_xticks(x_tick_values)
        inset_axes_object.set_xticklabels(x_tick_labels)
    else:
        inset_axes_object.set_xticks(tick_vals)

    for this_tick_object in inset_axes_object.xaxis.get_major_ticks():
        this_tick_object.label.set_fontsize(HISTOGRAM_FONT_SIZE)
        this_tick_object.label.set_rotation('vertical')

    for this_tick_object in inset_axes_object.yaxis.get_major_ticks():
        this_tick_object.label.set_fontsize(HISTOGRAM_FONT_SIZE)

    if title is None:
        title = 'Pred Freq' if has_predictions else 'Obs Freq'
    inset_axes_object.set_title(
        title,
        fontsize=HISTOGRAM_FONT_SIZE)


def _plot_inset_histogram(
        figure_object, bin_centers, bin_counts,
        bar_align=HISTOGRAM_ALIGN,
        bar_colour=HISTOGRAM_FACE_COLOUR,
        bar_edge=HISTOGRAM_EDGE_COLOUR,
        bar_width=HISTOGRAM_EDGE_WIDTH,
        has_predictions=False,
        shift_x=0.0,
        shift_y=0.0,
        tick_vals=None,
        title=None):
    """Plots histogram as inset in attributes diagram."""

    bin_frequencies = bin_counts.astype(float) \
        / np.sum(bin_counts)

    if has_predictions:
        inset_axes_object = figure_object.add_axes(
            [0.675 + shift_x, 0.225 + shift_y, 0.2, 0.2])
    else:
        inset_axes_object = figure_object.add_axes(
            [0.22 + shift_x, 0.625 + shift_y, 0.2, 0.2])

    inset_axes_object.bar(bin_centers, bin_frequencies,
                          align=bar_align,
                          color=bar_colour,
                          edgecolor=bar_edge,
                          width=bar_width)
    inset_axes_object.set_ylim(bottom=0.)

    if tick_vals is None:
        x_tick_values = bin_centers
        x_tick_labels = [str(int(b)) for b in bin_centers]
        inset_axes_object.set_xticks(x_tick_values)
        inset_axes_object.set_xticklabels(x_tick_labels)
    else:
        inset_axes_object.set_xticks(tick_vals)

    for this_tick_object in inset_axes_object.xaxis.get_major_ticks():
        this_tick_object.label.set_fontsize(HISTOGRAM_FONT_SIZE)
        this_tick_object.label.set_rotation('vertical')

    for this_tick_object in inset_axes_object.yaxis.get_major_ticks():
        this_tick_object.label.set_fontsize(HISTOGRAM_FONT_SIZE)

    if title is None:
        title = 'Pred Freq' if has_predictions else 'Obs Freq'
    inset_axes_object.set_title(
        title,
        fontsize=HISTOGRAM_FONT_SIZE)


def _plot_reliability_curve(
        axes_object,
        mean_predictions,
        mean_observations,
        min_value_to_plot,
        max_value_to_plot,
        labelx='Prediction Frequency',
        labely='Conditional Observed Frequency',
        line_colour=RELIABILITY_LINE_COLOUR,
        line_label=None,
        line_style='solid',
        line_width=DEFAULT_LINE_WIDTH,
        marker='x',
        marker_size=10,
        tick_vals=None):
    """Plots reliability curve.

    B = number of bins

    :param axes_object: Will plot on these axes (instance of
        `matplotlib.axes._subplots.AxesSubplot`).
    :param mean_predictions: length-B numpy array of mean predicted values.
    :param mean_observations: length-B numpy array of mean observed values.
    :param min_value_to_plot: See doc for `plot_attributes_diagram`.
    :param max_value_to_plot: Same.
    :param line_colour: Line colour (in any format accepted by matplotlib).
    :param line_style: Line style (in any format accepted by matplotlib).
    :param line_width: Line width (in any format accepted by matplotlib).
    :return: main_line_handle: Handle for main line (reliability curve).
    """

    perfect_x_coords = np.array([min_value_to_plot, max_value_to_plot])
    perfect_y_coords = np.array([min_value_to_plot, max_value_to_plot])

    axes_object.plot(
        perfect_x_coords, perfect_y_coords,
        color=REFERENCE_LINE_COLOUR,
        linestyle='dashed',
        linewidth=REFERENCE_LINE_WIDTH)

    nan_flags = np.logical_or(
        np.isnan(mean_predictions),
        np.isnan(mean_observations)
    )

    if np.all(nan_flags):
        main_line_handle = None
    else:
        real_indices = np.where(np.invert(nan_flags))[0]

        main_line_handle = axes_object.plot(
            mean_predictions[real_indices],
            mean_observations[real_indices],
            color=line_colour,
            label=line_label,
            linestyle=line_style,
            linewidth=line_width,
            marker=marker,
            markersize=marker_size)[0]

    axes_object.set_xlabel(labelx)
    axes_object.set_ylabel(labely)
    axes_object.set_xlim(min_value_to_plot, max_value_to_plot)
    axes_object.set_ylim(min_value_to_plot, max_value_to_plot)

    if tick_vals is not None:
        axes_object.xaxis.set_ticks(tick_vals)
        axes_object.yaxis.set_ticks(tick_vals)

    return main_line_handle


def _plot_sounding_attributes(
        skewt_object, option_dict, title_string):

    alpha = option_dict[ALPHA_KEY]
    dry_adiabat_colour = option_dict[DRY_ADIABAT_COLOUR_KEY]
    moist_adiabat_colour = option_dict[MOIST_ADIABAT_COLOUR_KEY]
    isohume_colour = option_dict[ISOHUME_COLOUR_KEY]
    contour_line_width = option_dict[CONTOUR_LINE_WIDTH_KEY]
    grid_line_colour = option_dict[GRID_LINE_COLOUR_KEY]
    grid_line_width = option_dict[GRID_LINE_WIDTH_KEY]

    axes_object = skewt_object.ax

    axes_object.grid(
        color=colour_from_numpy_to_tuple(grid_line_colour),
        linewidth=grid_line_width, linestyle='dashed',
    )

    skewt_object.plot_dry_adiabats(
        color=colour_from_numpy_to_tuple(dry_adiabat_colour),
        linewidth=contour_line_width, linestyle='solid', alpha=alpha
    )
    skewt_object.plot_moist_adiabats(
        color=colour_from_numpy_to_tuple(moist_adiabat_colour),
        linewidth=contour_line_width, linestyle='solid', alpha=alpha
    )
    skewt_object.plot_mixing_lines(
        color=colour_from_numpy_to_tuple(isohume_colour),
        linewidth=contour_line_width, linestyle='solid', alpha=alpha
    )

    temp_min = option_dict[TEMPERATURE_MIN_KEY]
    temp_max = option_dict[TEMPERATURE_MAX_KEY]
    axes_object.set_ylim(DEFAULT_PRESSURE_MAX, DEFAULT_PRESSURE_MIN)
    axes_object.set_xlim(temp_min, temp_max)
    axes_object.set_xlabel('')
    axes_object.set_ylabel('')

    # tick_values_deg_c = np.linspace(-50, 40, num=10)
    tick_values_deg_c = np.arange(temp_min, temp_max, 10)
    axes_object.set_xticks(tick_values_deg_c)

    x_tick_labels = [
        '{0:d}'.format(int(np.round(x))) for x in axes_object.get_xticks()
    ]

    axes_object.set_xticklabels(labels=x_tick_labels)

    y_tick_labels = [
        '{0:d}'.format(int(np.round(y))) for y in axes_object.get_yticks()
    ]
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        # UserWarning: FixedFormatter should only be used
        # together with FixedLocator
        axes_object.set_yticklabels(labels=y_tick_labels)

    if title_string is not None:
        plt.title(title_string)


def _vertex_arrays_to_list(vertex_x_coords, vertex_y_coords):
    """Converts vertices of simple polygon from two arrays to one list.

    V = number of vertices

    :param vertex_x_coords: length-V numpy array of x-coordinates.
    :param vertex_y_coords: length-V numpy array of y-coordinates.
    :return: vertex_coords_as_list: length-V list, where each element is an
        (x, y) tuple.
    """

    num_vertices = len(vertex_x_coords)
    vertex_coords_as_list = []

    for i in range(num_vertices):
        vertex_coords_as_list.append((vertex_x_coords[i], vertex_y_coords[i]))

    return vertex_coords_as_list


def colour_from_numpy_to_tuple(input_colour):
    """Converts colour from numpy array to tuple (if necessary).
    :param input_colour: Colour (possibly length-3 or length-4 numpy array).
    :return: output_colour: Colour (possibly length-3 or length-4 tuple).
    """

    if not isinstance(input_colour, np.ndarray):
        return input_colour

    return tuple(input_colour.tolist())


def convert_metpy_dewpoint(sounding_dict):
    dTemp = sounding_dict[DEWPOINT_COLUMN_KEY]
    if len(dTemp.shape) > 1:
        dTemp = dTemp.reshape(-1)
    return dTemp * munits.units.degC


def convert_metpy_dewpointUQ(sounding_dict):
    try:
        dTempl = sounding_dict[DEWPOINT_UQL_KEY]
        if dTempl is not None:
            dTempl = dTempl.reshape(-1) * munits.units.degC

        dTemph = sounding_dict[DEWPOINT_UQH_KEY]
        if dTemph is not None:
            dTemph = dTemph.reshape(-1) * munits.units.degC
    except KeyError:
        dTempl = None
        dTemph = None
    return dTempl, dTemph


def convert_metpy_pressure(sounding_dict):
    pTemp = sounding_dict[PRESSURE_COLUMN_KEY]
    if len(pTemp.shape) > 1:
        pTemp = pTemp.reshape(-1)
    return pTemp * munits.units.hPa


def convert_metpy_temperature(sounding_dict):
    tTemp = sounding_dict[TEMPERATURE_COLUMN_KEY]
    if len(tTemp.shape) > 1:
        tTemp = tTemp.reshape(-1)
    return tTemp * munits.units.degC


def convert_metpy_temperatureUQ(sounding_dict):
    try:
        tTempl = sounding_dict[TEMPERATURE_UQL_KEY]
        if tTempl is not None:
            tTempl = tTempl.reshape(-1) * munits.units.degC

        tTemph = sounding_dict[TEMPERATURE_UQH_KEY]
        if tTemph is not None:
            tTemph = tTemph.reshape(-1) * munits.units.degC
    except KeyError:
        tTempl = None
        tTemph = None
    return tTempl, tTemph


def image_mscores_inv(
    data,
    cbarShow=True,
    cbarTitle='NRS',
    # cbarTitle='Score (1 - Norm RMSE)',
    # cbarTitle='NRS (1 - Norm RMSE)',
    cmapStr=DEFAULT_FEAT_CMAP,
    figSize=(10, 10),
    fontSize=10,
    labelX='',
    labelY='',
    pad=0.05,
    rotateX=0,
    saveDPI=200,
    saveFile='',
    shrink=0.6,
    title='',
    valsX=None,
        valsY=None):

    cmap = cm.get_cmap(cmapStr)
    fig, ax = plt.subplots(1, 1, figsize=figSize)
    dataI = np.transpose(data)
    im1 = ax.imshow(dataI, cmap)

    ax.set_title(title, fontsize=fontSize)

    if valsX is not None:
        xt = range(dataI.shape[1])
        ax.set_xticks(xt)
        ax.set_xticklabels(valsX, rotation=rotateX)
        ax.set_xlabel(labelX)
    if valsY is not None:
        yt = range(dataI.shape[0])
        ax.set_yticks(yt)
        ax.set_yticklabels(valsY)
        ax.set_ylabel(labelY)
    set_plot_fonts_ax(ax, plotFontSize=fontSize)

    if cbarShow:
        cbar = fig.colorbar(im1, ax=ax,
                            orientation="vertical",
                            pad=pad,
                            shrink=shrink)
        cbar.set_label(cbarTitle, fontsize=fontSize)
        cbar.ax.tick_params(labelsize=fontSize)

    if saveFile:
        plt.savefig(saveFile, bbox_inches="tight", format='png', dpi=saveDPI)
        print("Saved File: {}".format(saveFile))
    else:
        plt.show()

    plt.close()
    return


def list_to_int(input_list):
    out_list = []
    for ele in input_list:
        out_list.append(int(ele))
    return out_list


def list_to_str(input_list, format="%.2f"):
    """ Use format="%d" for an integer"""
    return [format % i for i in input_list]


def plot_anoms(labels, mmmList,
               figSize=(6, 4),
               fontSize=14,
               align='edge',
               colors=QUANT_COLOR_ANOMS,
               edgeColor='black',
               labelY='K',
               legendLoc='lower right',
               legendShow=True,
               legendSize=12,
               saveFile='', showGrid=True,
               tickLabels=['Mean', 'Min', 'Max'],
               title='', titleSize=16,
               width=0.8):

    nms = len(labels)
    barWidth = width / nms

    nls = len(mmmList[0])
    xArray = np.array(range(1, nls + 1)) - barWidth * nms * 0.5

    _, ax = plt.subplots(1, 1, figsize=figSize)
    for arry, lbl, clrs in zip(mmmList, labels, colors):
        ax.bar(xArray, arry, align=align,
               color=clrs,
               edgecolor=edgeColor,
               label=lbl, width=barWidth)
        xArray += barWidth

    ax.set_ylabel(labelY, size=fontSize)
    ax.set_xticks(np.array(range(1, nls + 1)))
    ax.set_xticklabels(tickLabels)
    set_plot_fonts_ax(ax, plotFontSize=fontSize)

    ax.set_title(title, fontsize=titleSize)
    if legendShow:
        ax.legend(loc=legendLoc, fontsize=legendSize, ncol=2)
    if title:
        ax.set_title(title, fontsize=titleSize)

    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()
    plt.close()
    return


def plot_attributes_diagram_dict(
        aDict,
        axis_size=AXIS_FONT_SIZE,
        bar_color='tab:blue',
        bar_edges='tab:blue',
        labelx='Prediction',
        labely='Conditional Mean Obs',
        legend_show=True,
        legend_size=13,
        line_color=QUANT_COLOR_ANOMS,
        line_label=None,
        line_style='solid',
        line_width=DEFAULT_LINE_WIDTH,
        marker='x',
        marker_size=8,
        plot_obs_hist=True,
        plot_pred_hist=False,
        save_file='',
        show_background=True,
        show_msess=False,
        targetNum=0,
        title='',
        title_size=TITLE_FONT_SIZE,
        units='',
        verbose=True):
    """Plots regression attributes diagram."""

    expList = list(aDict.keys())
    refDict = aDict[expList[0]]
    tg = targetNum

    if units:
        labelx += f' [{units}]'
        labely += f' [{units}]'

    fig_object, axes_object = plt.subplots(
        1, 1, figsize=(6, 6))

    if show_background:
        _plot_attr_diagram_background(
            axes_object=axes_object,
            mean_value_in_training=refDict['attr_mean_val'][tg],
            min_value_in_plot=refDict['attr_min_val'][tg],
            max_value_in_plot=refDict['attr_max_val'][tg])

    if plot_obs_hist:
        _plot_inset_histogram(
            figure_object=fig_object,
            bin_centers=refDict['attr_obs_bins'][:, tg],
            bin_counts=refDict['attr_obs_counts'][:, tg],
            has_predictions=False,
            bar_colour=bar_color,
            bar_edge=bar_edges,
            tick_vals=refDict['attr_tick_vals'][:, tg])

    if plot_pred_hist:
        _plot_inset_histogram(
            figure_object=fig_object,
            bin_centers=refDict['attr_pred_bins'][:, tg],
            bin_counts=refDict['attr_pred_counts'][:, tg],
            has_predictions=True,
            bar_colour=bar_color,
            tick_vals=refDict['attr_tick_vals'][:, tg])

    iExp = 0
    for exp in expList:
        eDict = aDict[exp]
        obs_vals = eDict['attr_obs_vals'][:, tg]
        pred_vals = eDict['attr_pred_vals'][:, tg]
        min_val = eDict['attr_min_val'][tg]
        max_val = eDict['attr_max_val'][tg]

        if line_label is not None:
            lineLabel = line_label[iExp]
        else:
            lineLabel = exp.upper()
        try:
            if show_msess:
                lineLabel += " ({:.3f}, {:.3f})".format(
                    eDict['attr_rmse'][tg],
                    eDict['attr_msess'][tg])
            else:
                lineLabel += " ({:.2f})".format(
                    eDict['attr_rmse'][tg])
        except KeyError:
            pass

        _plot_reliability_curve(
            axes_object=axes_object,
            mean_predictions=pred_vals,
            mean_observations=obs_vals,
            min_value_to_plot=min_val,
            max_value_to_plot=max_val,
            labelx=labelx,
            labely=labely,
            line_colour=line_color[iExp],
            line_label=lineLabel,
            line_style=line_style,
            line_width=line_width,
            marker=marker,
            marker_size=marker_size)

        iExp += 1
    yticks = axes_object.get_yticks()[1:-1]
    axes_object.set_xticks(yticks)
    if legend_show:
        axes_object.legend(fontsize=legend_size,
                           loc='lower right')
    if title:
        axes_object.set_title(
            title,
            fontsize=title_size)
    if axis_size is not None:
        set_plot_fonts_ax(axes_object, axis_size)

    if save_file:
        plt.savefig(save_file, bbox_inches='tight')
        if verbose:
            print("Saved File: {}".format(save_file))
    else:
        plt.show()
    plt.close()
    return


def plot_autocorrelation(
    acList,
        figSize=(6, 4), fontSize=14,
        labelList=[], legendShow=True,
        saveFile='', showGrid=True,
        title='', titleSize=16):

    nACs = len(acList)
    if not labelList:
        labelList = []
        for _ in range(nACs):
            labelList.append(None)
        legendShow = False

    _, ax = plt.subplots(1, 1, figsize=figSize)
    for ar, la in zip(acList, labelList):
        ax.plot(ar, label=la)

    ax.set_xlabel('Lag', fontsize=fontSize)
    ax.set_ylabel('Correlation Coefficient', fontsize=fontSize)
    ax.set_title(title, fontsize=titleSize)
    if legendShow:
        ax.legend()
    if showGrid:
        ax.grid()

    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()
    plt.close()
    return


def plot_bar(labels, values,
             align='center',
             barColor='tab:blue',
             edgeColor='black',
             figSize=(7, 4),
             fontSize=16,
             hLine=None,
             labelX='', labelY='',
             limX=None, limY=None,
             rotateX=0,
             saveFile="",
             title="",
             width=0.8):
    """Create basic bar plot."""

    fig = plt.figure(figsize=figSize)
    ax = fig.add_axes([0, 0, 1, 1])

    nValues = len(values)
    xvals = range(nValues)
    ax.bar(xvals, values,
           align=align, color=barColor,
           edgecolor=edgeColor,
           width=width)
    if hLine is not None:
        ax.plot([-1, nValues], [hLine, hLine],
                color='gray', linestyle='--')

    if limX is None:
        limX = [-1, nValues]
    ax.set_xlim(limX)

    if limY is None:
        myMin = np.min(values) * 0.9
        if hLine is not None:
            myMin = np.min([myMin, hLine]) * 0.9
        myMax = np.max(values) * 1.05
        if hLine is not None:
            myMax = np.max([myMax, hLine]) * 1.05
        limY = [myMin, myMax]
    ax.set_ylim(limY)

    ax.set_xticks(xvals)
    ax.set_xticklabels(labels, rotation=rotateX)
    ax.set_title(title, fontsize=fontSize)
    ax.set_xlabel(labelX)
    ax.set_ylabel(labelY)
    set_plot_fonts_ax(ax, plotFontSize=fontSize)

    if saveFile == "":
        plt.show()
    else:
        save_figure(saveFile)

    plt.close()
    return


def plot_bars(
    colNames, values2D, labels,
        colorList=QUANT_ANTIQUE_LIST4,
        figSize=(8, 6),
        legendAlpha=0.8, legendPos='best',
        rotation='horizontal',
        saveFile='',
        title='', tsize=18,
        width=0.25,
        xlabel='', xticksize=15,
        ylabel=''):
    """Create multi-bar plot."""
    fig = plt.figure(figsize=figSize)
    ax = fig.add_axes([0, 0, 1, 1])

    vShape = np.shape(values2D)
    nBars = vShape[0]
    nCols = vShape[1]

    if colorList is None:
        plt.set_cmap(colormaps.antique)

    X = np.arange(nCols)
    for i in range(nBars):
        ax.bar(X + width * i, values2D[i, :],
               color=colorList[i], width=width)

    ax.set_xlabel(xlabel)
    ax.set_xticks(X + width * (np.floor(nBars / 2)) - 0.5 * width)
    ax.set_xticklabels(colNames,
                       rotation=rotation)
    ax.set_ylabel(ylabel)
    nLCols = 1
    if nCols > 4:
        nLCols = 2
    ax.legend(labels=labels, fontsize=xticksize,
              framealpha=legendAlpha, loc=legendPos, ncol=nLCols)
    set_plot_fonts_ax(ax, xticksize)

    ax.set_title(title, fontsize=tsize)

    if saveFile == '':
        plt.show()
    else:
        save_figure(saveFile)

    plt.close()
    return


def plot_bar_eval(
    labels, values,
        align='center',
        barColor='tab:blue',
        edgeColor='black',
        figSize=(7, 4),
        fontSize=16,
        vLine=None,
        labelX='', labelY='',
        limX=None, limY=None,
        saveFile="",
        title="", titleSize=20):
    """Create basic horizontal bar plot."""

    fig = plt.figure(figsize=figSize)
    ax = fig.add_axes([0, 0, 1, 1])

    nValues = len(values)
    yvals = range(nValues)
    ax.barh(yvals, values,
            align=align, color=barColor,
            edgecolor=edgeColor)
    if vLine is not None:
        ax.plot([vLine, vLine], [-1, nValues],
                color='gray', linestyle='--')

    if limY is None:
        limY = [-1, nValues]
    ax.set_ylim(limY)

    if limX is None:
        myMin = np.min(values) * 0.95
        if vLine is not None:
            myMin = np.min([myMin, vLine]) * 0.95
        myMax = np.max(values) * 1.05
        if vLine is not None:
            myMax = np.max([myMax, vLine]) * 1.05
        limX = [myMin, myMax]
    ax.set_xlim(limX)

    ax.set_yticks(yvals)
    ax.set_yticklabels(labels)
    ax.set_title(title, fontsize=titleSize)
    ax.set_xlabel(labelX)
    ax.set_ylabel(labelY)
    set_plot_fonts_ax(ax, plotFontSize=fontSize)

    if saveFile == "":
        plt.show()
    else:
        save_figure(saveFile)

    plt.close()
    return


def plot_bars_horiz(
        colNames, values2D, labels,
        colorList=QUANT_COLOR_LIST,
        figSize=(4, 6),
        legendAlpha=0.8,
        legendCols=1,
        legendPos='lower right',
        rotation='horizontal',
        saveFile='',
        title='', tsize=12,
        width=0.25,
        xlabel='',
        ylabel='', yticksize=10):
    """Create multi-bar plot."""
    fig = plt.figure(figsize=figSize)
    ax = fig.add_axes([0, 0, 1, 1])

    vShape = np.shape(values2D)
    nBars = vShape[0]
    nCols = vShape[1]

    X = np.arange(nCols)
    for i in range(nBars):
        ax.barh(X + width * i, values2D[i, :],
                color=colorList[i],
                height=width)
    ax.invert_yaxis()
    ax.set_xlabel(xlabel)

    ax.set_yticks(X + width * (np.floor(nBars / 2)) - 0.5 * width)
    ax.set_yticklabels(colNames,
                       rotation=rotation)
    ax.set_ylabel(ylabel)
    ax.legend(labels=labels, fontsize=yticksize,
              framealpha=legendAlpha, loc=legendPos, ncol=legendCols)
    set_plot_fonts_ax(ax, yticksize)

    ax.set_title(title, fontsize=tsize)

    if saveFile == '':
        plt.show()
    else:
        save_figure(saveFile)

    plt.close()
    return


def plot_box(
    data,
        colorList=None,
        figSize=(6, 6),
        plotWhiskers=True,
        roundLabels=2,
        saveFile='',
        sizeLabels=12,
        sizeTitle=16,
        title='',
        units='$\degree$C',
        xNames='',
        xRotate=0,
        yLabel='$\dfrac{d^2y}{dx^2}$',
        yLim=None):

    nBoxes = len(data)
    if colorList is None:
        colorList = ['white'] * nBoxes
        mcolorList = ['black'] * nBoxes
    else:
        mcolorList = colorList

    fig, ax = plt.subplots(figsize=figSize)
    fig.subplots_adjust(
        left=0.075, right=0.95,
        top=0.9, bottom=0.25)

    bp = ax.boxplot(data, notch=False,
                    vert=True, whis=1.5)
    plt.setp(bp['boxes'], color='black')
    plt.setp(bp['medians'], color='black', alpha=0.0)
    plt.setp(bp['whiskers'], color='black')
    if plotWhiskers:
        wsize = 1.0
    else:
        wsize = 0.0
    plt.setp(bp['fliers'], color='black',
             marker='+', markersize=wsize)

    # Add a horizontal grid to the plot
    ax.yaxis.grid(True, linestyle='-',
                  which='major',
                  color='lightgrey',
                  alpha=0.5)

    # fill the boxes with desired colors
    medians = np.empty(nBoxes)
    for i in range(nBoxes):
        box = bp['boxes'][i]
        box_x = []
        box_y = []
        for j in range(5):
            box_x.append(box.get_xdata()[j])
            box_y.append(box.get_ydata()[j])
        box_coords = np.column_stack([box_x, box_y])
        ax.add_patch(Polygon(
            box_coords, facecolor=colorList[i]))
        # draw the median lines back over what we just filled in
        med = bp['medians'][i]
        median_x = []
        median_y = []
        for j in range(2):
            median_x.append(med.get_xdata()[j])
            median_y.append(med.get_ydata()[j])
            ax.plot(median_x, median_y, 'k')
        medians[i] = median_y[0]
        # overplot the sample averages,
        # with horizontal alignment
        # in the center of each box
        ax.plot(np.average(med.get_xdata()),
                np.average(data[i]),
                color='w', marker='*', markeredgecolor='k')

    # add upper x-axis tick labels with sample medians
    pos = np.arange(nBoxes) + 1
    upper_labels = [str(round(s, roundLabels))
                    for s in medians]
    weights = ['bold', 'semibold']
    for tick, label in zip(range(nBoxes),
                           ax.get_xticklabels()):
        k = tick % 2
        ax.text(pos[tick], .95, upper_labels[tick],
                transform=ax.get_xaxis_transform(),
                horizontalalignment='center',
                size='medium',
                weight=weights[k],
                color=mcolorList[tick])

    # Set the axes ranges and axes labels
    ax.set_xlim(0.5, nBoxes + 0.5)
    if yLim is not None:
        ax.set_ylim(yLim)

    if units is not None:
        yLabel += ' [{}]'.format(units)
    ax.set_ylabel(yLabel, fontsize=sizeLabels)

    ax.set_xticklabels(xNames,
                       rotation=xRotate,
                       fontsize=sizeLabels)
    ax.tick_params(axis='y',
                   which='major', labelsize=sizeLabels)

    ax.set_title(title, fontsize=sizeTitle)
    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()
    plt.close()
    return


def plot_combo3(
    obsDict, rapDict, predDict,
        goesData, rtmaData,
        figSize=(10, 5),
        option_dict=DEFAULT_OPTION_DICT,
        saveDPI=200, saveFile='',
        text1='', text2='', text3='',
        text4='', text5='', text6='',
        textw1='', textw2='', textw3='',
        textw4='', textw5='', textw6='',
        texts1='', texts2='', texts3='',
        texts4='', texts5='', texts6='',
        textSize=14,
        title='', titleSize=15):

    # Create Figure
    fig = plt.figure(figsize=figSize, constrained_layout=False)
    gs = gridspec.GridSpec(nrows=2, ncols=5)

    # ==== SkewT
    fig, skew = plot_sounding_results(
        obsDict, rapDict, predDict,
        fig=fig, gs=gs[:, :3],
        option_dict=option_dict,
        return_objects=True)

    # ==== RTMA
    fig, rax = plot_rtma_patch_box(
        rtmaData, fig=fig, gs=gs[:, -3],
        return_objects=True)
    l, b, w, h = rax.get_position().bounds
    rax.set_position([l + 0.21, b + 0.04, w + 0.02, h + 0.05])

    # ==== GOES
    fig, gax = plot_goes_patch_box(
        goesData,
        fig=fig, gs=gs[:, -2],
        return_objects=True)
    l, b, w, h = gax.get_position().bounds
    gax.set_position([l + 0.38, b + 0.06, w + 0.18, h + 0.05])

    if title != '':
        plt.figtext(0.36, 0.96, title,
                    verticalalignment='bottom',
                    horizontalalignment='center',
                    fontsize=titleSize,
                    fontweight='bold')

    colorRAP = colour_from_numpy_to_tuple(option_dict[NWP_LINE_COLOUR_KEY])
    colorPRED = colour_from_numpy_to_tuple(
        option_dict[PREDICTED_LINE_COLOUR_KEY])
    hAlignment = 'right'
    vAlignment = 'top'
    x1 = 0.52
    x2 = 0.65
    x3 = 0.75
    y1 = 0.06
    y2 = 0.00
    if text1 != '':
        plt.figtext(x1, y1, text1,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    fontsize=textSize,
                    fontweight='bold')
    if text2 != '':
        plt.figtext(x1, y2, text2,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    fontsize=textSize,
                    fontweight='bold')
    if text3 != '':
        plt.figtext(x2, y1, text3,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorRAP,
                    fontsize=textSize)
    if text4 != '':
        plt.figtext(x2, y2, text4,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorPRED,
                    fontsize=textSize)
    if text5 != '':
        plt.figtext(x3, y1, text5,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorRAP,
                    fontsize=textSize)
    if text6 != '':
        plt.figtext(x3, y2, text6,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorPRED,
                    fontsize=textSize)

    x1 = 0.84
    x2 = 0.93
    x3 = 1.03
    y1 = 0.06
    y2 = 0.00
    if textw1 != '':
        plt.figtext(x1, y1, textw1,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    fontsize=textSize,
                    fontweight='bold')
    if textw2 != '':
        plt.figtext(x1, y2, textw2,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    fontsize=textSize,
                    fontweight='bold')
    if textw3 != '':
        plt.figtext(x2, y1, textw3,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorRAP,
                    fontsize=textSize)
    if textw4 != '':
        plt.figtext(x2, y2, textw4,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorPRED,
                    fontsize=textSize)
    if textw5 != '':
        plt.figtext(x3, y1, textw5,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorRAP,
                    fontsize=textSize)
    if textw6 != '':
        plt.figtext(x3, y2, textw6,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorPRED,
                    fontsize=textSize)

    x1 = 1.11
    x2 = 1.20
    x3 = 1.30
    y1 = 0.06
    y2 = 0.00
    if texts1 != '':
        plt.figtext(x1, y1, texts1,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    fontsize=textSize,
                    fontweight='bold')
    if texts2 != '':
        plt.figtext(x1, y2, texts2,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    fontsize=textSize,
                    fontweight='bold')
    if texts3 != '':
        plt.figtext(x2, y1, texts3,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorRAP,
                    fontsize=textSize)
    if texts4 != '':
        plt.figtext(x2, y2, texts4,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorPRED,
                    fontsize=textSize)
    if texts5 != '':
        plt.figtext(x3, y1, texts5,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorRAP,
                    fontsize=textSize)
    if texts6 != '':
        plt.figtext(x3, y2, texts6,
                    verticalalignment=vAlignment,
                    horizontalalignment=hAlignment,
                    color=colorPRED,
                    fontsize=textSize)

    if saveFile != '':
        save_figure(saveFile, dpi=saveDPI)
    else:
        plt.show()
    plt.close()

    return


def plot_discard_dict(
    discardDict,
        bar_color=QUANT_COLOR_LIST,
        bar_label=None,
        bar_label_results=True,
        font_size=18,
        legend_show=True,
        legend_loc='lower left',
        legend_size=14,
        save_file='',
        targetNum=0,
        title='',
        units='',
        ylabel='',
        ylim=None):
    expList = list(discardDict.keys())
    nExps = len(expList)

    discard_bins = discardDict[expList[0]]['discard_bins'][targetNum]
    discard_labels = list_to_str(
        discard_bins, format="%.1f")
    nBins = len(discard_labels)

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    barWidth = 0.8 / nExps
    r1 = np.arange(nBins) - barWidth * 0.5 * nExps

    iExp = 0
    for exp in expList:
        eDict = discardDict[exp]
        discard_vals = eDict['discard_vals'][targetNum]
        rNow = [x + barWidth * iExp for x in r1]

        if bar_label is not None:
            barLabel = bar_label[iExp]
        else:
            barLabel = exp.upper()
        if bar_label_results:
            try:
                barLabel += " (MF= {:.1f}, DI= {:.3f})".format(
                    eDict['discard_mf'][targetNum],
                    eDict['discard_imprv'][targetNum])
            except KeyError:
                pass

        ax.bar(rNow, discard_vals,
               color=bar_color[iExp],
               edgecolor='black',
               label=barLabel,
               width=barWidth)
        iExp += 1

    if legend_show and bar_label is not None:
        ax.legend(fontsize=legend_size,
                  loc=legend_loc)

    rfinal = np.arange(nBins) - barWidth / 2 * 0.25 * nExps

    ax.set_xticks(rfinal, discard_labels)
    ax.set_xlabel("Fraction Removed")

    if ylim is not None:
        ax.set_ylim(ylim)

    if not ylabel:
        if units:
            ylabel = "RMSE  [{}]".format(units)
        else:
            ylabel = "RMSE"
    ax.set_ylabel(ylabel)
    ax.set_title(title, fontsize=font_size)
    set_plot_fonts_ax(ax, plotFontSize=font_size)

    if save_file:
        save_figure(save_file)
    else:
        plt.show()

    plt.close()
    return


def plot_dxdv(
    labels, values,
        align='center',
        edgeColor='black',
        figSize=(7, 4),
        fontSize=16,
        labelBars=['Mean', 'Min', 'Max'],
        labelX='', labelY='dX/dV (K)',
        legendSize=14,
        saveFile="",
        title="",
        width=0.7):
    """Create basic bar plot."""

    fig = plt.figure(figsize=figSize)
    ax = fig.add_axes([0, 0, 1, 1])

    nGroups = len(labels)
    nBarsPerGroup = len(values)
    barWidth = width / nBarsPerGroup
    xvalsOrig = np.array(range(nGroups))
    xvals = np.array(range(nGroups)) - barWidth

    ax.bar(xvals, values[0],
           align=align,
           edgecolor=edgeColor,
           label=labelBars[0],
           width=barWidth)

    xvals += barWidth
    ax.bar(xvals, values[1],
           align=align,
           edgecolor=edgeColor,
           label=labelBars[1],
           width=barWidth)

    xvals += barWidth
    ax.bar(xvals, values[2],
           align=align,
           edgecolor=edgeColor,
           label=labelBars[2],
           width=barWidth)

    ax.set_title(title, fontsize=fontSize)
    ax.set_xlabel(labelX)
    ax.set_ylabel(labelY)
    ax.set_xticks(xvalsOrig)
    ax.set_xticklabels(labels)
    ax.legend(fontsize=legendSize)
    set_plot_fonts_ax(ax, plotFontSize=fontSize)

    if saveFile == "":
        plt.show()
    else:
        save_figure(saveFile)

    plt.close()
    return


def plot_fft(xArray, fftList,
             figSize=(6, 4), fontSize=14,
             labelList=[], legendShow=True,
             saveFile='', showGrid=True,
             title='', titleSize=16,
             xlims=[0, 127],
             ylims=[]):

    nFFTs = len(fftList)
    if not labelList:
        labelList = []
        for _ in range(nFFTs):
            labelList.append(None)
        legendShow = False

    _, ax = plt.subplots(1, 1, figsize=figSize)
    for fftArray, fftLabel in zip(fftList, labelList):
        ax.plot(xArray, fftArray, label=fftLabel)

    ax.set_xlabel('Frequency', fontsize=fontSize)
    ax.set_ylabel('Power', fontsize=fontSize)
    ax.set_xlim(xlims)
    if ylims:
        ax.set_ylim(ylims)
    ax.set_title(title, fontsize=titleSize)
    if legendShow:
        ax.legend()
    if showGrid:
        ax.grid()

    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()
    plt.close()
    return


def plot_goes_patch_box(
    data,
        channels=DEFAULT_GOES_CHANNELS,
        fig=None, gs=None,
        return_objects=False,
        sizeTitle=14, sizeLabels=12,
        yLabel=DEFAULT_GOES_UNITS,
        yLim=[0, 140]):

    errString = "PLOT_GOES_PATCH_BOX ERROR:"
    nChannels = len(channels)
    if data.shape[-1] != nChannels:
        print("{} Mismatch Between Data and Labels.".
              format(errString))
        return

    if fig is None:
        fig = plt.figure(figsize=(6, 4))
    if gs is None:
        gs = gridspec.GridSpec(nrows=1, ncols=1, figure=fig)[0, 0]

    dataList = []
    for i in range(nChannels):
        dataHere = data[..., i].reshape(-1)
        dataList.append(dataHere)

    ax = fig.add_subplot(gs)
    box = ax.boxplot(dataList)
    ax.set_title('GOES', fontsize=sizeTitle)
    ax.set_xlabel('Channel', fontsize=sizeLabels)
    ax.set_ylabel(yLabel, fontsize=sizeLabels)
    if yLim is not None:
        ax.set_ylim(yLim)

    xList = [item for item in range(1, nChannels + 1)]
    ax.set_xticks(xList)
    ax.set_xticklabels(channels)

    for element in ['boxes', 'whiskers', 'fliers',
                    'means', 'medians', 'caps']:
        plt.setp(box[element], color='k')

    if return_objects:
        return fig, ax
    else:
        plt.show()
        plt.close()
        return


def plot_improvement_bars(
    binCenters, predVals,
        barAlpha=1.0,
        colorList=QUANT_COLOR_ANOMS,
        figSize=(7, 5),
        labelSize=13,
        labelx='',
        labely='Fraction',
        legendLoc='upper left',
        legendnCol=2,
        legendShow=True,
        legendSize=11,
        plotBars=False,
        predLabelList=[],
        plot5050=True,
        saveFile='',
        title='',
        titleSize=18,
        width=30.,
        ylim=None):

    fig = plt.figure(figsize=figSize)
    ax = fig.add_subplot(1, 1, 1)

    ax.set_xlabel(labelx, fontsize=labelSize)
    ax.set_ylabel(labely, fontsize=labelSize)
    ax.set_title(title, fontsize=titleSize)

    if plotBars:
        nModels = len(predVals)
        binCentersH = np.array(binCenters)
        binAdjust = -(nModels - 1) * width \
            + width / 2
        for i in range(nModels):
            ax.bar(binCentersH + binAdjust,
                   predVals[i],
                   alpha=barAlpha,
                   color=colorList[i],
                   width=width,
                   label=predLabelList[i])
            binAdjust += width
    else:
        nModels = len(predVals)
        for i in range(nModels):
            ax.plot(binCenters, predVals[i],
                    '*-',
                    color=colorList[i],
                    label=predLabelList[i])

    if plot5050:
        xlimOld = ax.get_xlim()
        xextend = [binCenters[0] - 100,
                   binCenters[-1] + 200]
        ax.plot(xextend, [0.5, 0.5],
                color='gray', alpha=0.5, linestyle='--')
        ax.set_xlim(xlimOld)
        plt.locator_params(axis='x', nbins=10)

    if ylim is not None:
        ax.set_ylim(ylim)

    if legendShow:
        ax.legend(fontsize=legendSize,
                  loc=legendLoc, ncol=legendnCol)

    set_plot_fonts_ax(ax, plotFontSize=labelSize)

    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()

    plt.close()
    return


def plot_lineFits(yLine, arrayList, fitList,
                  figSize=(6, 4),
                  fontSize=16,
                  labelList=[],
                  legendShow=True,
                  saveFile='',
                  title='', titleSize=16):
    nLines = len(fitList)
    if not labelList:
        labelList = []
        for _ in range(nLines):
            labelList.append(None)
        legendShow = False

    _, ax = plt.subplots(1, 1, figsize=figSize)
    for ar, ft, lbl in zip(arrayList, fitList, labelList):
        ax.scatter(ar, yLine, s=2)
        # ax.plot(ar, ft, label=lbl)
        ax.plot(ft, yLine, label=lbl)

    ax.set_xlabel('K', fontsize=fontSize)
    ax.set_ylabel('Vertical Level', fontsize=fontSize)
    ax.set_title(title, fontsize=titleSize)
    if legendShow:
        ax.legend()

    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()
    plt.close()


def plot_loss(histDict,
              figSize=(8, 6), fontSize=14,
              legendShow=True, saveFile='',
              title=''):

    trLoss = histDict['loss'][1:]
    try:
        valLoss = histDict['val_loss'][1:]
    except KeyError:
        valLoss = None

    _, ax = plt.subplots(figsize=figSize)
    ax.plot(np.asarray(trLoss), label='Train')
    if valLoss is not None:
        ax.plot(np.asarray(valLoss), label='Validation')
    plt.xlabel("Epoch", fontsize=fontSize)
    plt.ylabel("Loss", fontsize=fontSize)
    plt.title(title)

    if legendShow:
        plt.legend(fancybox=True, framealpha=1.0, loc='upper right',
                   ncol=2, prop={'size': fontSize})

    if saveFile != '':
        save_figure(saveFile)
    else:
        plt.show()
    plt.close()

    return


def plot_mean_skills_cape(
        binCenters,
        predSkills,
        barAlpha=0.4,
        binCounts=None,
        colorList=QUANT_COLOR_ANOMS,
        figSize=(9, 6),
        labelSize=16,
        labelx='Value',
        labely1='RMSE',
        legendShow=True,
        legendSize=14,
        linewidth=2,
        markersize=10,
        predImprove=None,
        predLabelList=[],
        saveFile='',
        title='',
        titleSize=20,
        trueColor='black',
        units='',
        width=50.):

    if units:
        labely1 = labely1 + ' [' + units + ']'
        # labely2 = labely2 + ' [' + units + ']'

    fig = plt.figure(figsize=figSize)
    ax = fig.add_subplot(1, 1, 1)

    nModels = len(predSkills)
    for i in range(nModels):
        pskills = predSkills[i]
        ax.plot(binCenters,
                pskills,
                '*-',
                color=colorList[i],
                label=predLabelList[i],
                linewidth=linewidth,
                markersize=markersize)

    ax.set_xlabel(labelx, fontsize=labelSize)
    ax.set_ylabel(labely1, fontsize=labelSize)
    ax.set_title(title, fontsize=titleSize)
    if legendShow:
        ax.legend(fontsize=legendSize, ncol=2)

    if binCounts is not None:
        if predImprove is not None:
            binCentersH = np.array(binCenters) - width / 2
        else:
            binCentersH = np.array(binCenters)
        ax2 = ax.twinx()
        ax2.bar(binCentersH,
                binCounts,
                alpha=barAlpha,
                color=trueColor,
                width=width)
        if predImprove is None:
            ax2.set_ylabel('Counts')

    if predImprove is not None:
        if binCounts is not None:
            binCentersH = np.array(binCenters) + width / 2
        else:
            ax2 = ax.twinx()
            binCentersH = np.array(binCenters)
        ax2.bar(binCentersH,
                predImprove,
                alpha=barAlpha,
                color=colorList[-1],
                width=width)
        ax2.set_ylim([0., 1.0])

    set_plot_fonts_ax(ax, plotFontSize=labelSize)
    set_plot_fonts_ax(ax2, plotFontSize=labelSize)
    # set_plot_fonts_ax(ax3, plotFontSize=labelSize)

    ax.set_zorder(1)
    # plt.set_zorder(ax2.get_zorder() + 1)
    ax.set_frame_on(False)

    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()

    plt.close()
    return


def plot_pit_dict(
    pDict,
        bar_color=QUANT_COLOR_LIST,
        bar_label=None,
        font_size=14,
        hline=0.1,
        legend_show=True,
        legend_loc='lower right',
        legend_size=14,
        save_file='',
        show_error=True,
        targetNum=0,
        title=''):
    expList = list(pDict.keys())
    nExps = len(expList)

    pit_centers = pDict[expList[0]]['pit_centers'][targetNum]
    pit_labels = list_to_str(pit_centers)
    nBins = len(pit_centers)

    _, ax = plt.subplots(1, 1, figsize=(10, 6))

    barWidth = 0.8 / nExps
    r1 = np.arange(nBins) - barWidth * 0.5 * nExps

    iExp = 0
    for exp in expList:
        eDict = pDict[exp]
        pit_counts = eDict['pit_counts'][targetNum]
        rNow = [x + barWidth * iExp for x in r1]

        if bar_label is None:
            barLabel = exp.upper()
        else:
            barLabel = bar_label[iExp]
        if show_error:
            try:
                pit_dvalue = eDict['pit_dvalue'][targetNum]
                barLabel += ' ({:.3f})'.format(pit_dvalue)
                if iExp == 0:
                    pit_evalue = eDict['pit_evalue']
                    if pit_evalue > 0.0009:
                        barLabel += '(E: {:.3f})'.format(pit_evalue)
            except KeyError:
                pass

        ax.bar(rNow, pit_counts,
               color=bar_color[iExp],
               edgecolor='black',
               label=barLabel,
               width=barWidth)
        iExp += 1

    if legend_show and bar_label is not None:
        ax.legend(framealpha=1.0,
                  fontsize=legend_size,
                  loc=legend_loc)

    rfinal = np.arange(nBins) - barWidth / 2 * 0.25 * nExps

    if hline is not None:
        xlims = ax.get_xlim()
        ax.plot(xlims, [hline, hline],
                alpha=0.6,
                color='black',
                linestyle='--')
        ax.set_xlim(xlims)

    ax.set_xticks(rfinal, pit_labels)
    ax.set_xlabel("PIT")
    ax.set_ylabel("Probability")
    ax.set_title(title, fontsize=font_size)
    set_plot_fonts_ax(ax, plotFontSize=font_size)

    if save_file:
        save_figure(save_file)
    else:
        plt.show()

    plt.close()
    return


def plot_profiles(
    arList,
    colorList=QUANT_COLOR_ANOMS,
        figSize=(6, 4), fontSize=14,
        labelList=[],
        labelX='$\degree$C',
        labelY='Level',
        legendShow=True,
        saveFile='', showGrid=True,
        title='', titleSize=16,
        usePressure=False,
        yLine=None):

    nACs = len(arList)
    if not labelList:
        labelList = []
        for _ in range(nACs):
            labelList.append(None)
        legendShow = False

    if yLine is None:
        yLine = np.array(range(arList[0].shape[0]))
    _, ax = plt.subplots(1, 1, figsize=figSize)
    for ar, ca, la in zip(arList, colorList, labelList):
        ax.plot(ar, yLine,
                color=ca,
                label=la)

    if usePressure:
        ylimHere = [yLine.max(), yLine.min()]
        ax.set_ylim(ylimHere)
    ax.set_xlabel(labelX, fontsize=fontSize)
    ax.set_ylabel(labelY, fontsize=fontSize)
    ax.set_title(title, fontsize=titleSize)
    if legendShow:
        ax.legend()
    if showGrid:
        ax.grid()

    if saveFile:
        save_figure(saveFile)
    else:
        plt.show()
    plt.close()
    return


def plot_rtma_patch_box(
    data,
        fig=None, gs=None,
        color1=COLOR_MAROON_NP,
        color2=COLOR_BLUEDARK_NP,
        labels=DEFAULT_RTMA_LABELS,
        plotmb=True,
        return_objects=False,
        sizeTitle=14, sizeLabels=12,
        yLimP=[860, 1033], yLimT=[240, 315]):

    errString = "PLOT_TRMA_PATCH_BOX ERROR:"
    nChannels = len(labels)
    if data.shape[-1] != nChannels:
        print("{} Mismatch Between Data and Labels.".
              format(errString))
        return

    if fig is None:
        fig = plt.figure(figsize=(3, 4))
    if gs is None:
        gs = gridspec.GridSpec(nrows=1, ncols=1, figure=fig)[0, 0]

    color1 = colour_from_numpy_to_tuple(color1)
    color2 = colour_from_numpy_to_tuple(color2)

    tempList = [data[..., 2].reshape(-1), data[..., 1].reshape(-1)]
    ax1 = fig.add_subplot(gs)
    res1 = ax1.boxplot(tempList, patch_artist=True)
    ax1.set_title('RTMA', fontsize=sizeTitle)
    ax1.set_ylabel('Temperature ($K$)', fontsize=sizeLabels, color=color1)

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(res1[element], color='k')
    for patch in res1['boxes']:
        try:
            patch.set_facecolor(color1)
        except AttributeError:
            pass

    ax2 = ax1.twinx()
    if plotmb:
        ps = np.multiply(data[..., 0].reshape(-1), 0.01)
        ax2.set_ylabel('Pressure ($mb$)', fontsize=sizeLabels, color=color2)
    else:
        ps = data[..., 0].reshape(-1)
        ax2.set_ylabel('Pressure ($Pa$)', fontsize=sizeLabels, color=color2)

    res2 = ax2.boxplot(ps,
                       positions=[3], patch_artist=True)

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(res2[element], color='k')

    for patch in res2['boxes']:
        patch.set_facecolor(color2)

    ax1.set_xlim([0.5, 3.5])
    ax1.set_xticks([1, 2, 3])
    ax1.set_xticklabels(labels)

    if yLimT is not None:
        ax1.set_ylim(yLimT)
    if yLimP is not None:
        ax2.set_ylim(yLimP)

    if return_objects:
        return fig, ax1
    else:
        plt.show()
        plt.close()
        return


def plot_sounding_results(
    sounding_dict, rap_dict, pred_dict,
        cape_cin=False, cape_type='ml',
        fig=None, gs=None,
        file_name=None,
        option_dict=None,
        return_objects=False,
        title_string=None,
        verbose=False):
    """Plots atmospheric sounding.

    H = number of vertical levels in sounding
    :params
    ---
        sounding_dict : dict
            The following keys: pressures_mb, temperatures_deg_c,
            dewpoints_deg_c, wind_speed_kt, wind_dir_deg
        title_string : str
        option_dict : dict
        file_name : str
            Default `None` does not save profile to disk
    :return
    ---
        figure_object : Figure handle
        skewt_object : SkewT handle (skewt_object.ax)
    """
    if option_dict is None:
        option_dict = DEFAULT_OPTION_DICT.copy()
    if file_name:
        _init_save_img(option_dict)

    main_line_width = option_dict[MAIN_LINE_WIDTH_KEY]
    dewpoint_linestyle = option_dict[LINESTYLE_TD_KEY]
    temperature_linestyle = option_dict[LINESTYLE_T_KEY]
    figure_object, skewt_object = _init_skewT(
        option_dict,
        fig=fig,
        gs=gs)

    obsDewpoint = convert_metpy_dewpoint(sounding_dict)
    obsPressure = convert_metpy_pressure(sounding_dict)
    obsTemperature = convert_metpy_temperature(sounding_dict)
    if verbose:
        print("OBS MIN/MAX: {:.3f}, {:.3f}".
              format(np.min(obsTemperature), np.max(obsTemperature)))

    skewt_object.plot(
        obsPressure, obsTemperature,
        color=colour_from_numpy_to_tuple(
            option_dict[MAIN_LINE_COLOUR_KEY]),
        linewidth=main_line_width,
        linestyle=temperature_linestyle,
        label='RAOB')  # + '$_T$')

    skewt_object.plot(
        obsPressure, obsDewpoint,
        color=colour_from_numpy_to_tuple(
            option_dict[MAIN_LINE_COLOUR_KEY]),
        linewidth=main_line_width,
        linestyle=dewpoint_linestyle)
    # label='RAOB' + '$_{Td}$')
    try:
        rapDewpoint = convert_metpy_dewpoint(rap_dict)
        rapPressure = convert_metpy_pressure(rap_dict)
        rapTemperature = convert_metpy_temperature(rap_dict)
        skewt_object.plot(
            rapPressure, rapTemperature,
            color=colour_from_numpy_to_tuple(
                option_dict[NWP_LINE_COLOUR_KEY]),
            linewidth=main_line_width,
            linestyle=temperature_linestyle,
            label='RAP')  # + '$_T$')

        skewt_object.plot(
            rapPressure, rapDewpoint,
            color=colour_from_numpy_to_tuple(
                option_dict[NWP_LINE_COLOUR_KEY]),
            linewidth=main_line_width,
            linestyle=dewpoint_linestyle)

        if verbose:
            print("RAP MIN/MAX: {:.3f}, {:.3f}".
                  format(np.min(rapTemperature), np.max(rapTemperature)))
    except KeyError:
        pass

    if pred_dict:
        try:
            pColor = colour_from_numpy_to_tuple(
                option_dict[PREDICTED_LINE_COLOUR_KEY])
            pUQAlpha = option_dict[UQ_ALPHA_KEY]

            predDewpoint = convert_metpy_dewpoint(pred_dict)
            pTDUQL, pTDUQH = convert_metpy_dewpointUQ(pred_dict)
            predPressure = convert_metpy_pressure(pred_dict)
            predTemperature = convert_metpy_temperature(pred_dict)
            pTUQL, pTUQH = convert_metpy_temperatureUQ(pred_dict)
            skewt_object.plot(
                predPressure, predTemperature,
                color=pColor,
                linewidth=main_line_width,
                linestyle=temperature_linestyle,
                label='ML')  # + '$_T$')

            if pTUQL is not None:
                skewt_object.plot(predPressure, pTUQL,
                                  alpha=pUQAlpha, color=pColor,
                                  label='ML 95%',
                                  linewidth=main_line_width)

            if pTUQH is not None:
                skewt_object.plot(predPressure, pTUQH,
                                  alpha=pUQAlpha, color=pColor,
                                  linewidth=main_line_width)

            skewt_object.plot(
                predPressure, predDewpoint,
                color=colour_from_numpy_to_tuple(
                    option_dict[PREDICTED_LINE_COLOUR_KEY]),
                linewidth=main_line_width,
                linestyle=dewpoint_linestyle)

            if pTDUQL is not None:
                skewt_object.plot(
                    predPressure, pTDUQL,
                    alpha=pUQAlpha, color=pColor,
                    linewidth=main_line_width,
                    linestyle=dewpoint_linestyle)

            if pTDUQH is not None:
                skewt_object.plot(
                    predPressure, pTDUQH,
                    alpha=pUQAlpha, color=pColor,
                    linewidth=main_line_width,
                    linestyle=dewpoint_linestyle)

            if verbose:
                print(
                    "PRED MIN/MAX: {:.3f}, {:.3f}".
                    format(np.min(predTemperature), np.max(predTemperature)))
        except KeyError and TypeError:
            print("EXCEPTING??? ")
            pass

    _plot_sounding_attributes(skewt_object, option_dict, title_string)

    skewt_object.ax.set_xlabel('Temperature [C]')
    skewt_object.ax.set_ylabel('Pressure [mb]')
    skewt_object.ax.legend(handlelength=option_dict[HANDLE_LENGTH_KEY])

    if cape_cin:
        # Calculate LCL height and plot as black dot. Because `p`'s
        # first value is
        # ~1000 mb and its last value is ~250 mb, the `0` index is
        # selected for `p`, `T`, and `Td` to lift the parcel from the
        # surface. If `p` was inverted, i.e. start from low value, 250 mb,
        # to a high value, 1000 mb, the `-1` index should be selected.
        lcl_pressure, lcl_temperature = mcalc.lcl(
            obsPressure[0], obsTemperature[0], obsDewpoint[0])
        skewt_object.plot(lcl_pressure, lcl_temperature,
                          'ko', markerfacecolor='black')

        # Calculate full parcel profile and add to plot as black line
        if cape_type == 'sfc':
            tStart = obsTemperature[0]
            dpStart = obsDewpoint[0]
        else:
            _, t_mixed, dp_mixed = mcalc.mixed_parcel(
                obsPressure, obsTemperature, obsDewpoint)
            tStart = t_mixed
            dpStart = dp_mixed
        prof = mcalc.parcel_profile(
            obsPressure, tStart, dpStart).to('degC')

        # skewt_object.plot(pressure, prof, 'k', linewidth=2)
        skewt_object.plot(obsPressure, prof,
                          option_dict[ISOHUME_COLOUR_KEY],
                          linewidth=main_line_width)

        # Shade areas of CAPE and CIN
        skewt_object.shade_cin(obsPressure, obsTemperature, prof)
        skewt_object.shade_cape(obsPressure, obsTemperature, prof)

    if file_name:
        plt.savefig(file_name, dpi=option_dict[DOTS_PER_INCH_KEY])
        print("Saved Figure: {}".format(file_name))

    if return_objects:
        return figure_object, skewt_object
    else:
        # plt.legend()
        plt.show()
        plt.close()
        return


def plot_sounding_results_dict(
        sDict,
        cape_cin=False,
        cape_type='ml',
        file_name=None,
        show_legend=True,
        show_legend_loc="lower left",
        temp_min=None,
        temp_max=None,
        title_string=None):
    """Plots atmospheric sounding."""

    option_dict = DEFAULT_OPTION_DICT.copy()
    if temp_min is not None:
        option_dict[TEMPERATURE_MIN_KEY] = temp_min
    if temp_max is not None:
        option_dict[TEMPERATURE_MAX_KEY] = temp_max
    if file_name:
        _init_save_img(option_dict)

    main_line_width = option_dict[MAIN_LINE_WIDTH_KEY]
    dewpoint_linestyle = option_dict[LINESTYLE_TD_KEY]
    temperature_linestyle = option_dict[LINESTYLE_T_KEY]
    figure_object, skewt_object = _init_skewT(option_dict)

    keyList = list(sDict.keys())
    for key in keyList:
        sounding_dict = sDict[key]
        dewpoint = convert_metpy_dewpoint(sounding_dict)
        pressure = convert_metpy_pressure(sounding_dict)
        temperature = convert_metpy_temperature(sounding_dict)
        colorHere = sounding_dict['color']
        skewt_object.plot(
            pressure, temperature,
            color=colorHere,
            linewidth=main_line_width,
            linestyle=temperature_linestyle,
            label=key)

        skewt_object.plot(
            pressure, dewpoint,
            color=colorHere,
            linewidth=main_line_width,
            linestyle=dewpoint_linestyle)

    _plot_sounding_attributes(skewt_object, option_dict, title_string)

    skewt_object.ax.set_xlabel('Temperature [$\degree$C]')
    skewt_object.ax.set_ylabel('Pressure [mb]')
    if show_legend:
        skewt_object.ax.legend(
            handlelength=option_dict[HANDLE_LENGTH_KEY],
            fontsize=option_dict[DEFAULT_LABEL_SIZE_KEY],
            loc=show_legend_loc)

    if cape_cin:
        # Calculate LCL height and plot as black dot. Because `p`'s
        # first value is
        # ~1000 mb and its last value is ~250 mb, the `0` index is
        # selected for `p`, `T`, and `Td` to lift the parcel from the
        # surface. If `p` was inverted, i.e. start from low value, 250 mb,
        # to a high value, 1000 mb, the `-1` index should be selected.
        sounding_dict = sDict[keyList[0]]
        dewpoint = convert_metpy_dewpoint(sounding_dict)
        pressure = convert_metpy_pressure(sounding_dict)
        temperature = convert_metpy_temperature(sounding_dict)
        lcl_pressure, lcl_temperature = mcalc.lcl(
            pressure[0], temperature[0], dewpoint[0])
        skewt_object.plot(lcl_pressure, lcl_temperature,
                          'ko', markerfacecolor='black')

        # Calculate full parcel profile and add to plot as black line
        if cape_type == 'sfc':
            tStart = temperature[0]
            dpStart = dewpoint[0]
        else:
            _, t_mixed, dp_mixed = mcalc.mixed_parcel(
                pressure, temperature, dewpoint)
            tStart = t_mixed
            dpStart = dp_mixed
        prof = mcalc.parcel_profile(
            pressure, tStart, dpStart).to('degC')

        # skewt_object.plot(pressure, prof, 'k', linewidth=2)
        skewt_object.plot(pressure, prof,
                          option_dict[ISOHUME_COLOUR_KEY],
                          linewidth=main_line_width)

        # Shade areas of CAPE and CIN
        skewt_object.shade_cin(pressure, temperature, prof)
        skewt_object.shade_cape(pressure, temperature, prof)

    if file_name:
        plt.savefig(file_name, dpi=option_dict[DOTS_PER_INCH_KEY])
        print("Saved Figure: {}".format(file_name))
    else:
        plt.show()

    plt.close()
    return


def plot_sounding_results_uq(
        sDict,
        cape_cin=False,
        cape_type='ml',
        file_name=None,
        show_legend=True,
        show_legend_loc='lower left',
        temp_min=None,
        temp_max=None,
        title_string=None,
        use_uq_gray=False):
    """Plots atmospheric sounding."""

    option_dict = DEFAULT_OPTION_DICT.copy()
    uqargs = {'facecolor': 'tab:gray',
              'alpha': 0.3, 'where': None}
    if temp_min is not None:
        option_dict[TEMPERATURE_MIN_KEY] = temp_min
    if temp_max is not None:
        option_dict[TEMPERATURE_MAX_KEY] = temp_max
    if file_name:
        _init_save_img(option_dict)

    main_line_width = option_dict[MAIN_LINE_WIDTH_KEY]
    dewpoint_linestyle = option_dict[LINESTYLE_TD_KEY]
    temperature_linestyle = option_dict[LINESTYLE_T_KEY]
    figure_object, skewt_object = _init_skewT(option_dict)

    keyList = list(sDict.keys())
    for key in keyList:
        sounding_dict = sDict[key]
        dewpoint = convert_metpy_dewpoint(sounding_dict)
        pressure = convert_metpy_pressure(sounding_dict)
        temperature = convert_metpy_temperature(sounding_dict)
        colorHere = sounding_dict['color']
        try:
            colorUQ = sounding_dict['colorUQ']
        except KeyError:
            colorUQ = colorHere
        skewt_object.plot(
            pressure, temperature,
            color=colorHere,
            linewidth=main_line_width,
            linestyle=temperature_linestyle,
            label=key)

        skewt_object.plot(
            pressure, dewpoint,
            color=colorHere,
            linewidth=main_line_width,
            linestyle=dewpoint_linestyle)

        keysHere = list(sounding_dict.keys())
        if 't_err_left' in keysHere:
            t_err_left = sounding_dict['t_err_left']
            t_err_right = sounding_dict['t_err_right']

            if not use_uq_gray:
                uqargs['facecolor'] = colorUQ
            skewt_object.shade_area(
                pressure, t_err_left, t_err_right, which='both', **uqargs)

        if 'td_err_left' in keysHere:
            td_err_left = sounding_dict['td_err_left']
            td_err_right = sounding_dict['td_err_right']

            skewt_object.shade_area(
                pressure, td_err_left, td_err_right,
                # label='95% UQ',
                which='both', **uqargs)

    _plot_sounding_attributes(skewt_object, option_dict, title_string)

    skewt_object.ax.set_xlabel('\
        Temperature [$\degree$C]')
    skewt_object.ax.set_ylabel('Pressure [mb]')
    if show_legend:
        skewt_object.ax.legend(
            handlelength=option_dict[HANDLE_LENGTH_KEY],
            fontsize=option_dict[DEFAULT_LABEL_SIZE_KEY],
            loc=show_legend_loc)

    if cape_cin:
        dewpoint = convert_metpy_dewpoint(sounding_dict)
        pressure = convert_metpy_pressure(sounding_dict)
        temperature = convert_metpy_temperature(sounding_dict)
        lcl_pressure, lcl_temperature = mcalc.lcl(
            pressure[0], temperature[0], dewpoint[0])
        skewt_object.plot(lcl_pressure, lcl_temperature,
                          'ko', markerfacecolor='black')

        # Calculate full parcel profile and add to plot as black line
        if cape_type == 'sfc':
            tStart = temperature[0]
            dpStart = dewpoint[0]
        else:
            _, t_mixed, dp_mixed = mcalc.mixed_parcel(
                pressure, temperature, dewpoint)
            tStart = t_mixed
            dpStart = dp_mixed
        prof = mcalc.parcel_profile(
            pressure, tStart, dpStart).to('degC')

        # skewt_object.plot(pressure, prof, 'k', linewidth=2)
        skewt_object.plot(pressure, prof,
                          option_dict[ISOHUME_COLOUR_KEY],
                          linewidth=main_line_width)

        # Shade areas of CAPE and CIN
        skewt_object.shade_cin(pressure, temperature, prof)
        skewt_object.shade_cape(pressure, temperature, prof)

    if file_name:
        plt.savefig(file_name, dpi=option_dict[DOTS_PER_INCH_KEY])
        print("Saved Figure: {}".format(file_name))
    else:
        plt.show()

    plt.close()
    return


def plot_spread_skill_dict(
    sDict,
        figSize=(6, 6),
        fontSize=18,
        legend_show=True,
        legend_loc='lower right',
        legend_size=10,
        labelx='Spread (Uncertainty)',
        labely='Skill (RMSE)',
        lim_min=0.,
        lim_max=None,
        line_color=QUANT_COLOR_ANOMS,
        line_label=None,
        line_width=DEFAULT_LINE_WIDTH,
        plot_hist=False,
        save_file='',
        shiftx=0.0,
        shifty=0.0,
        spread_ticks=None,
        targetNum=0,
        title='Spread-Skill Plot',
        units=''):

    expList = list(sDict.keys())

    if units:
        labelx = labelx + ' [' + units + ']'
        labely = labely + ' [' + units + ']'

    if lim_max is None:
        lim_max = 0.
        for exp in expList:
            eDict = sDict[exp]
            error_vals = eDict['ss_error_vals'][targetNum]
            spread_vals = eDict['ss_spread_vals'][targetNum]
            lim_maxH = np.max(
                [spread_vals.max(), error_vals.max()]) \
                * 1.1
            if lim_maxH > lim_max:
                lim_max = lim_maxH
    lims = [lim_min, lim_max]

    fig = plt.figure(figsize=figSize)
    ax = fig.add_subplot(1, 1, 1)

    iExp = 0
    spread_counts = []
    spread_bins = []
    for exp in expList:
        eDict = sDict[exp]
        error_vals = eDict['ss_error_vals'][targetNum]
        errorTP = np.ma.masked_where(
            error_vals < 0., error_vals)

        spread_counts_exp = eDict['ss_spread_counts'][targetNum]
        spread_vals = eDict['ss_spread_vals'][targetNum]

        if line_label is not None:
            lineLabel = line_label[iExp]
        else:
            lineLabel = None

        try:
            lineLabel += ' (RAT={:.2f}, REL={:.2f})'.format(
                eDict['ss_ratio'], eDict['ss_reliability'])
        except KeyError:
            pass

        ax.plot(spread_vals, errorTP, 'x-',
                color=line_color[iExp],
                label=lineLabel,
                linewidth=line_width)
        ax.plot(lims, lims,
                alpha=0.4, color='black')

        if plot_hist:
            spread_counts.append(spread_counts_exp)
            spread_bins.append(spread_vals)

        iExp += 1

    if plot_hist:
        _plot_inset_counts(
            figure_object=fig,
            bin_centers=spread_bins,
            bin_counts=spread_counts,
            line_colour=line_color,
            shift_x=shiftx,
            shift_y=shifty,
            tick_vals=spread_ticks,
            title='Spread Freq')

    if legend_show:
        ax.legend(fontsize=legend_size,
                  loc=legend_loc)

    if spread_ticks is not None:
        ax.set_xticks(spread_ticks)

    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.set_xlabel(labelx)
    ax.set_ylabel(labely)
    ax.set_title(title, fontsize=fontSize)
    set_plot_fonts_ax(ax, plotFontSize=fontSize)

    if save_file:
        save_figure(save_file)
    else:
        plt.show()

    plt.close()
    return


def plot_spread_skill_dict_uq(
        sDict,
        figSize=(6, 6),
        fontSize=18,
        legend_show=True,
        legend_loc='lower right',
        legend_size=10,
        labelx='Spread (Uncertainty)',
        labely='Skill (RMSE)',
        lim_min=0.,
        lim_max=None,
        line_color=QUANT_COLOR_ANOMS,
        line_label=None,
        line_width=DEFAULT_LINE_WIDTH,
        plot_hist=False,
        save_file='',
        shiftx=0.0,
        shifty=0.0,
        show_grid=True,
        spread_ticks=None,
        title='Spread-Skill Plot',
        units=''):

    expList = list(sDict.keys())

    if units:
        labelx = labelx + ' [' + units + ']'
        labely = labely + ' [' + units + ']'

    if lim_max is None:
        lim_max = 0.
        for exp in expList:
            eDict = sDict[exp]
            error_vals = eDict['ss_error_vals']
            spread_vals = eDict['ss_spread_vals']
            lim_maxH = np.max(
                [spread_vals.max(), error_vals.max()])\
                * 1.1
            if lim_maxH > lim_max:
                lim_max = lim_maxH
    lims = [lim_min, lim_max]

    fig = plt.figure(figsize=figSize)
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(lims, lims,
            # alpha=0.4,
            color='black')

    iExp = 0
    spread_counts = []
    spread_bins = []
    for exp in expList:
        eDict = sDict[exp]
        # errorTP, spreadTP = _list_pull_valid(
        #    eDict['ss_error_vals'],
        #    eDict['ss_spread_vals'])
        error_vals = eDict['ss_error_vals']
        errorTP = np.ma.masked_where(
            error_vals < 0., error_vals)

        spread_counts_exp = eDict['ss_spread_counts']
        spread_vals = eDict['ss_spread_vals']

        if line_label is not None:
            lineLabel = line_label[iExp]
        else:
            lineLabel = exp.upper()

        try:
            lineLabel += ' (RAT={:.2f}, REL={:.2f})'.\
                format(
                    eDict['ss_ratio'],
                    eDict['ss_reliability'])
        except KeyError:
            pass

        ax.plot(spread_vals, errorTP, 'x-',
                color=line_color[iExp],
                label=lineLabel,
                linewidth=line_width)

        if plot_hist:
            spread_counts.append(spread_counts_exp)
            spread_bins.append(spread_vals)

        iExp += 1

    if plot_hist:
        if spread_ticks is None:
            spread_ticks = list_to_int(
                np.linspace(lim_min, lim_max, 6))
        _plot_inset_counts(
            fig,
            spread_bins,
            spread_counts,
            line_colour=line_color,
            shift_x=shiftx,
            shift_y=shifty,
            tick_vals=spread_ticks,
            title='Spread Freq')

    if legend_show:
        ax.legend(fontsize=legend_size, loc=legend_loc)

    if spread_ticks is not None:
        ax.set_xticks(spread_ticks)

    if show_grid:
        ax.grid(True)
    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.set_xlabel(labelx)
    ax.set_ylabel(labely)
    ax.set_title(title, fontsize=fontSize)
    set_plot_fonts_ax(ax, plotFontSize=fontSize)

    if save_file:
        save_figure(save_file)
    else:
        plt.show()

    plt.close()
    return


def plot_vertical_rmse(
    base_rmseList, ml_rmseList,
        figSize=(12, 6), title='',
        label1='RAP', label2='ML',
        labelSize=12, legendSize=13,
        lineWidth=2, titleSize=14,
        saveDPI=100, saveFile=''):
    """
    Plot the RMSE over different vertical levels.
    """

    # ... set colors
    base_color = DEFAULT_OPTION_DICT[NWP_LINE_COLOUR_KEY]
    ml_color = DEFAULT_OPTION_DICT[PREDICTED_LINE_COLOUR_KEY]

    fig, axs = plt.subplots(1, 2, figsize=figSize)

    ic = 0
    for v in ['temperature', 'dewpoint']:
        base_rmse = base_rmseList[ic]
        base_rmse_mean = np.mean(base_rmse)

        ml_rmse = ml_rmseList[ic]
        ml_rmse_mean = np.mean(ml_rmse)
        alt = np.arange(ml_rmse.shape[0])

        axs[ic].plot(base_rmse, alt, color=base_color, linewidth=lineWidth)
        axs[ic].axvline(base_rmse_mean,
                        label=f'{label1}: {base_rmse_mean:.3f}',
                        color=base_color,
                        linestyle='--',
                        linewidth=lineWidth)

        axs[ic].plot(ml_rmse, alt, color=ml_color, linewidth=lineWidth)
        axs[ic].axvline(ml_rmse_mean,
                        label=f'{label2}: {ml_rmse_mean:.3f}',
                        color=ml_color,
                        linestyle='--',
                        linewidth=lineWidth)

        axs[ic].set_ylabel('Altitude', fontsize=labelSize)
        axs[ic].set_xlabel('RMSE [C]', fontsize=labelSize)
        axs[ic].set_title(title + v.title(), fontsize=titleSize)
        axs[ic].legend(fontsize=legendSize)

        n_ticks = 5
        if len(alt) > 50:
            axs[ic].set_yticks(np.linspace(alt.min(), alt.max(), n_ticks))
            axs[ic].set_yticklabels(['sfc'] + [''] * (n_ticks - 2) + ['top'])
            for i, label in enumerate(axs[ic].get_yticklabels()):
                if i > 0 and i < len(axs[ic].get_yticklabels()) - 1:
                    label.set_visible(False)
        else:
            axs[ic].set_yticks(np.linspace(alt.min(), alt.max(), n_ticks))
            axs[ic].set_yticklabels(
                ['sfc'] + [''] * (n_ticks - 2) + ['${1}/{n}^{th}$'])
            for i, label in enumerate(axs[ic].get_yticklabels()):
                if i > 0 and i < len(axs[ic].get_yticklabels()) - 1:
                    label.set_visible(False)

        axs[ic].tick_params(axis='x', labelsize=labelSize)
        axs[ic].tick_params(axis='y', labelsize=labelSize)
        axs[ic].grid(True)

        ic += 1

    if saveFile != '':
        plt.savefig(saveFile, dpi=saveDPI)
        print("Saved File: {}".format(saveFile))
    else:
        plt.show()
    plt.close()

    return


def plot_vertical_rmse_altitude(
        base_rmseList, ml_rmseList,
        altitude=None,
        base_color=DEFAULT_OPTION_DICT[NWP_LINE_COLOUR_KEY],
        base_meanRMSEList=None,
        ml_color=DEFAULT_OPTION_DICT[PREDICTED_LINE_COLOUR_KEY],
        ml_meanRMSEList=None,
        figSize=(12, 6),
        label1='RAP', label2='ML',
        labelSize=12,
        legendLoc='lower right',
        legendSize=13,
        lineWidth=2, plotVLine=True,
        title='',
        title1='', title2='',
        titleSize=14,
        saveDPI=100, saveFile='',
        ytickFormat='%.0f'):
    """
    Plot the RMSE over different labels,
    and show height/pressure levels.
    """

    # ... set sizes if saving
    if saveFile != '':
        labelSize = 14
        lineWidth = 2.5

    # ... set altitude (if necessary)
    if altitude is None:
        altitude = np.arange(ml_rmseList[0].shape[0])
        yLabel = 'Altitude'
        n_ticks = 5
        yticksHere = np.linspace(altitude.min(), altitude.max(), n_ticks)
    elif altitude[-1] > altitude[0]:
        yLabel = 'Height (km)'
        ylimHere = [altitude.min(), altitude.max()]
        if altitude.max() < 3000:
            yticksHere = np.array(
                [500, 750, 1000, 1250,
                 1500, 1750, 2000])
            ylabelsHere = yticksHere / 1000.
            ylabelsHere = ylabelsHere.astype('str')
        else:
            yticksHere = np.array(
                [altitude.min(), 3000, 6000, 9000, 12000, 15000, 18000])
            ylabelsHere = ['SFC', 3, 6, 9, 12, 15, 18]
    else:
        yLabel = 'Pressure (mb)'
        ylimHere = [altitude.max(), altitude.min()]
        if altitude.min() < 700:
            yticksHere = np.array(
                [altitude.max(), 850, 700,
                 500, 300, 100])
            ylabelsHere = yticksHere.astype('int')
            ylabelsHere = ylabelsHere.astype('str')
            ylabelsHere[0] = 'SFC'
        else:
            yticksHere = np.array(
                [950, 925, 900, 875, 850, 825, 800])
            ylabelsHere = yticksHere.astype('str')

    if title1 or title2:
        titleList = [title1, title2]
    else:
        titleList = [None, None]

    fig, axs = plt.subplots(1, 2, figsize=figSize)
    ic = 0
    for v in ['temperature', 'dewpoint']:
        base_rmse = base_rmseList[ic]
        if base_meanRMSEList is None:
            base_rmse_mean = np.mean(base_rmse)
        else:
            base_rmse_mean = base_meanRMSEList[ic]

        ml_rmse = ml_rmseList[ic]
        if ml_meanRMSEList is None:
            ml_rmse_mean = np.mean(ml_rmse)
        else:
            ml_rmse_mean = ml_meanRMSEList[ic]

        if plotVLine:
            label1a = label1
        else:
            label1a = f'{label1} ({base_rmse_mean:.2f})'
        axs[ic].plot(base_rmse, altitude, color=base_color,
                     linewidth=lineWidth, label=label1a)
        if plotVLine:
            label1b = f'{label1} (mean)'
            axs[ic].axvline(
                base_rmse_mean,
                label=label1b,
                color=base_color, linestyle='--',
                linewidth=lineWidth)

        if plotVLine:
            label2a = label2
        else:
            label2a = f'{label2} ({ml_rmse_mean:.2f})'
        axs[ic].plot(ml_rmse, altitude, color=ml_color,
                     linewidth=lineWidth, label=label2a)
        if plotVLine:
            label2b = f'{label2} (mean)'
            axs[ic].axvline(
                ml_rmse_mean,
                label=label2b, color=ml_color,
                linestyle='--', linewidth=lineWidth)

        axs[ic].set_ylabel(yLabel, fontsize=labelSize)
        axs[ic].set_xlabel('RMSE (C)', fontsize=labelSize)
        # if ic == 0:
        axs[ic].legend(fontsize=legendSize, loc=legendLoc)

        axs[ic].set_yticks(yticksHere)
        if yLabel == 'Altitude':
            if len(altitude) > 50:
                axs[ic].set_yticklabels(
                    ['sfc'] + [''] * (n_ticks - 2) + ['top'])
                for i, label in enumerate(axs[ic].get_yticklabels()):
                    if i > 0 and i < len(axs[ic].get_yticklabels()) - 1:
                        label.set_visible(False)
            else:
                axs[ic].set_yticklabels(
                    ['sfc'] + [''] * (n_ticks - 2) + ['${1}/{n}^{th}$'])
                for i, label in enumerate(axs[ic].get_yticklabels()):
                    if i > 0 and i < len(axs[ic].get_yticklabels()) - 1:
                        label.set_visible(False)
        else:
            axs[ic].set_ylim(ylimHere)
            axs[ic].set_yticklabels(ylabelsHere)
            # axs.yaxis.set_major_formatter(FormatStrFormatter(ytickFormat))

        if titleList[ic]:
            titleHere = titleList[ic]
        else:
            titleHere = title + v.upper()
        axs[ic].tick_params(axis='x', labelsize=labelSize)
        axs[ic].tick_params(axis='y', labelsize=labelSize)
        axs[ic].set_title(titleHere, fontsize=titleSize)
        axs[ic].grid(True)

        ic += 1

    if saveFile != '':
        plt.savefig(saveFile, dpi=saveDPI)
        print("Saved File: {}".format(saveFile))
    else:
        plt.show()
    plt.close()
    return


def plot_vertical_rmse_altitudeArrayI(
        rmseArray,
        altitude=None,
        colorList=QUANT_COLOR_LIST,
        figSize=(8, 6),
        labelList=[],
        labelSize=12,
        legendSize=13,
        lineWidth=2,
        rap=None,
        rapColor='deeppink',
        saveDPI=100, saveFile='',
        title='', titleSize=14,
        xLabel='RMSE [$\degree$C]'):
    """
    Plot the RMSE over different labels,
    and show height/pressure levels.
    """

    # ... set sizes if saving
    if saveFile != '':
        labelSize = 14
        lineWidth = 2.5

    # ... set altitude (if necessary)
    if altitude is None:
        altitude = np.arange(rmseArray.shape[-1])
        yLabel = 'Altitude (mb)'
        n_ticks = 5
        yticksHere = np.linspace(altitude.min(), altitude.max(), n_ticks)
    elif altitude[-1] > altitude[0]:
        yLabel = 'Height (km)'
        ylimHere = [altitude.min(), altitude.max()]
        if altitude.max() < 3000:
            yticksHere = np.array([500, 750, 1000, 1250, 1500, 1750, 2000])
            ylabelsHere = yticksHere / 1000.
            ylabelsHere = ylabelsHere.astype('str')
        else:
            yticksHere = np.array(
                [altitude.min(), 3000, 6000, 9000, 12000, 15000, 18000])
            ylabelsHere = ['SFC', 3, 6, 9, 12, 15, 18]
    else:
        yLabel = 'Pressure (mb)'
        ylimHere = [altitude.max(), altitude.min()]
        if altitude.min() < 700:
            yticksHere = np.array([altitude.max(), 850, 700, 500, 300, 100])
            ylabelsHere = yticksHere.astype('int')
            ylabelsHere = ylabelsHere.astype('str')
            ylabelsHere[0] = 'SFC'
        else:
            yticksHere = np.array([950, 925, 900, 875, 850, 825, 800])
            ylabelsHere = yticksHere.astype('str')

    _, ax = plt.subplots(1, 1, figsize=figSize)
    nxa = rmseArray.shape[0]
    for ir1 in range(nxa):
        if labelList:
            ax.plot(rmseArray[ir1, :], altitude,
                    color=colorList[ir1],
                    label=labelList[ir1],
                    linewidth=lineWidth)
        else:
            ax.plot(rmseArray[ir1, :], altitude,
                    color=colorList[ir1],
                    linewidth=lineWidth)

    if rap is not None:
        ax.plot(rap, altitude,
                color=rapColor,
                label='RAP',
                linewidth=lineWidth,
                linestyle='solid')

    ax.set_xlabel(xLabel, fontsize=labelSize)
    ax.set_yticks(yticksHere)
    if 'Altitude' in yLabel:
        if len(altitude) > 50:
            ax.set_yticklabels(['sfc'] + [''] * (n_ticks - 2) + ['top'])
            for i, label in enumerate(ax.get_yticklabels()):
                if i > 0 and i < len(ax.get_yticklabels()) - 1:
                    label.set_visible(False)
        else:
            ax.set_yticklabels(
                ['sfc'] + [''] * (n_ticks - 2) + ['${1}/{n}^{th}$'])
            for i, label in enumerate(ax.get_yticklabels()):
                if i > 0 and i < len(ax.get_yticklabels()) - 1:
                    label.set_visible(False)
    else:
        ax.set_ylim(ylimHere)
        ax.set_yticklabels(ylabelsHere)
        # axs.yaxis.set_major_formatter(FormatStrFormatter(ytickFormat))
    ax.set_ylabel(
        yLabel, fontsize=labelSize)

    if labelList:
        ax.legend(fontsize=legendSize,
                  loc='lower right')
    titleHere = title
    ax.tick_params(axis='x', labelsize=labelSize)
    ax.tick_params(axis='y', labelsize=labelSize)
    ax.set_title(titleHere, fontsize=titleSize)
    ax.grid(True)

    if saveFile != '':
        plt.savefig(saveFile, dpi=saveDPI)
        print("Saved File: {}".format(saveFile))
    else:
        plt.show()

    plt.close()
    return


def plot_vertical_rmse_baseline(
    t_rmse,
        t_mean_rmse,
        td_rmse,
        td_mean_rmse,
        altitude=None,
        figSize=(6, 6),
        fileName=None,
        fontSize=12,
        label1='T', label2='TD',
        lineWidth=2,
        tColor=COLOR_RED_NP,
        tdColor=COLOR_BLUESKY_NP,
        title='',
        ytickFormat='%.0f'):
    """
    Plot the temperature and dewpoint RMSE
    over different altitudes.
    """

    if fileName:
        fontSize = 14
        lineWidth = 2.5

    # Set altitude (if necessary)
    if altitude is None:
        altitude = np.arange(t_rmse.shape[0])
        yLabel = 'Altitude'
        n_ticks = 5
        yticksHere = np.linspace(altitude.min(), altitude.max(), n_ticks)
    elif altitude[-1] > altitude[0]:
        yLabel = 'Height (km)'
        ylimHere = [altitude.min(), altitude.max()]
        if altitude.max() < 3000:
            yticksHere = np.array([500, 750, 1000, 1250, 1500, 1750, 2000])
            ylabelsHere = yticksHere / 1000.
            ylabelsHere = ylabelsHere.astype('str')
        else:
            yticksHere = np.array(
                [altitude.min(), 3000, 6000, 9000, 12000, 15000, 18000])
            ylabelsHere = ['SFC', 3, 6, 9, 12, 15, 18]
    else:
        yLabel = 'Pressure (mb)'
        ylimHere = [altitude.max(), altitude.min()]
        if altitude.min() < 700:
            yticksHere = np.array([altitude.max(), 850, 700, 500, 300, 100])
            ylabelsHere = yticksHere.astype('int')
            ylabelsHere = ylabelsHere.astype('str')
            ylabelsHere[0] = 'SFC'
        else:
            yticksHere = np.array([950, 925, 900, 875, 850, 825, 800])
            ylabelsHere = yticksHere.astype('str')

    fig, axs = plt.subplots(1, 1, figsize=figSize)
    axs.plot(t_rmse, altitude, color=tColor, linewidth=lineWidth)
    if t_mean_rmse is not None:
        axs.axvline(t_mean_rmse, label=f'{label1}: {t_mean_rmse:.3f}',
                    color=tColor, linestyle='--', linewidth=lineWidth)
    else:
        axs.lines[-1].set_label(label1)

    axs.plot(td_rmse, altitude, color=tdColor, linewidth=lineWidth)
    if td_mean_rmse is not None:
        axs.axvline(td_mean_rmse, label=f'{label2}: {td_mean_rmse:.3f}',
                    color=tdColor, linestyle='--', linewidth=lineWidth)
    else:
        axs.lines[-1].set_label(label2)

    axs.set_ylabel(yLabel, fontsize=fontSize)
    axs.set_xlabel('RMSE [C]', fontsize=fontSize)
    axs.legend(fontsize=fontSize)

    axs.set_yticks(yticksHere)
    if yLabel == 'Altitude':
        if len(altitude) > 50:
            axs.set_yticklabels(['sfc'] + [''] * (n_ticks - 2) + ['top'])
            for i, label in enumerate(axs.get_yticklabels()):
                if i > 0 and i < len(axs.get_yticklabels()) - 1:
                    label.set_visible(False)
        else:
            axs.set_yticklabels(
                ['sfc'] + [''] * (n_ticks - 2) + ['${1}/{n}^{th}$'])
            for i, label in enumerate(axs.get_yticklabels()):
                if i > 0 and i < len(axs.get_yticklabels()) - 1:
                    label.set_visible(False)
    else:
        axs.set_ylim(ylimHere)
        axs.set_yticklabels(ylabelsHere)
        # axs.yaxis.set_major_formatter(FormatStrFormatter(ytickFormat))

    axs.tick_params(axis='x', labelsize=fontSize)
    axs.tick_params(axis='y', labelsize=fontSize)
    axs.set_title(title, fontsize=fontSize)
    axs.grid(True)

    if fileName:
        plt.savefig(fileName, dpi=300)
    else:
        plt.show()
    plt.close()
    return


def save_figure(fileName, dpi=200):
    """Save a figure (e.g. png or eps)"""

    myType = fileName[-3:]
    if myType not in ['png', 'eps']:
        print("Expecting to save a png or eps file.")
    else:
        plt.savefig(fileName, bbox_inches="tight", format=myType, dpi=dpi)
        print("Saved File: {}".format(fileName))

    return


def set_plot_fonts_ax(ax, plotFontSize=20):
    for item in ([ax.xaxis.label, ax.yaxis.label]
                 + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(plotFontSize)
