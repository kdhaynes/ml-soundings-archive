"""
ML Soundings Research

Functions to setup Neural Network runs.

Modified: 2023/07
"""

import copy
import numpy as np
import os
import pickle
import time

import functions_misc as ff
import functions_loss as fl
import tensorflow as tf


DEFAULT_NLEVS = 256
DEFAULT_ANN_OPTS = {
    'exRef': 'NN',
    'network': 'NeuralNetwork',
    'activation_dense': 'tanh',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [0.1],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.01,
    'loss_type': 'mse',
    'n_dense_list': [],
    'standardizeX': False,
    'standardizeY': False,
    'uq_n_members': 51,
    'uq_std_min': 0.0001,
    'uq_std_max': 1000,
    'uq_tau_min': 0.04,
    'uq_tau_max': 2.0}

DEFAULT_RUN_OPTS = {
    'batch_size': 128,
    'epochs': 10,
    'learning_rate': 0.001,
    'optimizer': 'adam',
    'patience': 10}

DEFAULT_UNET_OPTS = {
    'exRef': 'UNET',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.05,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [0.05],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0001,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'mse',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [0],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 51,
    'uq_std_min': 0.0001,
    'uq_std_max': 1000,
    'uq_tau_min': 0.04,
    'uq_tau_max': 2.0}


TRAIN_MAE_NAME = "train_mae"
TRAIN_MAE_SFC_NAME = "train_mae_sfc"
TRAIN_MSE_NAME = "train_mse"
TRAIN_RMSE_NAME = "train_rmse"
TRAIN_RMSEW_NAME = "train_mse_weighted"


#################################
# FULLY-CONNECTED NEURAL NETWORK
class NeuralNetwork():
    def __init__(self, n_inputs, n_outputs,
                 modelOpts=DEFAULT_ANN_OPTS,
                 defaultOpts=DEFAULT_ANN_OPTS):

        erStr = "NeuralNetwork Error:"
        modelOpts = check_optDict(
            modelOpts, defaultOpts,
            erStr=erStr)

        tf.keras.backend.clear_session()

        dropout = modelOpts['dropout_flag']
        dropout_last = modelOpts['dropout_dense_last']
        dropout_list = modelOpts['dropout_dense_list']
        loss_type = modelOpts['loss_type']
        n_hiddens_list = modelOpts['n_dense_list']
        uq_n_members = modelOpts['uq_n_members']

        if not isinstance(n_hiddens_list, list):
            print("{} n_hiddens_list must be a list".format(erStr))
            print("    Using Simple Network With No Hidden Layers.")
            n_hiddens_list = []

        if loss_type == 'mae_dropout':
            dropout = True
        if dropout:
            if len(dropout_list) != len(n_hiddens_list):
                print("{} {}".format(
                    "dropout_list and n_hiddens_list must have same length",
                    erStr))
                defaultDropout = \
                    defaultOpts['dropout_list'][0]
                print("   Using Default Dropout Of {}".format(defaultDropout))
                dropout_list = []
                for _ in n_hiddens_list:
                    dropout_list.append(defaultDropout)

        self.history = None
        self.loss_type = loss_type
        self.n_hiddens_list = n_hiddens_list
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs
        self.training_time = None
        self.uq_n_members = uq_n_members
        self.uq_std_min = modelOpts['uq_std_min']
        self.uq_std_max = modelOpts['uq_std_max']
        self.uq_tau_min = modelOpts['uq_tau_min']
        self.uq_tau_max = modelOpts['uq_tau_max']

        self.XStandardize = modelOpts['standardizeX']
        self.Xmeans = None
        self.Xstds = None
        self.Xmax = None
        self.Xmin = None

        self.YStandardize = modelOpts['standardizeY']
        self.Ymeans = None
        self.Ystds = None
        self.Ymax = None
        self.Ymin = None

        bias_init = modelOpts['bias_init']
        if bias_init == 'random':
            bias_init = tf.keras.initializers.RandomNormal(
                seed=modelOpts['seed'])

        kernel_init = modelOpts['kernel_init']
        if kernel_init == 'random':
            kernel_init = tf.keras.initializers.RandomNormal(
                seed=modelOpts['seed'])

        kernel_reg = modelOpts['kernel_reg']
        kernel_reg_lval = modelOpts['kernel_reg_lval']
        if kernel_reg == 'L1' or kernel_reg == 'l1':
            kernel_reg = tf.keras.regularizers.L1(kernel_reg_lval)
        elif kernel_reg == 'L2' or kernel_reg == 'l2':
            kernel_reg = tf.keras.regularizers.L2(kernel_reg_lval)

        # Setup Initial and Hidden Layers
        X = Z = tf.keras.Input(shape=(n_inputs,))
        nLayers = len(n_hiddens_list)
        for i in range(nLayers):
            if 'dropout' in loss_type:
                Z = tf.keras.layers.Dropout(
                    dropout_list[i])(Z, training=True)
            elif dropout and dropout_list[i] > 0.0:
                Z = tf.keras.layers.Dropout(dropout_list[i])(Z)

            Z = tf.keras.layers.Dense(
                n_hiddens_list[i],
                activation=modelOpts['activation_dense'],
                use_bias=modelOpts['bias_flag'],
                bias_initializer=bias_init,
                bias_regularizer=modelOpts['bias_reg'],
                kernel_initializer=kernel_init,
                kernel_regularizer=kernel_reg)(Z)
            if modelOpts['batchnorm_flag']:
                Z = tf.keras.layers.BatchNormalization()(Z)

        # Setup Output Layer
        if 'crps' in loss_type:
            if dropout and dropout_last > 0.0:
                Z = tf.keras.layers.Dropout(dropout_last)(Z)
            if n_outputs < 2:
                Y = tf.keras.layers.Dense(
                    uq_n_members,
                    kernel_regularizer=kernel_reg,
                    name="ensemble_members")(Z)
            else:
                Y = tf.keras.layers.Dense(
                    n_outputs,
                    kernel_regularizer=kernel_reg)(Z)
                Y = tf.expand_dims(Y, axis=-1)

                for _ in range(1, uq_n_members):
                    outputT = tf.keras.layers.Dense(
                        n_outputs,
                        kernel_regularizer=kernel_reg)(Z)
                    Y = tf.keras.layers.concatenate(
                        (tf.expand_dims(outputT, axis=-1), Y),
                        axis=-1)

        elif 'dropout' in loss_type:
            if dropout_last > 0.0:
                Z = tf.keras.layers.Dropout(dropout_last)(Z, training=True)
            Y = tf.keras.layers.Dense(
                n_outputs,
                kernel_regularizer=kernel_reg,
                name="output")(Z)

        elif 'normal' in loss_type:
            mu_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=bias_init,
                kernel_initializer=kernel_init,
                name="mu_unit")(Z)
            logsigma_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="logsigma_unit")(Z)
            Y = tf.keras.layers.concatenate(
                (tf.expand_dims(mu_unit, axis=2),
                 tf.expand_dims(logsigma_unit, axis=2)),
                axis=2, name="output")

        elif 'sinh' in loss_type:
            mu_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=bias_init,
                kernel_initializer=kernel_init,
                name="mu_unit")(Z)
            logsigma_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="logsigma_unit")(Z)
            skew_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="skew_unit")(Z)
            logtau_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="logtau_unit")(Z)
            Y = tf.keras.layers.concatenate(
                (tf.expand_dims(mu_unit, axis=2),
                 tf.expand_dims(logsigma_unit, axis=2),
                 tf.expand_dims(skew_unit, axis=2),
                 tf.expand_dims(logtau_unit, axis=2)),
                axis=2, name="output")

        else:
            if dropout and dropout_last > 0.0:
                Z = tf.keras.layers.Dropout(dropout_last)(Z)
            Y = tf.keras.layers.Dense(
                n_outputs,
                kernel_regularizer=kernel_reg,
                name="output")(Z)

        self.model = tf.keras.Model(
            inputs=X, outputs=Y, name=modelOpts['exRef'])
        print("{} setup with {} inputs and {} outputs".format(
            type(self).__name__, self.n_inputs, self.n_outputs))

    def __repr__(self):
        str1 = f'{type(self).__name__}'
        str2 = "\n  n_inputs: {}, n_outputs: {}".\
            format(self.n_inputs, self.n_outputs)
        str3 = "\n  n_hiddens: {}, loss_type: {}".\
            format(self.n_hiddens_list, self.loss_type)
        if self.history:
            str4 = "\n  Training loss is {:.4f} in {:.3f} s.".\
                format(self.history['loss'][-1],
                       self.training_time)
        else:
            str4 = '\n  Network is not trained.'
        return str1 + str2 + str3 + str4

    def _setup_standardize(self, X, Y):
        if self.Xmeans is None:
            self.Xmeans = X.mean(axis=0)
            self.Xstds = X.std(axis=0)
            self.Xconstant = self.Xstds == 0
            self.XstdsFixed = copy.copy(self.Xstds)
            self.XstdsFixed[self.Xconstant] = 1

        if self.Ymeans is None:
            if len(Y.shape) == 3:
                YHere = Y[..., 0]
            else:
                YHere = Y
            self.Ymeans = YHere.mean(axis=0)
            self.Ystds = YHere.std(axis=0)
            self.Yconstant = self.Ystds == 0
            self.YstdsFixed = copy.copy(self.Ystds)
            self.YstdsFixed[self.Yconstant] = 1

    def _standardizeX(self, X):
        result = (X - self.Xmeans) / self.XstdsFixed
        result[..., self.Xconstant] = 0.0
        return result

    def _unstandardizeX(self, Xs):
        return self.Xstds * Xs + self.Xmeans

    def _standardizeY(self, Y):
        if len(Y.shape) == 3:
            YHere = Y[..., 0]
        else:
            YHere = Y
        result = (YHere - self.Ymeans) / self.YstdsFixed
        result[..., self.Yconstant] = 0.0

        if len(Y.shape) == 3:
            result = np.concatenate(
                (np.expand_dims(result, axis=2),
                 Y[..., 1:]), axis=2)

        return result

    def _unstandardizeY(self, Ys):
        return self.Ystds * Ys + self.Ymeans

    def _singlize(self, inData):
        arrayV = inData[0]
        arrayIm = inData[1]
        if arrayV is not None:
            self.nSamples = arrayV.shape[0]
            self.nRapLevs = arrayV.shape[1]
            self.nRapChs = arrayV.shape[2]
        else:
            self.nRapLevs = 0
            self.nRapChs = 0

        if arrayIm is not None:
            self.nSamples = arrayIm.shape[0]
            self.nImChs = arrayIm.shape[1]
        else:
            self.nImChs = 0

        arrayOut = np.empty((self.nSamples, self.n_inputs))
        count = 0
        for i in range(self.nRapLevs):
            for j in range(self.nRapChs):
                arrayOut[:, count] = arrayV[:, i, j]
                count += 1

        for i in range(self.nImChs):
            arrayOut[:, count] = arrayIm[:, i]
            count += 1

        if count != self.n_inputs:
            print("Incorrectly reshaped input data!")
            return None
        else:
            return arrayOut

    def compile(self, runOpts=DEFAULT_RUN_OPTS):
        """Compile Fully-Connected NN."""

        runOpts = check_optDict(runOpts, DEFAULT_RUN_OPTS)

        self.batch_size = runOpts['batch_size']
        self.epochs = runOpts['epochs']
        self.patience = runOpts['patience']
        algo = setup_optimizer(runOpts['optimizer'],
                               runOpts['learning_rate'])
        loss, metrics = setup_loss_metrics(
            self.loss_type,
            n_outputs=self.n_outputs,
            uq_std_min=self.uq_std_min,
            uq_std_max=self.uq_std_max,
            uq_tau_min=self.uq_tau_min,
            uq_tau_max=self.uq_tau_max)
        self.model.compile(optimizer=algo, loss=loss,
                           metrics=metrics)

    def organize_data(self, expDict, data, verbose=True):
        return organize_data(
            expDict, data, verbose=verbose)

    def save(self, path):
        tf_model_path = os.path.join(path, 'nn_model.h5')
        if not os.path.exists(path):
            os.makedirs(path)
        self.model.save(tf_model_path)
        del self.model
        with open(os.path.join(path, 'class.pickle'), 'wb') as f:
            pickle.dump(self, f)
        self.model = tf.keras.models.load_model(tf_model_path)

    def train(self, Xin, T,
              batch_size=None,
              epochs=None,
              patience=None,
              validation=None,
              verbose=False):
        """Use Keras Functional API to train model"""

        if batch_size is None:
            batch_size = self.batch_size
        if epochs is None:
            epochs = self.epochs
        if patience is None:
            patience = self.patience

        # ... check for list input, singularize if so
        if isinstance(Xin, list):
            X = self._singlize(Xin)
        else:
            X = Xin
        if len(X.shape) < 2:
            X = X.reshape((-1, 1))
        if len(T.shape) < 2:
            T = T.reshape((-1, 1))

        self._setup_standardize(X, T)
        if self.XStandardize:
            X = self._standardizeX(X)
        if self.YStandardize:
            T = self._standardizeY(T)

        if validation is not None:
            try:
                XV = validation[0]
                if isinstance(XV, list):
                    XV = self._singlize(XV)
                if len(XV.shape) < 2:
                    XV = XV.reshape((-1, 1))

                TV = validation[1]
                if len(TV.shape) < 2:
                    TV = TV.reshape((-1, 1))

                if self.XStandardize:
                    XV = self._standardizeX(XV)
                if self.YStandardize:
                    TV = self._standardizeY(TV)
                validation = (XV, TV)

            except Exception:
                raise TypeError(
                    'validation must have shape: (X, T)')

        if validation is not None:
            callback = [tf.keras.callbacks.EarlyStopping(
                monitor='val_loss', min_delta=0.0,
                patience=patience,
                verbose=1, mode='auto',
                restore_best_weights=True)]
        else:
            callback = []
        if verbose:
            callback.append(TrainLogger(epochs, step=epochs // 5))

        start_time = time.time()
        self.history = self.model.fit(
            X, T,
            batch_size=batch_size,
            epochs=epochs,
            callbacks=callback,
            validation_data=validation,
            verbose=0).history
        self.training_time = time.time() - start_time
        return self

    def use(self, Xin, returnType='uq', verbose=0):

        # Set to error logging after model is trained
        tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

        if isinstance(Xin, list):
            X = self._singlize(Xin)
        else:
            X = Xin

        if len(X.shape) < 2:
            X = X.reshape((-1, 1))
        if self.XStandardize:
            X = self._standardizeX(X)

        if 'crps' in self.loss_type:
            preds = self.model.predict(X, verbose=verbose)
            if self.YStandardize:
                for i in range(self.uq_n_members):
                    preds[..., i] = self._unstandardizeY(preds[..., i])

            if returnType == 'uq':
                return np.median(preds, axis=-1), \
                    preds.std(axis=-1)
            elif returnType == 'uq4':
                return preds.mean(axis=-1), preds.std(axis=-1), \
                    preds.min(axis=-1), preds.max(axis=-1)
            elif returnType == 'mc':
                return preds
            else:
                return np.median(preds, axis=-1)

        elif 'dropout' in self.loss_type:
            nTry = self.uq_n_members
            preds = np.concatenate(
                [np.expand_dims(
                    self.model.predict(X, verbose=verbose), axis=-1)
                    for _ in range(nTry)], axis=-1)

            if self.YStandardize:
                for i in range(self.uq_n_members):
                    preds[..., i] = self._unstandardizeY(preds[..., i])

            if returnType == 'uq':
                return preds.mean(axis=-1), preds.std(axis=-1)
            elif returnType == 'uq4':
                return preds.mean(axis=-1), preds.std(axis=-1), \
                    preds.min(axis=-1), preds.max(axis=-1)
            elif returnType == 'mc':
                return preds
            else:
                return preds.mean(axis=-1)

        elif 'normal' in self.loss_type:
            if returnType == 'mc':
                pvals = self.model.predict(X)
                pmean = pvals[..., 0]
                pstd = np.exp(pvals[..., 1])
                pens = fl.create_sample_norm_tfp(
                    pmean, pstd,
                    nSamples=self.uq_n_members,
                    Ymeans=self.Ymeans,
                    Ystds=self.Ystds)
                return np.moveaxis(pens, 0, -1)

            pmean, pstd, pmin, pmax = use_normalUQ(
                self.model, X,
                loss_type=self.loss_type,
                constrainA=self.uq_std_min,
                constrainB=self.uq_std_max,
                standardizeY=self.YStandardize,
                Ymeans=self.Ymeans,
                Ystds=self.Ystds)

            if returnType == 'uq':
                return pmean, pstd
            elif returnType == 'uq4':
                return pmean, pstd, pmin, pmax
            elif returnType == 'params':
                pvals = np.concatenate(
                    [np.expand_dims(pmean, axis=2),
                     np.expand_dims(pstd, axis=2)], axis=2)
                return pvals
            else:
                return pmean

        elif 'sinh' in self.loss_type:

            pvals = self.model.predict(X, verbose=verbose)
            mu = pvals[..., 0]
            sigma = np.exp(pvals[..., 1])
            gamma = pvals[..., 2]
            tau = np.exp(pvals[..., 3])

            if self.loss_type == 'uq_sinhr' \
                    or self.loss_type == 'uq_sinhrt':
                sigma = fl.determine_constraintmap_vals(
                    self.uq_std_min, self.uq_std_max, pvals[..., 1])
            if self.loss_type == 'uq_sinhrt':
                tau = fl.determine_constraintmap_vals(
                    self.uq_tau_min, self.uq_tau_max, pvals[..., 3])

            if returnType == 'mc':
                pens = fl.create_sample_sinh_tfp(
                    mu, sigma, gamma, tau,
                    nSamples=self.uq_n_members,
                    Ymeans=self.Ymeans,
                    Ystds=self.Ystds)
                return np.moveaxis(pens, 0, -1)

            if returnType == 'params':
                pvals[..., 1] = sigma
                pvals[..., 3] = tau
                return pvals

            if self.YStandardize:
                pmean, pmedian, pstd, pmin, pmax = \
                    fl.unscale_sinh_tf(
                        mu, sigma, gamma, tau,
                        self.Ymeans, self.Ystds)
            else:
                pmean = fl.sinh_mean(mu, sigma, gamma, tau)
                pmedian = fl.sinh_median(mu, sigma, gamma, tau)
                pstd = fl.sinh_stddev(mu, sigma, gamma, tau)
                pmin = fl.sinh_min(mu, sigma, gamma, tau)
                pmax = fl.sinh_max(mu, sigma, gamma, tau)

            if returnType == 'params':
                return pvals
            elif returnType == 'uq':
                return pmedian, pstd
            elif returnType == 'uq3':
                return pmean, pstd, pmedian
            elif returnType == 'uq4':
                return pmedian, pstd, pmin, pmax
            else:
                return pmedian

        else:
            preds = self.model.predict(X, verbose=verbose)
            if self.YStandardize:
                preds = self._unstandardizeY(preds)
            if returnType == 'uq':
                return preds, None
            else:
                return preds


#################################
# UNET
class SkipNeuralNetwork():
    """Skip Neural Network with multiple inputs (optionally).
    Specify, `n_im_inputs=None` to not add additional inputs.
    """

    def __init__(self,
                 shape_rap_inputs,
                 shape_im_inputs,
                 n_outputs,
                 addTRef=1,
                 addTDRef=2,
                 modelOpts=DEFAULT_UNET_OPTS,
                 defaultOpts=DEFAULT_UNET_OPTS,
                 verbose=False):

        erStr = "SkipNeuralNetwork Error:"
        modelOpts = check_optDict(modelOpts,
                                  defaultOpts,
                                  erStr=erStr)

        tf.keras.backend.clear_session()

        activation_conv = modelOpts['activation_conv']
        activation_dense = modelOpts['activation_dense']
        dropout = modelOpts['dropout_flag']
        dropout_conv_rate = modelOpts['dropout_conv_rate']
        dropout_dense_list = modelOpts['dropout_dense_list']
        dropout_dense_last = modelOpts['dropout_dense_last']

        kernels_size_and_stride = modelOpts['kernels_size_and_stride']
        kernel_size = kernels_size_and_stride[0]
        kernel_stride = kernels_size_and_stride[1]
        kernel_reg = modelOpts['kernel_reg']
        kernel_reg_lval = modelOpts['kernel_reg_lval']
        if kernel_reg == 'L1' or kernel_reg == 'l1':
            kernel_reg = tf.keras.regularizers.L1(kernel_reg_lval)
        elif kernel_reg == 'L2' or kernel_reg == 'l2':
            kernel_reg = tf.keras.regularizers.L2(kernel_reg_lval)

        loss_type = modelOpts['loss_type']

        self.history = None
        self.loss_type = loss_type
        self.n_conv_list = modelOpts['n_conv_list']
        self.n_dense_list = modelOpts['n_dense_list']
        self.shape_rap_inputs = shape_rap_inputs
        self.shape_im_inputs = shape_im_inputs
        self.n_inputs = shape_rap_inputs[0] * shape_rap_inputs[1] + \
            shape_im_inputs[0]
        self.n_outputs = n_outputs

        self.training_time = None

        self.uq_n_members = modelOpts['uq_n_members']
        self.uq_std_min = modelOpts['uq_std_min']
        self.uq_std_max = modelOpts['uq_std_max']
        self.uq_tau_min = modelOpts['uq_tau_min']
        self.uq_tau_max = modelOpts['uq_tau_max']

        self.XStandardize = modelOpts['standardizeX']
        self.YStandardize = modelOpts['standardizeY']
        self.RAPmeans = None
        self.RAPstds = None
        self.IMmeans = None
        self.IMstds = None
        self.RAOBmeans = None
        self.RAOBstds = None

        if self.loss_type == 'mc_dropout':
            dropout = True
            training = True
        else:
            training = False

        if dropout:
            if len(dropout_dense_list) != len(self.n_dense_list):
                print("{} {}".format(
                    "dropout_dense_list and n_dense_list need same length",
                    erStr))
                ddDropout = defaultOpts['dropout_dense_list'][0]
                print("   Using Default Dropout Of {}".format(ddDropout))
                dropout_dense_list = []
                for _ in self.n_dense_list:
                    dropout_dense_list.append(ddDropout)

        if shape_rap_inputs[-1] > 0:
            # encoder
            X1 = Z1 = tf.keras.Input(shape=shape_rap_inputs, name='rap')

            nConvs = len(self.n_conv_list)
            for i in range(nConvs):
                units = self.n_conv_list[i]
                Z1 = tf.keras.layers.Conv1D(
                    units,
                    kernel_regularizer=kernel_reg,
                    kernel_size=kernel_size,
                    padding='same',
                    strides=kernel_stride)(Z1)
                Z1 = tf.keras.layers.Activation(activation_conv)(Z1)
                Z1 = tf.keras.layers.Conv1D(
                    units,
                    kernel_regularizer=kernel_reg,
                    kernel_size=kernel_size,
                    padding='same',
                    strides=kernel_stride)(Z1)
                Z1 = tf.keras.layers.Activation(
                    activation_conv,
                    name=f'skip_conv1d_activation_{i}')(Z1)
                Z1 = tf.keras.layers.MaxPooling1D(pool_size=2)(Z1)
                if dropout:
                    Z1 = tf.keras.layers.Dropout(
                        dropout_conv_rate)(Z1)

            skips = list(reversed([layer for layer in tf.keras.Model(
                X1, Z1).layers if 'skip' in layer.name]))

            # bottleneck layer
            Z1 = tf.keras.layers.Conv1D(
                1,
                kernel_size=kernel_size,
                strides=kernel_stride,
                padding='same',
                kernel_regularizer=kernel_reg)(Z1)
            Z1 = tf.keras.layers.Activation(activation_conv)(Z1)

            # IM Input
            if self.shape_im_inputs[-1] > 0:
                X2 = Z2 = tf.keras.Input(shape=shape_im_inputs, name='im')
                # Z2 = tf.keras.layers.Flatten()(X2)
                bottleneck_shape = Z1.shape.as_list()[1:]
                Z1 = tf.keras.layers.Flatten()(Z1)

                # ... Join IM & RAP
                Z1 = tf.keras.layers.Concatenate(axis=1)([Z1, Z2])
                Z1 = tf.keras.layers.Dense(
                    np.prod(bottleneck_shape),
                    kernel_regularizer=kernel_reg)(Z1)
                Z1 = tf.keras.layers.Activation(activation_conv)(Z1)
                Z = tf.keras.layers.Reshape(bottleneck_shape)(Z1)
                inputs = [X1, X2]
            else:
                Z = Z1
                inputs = X1
            if dropout and dropout_conv_rate > 0.0:
                Z = tf.keras.layers.Dropout(
                    dropout_conv_rate)(Z, training=training)

            # decoder
            for units, skip in zip(
                    reversed(self.n_conv_list[:]), skips):
                Z = tf.keras.layers.Conv1D(
                    units,
                    kernel_size=kernel_size,
                    strides=kernel_stride,
                    padding='same',
                    kernel_regularizer=kernel_reg)(Z)
                Z = tf.keras.layers.Activation(activation_conv)(Z)
                Z = tf.keras.layers.UpSampling1D(size=2)(Z)
                Z = tf.keras.layers.Concatenate(axis=2)([Z, skip.output])
                Z = tf.keras.layers.Conv1D(
                    units,
                    kernel_size=kernel_size,
                    strides=kernel_stride,
                    padding='same',
                    kernel_regularizer=kernel_reg)(Z)
                Z = tf.keras.layers.Activation(activation_conv)(Z)
                if dropout and dropout_conv_rate > 0.0:
                    Z = tf.keras.layers.Dropout(
                        dropout_conv_rate)(Z, training=training)

            # final conv layer (linear; no activation)
            Z = tf.keras.layers.Conv1D(
                n_outputs / shape_rap_inputs[0],
                kernel_size=kernel_size,
                strides=kernel_stride,
                padding='same',
                kernel_regularizer=kernel_reg)(Z)

            # temperature & dewpoint, e.g. (256,4) + (256,2)
            # Z = tf.keras.layers.Add()([X1[:, :, 1:3], Z])
            if addTRef is not None and addTDRef is not None:
                A = tf.keras.layers.Concatenate(axis=2)(
                    [X1[:, :, addTRef:addTRef + 1],
                     X1[:, :, addTDRef:addTDRef + 1]])
                Z = tf.keras.layers.Add()([A, Z])
            Z = tf.keras.layers.Flatten()(Z)  # (512,)
        else:  # No vertical profiles to conv over
            X2 = tf.keras.Input(shape=shape_im_inputs, name='im')
            inputs = [X2]
            Z = tf.keras.layers.Dense(n_outputs, activation=activation_dense,
                                      kernel_regularizer=kernel_reg)(X2)

        # Dense Layers
        if not (self.n_dense_list == [] or self.n_dense_list == [0]):
            for i in range(len(self.n_dense_list)):
                units = self.n_dense_list[i]
                if dropout and dropout_dense_list[i] > 0.0:
                    Z = tf.keras.layers.Dropout(
                        dropout_dense_list[i])(Z, training=training)
                Z = tf.keras.layers.Dense(
                    units, activation=activation_dense,
                    kernel_regularizer=kernel_reg)(Z)
            if dropout and dropout_dense_last > 0.0:
                Z = tf.keras.layers.Dropout(
                    dropout_dense_last)(Z, training=training)
            # Output Layer
            Z = tf.keras.layers.Dense(n_outputs)(Z)

        # Output Layer
        if 'crps' in loss_type:
            if dropout and dropout_dense_last > 0.0:
                Z = tf.keras.layers.Dropout(dropout_dense_last)(Z)
            if n_outputs < 2:
                Y = tf.keras.layers.Dense(
                    self.uq_n_members,
                    kernel_regularizer=kernel_reg,
                    name="ensemble_members")(Z)
            else:
                # Y = tf.keras.layers.Dense(
                #    n_outputs,
                #    kernel_regularizer=kernel_reg)(Z)
                Y = tf.expand_dims(Z, axis=-1)

                for _ in range(1, self.uq_n_members):
                    outputT = tf.keras.layers.Dense(
                        n_outputs,
                        kernel_regularizer=kernel_reg)(Z)
                    Y = tf.keras.layers.concatenate(
                        (tf.expand_dims(outputT, axis=-1), Y),
                        axis=-1)

        elif 'dropout' in loss_type:
            if dropout_dense_last > 0.0:
                Z = tf.keras.layers.Dropout(
                    dropout_dense_last)(Z, training=True)
            Y = tf.keras.layers.Dense(
                n_outputs,
                kernel_regularizer=kernel_reg,
                name="output")(Z)

        elif 'normal' in self.loss_type:
            mu_unit = Z
            logsigma_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="logsigma_unit")(Z)
            Y = tf.keras.layers.concatenate(
                (tf.expand_dims(mu_unit, axis=2),
                 tf.expand_dims(logsigma_unit, axis=2)),
                axis=2, name="output")
        elif 'sinh' in self.loss_type:
            mu_unit = Z
            logsigma_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="logsigma_unit")(Z)
            skew_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="skew_unit")(Z)
            logtau_unit = tf.keras.layers.Dense(
                n_outputs, activation="linear",
                use_bias=True,
                bias_initializer=tf.keras.initializers.Zeros(),
                kernel_initializer=tf.keras.initializers.Zeros(),
                name="logtau_unit")(Z)
            Y = tf.keras.layers.concatenate(
                (tf.expand_dims(mu_unit, axis=2),
                 tf.expand_dims(logsigma_unit, axis=2),
                 tf.expand_dims(skew_unit, axis=2),
                 tf.expand_dims(logtau_unit, axis=2)),
                axis=2, name="output")
        else:
            Y = Z

        self.model = tf.keras.Model(
            inputs=inputs, outputs=Y,
            name=modelOpts['exRef'])
        if verbose:
            print("{} setup with {} outputs".format(
                type(self).__name__, self.n_outputs))

    def __repr__(self):
        str1 = f'{type(self).__name__}'
        str2 = "\n n_outputs: {}, loss_type: {}".format(
            self.n_outputs, self.loss_type)
        if self.history:
            str4 = "\n  Training loss is {:.4f} in {:.3f} s.".format(
                self.history['loss'][-1], self.training_time)
        else:
            str4 = '\n  Network is not trained.'
        return str1 + str2 + str4

    def _setup_standardize(self, rap, im, raob):
        if self.RAPmeans is None and self.shape_rap_inputs[-1] > 0:
            self.RAPmeans = rap.mean(axis=0)
            self.RAPstds = rap.std(axis=0)
            self.RAPconstant = self.RAPstds == 0
            self.RAPstdsFixed = copy.copy(self.RAPstds)
            self.RAPstdsFixed[self.RAPconstant] = 1

        if self.IMmeans is None and self.shape_im_inputs[-1] > 0:
            # check if IM is used as input to the model
            self.IMmeans = im.mean(axis=0)
            self.IMstds = im.std(axis=0)
            self.IMconstant = self.IMstds == 0
            self.IMstdsFixed = copy.copy(self.IMstds)
            self.IMstdsFixed[self.IMconstant] = 1

        if self.RAOBmeans is None:
            if len(raob.shape) == 3:
                raobHere = raob[..., 0]
            else:
                raobHere = raob
            self.RAOBmeans = raobHere.mean(axis=0)
            self.RAOBstds = raobHere.std(axis=0)
            self.RAOBconstant = self.RAOBstds == 0
            self.RAOBstdsFixed = copy.copy(self.RAOBstds)
            self.RAOBstdsFixed[self.RAOBconstant] = 1

    def _standardizeRAP(self, rap):
        result = (rap - self.RAPmeans) / self.RAPstdsFixed
        result[:, self.RAPconstant] = 0.0
        return result

    def _unstandardizeRAP(self, rap):
        return self.RAPstds * rap + self.RAPmeans

    def _standardizeIM(self, im):
        # only used if IM is used as input to the model
        result = (im - self.IMmeans) / self.IMstdsFixed
        result[:, self.IMconstant] = 0.0
        return result

    def _unstandardizeIM(self, im):
        # only used if IM is used as input to the model
        return self.IMstds * im + self.IMmeans

    def _standardizeRAOB(self, raob):
        if len(raob.shape) == 3:
            raobHere = raob[..., 0]
        else:
            raobHere = raob
        result = (raobHere - self.RAOBmeans) / self.RAOBstdsFixed
        result[:, self.RAOBconstant] = 0.0

        if len(raob.shape) == 3:
            result = np.concatenate(
                (np.expand_dims(result, axis=2),
                 raob[..., 1:]), axis=2)

        return result

    def _unstandardizeRAOB(self, raob):
        return self.RAOBstds * raob + self.RAOBmeans

    def compile(self,
                runOpts=DEFAULT_RUN_OPTS,
                defaultOpts=DEFAULT_RUN_OPTS):
        """Compile Skip CNN."""

        runOpts = check_optDict(runOpts, defaultOpts,
                                erStr="SkipNeuralNetwork error:")

        self.batch_size = runOpts['batch_size']
        self.epochs = runOpts['epochs']
        self.patience = runOpts['patience']
        algo = setup_optimizer(
            runOpts['optimizer'],
            runOpts['learning_rate'])
        loss, metrics = setup_loss_metrics(
            self.loss_type,
            n_outputs=self.n_outputs,
            uq_std_min=self.uq_std_min,
            uq_std_max=self.uq_std_max,
            uq_tau_min=self.uq_tau_min,
            uq_tau_max=self.uq_tau_max)
        self.model.compile(optimizer=algo, loss=loss,
                           metrics=metrics)

    def organize_data(self, expDict, data,
                      verbose=True):
        return organize_data(
            expDict, data, verbose=verbose)

    def train(self, X, T,
              batch_size=None,
              epochs=None,
              patience=None,
              validation=None,
              verbose=False):
        """Use Keras Functional API to train model"""

        if batch_size is None:
            batch_size = self.batch_size
        if epochs is None:
            epochs = self.epochs
        if patience is None:
            patience = self.patience

        X_rap = X[0]
        X_im = X[1]
        if len(T.shape) < 2:
            T = T.reshape((-1, 1))

        self._setup_standardize(X_rap, X_im, T)
        if self.shape_rap_inputs[-1] > 0:
            if self.XStandardize:
                X_rap = self._standardizeRAP(X_rap)
            inputs = {'rap': X_rap}
        else:
            inputs = {}

        if self.shape_im_inputs[-1] > 0:
            if self.XStandardize:
                X_im = self._standardizeIM(X_im)
            inputs['im'] = X_im

        if self.YStandardize:
            T = self._standardizeRAOB(T)

        if validation is not None:
            XVs = validation[0]
            XV_rap = XVs[0]
            if self.shape_rap_inputs[-1] > 0:
                if self.XStandardize:
                    XV_rap = self._standardizeRAP(XV_rap)
                inputsv = {'rap': XV_rap}
            else:
                inputsv = {}

            XV_im = XVs[1]
            if self.shape_im_inputs[-1] > 0:
                if self.XStandardize:
                    XV_im = self._standardizeIM(XV_im)
                inputsv['im'] = XV_im

            TV = validation[1]
            if len(TV.shape) < 2:
                TV = TV.reshape((-1, 1))
            if self.YStandardize:
                TV = self._standardizeRAOB(TV)
            outputsv = TV
            validation = (inputsv, outputsv)

            callback = [tf.keras.callbacks.EarlyStopping(
                monitor='val_loss', min_delta=1e-4,
                patience=patience,
                verbose=1, mode='auto',
                restore_best_weights=True)]
        else:
            callback = []
        if verbose:
            callback.append(TrainLogger(epochs, step=epochs // 5))

        start_time = time.time()
        self.history = self.model.fit(
            inputs, T,
            batch_size=batch_size,
            callbacks=callback,
            epochs=epochs,
            validation_data=validation,
            verbose=0).history
        self.training_time = time.time() - start_time
        return self

    def use(self, Xin, returnType='uq', verbose=0):
        """
        Inputs:
            X : {'rap': rap, 'im': im}
        """
        # Set to error logging after model is trained
        tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

        # check to make sure X is a list
        if not isinstance(Xin, list):
            X = self.unsinglize(Xin)
        else:
            X = Xin

        X_rap = X[0]
        if self.shape_rap_inputs[-1] > 0:
            if self.XStandardize:
                X_rap = self._standardizeRAP(X_rap)
            inputs = {'rap': X_rap}
        else:
            inputs = {}

        X_im = X[1]
        if self.shape_im_inputs[-1] > 0:
            if self.XStandardize:
                X_im = self._standardizeIM(X_im)
            inputs['im'] = X_im

        if 'crps' in self.loss_type:
            preds = self.model.predict(
                inputs, verbose=verbose)
            if self.YStandardize:
                for i in range(self.uq_n_members):
                    preds[..., i] = self._unstandardizeRAOB(preds[..., i])

            if returnType == 'uq':
                return np.median(preds, axis=-1), preds.std(axis=-1)
            elif returnType == 'uq4':
                return preds.mean(axis=-1), preds.std(axis=-1), \
                    preds.min(axis=-1), preds.max(axis=-1)
            elif returnType == 'mc':
                return preds
            else:
                return np.median(preds, axis=-1)

        elif 'dropout' in self.loss_type:
            nTry = self.uq_n_members
            preds = np.concatenate(
                [np.expand_dims(
                    self.model.predict(inputs, verbose=verbose),
                    axis=-1)
                    for _ in range(nTry)], axis=-1)

            if self.YStandardize:
                for i in range(self.uq_n_members):
                    preds[..., i] = self._unstandardizeRAOB(preds[..., i])

            if returnType == 'uq':
                return preds.mean(axis=-1), preds.std(axis=-1)
            elif returnType == 'uq4':
                return preds.mean(axis=-1), preds.std(axis=-1), \
                    preds.min(axis=-1), preds.max(axis=-1)
            elif returnType == 'mc':
                return preds
            else:
                return preds.mean(axis=-1)

        elif 'normal' in self.loss_type:
            if returnType == 'mc':
                pvals = self.model.predict(inputs)
                pmean = pvals[..., 0]
                pstd = np.exp(pvals[..., 1])
                pens = fl.create_sample_norm_tfp(
                    pmean, pstd,
                    nSamples=self.uq_n_members,
                    Ymeans=self.RAOBmeans,
                    Ystds=self.RAOBstds)
                return np.moveaxis(pens, 0, -1)

            pmean, pstd, pmin, pmax = use_normalUQ(
                self.model, inputs,
                loss_type=self.loss_type,
                constrainA=self.uq_std_min,
                constrainB=self.uq_std_max,
                standardizeY=self.YStandardize,
                Ymeans=self.RAOBmeans,
                Ystds=self.RAOBstds)

            if returnType == 'uq':
                return pmean, pstd
            elif returnType == 'uq4':
                return pmean, pstd, pmin, pmax
            elif returnType == 'params':
                pvals = np.concatenate(
                    [np.expand_dims(pmean, axis=2),
                     np.expand_dims(pstd, axis=2)], axis=2)
                return pvals
            else:
                return pmean

        elif 'sinh' in self.loss_type:
            pvals = self.model.predict(inputs, verbose=verbose)
            mu = pvals[..., 0]
            sigma = np.exp(pvals[..., 1])
            gamma = pvals[..., 2]
            tau = np.exp(pvals[..., 3])

            if self.loss_type == 'uq_sinhr' \
                    or self.loss_type == 'uq_sinhrt':
                sigma = fl.determine_constraintmap_vals(
                    self.uq_std_min, self.uq_std_max, pvals[..., 1])
            if self.loss_type == 'uq_sinhrt':
                tau = fl.determine_constraintmap_vals(
                    self.uq_tau_min, self.uq_tau_max, pvals[..., 3])

            if returnType == 'mc':
                pens = fl.create_sample_sinh_tfp(
                    mu, sigma, gamma, tau,
                    nSamples=self.uq_n_members,
                    Ymeans=self.RAOBmeans,
                    Ystds=self.RAOBstds)
                return np.moveaxis(pens, 0, -1)

            if returnType == 'params':
                pvals[..., 1] = sigma
                pvals[..., 3] = tau
                return pvals

            if self.YStandardize:
                pmean, pmedian, pstd, pmin, pmax = \
                    fl.unscale_sinh_tf(
                        mu, sigma, gamma, tau,
                        self.RAOBmeans, self.RAOBstds)

            else:
                pmean = fl.sinh_mean(mu, sigma, gamma, tau)
                pmedian = fl.sinh_median(mu, sigma, gamma, tau)
                pstd = fl.sinh_stddev(mu, sigma, gamma, tau)
                pmin = fl.sinh_min(mu, sigma, gamma, tau)
                pmax = fl.sinh_max(mu, sigma, gamma, tau)

            if returnType == 'params':
                return pvals
            elif returnType == 'uq':
                return pmedian, pstd
            elif returnType == 'uq3':
                return pmean, pstd, pmedian
            elif returnType == 'uq4':
                return pmedian, pstd, pmin, pmax
            else:
                return pmedian

        else:
            preds = self.model.predict(inputs, verbose=verbose)
            if self.YStandardize:
                preds = self._unstandardizeRAOB(preds)
            if returnType == 'uq':
                return preds, None
            else:
                return preds

    def save(self, path):
        tf_model_path = os.path.join(path, 'cnn_model.h5')
        if not os.path.exists(path):
            os.makedirs(path)
        self.model.save(tf_model_path)
        del self.model
        with open(path + '/class.pickle', 'wb') as f:
            pickle.dump(self, f)
        self.model = tf.keras.models.load_model(tf_model_path)

    def singlize(self, inData):
        arrayV = inData[0]
        arrayIm = inData[1]
        self.nSamples = arrayV.shape[0]
        self.nRapLevs = arrayV.shape[1]
        self.nRapChs = arrayV.shape[2]
        self.nImChs = arrayIm.shape[-1]

        arrayOut = np.empty((self.nSamples, self.n_inputs))
        count = 0
        for i in range(self.nRapLevs):
            for j in range(self.nRapChs):
                arrayOut[:, count] = arrayV[:, i, j]
                count += 1

        for i in range(self.nImChs):
            arrayOut[:, count] = arrayIm[:, 0, 0, i]
            count += 1

        if count != self.n_inputs:
            print("Incorrectly reshaped input data!")
            return None
        else:
            return arrayOut

    def unsinglize(self, arrayIn):
        arrayV = np.empty((self.nSamples, self.nRapLevs, self.nRapChs))
        arrayIm = np.empty((self.nSamples, 1, 1, self.nImChs))
        count = 0
        for i in range(self.nRapLevs):
            for j in range(self.nRapChs):
                arrayV[:, i, j] = arrayIn[:, count]
                count += 1

        for i in range(self.nImChs):
            arrayIm[:, 0, 0, i] = arrayIn[:, count]
            count += 1

        if count != self.n_inputs:
            print("Incorrectly unsinglized input data!")
            return []
        else:
            return [arrayV, arrayIm]


#################################
# TRAIN LOGGER
class TrainLogger(tf.keras.callbacks.Callback):

    def __init__(self, n_epochs, step=10):
        self.step = step
        self.n_epochs = n_epochs

    def on_epoch_end(self, epoch, logs=None, verbose=False):

        s = f"epoch: {epoch}"

        logKey = 'train_rmse'  # 'root_mean_squared_error'
        if logKey in logs:
            s += f", rmse {logs[logKey]:7.5f}"

        logKey = 'val_train_rmse'  # 'val_root_mean_squared_error'
        if logKey in logs:
            s += f", val_rmse {logs[logKey]:7.5f}"

        if epoch % self.step == 0:
            print(s)
        elif epoch + 1 == self.n_epochs:
            print(s)
            if verbose:
                print('Finished!')


################################
# NEURAL NETWORK FUNCTIONS
def build_linear_model_functional(
        learnRate=0.001,
        loss="mean_absolute_error",
        metrics=None,
        nFeatures=1,
        nTargets=1,
        verbose=True):
    """Build/Compile Functional Linear Model"""

    input_shape = (nFeatures, )
    inLayer = tf.keras.layers.Input(shape=input_shape, name="inputs")
    outLayer = tf.keras.layers.Dense(nTargets)(inLayer)
    model = tf.keras.Model(
        inputs=inLayer, outputs=outLayer, name="LinF_Model")

    if metrics is None:
        metrics = setup_metrics()
    model.compile(
        optimizer=tf.optimizers.Adam(learning_rate=learnRate),
        loss=loss, metrics=metrics)

    if verbose:
        model.summary()

    return model


def check_optDict(optDict, defaultDict,
                  erStr="NeuralNetwork Error:",
                  verbose=True):
    missingConfigs = ff.find_mismatching_keys(
        defaultDict, optDict)
    for key in missingConfigs:
        optDict[key] = defaultDict[key]
        if verbose:
            if key[:2] != 'uq':
                print("{} Missing {}".format(erStr, key))
    return optDict


def combine_layers(nCombine, data1, data2=None, data3=None):
    nLevs = data1.shape[1]
    nLevsNew = int(np.ceil(nLevs / nCombine))

    newShape1 = list(data1.shape)
    newShape1[1] = nLevsNew
    data1New = np.empty(newShape1)
    if data2 is not None:
        newShape2 = list(data2.shape)
        newShape2[1] = nLevsNew
        data2New = np.empty(newShape2)
    if data3 is not None:
        newShape3 = list(data3.shape)
        newShape3[1] = nLevsNew
        data3New = np.empty(newShape3)

    iVal = 0
    iNew = 0
    while iVal + nCombine <= nLevs:
        data1New[:, iNew, :] = np.mean(
            data1[:, iVal:iVal + nCombine, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iVal:iVal + nCombine, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iVal: iVal + nCombine, :], axis=1)
        iVal += nCombine
        iNew += 1

    if iVal != nLevs:
        data1New[:, iNew, :] = np.mean(data1[:, iVal:, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(data2[:, iVal:, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(data3[:, iVal:, :], axis=1)
        iNew += 1

    if iNew != nLevsNew:
        print("Problem Combining Layers!")

    if data3 is not None:
        return data1New, data2New, data3New
    elif data2 is not None:
        return data1New, data2New
    else:
        return data1New


def combine_layersA(nCombineSfc, nCombineA, data1,
                    data2=None, data3=None,
                    nLevsSfc=25,
                    returnLevs=False):
    nLevs = data1.shape[1]

    iComboStart = 0
    iComboStop = iComboStart + nCombineSfc
    iNew = 0
    while iComboStart <= nLevsSfc:
        iComboStart += nCombineSfc
        iComboStop += nCombineSfc
        iNew += 1
    while iComboStop <= nLevs:
        iComboStart += nCombineA
        iComboStop += nCombineA
        iNew += 1
    if iComboStop < nLevs:
        iNew += 1
    if returnLevs:
        return iNew
    else:
        nLevsNew = iNew

    newShape1 = list(data1.shape)
    newShape1[1] = nLevsNew
    data1New = np.empty(newShape1)
    if data2 is not None:
        newShape2 = list(data2.shape)
        newShape2[1] = nLevsNew
        data2New = np.empty(newShape2)
    if data3 is not None:
        newShape3 = list(data3.shape)
        newShape3[1] = nLevsNew
        data3New = np.empty(newShape3)

    iComboStart = 0
    iComboStop = iComboStart + nCombineSfc
    iNew = 0
    while iComboStart <= nLevsSfc:
        data1New[:, iNew, :] = np.mean(
            data1[:, iComboStart:iComboStop, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iComboStart:iComboStop, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iComboStart:iComboStop, :], axis=1)
        iComboStart += nCombineSfc
        iComboStop += nCombineSfc
        iNew += 1

    while iComboStop <= nLevs:
        data1New[:, iNew, :] = np.mean(
            data1[:, iComboStart:iComboStop, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iComboStart:iComboStop, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iComboStart:iComboStop, :], axis=1)
        iComboStart += nCombineA
        iComboStop += nCombineA
        iNew += 1

    if iComboStop < nLevs:
        data1New[:, iNew, :] = np.mean(
            data1[:, iComboStop:, :], axis=1)
        if data2 is not None:
            data2New[:, iNew, :] = np.mean(
                data2[:, iComboStop:, :], axis=1)
        if data3 is not None:
            data3New[:, iNew, :] = np.mean(
                data3[:, iComboStop:, :], axis=1)
        iNew += 1

    if iNew != nLevsNew:
        print("Problem Combining Layers!")

    if data3 is not None:
        return data1New, data2New, data3New
    elif data2 is not None:
        return data1New, data2New
    else:
        return data1New


def organize_data(expDict, data, verbose=True):

    input_cape = ff.get_exp_inputCAPE(expDict)
    input_channels_goes = ff.get_exp_inputGOES(expDict)
    input_channels_rap = ff.get_exp_inputRAP(expDict)
    input_channels_rtma = ff.get_exp_inputRTMA(expDict)
    input_extra_goes = ff.get_exp_inputGOESExtra(expDict)
    output_dims_raobs = ff.get_exp_outputRAOB(expDict)
    loss_type = ff.get_exp_loss(expDict)
    vcombine = ff.get_exp_vcombine(expDict)

    if verbose:
        print('INFO: data organization -')
        print("   CAPE/CIN Input: {}".format(input_cape))
        print("   GOES Input: {}".format(input_channels_goes))
        print("   GOES Input Extra: {}".format(input_extra_goes))
        print("   RAP Input: {}".format(input_channels_rap))
        print("   RTMA Input: {}".format(input_channels_rtma))
        print("   RAOB Output: {}".format(output_dims_raobs))
        print("   Vertical Combination: {}".format(vcombine))

    (RAPtrain, RAPval, RAPtest,
     RTMAtrain, RTMAval, RTMAtest,
     GOEStrain, GOESval, GOEStest,
     RAOBtrain, RAOBval, RAOBtest,
     _, _, _,
     RAPCCtrain, RAPCCval, RAPCCtest) = data

    if vcombine:
        if len(vcombine) == 1:
            RAPtr, RAPv, RAPte = combine_layers(
                vcombine[0], RAPtrain, RAPval, RAPtest)
        else:
            RAPtr, RAPv, RAPte = combine_layersA(
                vcombine[0], vcombine[1],
                RAPtrain, RAPval, RAPtest)
    else:
        RAPtr = RAPtrain
        RAPv = RAPval
        RAPte = RAPtest

    Xti, Xvi, Xei = [], [], []
    ia = (1, 2)
    if input_channels_rtma:
        Xti.append(RTMAtrain[..., input_channels_rtma].mean(axis=ia))
        Xvi.append(RTMAval[..., input_channels_rtma].mean(axis=ia))
        Xei.append(RTMAtest[..., input_channels_rtma].mean(axis=ia))
    if input_channels_goes:
        Xti.append(GOEStrain[..., input_channels_goes].mean(axis=ia))
        Xvi.append(GOESval[..., input_channels_goes].mean(axis=ia))
        Xei.append(GOEStest[..., input_channels_goes].mean(axis=ia))
    if input_extra_goes:
        for i in input_extra_goes:
            if i == 0:
                Xti.append(np.std(
                    GOEStrain[..., input_channels_goes], axis=ia))
                Xvi.append(np.std(
                    GOESval[..., input_channels_goes], axis=ia))
                Xei.append(np.std(
                    GOEStest[..., input_channels_goes], axis=ia))
            if i == 1:  # GOES 10 - 8
                t1 = np.expand_dims(
                    GOEStrain[..., 2].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStrain[..., 0].mean(axis=ia), axis=1)
                Xti.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOESval[..., 2].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOESval[..., 0].mean(axis=ia), axis=1)
                Xvi.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOEStest[..., 2].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStest[..., 0].mean(axis=ia), axis=1)
                Xei.append(np.subtract(t1, t2))

            if i == 2:  # GOES 13 - 15
                t1 = np.expand_dims(
                    GOEStrain[..., 4].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStrain[..., 6].mean(axis=ia), axis=1)
                Xti.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOESval[..., 4].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOESval[..., 6].mean(axis=ia), axis=1)
                Xvi.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOEStest[..., 4].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStest[..., 6].mean(axis=ia), axis=1)
                Xei.append(np.subtract(t1, t2))

            if i == 3:  # GOES 14 - 13
                t1 = np.expand_dims(
                    GOEStrain[..., 5].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStrain[..., 4].mean(axis=ia), axis=1)
                Xti.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOESval[..., 5].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOESval[..., 4].mean(axis=ia), axis=1)
                Xvi.append(np.subtract(t1, t2))
                t1 = np.expand_dims(
                    GOEStest[..., 5].mean(axis=ia), axis=1)
                t2 = np.expand_dims(
                    GOEStest[..., 4].mean(axis=ia), axis=1)
                Xei.append(np.subtract(t1, t2))

    if input_cape:
        Xti.append(RAPCCtrain[..., input_cape])
        Xvi.append(RAPCCval[..., input_cape])
        Xei.append(RAPCCtest[..., input_cape])

    # train
    Xtr = RAPtr[:, :, input_channels_rap]
    Xti = np.concatenate(Xti, axis=1) if len(Xti) else None
    Tt = RAOBtrain[:, :, output_dims_raobs].reshape(
        RAOBtrain.shape[0], -1)

    # validation
    Xvr = RAPv[:, :, input_channels_rap]
    Xvi = np.concatenate(Xvi, axis=1) if len(Xvi) else None
    Tv = RAOBval[:, :, output_dims_raobs].reshape(
        RAOBval.shape[0], -1)

    # test
    Xer = RAPte[:, :, input_channels_rap]
    Xei = np.concatenate(Xei, axis=1) if len(Xei) else None
    Te = RAOBtest[:, :, output_dims_raobs].reshape(
        RAOBtest.shape[0], -1)

    if loss_type == 'mse_weighted':
        Tt = organize_data_mse_weighted_ps(
            Tt, RAOBtrain[..., ff.RAOB_INDX_PRESSURE])
        Tv = organize_data_mse_weighted_ps(
            Tv, RAOBval[..., ff.RAOB_INDX_PRESSURE])
        Te = organize_data_mse_weighted_ps(
            Te, RAOBtest[..., ff.RAOB_INDX_PRESSURE])

    if verbose:
        Xti_print = Xti.shape if Xti is not None else 'None'
        Xvi_print = Xvi.shape if Xvi is not None else 'None'
        Xei_print = Xei.shape if Xei is not None else 'None'

        print('INFO: data dimensions --')
        print('   Train Shape X: {}, {} and T: {}'.format(
            Xtr.shape, Xti_print, Tt.shape))
        print('   Validation Shape X: {}, {} and T: {}'.format(
            Xvr.shape, Xvi_print, Tv.shape))
        print('   Test Shape X: {}, {} and T: {}'.format(
            Xer.shape, Xei_print, Te.shape))

    return [Xtr, Xti], Tt, [Xvr, Xvi], Tv, [Xer, Xei], Te


def organize_data_mse_weighted_ps(YArray, PSArray,
                                  weights=(0.5, 0.5)):

    YShape = YArray.shape
    nSamples = YShape[0]
    nZ = YShape[1]

    YNew = np.zeros((nSamples, nZ, 2))
    YNew[..., 0] = YArray.copy()

    PSTot = np.sum(PSArray, axis=1)
    PSWeights = np.divide(PSArray, PSTot[:, None])
    nV = PSWeights.shape[1]

    YNew[:, :nV, 1] = np.multiply(PSWeights, weights[0])
    YNew[:, nV:, 1] = np.multiply(PSWeights, weights[1])

    return YNew


def use_normalUQ(model, xvals,
                 loss_type='',
                 constrainA=None, constrainB=None,
                 standardizeY=False,
                 Ymeans=None, Ystds=None):

    erStr = "USE_NORMALUQ ERROR:"
    if standardizeY:
        if Ystds is None:
            print("{} Missing Ystds.".format(erStr))
            return None
        if Ymeans is None:
            print("{} Missing Ymeans.".format(erStr))
            return None

    constrain = False
    if loss_type == 'uq_normalr':
        constrain = True

    if constrain:
        if constrainA is None:
            print("{} Missing constrainA".format(erStr))
            return None
        if constrainB is None:
            print("{} Missing constrainB".format(erStr))
            return None

    pvals = model.predict(xvals)
    pmean = pvals[..., 0]
    if constrain:
        pstd = fl.determine_constraintmap_vals(
            constrainA, constrainB, pvals[..., 1])
    else:
        pstd = np.exp(pvals[..., 1])

    if standardizeY:
        pmean, pstd, pmin, pmax = \
            fl.unscale_normal_tf(pmean, pstd, Ymeans, Ystds)
    else:
        pmin = np.subtract(pmean, np.multiply(1.96, pstd))
        pmax = np.add(pmean, np.multiply(1.96, pstd))

    return pmean, pstd, pmin, pmax


def setup_clear():
    tf.keras.backend.clear_session()


def setup_gpu(restrictGPU=True,
              setGPU=True, numGPU=0):
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        print("Running on GPU ({} Available).".format(len(gpus)))
        if restrictGPU:
            # Restrict TensorFlow to only use the first GPU
            if numGPU < 0:
                myGPU = len(gpus) + numGPU
            else:
                myGPU = numGPU + 1
            print("Using GPU Number {}.".format(myGPU))
            try:
                tf.config.set_visible_devices(gpus[numGPU], 'GPU')
                if setGPU:
                    print("Limited GPU memory growth.")
                    tf.config.experimental.set_memory_growth(
                        gpus[numGPU], True)
                logical_gpus = tf.config.list_logical_devices('GPU')

            except RuntimeError as e:
                # Visible devices must be set before GPUs have been initialized
                print('SETUP_GPU ERROR: {}'.format(e))
        else:
            print("Using All GPUs")
            try:
                # Currently, memory growth needs to be the same across GPUs
                if setGPU:
                    for gpu in gpus:
                        tf.config.experimental.set_memory_growth(gpu, True)
                    print("Limited GPU memory growth.")
                logical_gpus = tf.config.list_logical_devices('GPU')
                print(len(logical_gpus), "Logical GPUs")

            except RuntimeError as e:
                # Memory growth must be set before GPUs have been initialized
                print('SETUP_GPU ERROR: {}'.format(e))
    else:
        print("Running on CPU.")
    return


def setup_loss_metrics(loss_f,
                       n_outputs=1,
                       uq_std_min=None,
                       uq_std_max=None,
                       uq_tau_min=None,
                       uq_tau_max=None):
    """Return loss function and metrics from string option."""

    metrics = setup_metrics()
    if loss_f == 'mae':
        loss = tf.keras.losses.MAE
    elif loss_f == 'mae_dropout':
        loss = tf.keras.losses.MAE
    elif loss_f == 'mae_surface':
        loss = fl.loss_mae_surface
    elif loss_f == 'mae_weighted':
        weight = 3.75 \
            * np.exp(-0.01
                     * np.arange(n_outputs)) + 0.25

        def loss(y_true, y_pred):
            return fl.loss_mae_weighted(y_true, y_pred, weight)
    elif loss_f == 'crps':
        if n_outputs < 2:
            loss = fl.loss_crps_sample_score
        else:
            loss = fl.loss_crps_sample_score2D
        metrics = setup_metrics(useCRPS=True)
    elif loss_f == 'mse':
        loss = tf.keras.losses.MSE
    elif loss_f == 'mse_dropout':
        loss = tf.keras.losses.MSE
    elif loss_f == 'mse_seperated':
        loss = fl.loss_mse_seperated
    elif loss_f == 'mse_weighted':
        loss = fl.loss_mse_weighted_ps2D
        metrics = setup_metrics(use2D=True)
    elif loss_f == 'uq_normal':
        loss = fl.loss_uq_normal
        metrics = setup_metrics(useNormal=True)
    elif loss_f == 'uq_normalr':
        loss = fl.loss_uq_normalr(stdMin=uq_std_min, stdMax=uq_std_max)
        metrics = setup_metrics(useNormal=True)
    elif loss_f == 'uq_sinh':
        loss = fl.loss_uq_sinh
        metrics = setup_metrics(useSinh=True)
    elif loss_f == 'uq_sinhr':
        loss = fl.loss_uq_sinhr(stdMin=uq_std_min, stdMax=uq_std_max)
        metrics = setup_metrics(useSinh=True)
    elif loss_f == 'uq_sinhrt':
        loss = fl.loss_uq_sinhrt(uq_std_min, uq_std_max,
                                 uq_tau_min, uq_tau_max)
        metrics = setup_metrics(useSinh=True)
    else:
        print("UNKNOWN LOSS FUNCTION, USING MSE")
        loss = tf.keras.losses.MSE
    return loss, metrics


def setup_metrics(use2D=False,
                  useNormal=False,
                  useSinh=False,
                  useCRPS=False):

    if use2D:
        fl.loss_mae2D.__name__ = TRAIN_MAE_NAME
        fl.loss_mse2D.__name__ = TRAIN_MSE_NAME
        fl.loss_rmse2D.__name__ = TRAIN_RMSE_NAME
        fl.loss_mse_weighted_ps2D.__name__ = TRAIN_RMSEW_NAME
        METRICS2D = [fl.loss_mae2D, fl.loss_mse2D,
                     fl.loss_rmse2D, fl.loss_mse_weighted_ps2D]
        return METRICS2D
    elif useNormal:
        fl.loss_mae_normal.__name__ = TRAIN_MAE_NAME
        fl.loss_mse_normal.__name__ = TRAIN_MSE_NAME
        fl.loss_rmse_normal.__name__ = TRAIN_RMSE_NAME
        METRICSUQ = [fl.loss_mae_normal,
                     fl.loss_mse_normal,
                     fl.loss_rmse_normal]
        return METRICSUQ
    elif useSinh:
        fl.loss_mae_sinh.__name__ = TRAIN_MAE_NAME
        fl.loss_mse_sinh.__name__ = TRAIN_MSE_NAME
        fl.loss_rmse_sinh.__name__ = TRAIN_RMSE_NAME
        METRICSUQ = [fl.loss_mae_sinh,
                     fl.loss_mse_sinh,
                     fl.loss_rmse_sinh]
        return []  # METRICSUQ
    elif useCRPS:
        return []
    else:
        fl.loss_mae_surface.__name__ = TRAIN_MAE_SFC_NAME
        METRICS1D = [
            tf.keras.metrics.MeanAbsoluteError(
                name=TRAIN_MAE_NAME),
            tf.keras.metrics.MeanSquaredError(
                name=TRAIN_MSE_NAME),
            tf.keras.metrics.RootMeanSquaredError(
                name=TRAIN_RMSE_NAME),
            fl.loss_mae_surface]
        return METRICS1D


def setup_nn(expDict,
             n_levs_in=DEFAULT_NLEVS,
             n_levs_out=DEFAULT_NLEVS,
             n_inputs=None, n_outputs=None,
             addTRef=1, addTDRef=2):

    erStr = "SETUP_NN ERROR: "

    nGOES, nRAP, nRTMA, nCC = ff.get_exp_dataInputNs(expDict)
    if n_outputs is None:
        n_outputs = ff.get_exp_dataOutputNs(expDict) * n_levs_out

    modelOpts = ff.get_exp_modelOpts(expDict)
    network_name = ff.get_exp_networkname(expDict)

    runOpts = ff.get_exp_runOpts(expDict)

    if network_name == 'NeuralNetwork':
        if n_inputs is None:
            n_inputs = nGOES + nRAP * n_levs_in + nRTMA + nCC
        model = NeuralNetwork(n_inputs, n_outputs,
                              modelOpts=modelOpts)
    elif network_name == 'SkipNeuralNetwork':
        shape_rap_inputs = (n_levs_in, nRAP)
        shape_im_inputs = (nGOES + nRTMA + nCC,)
        model = SkipNeuralNetwork(shape_rap_inputs,
                                  shape_im_inputs,
                                  n_outputs,
                                  addTRef=addTRef,
                                  addTDRef=addTDRef,
                                  modelOpts=modelOpts)
    else:
        print('{} {} Not Setup!'.format(
            erStr, network_name))
        model = None

    model.compile(runOpts=runOpts)

    return model


def setup_optimizer(method, learning_rate):
    """Return optimizer from string options."""
    if method == 'sgd':
        algo = tf.keras.optimizers.SGD(learning_rate=learning_rate)
    elif method == 'adam':
        algo = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    elif method == 'adagrad':
        algo = tf.keras.optimizers.Adagrad(learning_rate=learning_rate)
    elif method == 'rmsprop':
        algo = tf.keras.optimizers.RMSprop(learning_rate=learning_rate)
    elif method == 'adadelta':
        algo = tf.keras.optimizers.Adadelta(
            learning_rate=learning_rate)
    elif method == 'nadam':
        algo = tf.keras.optimizers.Nadam(
            learning_rate=learning_rate)
    else:
        raise Exception(
            "train: method={method} not a valid optimizer.")
    return algo
