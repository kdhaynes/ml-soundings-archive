"""
ML Soudings Check Dropout Results

Modified: 2023/07
"""

# %%
# Import Libraries
from functions_misc import get_best_trial_cape_indx
from functions_misc import pickle_read


# %%
# User Info
saveDir = '/mnt/data1/kdhaynes/mlsoundings/results/'
saveFile = 'crps_dropout_test.pkl'


# %%
# Read results
vDict = pickle_read(saveDir + saveFile)
vDictList = vDict['vDictList']
indxBest = get_best_trial_cape_indx(
    vDictList, verbose=True)


# %%
# Print results of best run
modelList = vDict['modelOptsList']
bestModel = modelList[indxBest]
print("  BEST Drop Conv: {}, Last: {}".format(
      bestModel['dropout_conv_rate'],
      bestModel['dropout_dense_last']))
print("  BEST Kernel Reg: {}, Val: {}\n".format(
      bestModel['kernel_reg'],
      bestModel['kernel_reg_lval']))
