"""
ML Soundings Research

Check dataset.

Modified: 2023/07
"""


# %%
# Libraries
import numpy as np
from datetime import datetime
from netCDF4 import Dataset


# %%
# User Info
fileDir = '/Users/kdhaynes/Data/cira/mlsoundings/nc/'
fileName = 'mlsoundings_dataset.nc'


# %%
# Open file
myFile = fileDir + fileName
ds = Dataset(myFile)


# %%
# Check root group
lat = ds.variables['latitude'][:]
print("Lat Min: {:.3f}, Max: {:.3f}".format(
    np.min(lat), np.max(lat)))
lon = ds.variables['longitude'][:]
print("Lon Min: {:.3f}, Max: {:.3f}".format(
    lon.min(), lon.max()))
timestamp = ds.variables['time'][:]
print("Time Min: {}, Max: {}".format(
    timestamp.min(), timestamp.max()))
print("   Units: {}".format(ds.variables['time'].units))
print("   Start: {}".format(
    datetime.fromtimestamp(timestamp[0]).strftime(
        "%A, %B %d, %Y %I:%M:%S")))
print("   Stop:  {}".format(
    datetime.fromtimestamp(timestamp[-1]).strftime(
        "%A, %B %d, %Y %I:%M:%S")))
altitude = ds.variables['altitude'][:]
print("Altitude Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    altitude.min(), altitude.max(),
    ds.variables['altitude'].units))


# %%
# Check radiosonde group
sitenameschar = ds['radiosonde'].variables['site_name'][...]
nsoundings = sitenameschar.shape[0]
sitenames = np.empty(nsoundings, dtype='object')
for i in range(nsoundings):
    sitenames[i] = str(sitenameschar[i, :].tobytes().decode())
sitesunique = np.unique(sitenames)
print(f"Site names: {len(sitesunique)} unique")
print(sitesunique)

pressure = ds['radiosonde'].variables['raob_pressure'][...]
print("RAOB Pressure Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    pressure.min(), pressure.max(),
    ds['radiosonde'].variables['raob_pressure'].units))
temperature = ds['radiosonde'].variables['raob_temperature'][...]
print("RAOB Temperature Min: {:.3f}. Max: {:.3f}, Units: {}".format(
    temperature.min(), temperature.max(),
    ds['radiosonde'].variables['raob_temperature'].units))
dewpoint = ds['radiosonde'].variables['raob_dewpoint'][...]
print("RAOB Dewpoint Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    dewpoint.min(), dewpoint.max(),
    ds['radiosonde'].variables['raob_dewpoint'].units))
gheight = ds['radiosonde'].variables['raob_geopotential_height'][...]
print("RAOB Geopotential Height Min: {:.3f}, Max: {:.3f}".format(
      gheight.min(), gheight.max()))
print("    Units: {}".format(
    ds['radiosonde']['raob_geopotential_height'].units))
cape = ds['radiosonde'].variables['raob_cape'][:]
print("RAOB CAPE Min: {:.3f}, Max: {:.3f}, Units: {}".format(
      cape.min(), cape.max(),
      ds['radiosonde']['raob_cape'].units))
cin = ds['radiosonde'].variables['raob_cin'][:]
print("RAOB CIN Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    cin.min(), cin.max(),
    ds['radiosonde']['raob_cin'].units))


# %%
# Check RAP group
rap_pressure = ds['rap'].variables['rap_pressure'][...]
print("RAP Pressure Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    rap_pressure.min(), rap_pressure.max(),
    ds['rap'].variables['rap_pressure'].units))
rap_temperature = ds['rap'].variables['rap_temperature'][...]
print("RAP Temperature Min: {:.3f}. Max: {:.3f}, Units: {}".format(
    rap_temperature.min(), rap_temperature.max(),
    ds['rap'].variables['rap_temperature'].units))
rap_dewpoint = ds['rap'].variables['rap_dewpoint'][...]
print("RAP Dewpoint Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    rap_dewpoint.min(), rap_dewpoint.max(),
    ds['rap']['rap_dewpoint'].units))
rap_gheight = ds['rap'].variables['rap_geopotential_height'][...]
print("RAP Geopotential Height Min: {:.3f}, Max: {:.3f}".format(
      rap_gheight.min(), rap_gheight.max()))
print("    Units: {}".format(
    ds['rap']['rap_geopotential_height'].units))
rap_cape = ds['rap'].variables['rap_cape'][:]
print("RAP CAPE Min: {:.3f}, Max: {:.3f}, Units: {}".format(
      rap_cape.min(), rap_cape.max(),
      ds['rap']['rap_cape'].units))
rap_cin = ds['rap'].variables['rap_cin'][:]
print("RAP CIN Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    rap_cin.min(), rap_cin.max(),
    ds['rap']['rap_cin'].units))


# %%
# Check RTMA group
rtma_pressure = ds['rtma'].variables['rtma_pressure'][...]
print("RTMA Pressure Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    rtma_pressure.min(), rtma_pressure.max(),
    ds['rtma']['rtma_pressure'].units))
rtma_temperature = ds['rtma'].variables['rtma_temperature'][...]
print("RTMA Temperature Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    rtma_temperature.min(), rtma_temperature.max(),
    ds['rtma']['rtma_temperature'].units))
rtma_dewpoint = ds['rtma'].variables['rtma_dewpoint'][...]
print("RTMA Dewpoint Min: {:.3f}, Max: {:.3f}, Units: {}".format(
    rtma_dewpoint.min(), rtma_dewpoint.max(),
    ds['rtma']['rtma_dewpoint'].units))


# %%
# Check GOES group
goes8 = ds['goes'].variables['goes_band08'][...]
print("GOES Band 08 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes8.min(), goes8.max(),
          ds['goes']['goes_band08'].units))
goes9 = ds['goes'].variables['goes_band09'][...]
print("GOES Band 09 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes9.min(), goes9.max(),
          ds['goes']['goes_band09'].units))
goes10 = ds['goes'].variables['goes_band10'][...]
print("GOES Band 10 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes10.min(), goes10.max(),
          ds['goes']['goes_band10'].units))
goes11 = ds['goes'].variables['goes_band11'][...]
print("GOES Band 11 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes11.min(), goes11.max(),
          ds['goes']['goes_band11'].units))
goes13 = ds['goes'].variables['goes_band13'][...]
print("GOES Band 13 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes13.min(), goes13.max(),
          ds['goes']['goes_band13'].units))
goes14 = ds['goes'].variables['goes_band14'][...]
print("GOES Band 14 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes14.min(), goes14.max(),
          ds['goes']['goes_band14'].units))
goes15 = ds['goes'].variables['goes_band15'][...]
print("GOES Band 15 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes15.min(), goes15.max(),
          ds['goes']['goes_band15'].units))
goes16 = ds['goes'].variables['goes_band16'][...]
print("GOES Band 16 Min: {:.3f}, Max: {:.3f}, Units: {}".
      format(
          goes16.min(), goes16.max(),
          ds['goes']['goes_band16'].units))


# %%
# Check ML variables
ntrain = ds['ml'].dimensions['ntrain'].size
print(f"ML ntrain: {ntrain}")
trind = ds['ml'].variables['train_indices'][:]
print(f"    Min: {trind.min()}, Max: {trind.max()}")
nval = ds['ml'].dimensions['nvalidate'].size
print(f"ML nvalidate: {nval}")
vind = ds['ml'].variables['validation_indices'][:]
print(f"    Min: {vind.min()}, Max: {vind.max()}")
ntest = ds['ml'].dimensions['ntest'].size
print(f"ML ntest: {ntest}")
teind = ds['ml'].variables['test_indices'][:]
print(f"    Min: {teind.min()}, Max: {teind.max()}")


# %%
# Close file
ds.close()
