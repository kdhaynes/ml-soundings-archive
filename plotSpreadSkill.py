"""
ML Soundings Project

Program to plot the spread-skill diagram.

Modified: 2023/09
"""

# %%
# Import Libraries
import functions_misc as fm
import functions_plot as fp
from importlib import reload

import var_opts as vo

# %%
# User Options

plotSpreadSkill = True
saveSpreadSkill = False

refLow = 0
refHigh = 238
expList = [vo.exp015, vo.exp137,
           vo.exp515, vo.exp527]
labelList = ['LIN CRPS', 'NN SHASH',
             'UNet CRPS', 'UNet SHASH']
colorList = ['mediumseagreen', 'darkorange',
             'darkslateblue', 'mediumslateblue']

targetNames = ['Temperature', 'Dewpoint']
spread_binsT = [0.0, 0.5, 0.75, 1.0,
                1.25, 1.5, 1.75, 2.0,
                2.25, 2.5, 2.75, 3.,
                3.25, 3.5, 3.75, 4.]
spread_lastT = 4.0
spread_ticksT = [1, 2, 3, 4]
spread_widthT = 0.15
spread_binsTD = [0.0, 1.5, 3.0, 4.5,
                 6.0, 7.5, 9., 20]
#                 18]  # 15., 20.]
spread_lastTD = 10
spread_ticksTD = [0, 2, 4, 6, 8, 10]
spread_widthTD = 0.8

spread_bins = [spread_binsT, spread_binsTD]
spread_lasts = [spread_lastT, spread_lastTD]
spread_ticks = [spread_ticksT, spread_ticksTD]
spread_widths = [spread_widthT, spread_widthTD]

legend_loc = 'lower right'
plot_hist = 'inset'
shiftx = 0.04
shifty = -0.02
xLabel = 'Spread (Std Dev)'

useShannon = True
shuffleAll = True
shuffleSites = False


# %%
# Read Data and Predictions
reload(fm)
dDict = {}
nTargets = len(targetNames)

for expDict in expList:

    eDict = {}
    exRef = fm.get_exp_refname(expDict)
    restrictData = fm.get_exp_restrict_data(expDict)
    data = fm.load_data(
        useShannon,
        restrictData=restrictData,
        returnFileInfo=True,
        shuffleAll=shuffleAll,
        shuffleSites=shuffleSites)
    RAOBte = data[11]
    YTest = fm.organize_data_y(RAOBte)

    filePreds = fm.get_exp_filenamePreds(
        useShannon, expDict)
    pDict = fm.pickle_read(filePreds)
    PTest_mean = pDict['PREDtest']
    PTest_std = pDict['PREDstd']

    eDict['YTest'] = YTest
    eDict['PREDtest'] = PTest_mean
    eDict['PREDstd'] = PTest_std

    eDict = fm.get_spread_skill_points(
        YTest[:, refLow:refHigh, :],
        PTest_mean[:, refLow:refHigh, :],
        PTest_std[:, refLow:refHigh, :],
        sDict=eDict,
        spread_bins=spread_bins,
        spread_last=spread_lasts)
    dDict[exRef] = eDict
print("Read True Data and Predictions.")


# %%
# Plot Spread-Skill
if plotSpreadSkill:
    reload(fm)
    reload(fp)

    saveFileList = []
    if saveSpreadSkill:
        save_file = 'SpreadSkill_'
        for exp in expList:
            expRef = fm.get_exp_refname(exp)
            save_file += expRef + '_'

        for tg in range(nTargets):
            saveFileList.append(
                save_file
                + targetNames[tg] + '.png')
    else:
        for tg in range(nTargets):
            saveFileList.append('')

    if len(expList) > 1:
        for tg in range(nTargets):
            fp.plot_spread_skill_dict(
                dDict,
                labelx=xLabel,
                legend_show=True,
                legend_loc=legend_loc,
                line_color=colorList,
                line_label=labelList,
                plot_hist=plot_hist,
                save_file=saveFileList[tg],
                spread_ticks=spread_ticks[tg],
                targetNum=tg,
                title=targetNames[tg],
                units='C')

    else:
        for tg in range(nTargets):
            binsHere = spread_bins[tg]
            ticksHere = spread_ticks[tg]
            widthsHere = spread_widths[tg]

            spread, error = \
                fp.calculate_spread_skill(
                    YTest[:, refLow:refHigh, tg],
                    PTest_mean[:, refLow:refHigh, tg],
                    PTest_std[:, refLow:refHigh, tg],
                    bins=binsHere)
            spread[-1] = spread_lasts[tg]
            spread_counts, spread_centers = \
                fm.get_histogram(
                    PTest_std[..., tg], bins=binsHere)

            fp.plot_spread_skill(
                spread, error,
                figSize=(5, 5),
                fontSize=15,
                labelx=xLabel,
                plot_hist=plot_hist,
                saveFile=saveFileList[tg],
                shiftx=shiftx,
                shifty=shifty,
                spreadCounts=spread_counts,
                spreadTicks=ticksHere,
                spreadWidth=widthsHere,
                title=targetNames[tg],
                units='C')
