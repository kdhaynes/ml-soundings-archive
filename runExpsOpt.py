"""
ML Soudings Experiment Driver


Modified: 2023/08
"""


# %%
# Import Libraries
import functions_misc as fm
import functions_nn as fnn

from copy import deepcopy


# %%
# Set User Info
n_dense_list = [[500], [500, 500],
                [1024], [1024, 1024]]
n_members = [30, 40, 50, 60, 70]
batch_size = [128, 256]
learning_rate = [0.00001, 0.0001]

dataOpts = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts = {
    'exRef': 'NN',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': True,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [0.0, 0.0],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': 'L1',
    'kernel_reg_lval': 0.0,
    'loss_type': 'crps',
    'n_dense_list': [1024, 1024],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0005,
    'optimizer': 'adam',
    'patience': 15}

shuffleAll = True
shuffleSites = False

calcCAPE = True
saveResults = True
saveDir = '/mnt/data1/kdhaynes/mlsoundings/results/'
saveFile = 'crpsnn_test.pkl'

refGPU = 1
setGPU = True
restrictGPU = True
useShannon = True


# %%
# Setup GPU
if useShannon:
    fnn.setup_gpu(restrictGPU=restrictGPU,
                  setGPU=setGPU, numGPU=refGPU)


# %%
# Read Data
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=dataOpts['restrict_data'],
    returnFileInfo=False,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
indxTr, indxVal, indxTe = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictData=dataOpts['restrict_data'],
    returnFileInfo=False,
    returnIndices=True,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites,
    verbose=False)

RAPCCv = data[16]
RAPv = data[1]
RAOBCCv = data[13]
RAOBv = data[10]


# %%
# Organize training/testing data
expDict = {'dataOpts': dataOpts,
           'modelOpts': modelOpts,
           'runOpts': runOpts}
addTRef = fm.get_exp_addTRef(expDict)
addTDRef = fm.get_exp_addTDRef(expDict)
output_dims = fm.get_exp_outputRAOB(expDict)
XTrain, YTrain, XVal, YVal, XTest, YTest = \
    fm.organize_data(
        expDict, data)
if XTrain[0] is not None:
    nIn = XTrain[0].shape[1]
else:
    nIn = 0


# %%
# Get RAP evaluation
rapDict = fm.calculate_results_rap(
    RAOBv, RAPv,
    type='val')
rapDict = fm.calculate_results_rapcc(
    RAOBCCv, RAPCCv,
    rDict=rapDict, type='val')


# %%
# Train model - loop model training
count = 0
vDictList = []
modelOptsList = []
runOptsList = []
print("INFO:  training --")
for nl in n_dense_list:
    for nm in n_members:
        mOpts = deepcopy(modelOpts)
        mOpts['n_dense_list'] = nl
        if len(nl) == 1:
            mOpts['dropout_dense_list'] = [0.0]
        mOpts['uq_n_members'] = nm
        expDict['modelOpts'] = mOpts
        for bs in batch_size:
            for lr in learning_rate:
                rOpts = deepcopy(runOpts)
                rOpts['batch_size'] = bs
                rOpts['learning_rate'] = lr
                expDict['runOpts'] = rOpts
                vDict = deepcopy(rapDict)

                print(f"=== Training Model {count} ===")
                print(f"  ndense= {nl}, nmembers= {nm}")
                print(f"  batch= {bs}, learn= {lr}")
                nnet = fnn.setup_nn(
                    expDict,
                    n_levs_in=nIn,
                    addTRef=addTRef,
                    addTDRef=addTDRef)

                nnet.train(XTrain, YTrain,
                           validation=(XVal, YVal))

                PVal_mean = nnet.use(XVal, returnType='')
                PREDv = PVal_mean.reshape(
                    RAOBv[:, :, output_dims].shape)
                vDict = fm.calculate_results(
                    RAOBv, YVal,
                    PVal_mean,
                    calcCAPE=True,
                    rDict=vDict,
                    type='val')
                vDictList.append(vDict)
                modelOptsList.append(mOpts)
                runOptsList.append(rOpts)
                fm.print_results(vDict, type='val')

                count += 1
                fnn.setup_clear()
                del nnet


# %%
# ... best model of the multiples trained
nnetIndx = fm.get_best_trial_cape_indx(
    vDictList, verbose=True)
myDict = modelOptsList[nnetIndx]
bestNL = myDict['n_dense_list']
bestNM = myDict['uq_n_members']
myDict = runOptsList[nnetIndx]
bestBS = myDict['batch_size']
bestLR = myDict['learning_rate']
print(f"  BEST Layers: {bestNL}, Members: {bestNM}")
print(f"  BEST Batch: {bestBS}, Learn: {bestLR}")


# %%
# Save Results
if saveResults:
    outDict = {
        'bestIndex': nnetIndx,
        'n_dense_last': bestNL,
        'n_members': bestNM,
        'batch_size': bestBS,
        'learning_rate': bestLR,
        'vDictList': vDictList,
        'modelOptsList': modelOptsList,
        'runOptsList': runOptsList}
    fm.pickle_dump(saveDir + saveFile,
                   outDict)


# %%
# Print best results
myDict = vDictList[nnetIndx]
fm.print_results(myDict, type='val')
