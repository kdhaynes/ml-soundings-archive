"""
ML Soundings Project

Program to try predicting CAPE and CIN
directly using a Random Forest or NN.

Modified: 2023/09
"""


# %%
# Import Libraries
import functions_cape as fc
import functions_misc as fm
import numpy as np

from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score

from var_opts import exp801 as expDict


# %%
# Set User Options

restrictCAPEHigh = 4000.
shuffleAll = True
shuffleSites = False
useShannon = True
verbose = True

criterionList = \
    ['squared_error',
     'absolute_error',
     'friedman_mse']
maxDepthList = [None]
maxFeaturesList = [None]
bootstrapList = [True]
nEstimators = [150]


# %%
# Load Data
expName = fm.get_exp_refname(expDict)
print('\nINFO: Running experiment {}'.format(expName))

outdims = fm.get_exp_outputCAPE(expDict)
mOpts = expDict['modelOpts']
restrictData = fm.get_exp_restrict_data(expDict)
data = fm.load_data(
    useShannon,
    includeCAPE=True,
    restrictCAPEHigh=restrictCAPEHigh,
    restrictData=restrictData,
    shuffleAll=shuffleAll,
    shuffleSites=shuffleSites)
(RAPtrain, RAPval, RAPtest,
 RTMAtrain, RTMAval, RTMAtest,
 GOEStrain, GOESval, GOEStest,
 RAOBtrain, RAOBval, RAOBtest,
 RAOBtrain_cape_cin, RAOBval_cape_cin,
 RAOBtest_cape_cin,
 RAPtrain_cape_cin, RAPval_cape_cin,
 RAPtest_cape_cin) = data


# %%
# Organize data
Xtr, Ytr, Xval, Yval, _, _ = fc.organize_data_cape(
    expDict, data)
nTrainAll = Xtr.shape[0]
nFeaturesAll = Xtr.shape[1]
nTargetsAll = Ytr.shape[1]
rapR2 = r2_score(Yval, RAPval_cape_cin[..., outdims])
print(f"Xtr shape: {Xtr.shape}")
print(f"Ytr shape: {Ytr.shape}")
print(f"RAP Baseline R2: {rapR2:.2f}")

if Ytr.shape[1] == 1:
    Ytr = Ytr.reshape(-1)
    Yval = Yval.reshape(-1)


# %%
# Run tests
bestCrit = None
bestDepth = None
bestFeat = None
bestBoot = None
bestEst = None
bestScore = np.NINF
bestModel = None

cStr = 'Criterion: '
bStr = 'Bootstrap: '
dStr = 'Max Depth: '
fStr = 'Max Features: '
neStr = 'Num Estimtors: '

count = 1
for crit in criterionList:
    for mdl in maxDepthList:
        for mfl in maxFeaturesList:
            for bs in bootstrapList:
                for nest in nEstimators:
                    print(f"\nExperiment {count}")
                    model = RandomForestRegressor(
                        bootstrap=bs,
                        criterion=crit,
                        max_depth=mdl,
                        max_features=mfl,
                        n_estimators=nest)
                    print(f"{cStr}{crit}, {bStr}{bs}")
                    print(f"{dStr}{mdl}, {fStr}{mfl}")
                    print(f"{neStr}{nest}")

                    # ...train
                    model.fit(Xtr, Ytr)

                    # ...create predictions
                    Pval = model.predict(Xval)
                    scoreHere = r2_score(Yval, Pval)

                    if scoreHere > bestScore:
                        bestScore = scoreHere
                        bestBoot = bs
                        bestCrit = crit,
                        bestDepth = mdl,
                        bestFeat = mfl,
                        bestEst = nest
                        bestModel = model
                    print(f"R2 Score: {scoreHere:.2f}, ",
                          f"Best: {bestScore:.2f}, ",
                          f"RAP: {rapR2:.2f}")

                    count += 1
print(f"\nPerformed {count - 1} tests.")


# %%
# Calculate results
print("\nBest Results: ")
print(f"  {cStr}{bestCrit}, {bStr}{bestBoot}")
print(f"  {dStr}{bestDepth}, {fStr}{bestFeat}")
print(f"  {neStr}{bestEst}")

Pval = bestModel.predict(Xval)
r2Best = r2_score(Yval, Pval)
print(f"   R2 Score: {r2Best:.3f}, RAP: {rapR2:.3f}")

if len(outdims) == 2:
    results = fc.calculate_results_cape_cin(
        Yval, Pval)
    resultNames = list(results.keys())
    print("ML Results:")
    for res in resultNames:
        resultHere = results[res]
        if type(resultHere) == np.float64:
            print("  {}= {:.2f}".format(
                res, resultHere))
        else:
            print("  {} Mean= {:.2f}".format(
                res, np.mean(np.array(resultHere))))

    resultsRAP = fc.calculate_results_cape_cin(
        Yval, RAPval_cape_cin, model='rap')
    resultNamesRAP = list(resultsRAP.keys())
    print("RAP Baseline Results:")
    for res in resultNamesRAP:
        resultHere = resultsRAP[res]
        if type(resultHere) == np.float64:
            print("  {}= {:.2f}".format(res, resultHere))
        else:
            print("  {} Mean= {:.2f}".format(
                res, np.mean(np.array(resultHere))))
