"""
===========================================
ML Soundings Experiments
===========================================

VALID LOSSES:
'MAE', 'MSE',
'mae_surface', 'mae_weighted',
'mse_seperated', 'mse_weighted'
'uq_normal', 'uq_sinh',
'crps'

Order Used In Paper:
1: MSE, 2: MAES, 3: MAEW, 4: MSEW,
5: CRPS, 6: NORM, 7: SHASH, 8: DROPOUT

"""

# LINEAR MODEL TESTS -------------------------------
# ...Loss Tests
dataOpts001 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts001 = {
    'exRef': 'LIN001',
    'network': 'NeuralNetwork',
    'activation_dense': None,
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'mse',
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts001 = {
    'batch_size': 128,
    'epochs': 200,
    'learning_rate': 0.001,
    'optimizer': 'adam',
    'patience': 10}
exp001 = {'dataOpts': dataOpts001,
          'modelOpts': modelOpts001,
          'runOpts': runOpts001}

dataOpts002 = dataOpts001.copy()
modelOpts002 = modelOpts001.copy()
modelOpts002['exRef'] = 'LIN002'
modelOpts002['loss_type'] = 'mae_surface'
runOpts002 = runOpts001.copy()
exp002 = {'dataOpts': dataOpts002,
          'modelOpts': modelOpts002,
          'runOpts': runOpts002}

dataOpts003 = dataOpts001.copy()
modelOpts003 = modelOpts001.copy()
modelOpts003['exRef'] = 'LIN003'
modelOpts003['loss_type'] = 'mae_weighted'
runOpts003 = runOpts001.copy()
exp003 = {'dataOpts': dataOpts003,
          'modelOpts': modelOpts003,
          'runOpts': runOpts003}

dataOpts004 = dataOpts001.copy()
modelOpts004 = modelOpts001.copy()
modelOpts004['exRef'] = 'LIN004'
modelOpts004['loss_type'] = 'mse_weighted'
runOpts004 = runOpts001.copy()
exp004 = {'dataOpts': dataOpts004,
          'modelOpts': modelOpts004,
          'runOpts': runOpts004}

dataOpts005 = dataOpts001.copy()
modelOpts005 = modelOpts001.copy()
modelOpts005['exRef'] = 'LIN005'
modelOpts005['loss_type'] = 'crps'
runOpts005 = {
    'batch_size': 128,
    'epochs': 600,
    'learning_rate': 0.0005,
    'optimizer': 'adam',
    'patience': 40}
exp005 = {'dataOpts': dataOpts005,
          'modelOpts': modelOpts005,
          'runOpts': runOpts005}

dataOpts006 = dataOpts001.copy()
modelOpts006 = modelOpts001.copy()
modelOpts006['exRef'] = 'LIN006'
modelOpts006['loss_type'] = 'uq_normal'
runOpts006 = {
    'batch_size': 128,
    'epochs': 2000,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 70}
exp006 = {'dataOpts': dataOpts006,
          'modelOpts': modelOpts006,
          'runOpts': runOpts006}

dataOpts007 = dataOpts001.copy()
modelOpts007 = modelOpts001.copy()
modelOpts007['exRef'] = 'LIN007'
modelOpts007['loss_type'] = 'uq_sinh'
runOpts007 = {
    'batch_size': 128,
    'epochs': 1500,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 100}
exp007 = {'dataOpts': dataOpts007,
          'modelOpts': modelOpts007,
          'runOpts': runOpts007}

dataOpts008 = dataOpts001.copy()
modelOpts008 = modelOpts001.copy()
modelOpts008['exRef'] = 'LIN008'
modelOpts008['dropout_flag'] = True
modelOpts008['dropout_dense_last'] = 0.1
modelOpts008['loss_type'] = 'mse_dropout'
runOpts008 = runOpts001.copy()
exp008 = {'dataOpts': dataOpts008,
          'modelOpts': modelOpts008,
          'runOpts': runOpts008}

dataOpts015 = dataOpts005.copy()
modelOpts015 = modelOpts005.copy()
modelOpts015['exRef'] = 'LIN015'
runOpts015 = runOpts005.copy()
exp015 = {'dataOpts': dataOpts015,
          'modelOpts': modelOpts015,
          'runOpts': runOpts015}

dataOpts055 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2],
    'input_channels_rtma': [0, 1, 2],
    'input_goes_extra': [],
    'input_rap_cape_cin': [0, 1],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': []}
modelOpts055 = modelOpts005.copy()
modelOpts055['exRef'] = 'LIN055'
runOpts055 = runOpts005.copy()
exp055 = {'dataOpts': dataOpts055,
          'modelOpts': modelOpts055,
          'runOpts': runOpts055}

dataOpts058 = {
    'input_channels_goes': [4, 6, 7],
    'input_channels_rap': [0, 1, 2],
    'input_channels_rtma': [0, 1, 2],
    'input_goes_extra': [2],
    'input_rap_cape_cin': [0],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts058 = {
    'exRef': 'LIN058',
    'network': 'NeuralNetwork',
    'activation_dense': None,
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'crps',
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 25}
runOpts058 = {
    'batch_size': 128,
    'epochs': 50,
    'learning_rate': 0.001,
    'optimizer': 'adam',
    'patience': 10}
exp058 = {'dataOpts': dataOpts058,
          'modelOpts': modelOpts058,
          'runOpts': runOpts058}

# ... Ablation Tests
dataOpts062 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts062 = modelOpts001.copy()  # MSE
modelOpts062['exRef'] = 'LIN062'
runOpts062 = runOpts001.copy()
exp062 = {'dataOpts': dataOpts062,
          'modelOpts': modelOpts062,
          'runOpts': runOpts062}

dataOpts063 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts063 = modelOpts001.copy()  # MSE
modelOpts063['exRef'] = 'LIN063'
runOpts063 = runOpts001.copy()
exp063 = {'dataOpts': dataOpts063,
          'modelOpts': modelOpts063,
          'runOpts': runOpts063}

dataOpts064 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts064 = modelOpts001.copy()  # MSE
modelOpts064['exRef'] = 'LIN064'
runOpts064 = runOpts001.copy()
exp064 = {'dataOpts': dataOpts064,
          'modelOpts': modelOpts064,
          'runOpts': runOpts064}

dataOpts065 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts065 = modelOpts001.copy()  # MSE
modelOpts065['exRef'] = 'LIN065'
runOpts065 = runOpts001.copy()
exp065 = {'dataOpts': dataOpts065,
          'modelOpts': modelOpts065,
          'runOpts': runOpts065}

dataOpts066 = dataOpts062.copy()
modelOpts066 = modelOpts002.copy()  # MAES
modelOpts066['exRef'] = 'LIN066'
runOpts066 = runOpts002.copy()
exp066 = {'dataOpts': dataOpts066,
          'modelOpts': modelOpts066,
          'runOpts': runOpts066}

dataOpts067 = dataOpts063.copy()
modelOpts067 = modelOpts002.copy()  # MAES
modelOpts067['exRef'] = 'LIN067'
runOpts067 = runOpts002.copy()
exp067 = {'dataOpts': dataOpts067,
          'modelOpts': modelOpts067,
          'runOpts': runOpts067}

dataOpts068 = dataOpts064.copy()
modelOpts068 = modelOpts002.copy()  # MAES
modelOpts068['exRef'] = 'LIN068'
runOpts068 = runOpts002.copy()
exp068 = {'dataOpts': dataOpts068,
          'modelOpts': modelOpts068,
          'runOpts': runOpts068}

dataOpts069 = dataOpts065.copy()
modelOpts069 = modelOpts002.copy()  # MAES
modelOpts069['exRef'] = 'LIN069'
runOpts069 = runOpts002.copy()
exp069 = {'dataOpts': dataOpts069,
          'modelOpts': modelOpts069,
          'runOpts': runOpts069}

dataOpts070 = dataOpts062.copy()
modelOpts070 = modelOpts003.copy()  # MAEW
modelOpts070['exRef'] = 'LIN070'
runOpts070 = runOpts003.copy()
exp070 = {'dataOpts': dataOpts070,
          'modelOpts': modelOpts070,
          'runOpts': runOpts070}

dataOpts071 = dataOpts063.copy()
modelOpts071 = modelOpts003.copy()  # MAEW
modelOpts071['exRef'] = 'LIN071'
runOpts071 = runOpts070.copy()
exp071 = {'dataOpts': dataOpts071,
          'modelOpts': modelOpts071,
          'runOpts': runOpts071}

dataOpts072 = dataOpts064.copy()
modelOpts072 = modelOpts003.copy()  # MAEW
modelOpts072['exRef'] = 'LIN072'
runOpts072 = runOpts003.copy()
exp072 = {'dataOpts': dataOpts072,
          'modelOpts': modelOpts072,
          'runOpts': runOpts072}

dataOpts073 = dataOpts065.copy()
modelOpts073 = modelOpts003.copy()  # MAEW
modelOpts073['exRef'] = 'LIN073'
runOpts073 = runOpts003.copy()
exp073 = {'dataOpts': dataOpts073,
          'modelOpts': modelOpts073,
          'runOpts': runOpts073}

dataOpts074 = dataOpts062.copy()
modelOpts074 = modelOpts006.copy()  # NORM
modelOpts074['exRef'] = 'LIN074'
runOpts074 = runOpts006.copy()
exp074 = {'dataOpts': dataOpts074,
          'modelOpts': modelOpts074,
          'runOpts': runOpts074}

dataOpts075 = dataOpts063.copy()
modelOpts075 = modelOpts006.copy()  # NORM
modelOpts075['exRef'] = 'LIN075'
runOpts075 = runOpts006.copy()
exp075 = {'dataOpts': dataOpts075,
          'modelOpts': modelOpts075,
          'runOpts': runOpts075}

dataOpts076 = dataOpts064.copy()
modelOpts076 = modelOpts006.copy()  # NORM
modelOpts076['exRef'] = 'LIN076'
runOpts076 = runOpts006.copy()
exp076 = {'dataOpts': dataOpts076,
          'modelOpts': modelOpts076,
          'runOpts': runOpts076}

dataOpts077 = dataOpts065.copy()
modelOpts077 = modelOpts006.copy()  # NORM
modelOpts077['exRef'] = 'LIN077'
runOpts077 = runOpts006.copy()
exp077 = {'dataOpts': dataOpts077,
          'modelOpts': modelOpts077,
          'runOpts': runOpts077}

dataOpts078 = dataOpts062.copy()
modelOpts078 = modelOpts007.copy()  # SHASH
modelOpts078['exRef'] = 'LIN078'
runOpts078 = runOpts007.copy()
exp078 = {'dataOpts': dataOpts078,
          'modelOpts': modelOpts078,
          'runOpts': runOpts078}

dataOpts079 = dataOpts063.copy()
modelOpts079 = modelOpts007.copy()  # SHASH
modelOpts079['exRef'] = 'LIN079'
runOpts079 = runOpts007.copy()
exp079 = {'dataOpts': dataOpts079,
          'modelOpts': modelOpts079,
          'runOpts': runOpts079}

dataOpts080 = dataOpts064.copy()
modelOpts080 = modelOpts007.copy()  # SHASH
modelOpts080['exRef'] = 'LIN080'
runOpts080 = runOpts007.copy()
exp080 = {'dataOpts': dataOpts080,
          'modelOpts': modelOpts080,
          'runOpts': runOpts080}

dataOpts081 = dataOpts065.copy()
modelOpts081 = modelOpts007.copy()  # SHASH
modelOpts081['exRef'] = 'LIN081'
runOpts081 = runOpts007.copy()
exp081 = {'dataOpts': dataOpts081,
          'modelOpts': modelOpts081,
          'runOpts': runOpts081}

dataOpts082 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts082 = modelOpts015.copy()
modelOpts082['exRef'] = 'LIN082'  # crps
runOpts082 = runOpts015.copy()
exp082 = {'dataOpts': dataOpts082,
          'modelOpts': modelOpts082,
          'runOpts': runOpts082}

dataOpts083 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts083 = modelOpts015.copy()
modelOpts083['exRef'] = 'LIN083'
runOpts083 = runOpts015.copy()
exp083 = {'dataOpts': dataOpts083,
          'modelOpts': modelOpts083,
          'runOpts': runOpts083}

dataOpts084 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts084 = modelOpts015.copy()
modelOpts084['exRef'] = 'LIN084'  # crps
runOpts084 = runOpts015.copy()
exp084 = {'dataOpts': dataOpts084,
          'modelOpts': modelOpts084,
          'runOpts': runOpts084}

dataOpts085 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts085 = modelOpts082.copy()
modelOpts085['exRef'] = 'LIN085'
runOpts085 = runOpts082.copy()
exp085 = {'dataOpts': dataOpts085,
          'modelOpts': modelOpts085,
          'runOpts': runOpts085}

dataOpts086 = dataOpts082.copy()
modelOpts086 = modelOpts004.copy()  # mse_weighted
modelOpts086['exRef'] = 'LIN086'
runOpts086 = runOpts004.copy()
exp086 = {'dataOpts': dataOpts086,
          'modelOpts': modelOpts086,
          'runOpts': runOpts086}

dataOpts087 = dataOpts083.copy()
modelOpts087 = modelOpts086.copy()
modelOpts087['exRef'] = 'LIN087'
runOpts087 = runOpts086.copy()
exp087 = {'dataOpts': dataOpts087,
          'modelOpts': modelOpts087,
          'runOpts': runOpts087}

dataOpts088 = dataOpts084.copy()
modelOpts088 = modelOpts086.copy()
modelOpts088['exRef'] = 'LIN088'
runOpts088 = runOpts086.copy()
exp088 = {'dataOpts': dataOpts088,
          'modelOpts': modelOpts088,
          'runOpts': runOpts088}

dataOpts089 = dataOpts085.copy()
modelOpts089 = modelOpts086.copy()
modelOpts089['exRef'] = 'LIN089'
runOpts089 = runOpts086.copy()
exp089 = {'dataOpts': dataOpts089,
          'modelOpts': modelOpts089,
          'runOpts': runOpts089}

dataOpts093 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts093 = modelOpts015.copy()
modelOpts093['exRef'] = 'LIN093'
runOpts093 = runOpts015.copy()
exp093 = {'dataOpts': dataOpts093,
          'modelOpts': modelOpts093,
          'runOpts': runOpts093}


# FULLY CONNECTED NEURAL NETWORK TESTS ----------------
# ...Loss Tests
dataOpts101 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts101 = {
    'exRef': 'NN101',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': True,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [0.0, 0.0],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': 'L1',
    'kernel_reg_lval': 0.0,
    'loss_type': 'mse',
    'n_dense_list': [1024, 1024],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts101 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 20}
exp101 = {'dataOpts': dataOpts101,
          'modelOpts': modelOpts101,
          'runOpts': runOpts101}

dataOpts102 = dataOpts101.copy()
modelOpts102 = modelOpts101.copy()
modelOpts102['exRef'] = 'NN102'
modelOpts102['loss_type'] = 'mae_surface'
runOpts102 = runOpts101.copy()
exp102 = {'dataOpts': dataOpts102,
          'modelOpts': modelOpts102,
          'runOpts': runOpts102}

dataOpts103 = dataOpts101.copy()
modelOpts103 = modelOpts101.copy()
modelOpts103['exRef'] = 'NN103'
modelOpts103['loss_type'] = 'mae_weighted'
runOpts103 = runOpts101.copy()
exp103 = {'dataOpts': dataOpts103,
          'modelOpts': modelOpts103,
          'runOpts': runOpts103}

dataOpts104 = dataOpts101.copy()
modelOpts104 = modelOpts101.copy()
modelOpts104['exRef'] = 'NN104'
modelOpts104['loss_type'] = 'mse_weighted'
runOpts104 = runOpts101.copy()
exp104 = {'dataOpts': dataOpts104,
          'modelOpts': modelOpts104,
          'runOpts': runOpts104}

dataOpts105 = dataOpts101.copy()
modelOpts105 = {
    'exRef': 'LIN058',
    'network': 'NeuralNetwork',
    'activation_dense': None,
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'crps',
    'n_dense_list': [1024],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 40}
modelOpts105 = modelOpts101.copy()
modelOpts105['exRef'] = 'NN105'
modelOpts105['loss_type'] = 'crps'
runOpts105 = {
    'batch_size': 128,
    'epochs': 300,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 10}
exp105 = {'dataOpts': dataOpts105,
          'modelOpts': modelOpts105,
          'runOpts': runOpts105}

dataOpts106 = dataOpts101.copy()
modelOpts106 = modelOpts101.copy()
modelOpts106['exRef'] = 'NN106'
modelOpts106['loss_type'] = 'uq_normal'
runOpts106 = runOpts006.copy()
exp106 = {'dataOpts': dataOpts106,
          'modelOpts': modelOpts106,
          'runOpts': runOpts106}

dataOpts107 = dataOpts101.copy()
modelOpts107 = modelOpts101.copy()
modelOpts107['exRef'] = 'NN107'
modelOpts107['loss_type'] = 'uq_sinh'
runOpts107 = runOpts007.copy()
exp107 = {'dataOpts': dataOpts107,
          'modelOpts': modelOpts107,
          'runOpts': runOpts107}

dataOpts108 = dataOpts101.copy()
modelOpts108 = modelOpts101.copy()
modelOpts108['exRef'] = 'NN108'
modelOpts108['loss_type'] = 'mse_dropout'
runOpts108 = runOpts101.copy()
exp108 = {'dataOpts': dataOpts108,
          'modelOpts': modelOpts108,
          'runOpts': runOpts108}

dataOpts117 = dataOpts101.copy()
modelOpts117 = modelOpts101.copy()
modelOpts117['exRef'] = 'NN117'
modelOpts117['loss_type'] = 'uq_sinh'
modelOpts117['uq_n_members'] = 200
runOpts117 = runOpts007.copy()
exp117 = {'dataOpts': dataOpts117,
          'modelOpts': modelOpts117,
          'runOpts': runOpts117}

dataOpts127 = dataOpts101.copy()
modelOpts127 = modelOpts101.copy()
modelOpts127['exRef'] = 'NN127'
modelOpts127['loss_type'] = 'uq_sinh'
modelOpts127['uq_n_members'] = 200
runOpts127 = {
    'batch_size': 128,
    'epochs': 1400,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 40}
exp127 = {'dataOpts': dataOpts127,
          'modelOpts': modelOpts127,
          'runOpts': runOpts127}

dataOpts137 = dataOpts101.copy()
modelOpts137 = {
    'exRef': 'NN137',
    'network': 'NeuralNetwork',
    'activation_dense': None,
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [0.0, 0.0],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'uq_sinh',
    'n_dense_list': [1024, 1027],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 200}
runOpts137 = {
    'batch_size': 128,
    'epochs': 600,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 20}
exp137 = {'dataOpts': dataOpts137,
          'modelOpts': modelOpts137,
          'runOpts': runOpts137}

dataOpts140 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts140 = {
    'exRef': 'NN140',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': True,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.4,
    'dropout_dense_list': [0.0, 0.3],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'mse',
    'n_dense_list': [1024, 1024],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts140 = {
    'batch_size': 128,
    'epochs': 80,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 10}
exp140 = {
    'dataOpts': dataOpts140,
    'modelOpts': modelOpts140,
    'runOpts': runOpts140}

dataOpts150 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_extra_goes': [0, 1, 2, 3],
    'input_rap_cape_cin': [0, 1],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts150 = {
    'exRef': 'NN150',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': True,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.4,
    'dropout_dense_list': [0.0, 0.3],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'crps',
    'n_dense_list': [1024, 1024],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts150 = {
    'batch_size': 128,
    'epochs': 80,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 10}
exp150 = {'dataOpts': dataOpts150,
          'modelOpts': modelOpts150,
          'runOpts': runOpts150}

dataOpts151 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_extra_goes': [],
    'input_rap_cape_cin': [0, 1],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8],
    'vcombine': []}
modelOpts151 = {
    'exRef': 'NN151',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': True,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.,
    'dropout_dense_list': [0.0],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'crps',
    'n_dense_list': [1024],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 40}
runOpts151 = {
    'batch_size': 128,
    'epochs': 120,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 10}
exp151 = {'dataOpts': dataOpts151,
          'modelOpts': modelOpts151,
          'runOpts': runOpts151}

dataOpts157 = dataOpts055.copy()
modelOpts157 = modelOpts107.copy()
modelOpts157['exRef'] = 'NN157'
runOpts157 = {
    'batch_size': 128,
    'epochs': 2500,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 100}
exp157 = {'dataOpts': dataOpts157,
          'modelOpts': modelOpts157,
          'runOpts': runOpts157}

# ...Ablation Tests
dataOpts162 = dataOpts082.copy()
modelOpts162 = modelOpts101.copy()  # mse
modelOpts162['exRef'] = 'NN162'
runOpts162 = runOpts101.copy()
exp162 = {'dataOpts': dataOpts162,
          'modelOpts': modelOpts162,
          'runOpts': runOpts162}

dataOpts163 = dataOpts083.copy()
modelOpts163 = modelOpts101.copy()
modelOpts163['exRef'] = 'NN163'
runOpts163 = runOpts101.copy()
exp163 = {'dataOpts': dataOpts163,
          'modelOpts': modelOpts163,
          'runOpts': runOpts163}

dataOpts164 = dataOpts084.copy()
modelOpts164 = modelOpts162.copy()
modelOpts164['exRef'] = 'NN164'
runOpts164 = runOpts162.copy()
exp164 = {'dataOpts': dataOpts164,
          'modelOpts': modelOpts164,
          'runOpts': runOpts164}

dataOpts165 = dataOpts085.copy()
modelOpts165 = modelOpts162.copy()
modelOpts165['exRef'] = 'NN165'
runOpts165 = runOpts162.copy()
exp165 = {'dataOpts': dataOpts165,
          'modelOpts': modelOpts165,
          'runOpts': runOpts165}

dataOpts166 = dataOpts082.copy()
modelOpts166 = modelOpts102.copy()  # maes
modelOpts166['exRef'] = 'NN166'
runOpts166 = runOpts102.copy()
exp166 = {'dataOpts': dataOpts166,
          'modelOpts': modelOpts166,
          'runOpts': runOpts166}

dataOpts167 = dataOpts083.copy()
modelOpts167 = modelOpts102.copy()  # maes
modelOpts167['exRef'] = 'NN167'
runOpts167 = runOpts102.copy()
exp167 = {'dataOpts': dataOpts167,
          'modelOpts': modelOpts167,
          'runOpts': runOpts167}

dataOpts168 = dataOpts084.copy()
modelOpts168 = modelOpts102.copy()
modelOpts168['exRef'] = 'NN168'
runOpts168 = runOpts102.copy()
exp168 = {'dataOpts': dataOpts168,
          'modelOpts': modelOpts168,
          'runOpts': runOpts168}

dataOpts169 = dataOpts085.copy()
modelOpts169 = modelOpts162.copy()
modelOpts169['exRef'] = 'NN169'
runOpts169 = runOpts102.copy()
exp169 = {'dataOpts': dataOpts169,
          'modelOpts': modelOpts169,
          'runOpts': runOpts169}

dataOpts170 = dataOpts082.copy()
modelOpts170 = modelOpts104.copy()  # msew
modelOpts170['exRef'] = 'NN170'
runOpts170 = runOpts104.copy()
exp170 = {'dataOpts': dataOpts170,
          'modelOpts': modelOpts170,
          'runOpts': runOpts170}

dataOpts171 = dataOpts083.copy()
modelOpts171 = modelOpts104.copy()
modelOpts171['exRef'] = 'NN171'
runOpts171 = runOpts104.copy()
exp171 = {'dataOpts': dataOpts171,
          'modelOpts': modelOpts171,
          'runOpts': runOpts171}

dataOpts172 = dataOpts084.copy()
modelOpts172 = modelOpts104.copy()
modelOpts172['exRef'] = 'NN172'
runOpts172 = runOpts104.copy()
exp172 = {'dataOpts': dataOpts172,
          'modelOpts': modelOpts172,
          'runOpts': runOpts172}

dataOpts173 = dataOpts085.copy()
modelOpts173 = modelOpts104.copy()
modelOpts173['exRef'] = 'NN173'
runOpts173 = runOpts104.copy()
exp173 = {'dataOpts': dataOpts173,
          'modelOpts': modelOpts173,
          'runOpts': runOpts173}

dataOpts174 = dataOpts082.copy()
modelOpts174 = modelOpts106.copy()  # norm
modelOpts174['exRef'] = 'NN174'
runOpts174 = runOpts106.copy()
exp174 = {'dataOpts': dataOpts174,
          'modelOpts': modelOpts174,
          'runOpts': runOpts174}

dataOpts175 = dataOpts083.copy()
modelOpts175 = modelOpts106.copy()
modelOpts175['exRef'] = 'NN175'
runOpts175 = runOpts106.copy()
exp175 = {'dataOpts': dataOpts175,
          'modelOpts': modelOpts175,
          'runOpts': runOpts175}

dataOpts176 = dataOpts084.copy()
modelOpts176 = modelOpts106.copy()
modelOpts176['exRef'] = 'NN176'
runOpts176 = runOpts106.copy()
exp176 = {'dataOpts': dataOpts176,
          'modelOpts': modelOpts176,
          'runOpts': runOpts176}

dataOpts177 = dataOpts085.copy()
modelOpts177 = modelOpts106.copy()
modelOpts177['exRef'] = 'NN177'
runOpts177 = runOpts106.copy()
exp177 = {'dataOpts': dataOpts177,
          'modelOpts': modelOpts177,
          'runOpts': runOpts177}

dataOpts178 = dataOpts082.copy()
modelOpts178 = modelOpts107.copy()  # shash
modelOpts178['exRef'] = 'NN178'
runOpts178 = runOpts107.copy()
exp178 = {'dataOpts': dataOpts178,
          'modelOpts': modelOpts178,
          'runOpts': runOpts178}

dataOpts179 = dataOpts083.copy()
modelOpts179 = modelOpts107.copy()
modelOpts179['exRef'] = 'NN179'
runOpts179 = runOpts107.copy()
exp179 = {'dataOpts': dataOpts179,
          'modelOpts': modelOpts179,
          'runOpts': runOpts179}

dataOpts180 = dataOpts084.copy()
modelOpts180 = modelOpts107.copy()
modelOpts180['exRef'] = 'NN180'
runOpts180 = runOpts107.copy()
exp180 = {'dataOpts': dataOpts180,
          'modelOpts': modelOpts180,
          'runOpts': runOpts180}

dataOpts181 = dataOpts085.copy()
modelOpts181 = modelOpts107.copy()  # shash
modelOpts181['exRef'] = 'NN181'
runOpts181 = runOpts107.copy()
exp181 = {'dataOpts': dataOpts181,
          'modelOpts': modelOpts181,
          'runOpts': runOpts181}

dataOpts182 = dataOpts082.copy()
modelOpts182 = modelOpts105.copy()  # crps
modelOpts182['exRef'] = 'NN182'
runOpts182 = runOpts105.copy()
exp182 = {'dataOpts': dataOpts182,
          'modelOpts': modelOpts182,
          'runOpts': runOpts182}

dataOpts183 = dataOpts083.copy()
modelOpts183 = modelOpts182.copy()
modelOpts183['exRef'] = 'NN183'
runOpts183 = runOpts182.copy()
exp183 = {'dataOpts': dataOpts183,
          'modelOpts': modelOpts183,
          'runOpts': runOpts183}

dataOpts184 = dataOpts084.copy()
modelOpts184 = modelOpts182.copy()
modelOpts184['exRef'] = 'NN184'
runOpts184 = runOpts182.copy()
exp184 = {'dataOpts': dataOpts184,
          'modelOpts': modelOpts184,
          'runOpts': runOpts184}

dataOpts185 = dataOpts085.copy()
modelOpts185 = modelOpts182.copy()
modelOpts185['exRef'] = 'NN185'
runOpts185 = runOpts182.copy()
exp185 = {'dataOpts': dataOpts185,
          'modelOpts': modelOpts185,
          'runOpts': runOpts185}

dataOpts186 = dataOpts086.copy()
modelOpts186 = modelOpts103.copy()  # mae_weighted
modelOpts186['exRef'] = 'NN186'
runOpts186 = runOpts103.copy()
exp186 = {'dataOpts': dataOpts186,
          'modelOpts': modelOpts186,
          'runOpts': runOpts186}

dataOpts187 = dataOpts087.copy()
modelOpts187 = modelOpts186.copy()
modelOpts187['exRef'] = 'NN187'
runOpts187 = runOpts186.copy()
exp187 = {'dataOpts': dataOpts187,
          'modelOpts': modelOpts187,
          'runOpts': runOpts187}

dataOpts188 = dataOpts088.copy()
modelOpts188 = modelOpts186.copy()
modelOpts188['exRef'] = 'NN188'
runOpts188 = runOpts186.copy()
exp188 = {'dataOpts': dataOpts188,
          'modelOpts': modelOpts188,
          'runOpts': runOpts188}

dataOpts189 = dataOpts089.copy()
modelOpts189 = modelOpts186.copy()
modelOpts189['exRef'] = 'NN189'
runOpts189 = runOpts186.copy()
exp189 = {'dataOpts': dataOpts189,
          'modelOpts': modelOpts189,
          'runOpts': runOpts189}

dataOpts194 = dataOpts082.copy()
modelOpts194 = modelOpts137.copy()  # shash
modelOpts194['exRef'] = 'NN194'
modelOpts194['dropout_flag'] = False
modelOpts104['dropout_dense_last'] = 0.01
runOpts194 = runOpts137.copy()
runOpts194['epochs'] = 200
runOpts194['learning_rate'] = 0.00005
exp194 = {'dataOpts': dataOpts194,
          'modelOpts': modelOpts194,
          'runOpts': runOpts194}

dataOpts195 = dataOpts083.copy()
modelOpts195 = modelOpts137.copy()  # shash
modelOpts195['exRef'] = 'NN195'
runOpts195 = runOpts137.copy()
exp195 = {'dataOpts': dataOpts195,
          'modelOpts': modelOpts195,
          'runOpts': runOpts195}

dataOpts196 = dataOpts084.copy()
modelOpts196 = modelOpts137.copy()  # shash
modelOpts196['exRef'] = 'NN196'
modelOpts196['dropout_flag'] = False
runOpts196 = runOpts137.copy()
# runOpts196['epochs'] = 1200
# runOpts196['learning_rate'] = 0.0000001
exp196 = {'dataOpts': dataOpts196,
          'modelOpts': modelOpts196,
          'runOpts': runOpts196}

dataOpts197 = dataOpts085.copy()
modelOpts197 = modelOpts137.copy()  # shash
modelOpts197['exRef'] = 'NN197'
runOpts197 = runOpts137.copy()
exp197 = {'dataOpts': dataOpts197,
          'modelOpts': modelOpts197,
          'runOpts': runOpts197}

dataOpts204 = dataOpts082.copy()
modelOpts204 = modelOpts137.copy()  # shash
modelOpts204['exRef'] = 'NN204'
modelOpts204['dropout_flag'] = False
modelOpts204['dropout_dense_last'] = 0.01
runOpts204 = runOpts137.copy()
exp204 = {'dataOpts': dataOpts204,
          'modelOpts': modelOpts204,
          'runOpts': runOpts204}

# UNET TESTS -----------------------------------------
# ... Loss Tests
dataOpts501 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts501 = {
    'exRef': 'UNet501',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': True,
    'dropout_conv_rate': 0.05,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'mse',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts501 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 15}
exp501 = {'dataOpts': dataOpts501,
          'modelOpts': modelOpts501,
          'runOpts': runOpts501}

dataOpts502 = dataOpts501.copy()
modelOpts502 = modelOpts501.copy()
modelOpts502['exRef'] = 'UNet502'
modelOpts502['loss_type'] = 'mae_surface'
runOpts502 = runOpts501.copy()
exp502 = {'dataOpts': dataOpts502,
          'modelOpts': modelOpts502,
          'runOpts': runOpts502}

dataOpts503 = dataOpts501.copy()
modelOpts503 = modelOpts501.copy()
modelOpts503['exRef'] = 'UNet503'
modelOpts503['loss_type'] = 'mae_weighted'
runOpts503 = runOpts501.copy()
exp503 = {'dataOpts': dataOpts503,
          'modelOpts': modelOpts503,
          'runOpts': runOpts503}

dataOpts504 = dataOpts501.copy()
modelOpts504 = modelOpts501.copy()
modelOpts504['exRef'] = 'UNet504'
modelOpts504['loss_type'] = 'mse_weighted'
runOpts504 = runOpts501.copy()
exp504 = {'dataOpts': dataOpts504,
          'modelOpts': modelOpts504,
          'runOpts': runOpts504}

dataOpts505 = dataOpts501.copy()
modelOpts505 = {
    'exRef': 'UNet505',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': True,
    'dropout_conv_rate': 0.05,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts505 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 15}
exp505 = {'dataOpts': dataOpts505,
          'modelOpts': modelOpts505,
          'runOpts': runOpts505}

dataOpts506 = dataOpts501.copy()
modelOpts506 = {
    'exRef': 'UNet506',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': True,
    'dropout_conv_rate': 0.0,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'uq_normal',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 0}
runOpts506 = {
    'batch_size': 128,
    'epochs': 500,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 30}
exp506 = {'dataOpts': dataOpts506,
          'modelOpts': modelOpts506,
          'runOpts': runOpts506}

dataOpts507 = dataOpts501.copy()
modelOpts507 = modelOpts501.copy()
modelOpts507['exRef'] = 'UNet507'
modelOpts507['loss_type'] = 'uq_sinh'
runOpts507 = runOpts107.copy()
exp507 = {'dataOpts': dataOpts507,
          'modelOpts': modelOpts507,
          'runOpts': runOpts507}

dataOpts508 = dataOpts501.copy()
modelOpts508 = modelOpts501.copy()
modelOpts508['exRef'] = 'UNet508'
modelOpts508['loss_type'] = 'mse_dropout'
runOpts508 = runOpts108.copy()
exp508 = {'dataOpts': dataOpts508,
          'modelOpts': modelOpts508,
          'runOpts': runOpts508}

dataOpts515 = dataOpts501.copy()
modelOpts515 = {
    'exRef': 'UNet515',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': True,
    'dropout_flag': False,
    'dropout_conv_rate': 0.0,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts515 = {
    'batch_size': 128,
    'epochs': 1400,
    'learning_rate': 0.00005,
    'optimizer': 'adam',
    'patience': 40}
exp515 = {'dataOpts': dataOpts515,
          'modelOpts': modelOpts515,
          'runOpts': runOpts515}

dataOpts516 = dataOpts501.copy()
modelOpts516 = {
    'exRef': 'UNet516',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': True,
    'dropout_conv_rate': 0.0,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.001,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'uq_normal',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts516 = {
    'batch_size': 128,
    'epochs': 1000,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 30}
exp516 = {'dataOpts': dataOpts516,
          'modelOpts': modelOpts516,
          'runOpts': runOpts516}

dataOpts517 = dataOpts501.copy()
modelOpts517 = {
    'exRef': 'UNet517',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': True,
    'dropout_conv_rate': 0.0,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'uq_sinh',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 200}
runOpts517 = {
    'batch_size': 128,
    'epochs': 1400,
    'learning_rate': 0.00001,
    'optimizer': 'adam',
    'patience': 40}
exp517 = {'dataOpts': dataOpts517,
          'modelOpts': modelOpts517,
          'runOpts': runOpts517}

dataOpts525 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts525 = modelOpts515.copy()  # CRPS
modelOpts525['exRef'] = 'UNet525'
runOpts525 = runOpts515.copy()
exp525 = {'dataOpts': dataOpts525,
          'modelOpts': modelOpts525,
          'runOpts': runOpts525}

dataOpts527 = dataOpts507.copy()
modelOpts527 = modelOpts507.copy()
modelOpts527['exRef'] = 'UNet527'  # SHASH
modelOpts527['uq_n_members'] = 200
runOpts527 = runOpts107.copy()
exp527 = {'dataOpts': dataOpts527,
          'modelOpts': modelOpts527,
          'runOpts': runOpts527}

dataOpts531 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts531 = {
    'exRef': 'UNet531',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.1,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'mae_surface',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts531 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 10}
exp531 = {'dataOpts': dataOpts531,
          'modelOpts': modelOpts531,
          'runOpts': runOpts531}

# ...Ablation Tests
dataOpts562 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts562 = modelOpts501.copy()  # MSE
modelOpts562['exRef'] = 'UNet562'
runOpts562 = runOpts501.copy()
exp562 = {'dataOpts': dataOpts562,
          'modelOpts': modelOpts562,
          'runOpts': runOpts562}

dataOpts563 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts563 = modelOpts501.copy()  # MSE
modelOpts563['exRef'] = 'UNet563'
runOpts563 = runOpts501.copy()
exp563 = {'dataOpts': dataOpts563,
          'modelOpts': modelOpts563,
          'runOpts': runOpts563}

dataOpts564 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts564 = modelOpts501.copy()  # MSE
modelOpts564['exRef'] = 'UNet564'
runOpts564 = runOpts501.copy()
exp564 = {'dataOpts': dataOpts564,
          'modelOpts': modelOpts564,
          'runOpts': runOpts564}

dataOpts565 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts565 = modelOpts501.copy()  # MSE
modelOpts565['exRef'] = 'UNet565'
runOpts565 = runOpts501.copy()
exp565 = {'dataOpts': dataOpts565,
          'modelOpts': modelOpts565,
          'runOpts': runOpts565}

dataOpts566 = dataOpts562.copy()
modelOpts566 = modelOpts502.copy()  # MAES
modelOpts566['exRef'] = 'UNet566'
runOpts566 = runOpts502.copy()
exp566 = {'dataOpts': dataOpts566,
          'modelOpts': modelOpts566,
          'runOpts': runOpts566}

dataOpts567 = dataOpts563.copy()
modelOpts567 = modelOpts502.copy()  # MAES
modelOpts567['exRef'] = 'UNet567'
runOpts567 = runOpts502.copy()
exp567 = {'dataOpts': dataOpts567,
          'modelOpts': modelOpts567,
          'runOpts': runOpts567}

dataOpts568 = dataOpts564.copy()
modelOpts568 = modelOpts502.copy()  # MAES
modelOpts568['exRef'] = 'UNet568'
runOpts568 = runOpts502.copy()
exp568 = {'dataOpts': dataOpts568,
          'modelOpts': modelOpts568,
          'runOpts': runOpts568}

dataOpts569 = dataOpts565.copy()
modelOpts569 = modelOpts502.copy()  # MAES
modelOpts569['exRef'] = 'UNet569'
runOpts569 = runOpts502.copy()
exp569 = {'dataOpts': dataOpts569,
          'modelOpts': modelOpts569,
          'runOpts': runOpts569}

dataOpts570 = dataOpts562.copy()
modelOpts570 = modelOpts506.copy()  # NORM
modelOpts570['exRef'] = 'UNet570'
runOpts570 = {
    'batch_size': 182,
    'epochs': 200,
    'learning_rate': 0.00005,
    'optimizer': 'adam',
    'patience': 30}
exp570 = {'dataOpts': dataOpts570,
          'modelOpts': modelOpts570,
          'runOpts': runOpts570}

dataOpts571 = dataOpts563.copy()
modelOpts571 = modelOpts506.copy()  # NORM
modelOpts571['exRef'] = 'UNet571'
runOpts571 = runOpts570.copy()
exp571 = {'dataOpts': dataOpts571,
          'modelOpts': modelOpts571,
          'runOpts': runOpts571}

dataOpts572 = dataOpts564.copy()
modelOpts572 = modelOpts506.copy()  # NORM
modelOpts572['exRef'] = 'UNet572'
runOpts572 = runOpts570.copy()
exp572 = {'dataOpts': dataOpts572,
          'modelOpts': modelOpts572,
          'runOpts': runOpts572}

dataOpts573 = dataOpts565.copy()
modelOpts573 = modelOpts506.copy()  # NORM
modelOpts573['exRef'] = 'UNet573'
runOpts573 = runOpts570.copy()
exp573 = {'dataOpts': dataOpts573,
          'modelOpts': modelOpts573,
          'runOpts': runOpts573}

dataOpts574 = dataOpts562.copy()
modelOpts574 = modelOpts504.copy()  # MSEW
modelOpts574['exRef'] = 'UNet574'
runOpts574 = runOpts504.copy()
exp574 = {'dataOpts': dataOpts574,
          'modelOpts': modelOpts574,
          'runOpts': runOpts574}

dataOpts575 = dataOpts563.copy()
modelOpts575 = modelOpts504.copy()  # MSEW
modelOpts575['exRef'] = 'UNet575'
runOpts575 = runOpts504.copy()
exp575 = {'dataOpts': dataOpts575,
          'modelOpts': modelOpts575,
          'runOpts': runOpts575}

dataOpts576 = dataOpts564.copy()
modelOpts576 = modelOpts504.copy()  # MSEW
modelOpts576['exRef'] = 'UNet576'
runOpts576 = runOpts504.copy()
exp576 = {'dataOpts': dataOpts576,
          'modelOpts': modelOpts576,
          'runOpts': runOpts576}

dataOpts577 = dataOpts565.copy()
modelOpts577 = modelOpts504.copy()  # MSEW
modelOpts577['exRef'] = 'UNet577'
runOpts577 = runOpts504.copy()
exp577 = {'dataOpts': dataOpts577,
          'modelOpts': modelOpts577,
          'runOpts': runOpts577}

dataOpts582 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts582 = modelOpts503.copy()  # MAEW
modelOpts582['exRef'] = 'UNet582'
runOpts582 = runOpts503.copy()
exp582 = {'dataOpts': dataOpts582,
          'modelOpts': modelOpts582,
          'runOpts': runOpts582}

dataOpts583 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts583 = modelOpts503.copy()  # MAEW
modelOpts583['exRef'] = 'UNet583'
runOpts583 = runOpts503.copy()
exp583 = {'dataOpts': dataOpts583,
          'modelOpts': modelOpts583,
          'runOpts': runOpts583}

dataOpts584 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts584 = modelOpts503.copy()  # MAEW
modelOpts584['exRef'] = 'UNet584'
runOpts584 = runOpts503.copy()
exp584 = {'dataOpts': dataOpts584,
          'modelOpts': modelOpts584,
          'runOpts': runOpts584}

dataOpts585 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts585 = modelOpts503.copy()  # MAEW
modelOpts585['exRef'] = 'UNet585'
runOpts585 = runOpts503.copy()
exp585 = {'dataOpts': dataOpts585,
          'modelOpts': modelOpts585,
          'runOpts': runOpts585}

dataOpts586 = dataOpts582.copy()
modelOpts586 = modelOpts515.copy()  # CRPS
modelOpts586['exRef'] = 'UNet586'
runOpts586 = runOpts515.copy()
exp586 = {'dataOpts': dataOpts586,
          'modelOpts': modelOpts586,
          'runOpts': runOpts586}

dataOpts587 = dataOpts583.copy()
modelOpts587 = modelOpts515.copy()  # CRPS
modelOpts587['exRef'] = 'UNet587'
runOpts587 = runOpts515.copy()
exp587 = {'dataOpts': dataOpts587,
          'modelOpts': modelOpts587,
          'runOpts': runOpts587}

dataOpts588 = dataOpts584.copy()
modelOpts588 = modelOpts515.copy()  # CRPS
modelOpts588['exRef'] = 'UNet588'
runOpts588 = runOpts515.copy()
exp588 = {'dataOpts': dataOpts588,
          'modelOpts': modelOpts588,
          'runOpts': runOpts588}

dataOpts589 = dataOpts585.copy()
modelOpts589 = modelOpts515.copy()  # CRPS
modelOpts589['exRef'] = 'UNet589'
runOpts589 = runOpts515.copy()
exp589 = {'dataOpts': dataOpts589,
          'modelOpts': modelOpts589,
          'runOpts': runOpts589}

dataOpts590 = dataOpts582.copy()
modelOpts590 = modelOpts507.copy()  # SHASH
modelOpts590['exRef'] = 'UNet590'
runOpts590 = runOpts507.copy()
exp590 = {'dataOpts': dataOpts590,
          'modelOpts': modelOpts590,
          'runOpts': runOpts590}

dataOpts591 = dataOpts583.copy()
modelOpts591 = modelOpts507.copy()  # SHASH
modelOpts591['exRef'] = 'UNet591'
runOpts591 = runOpts507.copy()
exp591 = {'dataOpts': dataOpts591,
          'modelOpts': modelOpts591,
          'runOpts': runOpts591}

dataOpts592 = dataOpts584.copy()
modelOpts592 = modelOpts507.copy()  # SHASH
modelOpts592['exRef'] = 'UNet592'
runOpts592 = runOpts507.copy()
exp592 = {'dataOpts': dataOpts592,
          'modelOpts': modelOpts592,
          'runOpts': runOpts592}

dataOpts593 = dataOpts585.copy()
modelOpts593 = modelOpts507.copy()  # SHASH
modelOpts593['exRef'] = 'UNet593'
runOpts593 = runOpts507.copy()
exp593 = {'dataOpts': dataOpts593,
          'modelOpts': modelOpts593,
          'runOpts': runOpts593}

# -----------
# Top UNets

dataOpts602 = dataOpts585.copy()
modelOpts602 = modelOpts502.copy()
modelOpts602['exRef'] = 'UNet602'
runOpts602 = {
    'batch_size': 128,
    'epochs': 200,
    'learning_rate': 0.00005,
    'optimizer': 'adam',
    'patience': 10}
exp602 = {'dataOpts': dataOpts602,
          'modelOpts': modelOpts602,
          'runOpts': runOpts602}

dataOpts603 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_extra_goes': [0, 1, 2, 3],
    'input_rap_cape_cin': [0, 1],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts603 = modelOpts503.copy()
modelOpts603['exRef'] = 'UNet603'
runOpts603 = runOpts503.copy()
exp603 = {'dataOpts': dataOpts603,
          'modelOpts': modelOpts603,
          'runOpts': runOpts603}

dataOpts605 = dataOpts585.copy()
modelOpts605 = modelOpts505.copy()
modelOpts605['exRef'] = 'UNet605'
modelOpts605['dropout_flag'] = False
runOpts605 = {
    'batch_size': 128,
    'epochs': 200,
    'learning_rate': 0.00005,
    'optimizer': 'adam',
    'patience': 20}
exp605 = {'dataOpts': dataOpts605,
          'modelOpts': modelOpts605,
          'runOpts': runOpts605}

dataOpts613 = dataOpts585.copy()
modelOpts613 = {
    'exRef': 'UNet613',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': True,
    'dropout_conv_rate': 0.0,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'mae_weighted',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts613 = {
    'batch_size': 128,
    'epochs': 80,
    'learning_rate': 0.00005,
    'optimizer': 'adam',
    'patience': 10}
exp613 = {'dataOpts': dataOpts613,
          'modelOpts': modelOpts613,
          'runOpts': runOpts613}

dataOpts615 = dataOpts585.copy()
modelOpts615 = modelOpts505.copy()
modelOpts615['exRef'] = 'UNet615'
modelOpts615['dropout_conv_rate'] = 0.0
modelOpts615['droput_dense_last'] = 0.05
runOpts615 = {
    'batch_size': 128,
    'epochs': 200,
    'learning_rate': 0.00005,
    'optimizer': 'adam',
    'patience': 20}
exp615 = {'dataOpts': dataOpts615,
          'modelOpts': modelOpts615,
          'runOpts': runOpts615}

dataOpts628 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts628 = {
    'exRef': 'UNet628',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.1,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'mae_weighted',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 0}
runOpts628 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 10}
exp628 = {'dataOpts': dataOpts628,
          'modelOpts': modelOpts628,
          'runOpts': runOpts628}

dataOpts634 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts634 = {
    'exRef': 'UNet634',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.1,
    'dropout_dense_last': 0.05,
    'dropout_dense_list': [],
    'kernel_reg': 'L2',
    'kernel_reg_lval': 0.001,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts634 = {
    'batch_size': 200,
    'epochs': 100,
    'learning_rate': 0.001,
    'optimizer': 'adam',
    'patience': 15}
exp634 = {'dataOpts': dataOpts634,
          'modelOpts': modelOpts634,
          'runOpts': runOpts634}

dataOpts635 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts635 = {
    'exRef': 'UNet635',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.0,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': 'L2',
    'kernel_reg_lval': 0.001,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts635 = {
    'batch_size': 200,
    'epochs': 100,
    'learning_rate': 0.001,
    'optimizer': 'adam',
    'patience': 15}
exp635 = {'dataOpts': dataOpts635,
          'modelOpts': modelOpts635,
          'runOpts': runOpts635}

dataOpts636 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts636 = {
    'exRef': 'UNet636',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.1,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts636 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0005,
    'optimizer': 'adam',
    'patience': 15}
exp636 = {'dataOpts': dataOpts636,
          'modelOpts': modelOpts636,
          'runOpts': runOpts636}

dataOpts637 = dataOpts603.copy()
modelOpts637 = modelOpts636.copy()
modelOpts637['exRef'] = 'UNet637'
runOpts637 = runOpts636.copy()
exp637 = {'dataOpts': dataOpts637,
          'modelOpts': modelOpts637,
          'runOpts': runOpts637}

dataOpts640 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_extra_goes': [0, 1, 2, 3],
    'input_rap_cape_cin': [0, 1],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts640 = {
    'exRef': 'UNet640',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.1,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts640 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0005,
    'optimizer': 'adam',
    'patience': 15}
exp640 = {'dataOpts': dataOpts640,
          'modelOpts': modelOpts640,
          'runOpts': runOpts640}

dataOpts641 = dataOpts640.copy()
modelOpts641 = modelOpts640.copy()  # crps
modelOpts641['exRef'] = 'UNet641'
runOpts641 = runOpts640.copy()
exp641 = {'dataOpts': dataOpts641,
          'modelOpts': modelOpts641,
          'runOpts': runOpts641}

dataOpts642 = dataOpts640.copy()
modelOpts642 = {
    'exRef': 'UNet642',
    'network': 'SkipNeuralNetwork',
    'activation_conv': 'relu',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'dropout_flag': False,
    'dropout_conv_rate': 0.1,
    'dropout_dense_last': 0.1,
    'dropout_dense_list': [0.0],
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'kernels_size_and_stride': (3, 1),
    'loss_type': 'crps',
    'n_conv_list': [32, 64, 128, 256],
    'n_dense_list': [64],
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts642 = runOpts640.copy()
exp642 = {'dataOpts': dataOpts642,
          'modelOpts': modelOpts642,
          'runOpts': runOpts642}

dataOpts685 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'output_channels_raobs': [1, 2],
    'restrict_data': [4, 8]}
modelOpts685 = modelOpts503.copy()  # MAEW
modelOpts685['exRef'] = 'UNet685'
runOpts685 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.00005,
    'optimizer': 'adam',
    'patience': 15}
exp685 = {'dataOpts': dataOpts685,
          'modelOpts': modelOpts685,
          'runOpts': runOpts685}


# DIRECTLY PREDICTING CAPE
# LINEAR REGRESSION TESTS
dataOpts701 = {
    'cape_restrict_high': 4000,
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [0, 1],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts701 = {
    'exRef': 'LIN701',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.01,
    'loss_type': 'mse',
    'n_dense_list': [],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True}
runOpts701 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.001,
    'optimizer': 'adam',
    'patience': 10}
exp701 = {'dataOpts': dataOpts701,
          'modelOpts': modelOpts701,
          'runOpts': runOpts701}

dataOpts702 = dataOpts701.copy()
modelOpts702 = modelOpts701.copy()
modelOpts702['exRef'] = 'LIN702'
modelOpts702['loss_type'] = 'mae'
runOpts702 = runOpts701.copy()
exp702 = {'dataOpts': dataOpts702,
          'modelOpts': modelOpts702,
          'runOpts': runOpts702}

dataOpts705 = dataOpts701.copy()
modelOpts705 = modelOpts701.copy()
modelOpts705['exRef'] = 'LIN705'
modelOpts705['loss_type'] = 'crps'
runOpts705 = runOpts005.copy()
exp705 = {'dataOpts': dataOpts705,
          'modelOpts': modelOpts705,
          'runOpts': runOpts705}

dataOpts706 = dataOpts701.copy()
modelOpts706 = modelOpts701.copy()
modelOpts706['exRef'] = 'LIN706'
modelOpts706['loss_type'] = 'uq_normal'
runOpts706 = runOpts006.copy()
exp706 = {'dataOpts': dataOpts706,
          'modelOpts': modelOpts706,
          'runOpts': runOpts706}

dataOpts707 = dataOpts701.copy()
modelOpts707 = modelOpts701.copy()
modelOpts707['exRef'] = 'LIN707'
modelOpts707['loss_type'] = 'uq_sinh'
runOpts707 = runOpts007.copy()
runOpts707['epochs'] = 3400
exp707 = {'dataOpts': dataOpts707,
          'modelOpts': modelOpts707,
          'runOpts': runOpts707}

# RANDOM FOREST MODEL TESTS
dataOpts801 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [0, 1],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts801 = {
    'exRef': 'RF801',
    'network': 'RandomForest',
    'bootstrap': True,
    'ccp_alpha': 0.001,
    'criterion': 'squared_error',
    'max_depth': 50,
    'max_features': None,
    'max_leaf_nodes': None,
    'max_samples': None,
    'min_impurity_decrease': 0.0,
    'min_samples_leaf': 1,
    'min_samples_split': 2,
    'min_weight_fraction_leaf': 0.0,
    'n_estimators': 100,
    'n_jobs': None,
    'oob_score': False,
    'random_state': None,
    'verbose': 0,
    'warm_start': False}
exp801 = {'dataOpts': dataOpts801,
          'modelOpts': modelOpts801}

dataOpts802 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts802 = modelOpts801.copy()
modelOpts802['exRef'] = 'RF802'
exp802 = {'dataOpts': dataOpts802,
          'modelOpts': modelOpts802}

dataOpts803 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts803 = modelOpts801.copy()
modelOpts803['exRef'] = 'RF803'
exp803 = {'dataOpts': dataOpts803,
          'modelOpts': modelOpts803}

dataOpts804 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts804 = modelOpts801.copy()
modelOpts804['exRef'] = 'RF804'
exp804 = {'dataOpts': dataOpts804,
          'modelOpts': modelOpts804}

dataOpts805 = {
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts805 = modelOpts801.copy()
modelOpts805['exRef'] = 'RF805'
exp805 = {'dataOpts': dataOpts805,
          'modelOpts': modelOpts805}

dataOpts850 = {
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [0, 1],
    'output_cape_cin': [0],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts850 = {
    'exRef': 'RF850',
    'network': 'RandomForest',
    'bootstrap': False,
    'ccp_alpha': 0.0,
    'criterion': 'squared_error',
    'max_depth': 10,
    'max_features': 'auto',
    'max_leaf_nodes': None,
    'max_samples': None,
    'min_impurity_decrease': 0.0,
    'min_samples_leaf': 1,
    'min_samples_split': 2,
    'min_weight_fraction_leaf': 0.0,
    'n_estimators': 100,
    'n_jobs': None,
    'oob_score': False,
    'random_state': None,
    'verbose': 0,
    'warm_start': False}
exp850 = {'dataOpts': dataOpts850,
          'modelOpts': modelOpts850}

dataOpts860 = dataOpts850.copy()
dataOpts860['output_cape_cin'] = [1]
modelOpts860 = modelOpts801.copy()
modelOpts860['exRef'] = 'RF860'
exp860 = {'dataOpts': dataOpts860,
          'modelOpts': modelOpts860}

# NN MODEL TESTS
dataOpts901 = {
    'cape_restrict_high': 4000,
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [0, 1],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts901 = {
    'exRef': 'NN901',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [0.],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.01,
    'loss_type': 'mse',
    'n_dense_list': [1000],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True}
runOpts901 = {
    'batch_size': 256,
    'epochs': 160,
    'learning_rate': 0.005,
    'optimizer': 'adam',
    'patience': 10}
exp901 = {'dataOpts': dataOpts901,
          'modelOpts': modelOpts901,
          'runOpts': runOpts901}

dataOpts902 = dataOpts901.copy()
modelOpts902 = {
    'exRef': 'NN902',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [0., 0., 0.],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.01,
    'loss_type': 'mae',
    'n_dense_list': [960, 500, 200],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True}
runOpts902 = runOpts901.copy()
exp902 = {'dataOpts': dataOpts902,
          'modelOpts': modelOpts902,
          'runOpts': runOpts902}

dataOpts905 = dataOpts901.copy()
modelOpts905 = {
    'exRef': 'NN905',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [0., 0., 0.],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.01,
    'loss_type': 'crps',
    'n_dense_list': [960, 500, 200],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts905 = runOpts901.copy()
exp905 = {'dataOpts': dataOpts905,
          'modelOpts': modelOpts905,
          'runOpts': runOpts905}

dataOpts906 = dataOpts901.copy()
modelOpts906 = {
    'exRef': 'NN906',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [0., 0., 0.],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.01,
    'loss_type': 'uq_normal',
    'n_dense_list': [960, 500, 200],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts906 = {
    'batch_size': 256,
    'epochs': 460,
    'learning_rate': 0.0005,
    'optimizer': 'adam',
    'patience': 10}
exp906 = {'dataOpts': dataOpts906,
          'modelOpts': modelOpts906,
          'runOpts': runOpts906}

dataOpts907 = dataOpts901.copy()
modelOpts907 = {
    'exRef': 'NN907',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [0., 0., 0.],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.01,
    'loss_type': 'crps',
    'n_dense_list': [960, 500, 200],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 60}
runOpts907 = {
    'batch_size': 256,
    'epochs': 140,
    'learning_rate': 0.0005,
    'optimizer': 'adam',
    'patience': 10}
exp907 = {'dataOpts': dataOpts907,
          'modelOpts': modelOpts907,
          'runOpts': runOpts907}


dataOpts915 = dataOpts901.copy()
modelOpts915 = {
    'exRef': 'NN915',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': True,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': True,
    'dropout_dense_last': 0.0,
    'dropout_dense_list': [0.05, 0.05, 0.05],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'crps',
    'n_dense_list': [30, 20, 80],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 40}
runOpts915 = {
    'batch_size': 256,
    'epochs': 560,
    'learning_rate': 0.005,
    'optimizer': 'nadam',
    'patience': 10}
exp915 = {'dataOpts': dataOpts915,
          'modelOpts': modelOpts915,
          'runOpts': runOpts915}

dataOpts980 = {
    'cape_restrict_high': 4000,
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [0, 1],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts980 = {
    'exRef': 'NN980',
    'network': 'NeuralNetwork',
    'activation_dense': 'relu',
    'batchnorm_flag': False,
    'bias_flag': True,
    'bias_init': 'zeros',
    'bias_reg': None,
    'dropout_flag': False,
    'dropout_dense_last': 0.15,
    'dropout_dense_list': [0.0, 0.0],
    'kernel_init': 'glorot_uniform',
    'kernel_reg': None,
    'kernel_reg_lval': 0.0,
    'loss_type': 'crps',
    'n_dense_list': [960, 350],
    'seed': None,
    'standardizeX': True,
    'standardizeY': True,
    'uq_n_members': 40}
runOpts980 = {
    'batch_size': 128,
    'epochs': 100,
    'learning_rate': 0.0001,
    'optimizer': 'adam',
    'patience': 10}
exp980 = {'dataOpts': dataOpts980,
          'modelOpts': modelOpts980,
          'runOpts': runOpts980}

dataOpts981 = {
    'cape_restrict_high': 4000,
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts981 = modelOpts980.copy()
modelOpts981['exRef'] = 'NN981'
runOpts981 = runOpts980.copy()
exp981 = {'dataOpts': dataOpts981,
          'modelOpts': modelOpts981,
          'runOpts': runOpts981}

dataOpts982 = {
    'cape_restrict_high': 4000,
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [0, 1, 2],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts982 = modelOpts980.copy()
modelOpts982['exRef'] = 'NN982'
runOpts982 = runOpts980.copy()
exp982 = {'dataOpts': dataOpts982,
          'modelOpts': modelOpts982,
          'runOpts': runOpts982}

dataOpts983 = {
    'cape_restrict_high': 4000,
    'input_channels_goes': [0, 1, 2, 3, 4, 5, 6, 7],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts983 = modelOpts980.copy()
modelOpts983['exRef'] = 'NN983'
runOpts983 = runOpts980.copy()
exp983 = {'dataOpts': dataOpts983,
          'modelOpts': modelOpts983,
          'runOpts': runOpts983}

dataOpts984 = {
    'cape_restrict_high': 4000,
    'input_channels_goes': [],
    'input_channels_rap': [0, 1, 2, 3],
    'input_channels_rtma': [],
    'input_rap_cape_cin': [],
    'output_cape_cin': [0, 1],
    'restrict_data': [4, 8],
    'vcombine': [1, 8]}
modelOpts984 = modelOpts980.copy()
modelOpts984['exRef'] = 'NN984'
runOpts984 = runOpts980.copy()
exp984 = {'dataOpts': dataOpts984,
          'modelOpts': modelOpts984,
          'runOpts': runOpts984}
